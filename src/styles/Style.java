package styles;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;

import enums.EnumDegre;
import enums.EnumDurationChord;
import enums.EnumInstruments;
import enums.EnumTonalite;
import musicRandom.Chord;

public class Style {
	
	private String styleName;	
	private int numberInstrument;
	private int numberPercussion;

	private List<EnumTonalite> availableTonalities;
	private int rythmeMin, rythmeMax, rythme;
	
	private int complexityChordMin;
	private int complexityChordMax;
	private int numberChordMin;
	private int numberChordMax;
	
	private List<Chord> choosedChords;
	
	private StyleCoefDegres styleCoefDegres;
	private MesureTempsFort mesureTempsFort;
	private List<StyleGroupIns> styleGroupInstruments;
	private List<StyleGroupDrums> styleGroupDrums;
	
	private transient Random RANDOM = new Random();
	
	private int randomCustom(int value) {
		if(value == 0) {
			return 0;
		}
		else {
			return RANDOM.nextInt(value+1);
		}
	}
	
	public Style( String styleName,
		List<Integer> availableTon,
		int rythme,
		int rythmeMax,
		int rythmeMin, 
		int nInstrumentMin, 
		int nInstrumentMax,
		int nPercussionMin, 
		int nPercussionMax,
		int complexityChordMin,
		int complexityChordMax,
		int numberChordMin,
		int numberChordMax,
		List<StyleCoefDegres> styleCoefDegres,
		List<MesureTempsFort> mesureTempsForts,
		List<StyleGroupIns> styleGroupInstruments,
		List<StyleGroupDrums> styleGroupDrums) {
		
		this.styleName = styleName;
		
		this.styleGroupInstruments = styleGroupInstruments;
		this.styleGroupDrums = styleGroupDrums;
		
		this.styleCoefDegres = styleCoefDegres.get(RANDOM.nextInt(styleCoefDegres.size()));
		
		this.mesureTempsFort = mesureTempsForts.get(RANDOM.nextInt(mesureTempsForts.size()));
		
		this.availableTonalities = new ArrayList<EnumTonalite>();
		for(Integer i:availableTon) {
			this.availableTonalities.add(EnumTonalite.getByIndex(i));
		}
		
		this.rythmeMax = rythmeMax;
		this.rythmeMin = rythmeMin;
		this.numberInstrument = this.randomCustom(nInstrumentMax - nInstrumentMin) + nInstrumentMin;
		this.numberPercussion = this.randomCustom(nPercussionMax - nPercussionMin) + nPercussionMin;
		
		this.numberChordMin = numberChordMin;
		this.numberChordMax = numberChordMax;
		this.complexityChordMin = complexityChordMin;
		this.complexityChordMax = complexityChordMax;

		if(rythme != 0) {
			this.rythme = rythme;
		}
		else {
			this.rythme = this.randomCustom(this.rythmeMax - this.rythmeMin) + this.rythmeMin;
		}
		
		this.chooseChords();
	}
	
	// TODO a revoir
	private void chooseChords() {
		
		this.choosedChords = new ArrayList<Chord>();
		List<EnumDegre> allDegres = new ArrayList<EnumDegre>();
		int theoricalMaximum = (this.complexityChordMax +1 - this.complexityChordMin) * 4; 
		int numberChords = this.randomCustom(this.numberChordMax - this.numberChordMin) + this.numberChordMin;
		
		numberChords = Math.min(numberChords,theoricalMaximum);
		
		for(int i=0;i<numberChords;i++) {

			Chord acc = new Chord(this.complexityChordMin, this.complexityChordMax, allDegres, this.choosedChords, this.styleCoefDegres);
			this.choosedChords.add(acc);
			allDegres.add(acc.getDegre());
		}
	}
	
	public void assignNewStyleGroupIns(Map<EnumInstruments, StyleGroupIns> allInstruments) {
		List<StyleGroupIns> allStyleGroupIns = new ArrayList<StyleGroupIns>();
		
		for (Entry<EnumInstruments, StyleGroupIns> entry : allInstruments.entrySet()) {
			allStyleGroupIns.add(entry.getValue());
		}
		
		this.styleGroupInstruments = allStyleGroupIns;
	}
	
	public void generateAllowedRythms(EnumDurationChord chord) {
		for(StyleGroupIns sg: this.styleGroupInstruments) {
			sg.generateAllowedRythms(chord, this.rythme);
		}
	}

	public int getRythmeMax() {
		return this.rythmeMax; 
	}
	
	public int getRythmeMin() {
		return this.rythmeMin; 
	}
	
	public int getNumberInstrument() {
		return this.numberInstrument;
	}

	public int getRythm() {
		return this.rythme;
	}
	
	public List<EnumTonalite> getTonalities() {
		return this.availableTonalities;
	}

	public List<StyleGroupIns> getStyleGroupInstruments() {
		return styleGroupInstruments;
	}

	public void setStyleGroupInstruments(List<StyleGroupIns> styleGroupInstruments) {
		this.styleGroupInstruments = styleGroupInstruments;
	}

	public String getStyleName() {
		return styleName;
	}

	public void setStyleName(String styleName) {
		this.styleName = styleName;
	}

	public int getNumberPercussion() {
		return numberPercussion;
	}

	public void setNumberPercussion(int numberPercussion) {
		this.numberPercussion = numberPercussion;
	}

	public int getComplexityChordMin() {
		return complexityChordMin;
	}

	public void setComplexityChordMin(int complexityChordMin) {
		this.complexityChordMin = complexityChordMin;
	}

	public int getComplexityChordMax() {
		return complexityChordMax;
	}

	public void setComplexityChordMax(int complexityChordMax) {
		this.complexityChordMax = complexityChordMax;
	}

	public int getNumberChordMin() {
		return numberChordMin;
	}

	public void setNumberChordMin(int numberChordMin) {
		this.numberChordMin = numberChordMin;
	}

	public int getNumberChordMax() {
		return numberChordMax;
	}

	public void setNumberChordMax(int numberChordMax) {
		this.numberChordMax = numberChordMax;
	}

	public MesureTempsFort getMesureTempsFort() {
		return mesureTempsFort;
	}

	public void setMesureTempsFort(MesureTempsFort mesureTempsFort) {
		this.mesureTempsFort = mesureTempsFort;
	}

	public List<Chord> getChoosedChords() {
		return choosedChords;
	}

	public void setChoosedChords(List<Chord> choosedChords) {
		this.choosedChords = choosedChords;
	}

	public List<StyleGroupDrums> getStyleGroupDrums() {
		return styleGroupDrums;
	}

	public void setStyleGroupDrums(List<StyleGroupDrums> styleGroupDrums) {
		this.styleGroupDrums = styleGroupDrums;
	}

	public StyleCoefDegres getStyleCoefDegres() {
		return styleCoefDegres;
	}

	public void setStyleCoefDegres(StyleCoefDegres styleCoefDegres) {
		this.styleCoefDegres = styleCoefDegres;
	}
}
