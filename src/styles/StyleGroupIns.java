package styles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import enums.EnumDurationChord;
import enums.EnumDurationNote;
import enums.EnumInstruments;

public class StyleGroupIns {

	private long id;
	private String name;
	private long motherId;
	private List<EnumInstruments> instruments;

	private int pourcentagePlaying;
	private int balanceBassMelody;
	private int balanceMelodyMelodyBis;
	private int gBetweenNotes;
	private int dispersionNotes;
	private int pourcentageSil;
	private int speed;
	private Map<EnumDurationNote, Double> allowedR;
	private int pSyncopes;
	private int pSyncopettes;
	private int pSyncopesTempsForts;
	private int pContreTemps;
	private int pDissonance;
	private int pIrregularites;
	private int bBinaireTernaire;
	private int pChanceSupSameRythm;
	private int numberNotesSimultaneously;
	private int numberRythm;
	private int pChanceSameNoteInMotif;
	private int pChanceStartByTonaleAsBass;
	private int pChanceStayAroundNoteRef;
	private int pChancePlayChord;

	int pourcentagePlayingMin;
	int pourcentagePlayingMax;
	int balanceBassMelodyMin;
	int balanceBassMelodyMax;
	int balanceMelodyMelodyBisMin;
	int balanceMelodyMelodyBisMax;
	int gBetweenNotesMin;
	int gBetweenNotesMax;
	int dispersionNotesMin;
	int dispersionNotesMax;
	int pourcentageSilMin;
	int pourcentageSilMax;
	int speedMax;
	int speedMin;
	int pSyncopesMin;
	int pSyncopesMax;
	int pSyncopettesMin;
	int pSyncopettesMax;
	int pSyncopesTempsFortsMin;
	int pSyncopesTempsFortsMax;
	int pContreTempsMin;
	int pContreTempsMax;
	int pDissonanceMin;
	int pDissonanceMax;
	int pIrregularitesMin;
	int pIrregularitesMax;
	int bBinaireTernaireMin;
	int bBinaireTernaireMax;
	int pChanceSupSameRythmMin;
	int pChanceSupSameRythmMax;
	int numberNotesSimultaneouslyMin;
	int numberNotesSimultaneouslyMax;
	int probabilityRythmMin;
	int probabilityRythmMax;
	int numberRythmMin;
	int numberRythmMax;
	int weightRythmMin;
	int weightRythmMax;

	int pChanceSameNoteInMotifMin;
	int pChanceSameNoteInMotifMax;
	int pChanceStartByTonaleAsBassMin;
	int pChanceStartByTonaleAsBassMax;
	int pChanceStayAroundNoteRefMin;
	int pChanceStayAroundNoteRefMax;
	int pChancePlayChordMin;
	int pChancePlayChordMax;

	private EnumDurationNote preferedRythm;

	private Random random = new Random();

	public StyleGroupIns() {
	}

	public StyleGroupIns(long id, String name, long motherId, List<Integer> ins, int pourcentagePlayingMin,
			int pourcentagePlayingMax, int balanceBassMelodyMin, int balanceBassMelodyMax,
			int balanceMelodyMelodyBisMin, int balanceMelodyMelodyBisMax, int gBetweenNotesMin, int gBetweenNotesMax,
			int dispersionNotesMin, int dispersionNotesMax, int pourcentageSilMin, int pourcentageSilMax, int speedMax,
			int speedMin, int pSyncopesMin, int pSyncopesMax, int pSyncopettesMin, int pSyncopettesMax,
			int pSyncopesTempsFortsMin, int pSyncopesTempsFortsMax, int pContreTempsMin, int pContreTempsMax,
			int pDissonanceMin, int pDissonanceMax, int pIrregularitesMin, int pIrregularitesMax,
			int bBinaireTernaireMin, int bBinaireTernaireMax, int pChanceSupSameRythmMin, int pChanceSupSameRythmMax,
			int numberNotesSimultaneouslyMin, int numberNotesSimultaneouslyMax, int probabilityRythmMin,
			int probabilityRythmMax, int numberRythmMin, int numberRythmMax, int weightRythmMin, int weightRythmMax,
			int pChanceSameNoteInMotifMin, int pChanceSameNoteInMotifMax, int pChanceStartByTonaleAsBassMin,
			int pChanceStartByTonaleAsBassMax, int pChanceStayAroundNoteRefMin, int pChanceStayAroundNoteRefMax,
			int pChancePlayChordMin, int pChancePlayChordMax) {

		this.id = id;
		this.name = name;
		this.setMotherId(motherId);

		this.instruments = new ArrayList<>();
		for (Integer i : ins) {
			this.instruments.add(EnumInstruments.getByIndex(i));
		}

		this.pourcentagePlayingMin = pourcentagePlayingMin;
		this.pourcentagePlayingMax = pourcentagePlayingMax;
		this.balanceBassMelodyMin = balanceBassMelodyMin;
		this.balanceBassMelodyMax = balanceBassMelodyMax;
		this.balanceMelodyMelodyBisMin = balanceMelodyMelodyBisMin;
		this.balanceMelodyMelodyBisMax = balanceMelodyMelodyBisMax;
		this.setgBetweenNotesMin(gBetweenNotesMin);
		this.setgBetweenNotesMax(gBetweenNotesMax);
		this.dispersionNotesMin = dispersionNotesMin;
		this.dispersionNotesMax = dispersionNotesMax;
		this.pourcentageSilMin = pourcentageSilMin;
		this.pourcentageSilMax = pourcentageSilMax;
		this.speedMax = speedMax;
		this.speedMin = speedMin;
		this.pSyncopesMin = pSyncopesMin;
		this.pSyncopesMax = pSyncopesMax;
		this.pSyncopettesMin = pSyncopettesMin;
		this.pSyncopettesMax = pSyncopettesMax;
		this.pSyncopesTempsFortsMin = pSyncopesTempsFortsMin;
		this.pSyncopesTempsFortsMax = pSyncopesTempsFortsMax;
		this.pContreTempsMin = pContreTempsMin;
		this.pContreTempsMax = pContreTempsMax;
		this.pDissonanceMin = pDissonanceMin;
		this.pDissonanceMax = pDissonanceMax;
		this.pIrregularitesMin = pIrregularitesMin;
		this.pIrregularitesMax = pIrregularitesMax;
		this.bBinaireTernaireMin = bBinaireTernaireMin;
		this.bBinaireTernaireMax = bBinaireTernaireMax;
		this.pChanceSupSameRythmMin = pChanceSupSameRythmMin;
		this.pChanceSupSameRythmMax = pChanceSupSameRythmMax;
		this.numberNotesSimultaneouslyMin = numberNotesSimultaneouslyMin;
		this.numberNotesSimultaneouslyMax = numberNotesSimultaneouslyMax;
		this.probabilityRythmMin = probabilityRythmMin;
		this.probabilityRythmMax = probabilityRythmMax;
		this.numberRythmMin = numberRythmMin;
		this.numberRythmMax = numberRythmMax;
		this.weightRythmMin = weightRythmMin;
		this.weightRythmMax = weightRythmMax;
		this.pChanceSameNoteInMotifMin = pChanceSameNoteInMotifMin;
		this.pChanceSameNoteInMotifMax = pChanceSameNoteInMotifMax;
		this.pChanceStartByTonaleAsBassMin = pChanceStartByTonaleAsBassMin;
		this.pChanceStartByTonaleAsBassMax = pChanceStartByTonaleAsBassMax;
		this.pChanceStayAroundNoteRefMin = pChanceStayAroundNoteRefMin;
		this.pChanceStayAroundNoteRefMax = pChanceStayAroundNoteRefMax;
		this.pChancePlayChordMin = pChancePlayChordMin;
		this.pChancePlayChordMax = pChancePlayChordMax;

		this.dispersionNotes = this.randomCustom(dispersionNotesMax - dispersionNotesMin) + dispersionNotesMin;
		this.pourcentagePlaying = this.randomCustom(pourcentagePlayingMax - pourcentagePlayingMin)
				+ pourcentagePlayingMin;
		this.balanceBassMelody = this.randomCustom(balanceBassMelodyMax - balanceBassMelodyMin) + balanceBassMelodyMin;
		this.balanceMelodyMelodyBis = this.randomCustom(balanceMelodyMelodyBisMax - balanceMelodyMelodyBisMin)
				+ balanceMelodyMelodyBisMin;
		this.gBetweenNotes = this.randomCustom(gBetweenNotesMax - gBetweenNotesMin) + gBetweenNotesMin;
		this.pourcentageSil = this.randomCustom(pourcentageSilMax - pourcentageSilMin) + pourcentageSilMin;
		this.speed = this.randomCustom(speedMax - speedMin) + speedMin;
		this.pSyncopes = this.randomCustom(pSyncopesMax - pSyncopesMin) + pSyncopesMin;
		this.pSyncopettes = this.randomCustom(pSyncopettesMax - pSyncopettesMin) + pSyncopettesMin;
		this.pSyncopesTempsForts = this.randomCustom(pSyncopesTempsFortsMax - pSyncopesTempsFortsMin)
				+ pSyncopesTempsFortsMin;
		this.pContreTemps = this.randomCustom(pContreTempsMax - pContreTempsMin) + pContreTempsMin;
		this.pDissonance = this.randomCustom(pDissonanceMax - pDissonanceMin) + pDissonanceMin;
		this.pIrregularites = this.randomCustom(pIrregularitesMax - pIrregularitesMin) + pIrregularitesMin;
		this.bBinaireTernaire = this.randomCustom(bBinaireTernaireMax - bBinaireTernaireMin) + bBinaireTernaireMin;
		this.pChanceSupSameRythm = this.randomCustom(pChanceSupSameRythmMax - pChanceSupSameRythmMin)
				+ pChanceSupSameRythmMin;
		this.numberNotesSimultaneously = this.randomCustom(numberNotesSimultaneouslyMax - numberNotesSimultaneouslyMin)
				+ numberNotesSimultaneouslyMin;
		this.numberRythm = this.randomCustom(numberRythmMax - numberRythmMin) + numberRythmMin;
		this.pChanceSameNoteInMotif = this.randomCustom(pChanceSameNoteInMotifMax - pChanceSameNoteInMotifMin)
				+ pChanceSameNoteInMotifMin;
		this.pChanceStartByTonaleAsBass = this.randomCustom(
				pChanceStartByTonaleAsBassMax - pChanceStartByTonaleAsBassMin) + pChanceStartByTonaleAsBassMin;
		this.pChanceStayAroundNoteRef = this.randomCustom(pChanceStayAroundNoteRefMax - pChanceStayAroundNoteRefMin)
				+ pChanceStayAroundNoteRefMin;
		this.pChancePlayChord = this.randomCustom(pChancePlayChordMax - pChancePlayChordMin) + pChancePlayChordMin;

		this.allowedR = new EnumMap<>(EnumDurationNote.class);

	}

	public StyleGroupIns(StyleGroupIns styleGroupI) {

		this.id = styleGroupI.getId();
		this.name = styleGroupI.getName();
		this.motherId = styleGroupI.getMotherId();
		this.instruments = styleGroupI.getInstruments();

		this.pourcentagePlayingMin = styleGroupI.pourcentagePlayingMin;
		this.pourcentagePlayingMax = styleGroupI.pourcentagePlayingMax;
		this.balanceBassMelodyMin = styleGroupI.balanceBassMelodyMin;
		this.balanceBassMelodyMax = styleGroupI.balanceBassMelodyMax;
		this.balanceMelodyMelodyBisMin = styleGroupI.balanceMelodyMelodyBisMin;
		this.balanceMelodyMelodyBisMax = styleGroupI.balanceMelodyMelodyBisMax;
		this.dispersionNotesMin = styleGroupI.dispersionNotesMin;
		this.dispersionNotesMax = styleGroupI.dispersionNotesMax;
		this.gBetweenNotesMin = styleGroupI.gBetweenNotesMin;
		this.gBetweenNotesMax = styleGroupI.gBetweenNotesMax;
		this.pourcentageSilMin = styleGroupI.pourcentageSilMin;
		this.pourcentageSilMax = styleGroupI.pourcentageSilMax;
		this.speedMax = styleGroupI.speedMax;
		this.speedMin = styleGroupI.speedMin;
		this.pSyncopesMin = styleGroupI.pSyncopesMin;
		this.pSyncopesMax = styleGroupI.pSyncopesMax;
		this.pSyncopettesMin = styleGroupI.pSyncopettesMin;
		this.pSyncopettesMax = styleGroupI.pSyncopettesMax;
		this.pSyncopesTempsFortsMin = styleGroupI.pSyncopesTempsFortsMin;
		this.pSyncopesTempsFortsMax = styleGroupI.pSyncopesTempsFortsMax;
		this.pContreTempsMin = styleGroupI.pContreTempsMin;
		this.pContreTempsMax = styleGroupI.pContreTempsMax;
		this.pDissonanceMin = styleGroupI.pDissonanceMin;
		this.pDissonanceMax = styleGroupI.pDissonanceMax;
		this.pIrregularitesMin = styleGroupI.pIrregularitesMin;
		this.pIrregularitesMax = styleGroupI.pIrregularitesMax;
		this.bBinaireTernaireMin = styleGroupI.bBinaireTernaireMin;
		this.bBinaireTernaireMax = styleGroupI.bBinaireTernaireMax;
		this.pChanceSupSameRythmMin = styleGroupI.pChanceSupSameRythmMin;
		this.pChanceSupSameRythmMax = styleGroupI.pChanceSupSameRythmMax;
		this.numberNotesSimultaneouslyMin = styleGroupI.numberNotesSimultaneouslyMin;
		this.numberNotesSimultaneouslyMax = styleGroupI.numberNotesSimultaneouslyMax;
		this.probabilityRythmMin = styleGroupI.probabilityRythmMin;
		this.probabilityRythmMax = styleGroupI.probabilityRythmMax;
		this.numberRythmMin = styleGroupI.numberRythmMin;
		this.numberRythmMax = styleGroupI.numberRythmMax;
		this.weightRythmMin = styleGroupI.weightRythmMin;
		this.weightRythmMax = styleGroupI.weightRythmMax;
		this.pChanceSameNoteInMotifMin = styleGroupI.pChanceSameNoteInMotifMin;
		this.pChanceSameNoteInMotifMax = styleGroupI.pChanceSameNoteInMotifMax;
		this.pChanceStartByTonaleAsBassMin = styleGroupI.pChanceStartByTonaleAsBassMin;
		this.pChanceStartByTonaleAsBassMax = styleGroupI.pChanceStartByTonaleAsBassMax;
		this.pChanceStayAroundNoteRefMin = styleGroupI.pChanceStayAroundNoteRefMin;
		this.pChanceStayAroundNoteRefMax = styleGroupI.pChanceStayAroundNoteRefMax;
		this.pChancePlayChordMin = styleGroupI.pChancePlayChordMin;
		this.pChancePlayChordMax = styleGroupI.pChancePlayChordMax;

		this.dispersionNotes = this.randomCustom(dispersionNotesMax - dispersionNotesMin) + dispersionNotesMin;
		this.pourcentagePlaying = this.randomCustom(pourcentagePlayingMax - pourcentagePlayingMin)
				+ pourcentagePlayingMin;
		this.balanceBassMelody = this.randomCustom(balanceBassMelodyMax - balanceBassMelodyMin) + balanceBassMelodyMin;
		this.balanceMelodyMelodyBis = this.randomCustom(balanceMelodyMelodyBisMax - balanceMelodyMelodyBisMin)
				+ balanceMelodyMelodyBisMin;
		this.gBetweenNotes = this.randomCustom(gBetweenNotesMax - gBetweenNotesMin) + gBetweenNotesMin;
		this.pourcentageSil = this.randomCustom(pourcentageSilMax - pourcentageSilMin) + pourcentageSilMin;
		this.speed = this.randomCustom(speedMax - speedMin) + speedMin;
		this.pSyncopes = this.randomCustom(pSyncopesMax - pSyncopesMin) + pSyncopesMin;
		this.pSyncopettes = this.randomCustom(pSyncopettesMax - pSyncopettesMin) + pSyncopettesMin;
		this.pSyncopesTempsForts = this.randomCustom(pSyncopesTempsFortsMax - pSyncopesTempsFortsMin)
				+ pSyncopesTempsFortsMin;
		this.pContreTemps = this.randomCustom(pContreTempsMax - pContreTempsMin) + pContreTempsMin;
		this.pDissonance = this.randomCustom(pDissonanceMax - pDissonanceMin) + pDissonanceMin;
		this.pIrregularites = this.randomCustom(pIrregularitesMax - pIrregularitesMin) + pIrregularitesMin;
		this.bBinaireTernaire = this.randomCustom(bBinaireTernaireMax - bBinaireTernaireMin) + bBinaireTernaireMin;
		this.pChanceSupSameRythm = this.randomCustom(pChanceSupSameRythmMax - pChanceSupSameRythmMin)
				+ pChanceSupSameRythmMin;
		this.numberNotesSimultaneously = this.randomCustom(numberNotesSimultaneouslyMax - numberNotesSimultaneouslyMin)
				+ numberNotesSimultaneouslyMin;
		this.numberRythm = this.randomCustom(numberRythmMax - numberRythmMin) + numberRythmMin;
		this.pChanceSameNoteInMotif = this.randomCustom(pChanceSameNoteInMotifMax - pChanceSameNoteInMotifMin)
				+ pChanceSameNoteInMotifMin;
		this.pChanceStartByTonaleAsBass = this.randomCustom(
				pChanceStartByTonaleAsBassMax - pChanceStartByTonaleAsBassMin) + pChanceStartByTonaleAsBassMin;
		this.pChanceStayAroundNoteRef = this.randomCustom(pChanceStayAroundNoteRefMax - pChanceStayAroundNoteRefMin)
				+ pChanceStayAroundNoteRefMin;
		this.pChancePlayChord = this.randomCustom(pChancePlayChordMax - pChancePlayChordMin) + pChancePlayChordMin;

		this.allowedR = new EnumMap<>(EnumDurationNote.class);

	}

	private int randomCustom(int value) {
		if (value == 0) {
			return 0;
		} else {
			return random.nextInt(value + 1);
		}
	}

	public void ajustSwing(int sign) {

		int ran = random.nextInt(8);

		switch (ran) {
		case 0:
			this.pourcentageSil = Math.max(pourcentageSilMin, Math.min(this.pourcentageSil + sign, pourcentageSilMax));
			break;
		case 1:
			this.pSyncopes = Math.max(pSyncopesMin, Math.min(this.pSyncopes + sign, pSyncopesMax));
			break;
		case 2:
			this.pSyncopettes = Math.max(pSyncopettesMin, Math.min(this.pSyncopettes + sign, pSyncopettesMax));
			break;
		case 3:
			this.pSyncopesTempsForts = Math.max(pSyncopesTempsFortsMin,
					Math.min(this.pSyncopesTempsForts + sign, pSyncopesTempsFortsMax));
			break;
		case 4:
			this.pContreTemps = Math.max(pContreTempsMin, Math.min(this.pContreTemps + sign, pContreTempsMax));
			break;
		case 5:
			this.pDissonance = Math.max(pDissonanceMin, Math.min(this.pDissonance + sign, pDissonanceMax));
			break;
		case 6:
			this.pIrregularites = Math.max(pIrregularitesMin, Math.min(this.pIrregularites + sign, pIrregularitesMax));
			break;
		case 7:
			this.pChanceSupSameRythm = Math.max(pChanceSupSameRythmMin,
					Math.min(this.pChanceSupSameRythm + sign, pChanceSupSameRythmMax));
			break;
		default:
			break;
		}
	}

	public void generateAllowedRythms(EnumDurationChord chord, int rythm) {

		double ratio = this.speed / (double) rythm;
		double sigma;
		double result;

		this.preferedRythm = EnumDurationNote.getEnumDurationByClosestCoefRythme(ratio, chord);

		// calcul du sigma : dispersion
		sigma = Math.max(1, ((Math.max(100 - this.pChanceSupSameRythm, 1) * EnumDurationNote.values().length) / 100.0));

		for (EnumDurationNote duration : EnumDurationNote.getList(chord)) {

			result = pdf(duration.ordinal(), this.preferedRythm.ordinal(), sigma) * 100;
			if (duration.equals(EnumDurationNote.BLANCHEPOINTEE) || duration.equals(EnumDurationNote.RONDEPOINTEE)
					|| duration.equals(EnumDurationNote.CROCHEPOINTEE)
					|| duration.equals(EnumDurationNote.NOIREPOINTEE)) {
				result = result + this.pIrregularites * result / 100.0;
			} else {
				result = result + (100 - this.pIrregularites) * result / 100.0;
			}

			this.allowedR.put(duration, result);
		}

		this.normalizeAllowedRythms();
	}

	// normalize the values between probabilityRythmMin and probabilityRythmMax
	private void normalizeAllowedRythms() {

		double min = Collections.min(this.allowedR.values());
		double max = Collections.max(this.allowedR.values());
		double newValue;

		for (Map.Entry<EnumDurationNote, Double> entry : this.allowedR.entrySet()) {

			newValue = ((entry.getValue() - min) / (max - min)) * (probabilityRythmMax - probabilityRythmMin)
					+ probabilityRythmMin;
			this.allowedR.put(entry.getKey(), newValue);
		}
	}

	// return pdf(x) = standard Gaussian pdf
	private static double pdf(double x) {
		return Math.exp(-x * x / 2) / Math.sqrt(2 * Math.PI);
	}

	// return pdf(x, mu, signma) = Gaussian pdf with mean mu and stddev sigma
	private static double pdf(double x, double mu, double sigma) {
		return pdf((x - mu) / sigma) / sigma;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getgBetweenNotes() {
		return gBetweenNotes;
	}

	public void setgBetweenNotes(int gBetweenNotes) {
		this.gBetweenNotes = gBetweenNotes;
	}

	public int getPourcentageSil() {
		return pourcentageSil;
	}

	public void setPourcentageSil(int pourcentageSil) {
		this.pourcentageSil = pourcentageSil;
	}

	public int getpSyncopes() {
		return pSyncopes;
	}

	public void setpSyncopes(int pSyncopes) {
		this.pSyncopes = pSyncopes;
	}

	public int getpSyncopettes() {
		return pSyncopettes;
	}

	public void setpSyncopettes(int pSyncopettes) {
		this.pSyncopettes = pSyncopettes;
	}

	public int getpSyncopesTempsForts() {
		return pSyncopesTempsForts;
	}

	public void setpSyncopesTempsForts(int pSyncopesTempsForts) {
		this.pSyncopesTempsForts = pSyncopesTempsForts;
	}

	public int getpContreTemps() {
		return pContreTemps;
	}

	public void setpContreTemps(int pContreTemps) {
		this.pContreTemps = pContreTemps;
	}

	public int getpDissonance() {
		return pDissonance;
	}

	public void setpDissonance(int pDissonance) {
		this.pDissonance = pDissonance;
	}

	public int getpIrregularites() {
		return pIrregularites;
	}

	public void setpIrregularites(int pIrregularites) {
		this.pIrregularites = pIrregularites;
	}

	public int getpChanceSupSameRythm() {
		return pChanceSupSameRythm;
	}

	public void setpChanceSupSameRythm(int pChanceSupSameRythm) {
		this.pChanceSupSameRythm = pChanceSupSameRythm;
	}

	public int getbBinaireTernaire() {
		return bBinaireTernaire;
	}

	public void setbBinaireTernaire(int bBinaireTernaire) {
		this.bBinaireTernaire = bBinaireTernaire;
	}

	public List<EnumInstruments> getInstruments() {
		return instruments;
	}

	public Map<EnumDurationNote, Double> getAllowedR() {
		return allowedR;
	}

	public void setAllowedR(Map<EnumDurationNote, Double> allowedR) {
		this.allowedR = allowedR;
	}

	public int getPourcentagePlaying() {
		return pourcentagePlaying;
	}

	public void setPourcentagePlaying(int pourcentagePlaying) {
		this.pourcentagePlaying = pourcentagePlaying;
	}

	public int getBalanceBassMelody() {
		return balanceBassMelody;
	}

	public void setBalanceBassMelody(int balanceBassMelody) {
		this.balanceBassMelody = balanceBassMelody;
	}

	public int getBalanceMelodyMelodyBis() {
		return balanceMelodyMelodyBis;
	}

	public void setBalanceMelodyMelodyBis(int balanceMelodyMelodyBis) {
		this.balanceMelodyMelodyBis = balanceMelodyMelodyBis;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getMotherId() {
		return motherId;
	}

	public void setMotherId(long motherId) {
		this.motherId = motherId;
	}

	public int getgBetweenNotesMin() {
		return gBetweenNotesMin;
	}

	public void setgBetweenNotesMin(int gBetweenNotesMin) {
		this.gBetweenNotesMin = gBetweenNotesMin;
	}

	public int getgBetweenNotesMax() {
		return gBetweenNotesMax;
	}

	public void setgBetweenNotesMax(int gBetweenNotesMax) {
		this.gBetweenNotesMax = gBetweenNotesMax;
	}

	public int getDispersionNotes() {
		return dispersionNotes;
	}

	public void setDispersionNotes(int dispersionNotes) {
		this.dispersionNotes = dispersionNotes;
	}

	public int getNumberNotesSimultaneously() {
		return numberNotesSimultaneously;
	}

	public void setNumberNotesSimultaneously(int numberNotesSimultaneously) {
		this.numberNotesSimultaneously = numberNotesSimultaneously;
	}

	public int getNumberRythm() {
		return numberRythm;
	}

	public void setNumberRythm(int numberRythm) {
		this.numberRythm = numberRythm;
	}

	public int getRandomWeight() {
		return this.randomCustom(weightRythmMax - weightRythmMin) + weightRythmMin;
	}

	public int getpChanceSameNoteInMotif() {
		return pChanceSameNoteInMotif;
	}

	public void setpChanceSameNoteInMotif(int pChanceSameNoteInMotif) {
		this.pChanceSameNoteInMotif = pChanceSameNoteInMotif;
	}

	public int getpChanceStartByTonaleAsBass() {
		return pChanceStartByTonaleAsBass;
	}

	public void setpChanceStartByTonaleAsBass(int pChanceStartByTonaleAsBass) {
		this.pChanceStartByTonaleAsBass = pChanceStartByTonaleAsBass;
	}

	public int getpChanceStayAroundNoteRef() {
		return pChanceStayAroundNoteRef;
	}

	public void setpChanceStayAroundNoteRef(int pChanceStayAroundNoteRef) {
		this.pChanceStayAroundNoteRef = pChanceStayAroundNoteRef;
	}

	public int getpChancePlayChord() {
		return pChancePlayChord;
	}

	public void setpChancePlayChord(int pChancePlayChord) {
		this.pChancePlayChord = pChancePlayChord;
	}

	public int getPourcentagePlayingMin() {
		return pourcentagePlayingMin;
	}

	public void setPourcentagePlayingMin(int pourcentagePlayingMin) {
		this.pourcentagePlayingMin = pourcentagePlayingMin;
	}

	public int getPourcentagePlayingMax() {
		return pourcentagePlayingMax;
	}

	public void setPourcentagePlayingMax(int pourcentagePlayingMax) {
		this.pourcentagePlayingMax = pourcentagePlayingMax;
	}

	public int getBalanceBassMelodyMin() {
		return balanceBassMelodyMin;
	}

	public void setBalanceBassMelodyMin(int balanceBassMelodyMin) {
		this.balanceBassMelodyMin = balanceBassMelodyMin;
	}

	public int getBalanceBassMelodyMax() {
		return balanceBassMelodyMax;
	}

	public void setBalanceBassMelodyMax(int balanceBassMelodyMax) {
		this.balanceBassMelodyMax = balanceBassMelodyMax;
	}

	public int getBalanceMelodyMelodyBisMin() {
		return balanceMelodyMelodyBisMin;
	}

	public void setBalanceMelodyMelodyBisMin(int balanceMelodyMelodyBisMin) {
		this.balanceMelodyMelodyBisMin = balanceMelodyMelodyBisMin;
	}

	public int getBalanceMelodyMelodyBisMax() {
		return balanceMelodyMelodyBisMax;
	}

	public void setBalanceMelodyMelodyBisMax(int balanceMelodyMelodyBisMax) {
		this.balanceMelodyMelodyBisMax = balanceMelodyMelodyBisMax;
	}

	public int getDispersionNotesMin() {
		return dispersionNotesMin;
	}

	public void setDispersionNotesMin(int dispersionNotesMin) {
		this.dispersionNotesMin = dispersionNotesMin;
	}

	public int getDispersionNotesMax() {
		return dispersionNotesMax;
	}

	public void setDispersionNotesMax(int dispersionNotesMax) {
		this.dispersionNotesMax = dispersionNotesMax;
	}

	public int getPourcentageSilMin() {
		return pourcentageSilMin;
	}

	public void setPourcentageSilMin(int pourcentageSilMin) {
		this.pourcentageSilMin = pourcentageSilMin;
	}

	public int getPourcentageSilMax() {
		return pourcentageSilMax;
	}

	public void setPourcentageSilMax(int pourcentageSilMax) {
		this.pourcentageSilMax = pourcentageSilMax;
	}

	public int getSpeedMax() {
		return speedMax;
	}

	public void setSpeedMax(int speedMax) {
		this.speedMax = speedMax;
	}

	public int getSpeedMin() {
		return speedMin;
	}

	public void setSpeedMin(int speedMin) {
		this.speedMin = speedMin;
	}

	public int getpSyncopesMin() {
		return pSyncopesMin;
	}

	public void setpSyncopesMin(int pSyncopesMin) {
		this.pSyncopesMin = pSyncopesMin;
	}

	public int getpSyncopesMax() {
		return pSyncopesMax;
	}

	public void setpSyncopesMax(int pSyncopesMax) {
		this.pSyncopesMax = pSyncopesMax;
	}

	public int getpSyncopettesMin() {
		return pSyncopettesMin;
	}

	public void setpSyncopettesMin(int pSyncopettesMin) {
		this.pSyncopettesMin = pSyncopettesMin;
	}

	public int getpSyncopettesMax() {
		return pSyncopettesMax;
	}

	public void setpSyncopettesMax(int pSyncopettesMax) {
		this.pSyncopettesMax = pSyncopettesMax;
	}

	public int getpSyncopesTempsFortsMin() {
		return pSyncopesTempsFortsMin;
	}

	public void setpSyncopesTempsFortsMin(int pSyncopesTempsFortsMin) {
		this.pSyncopesTempsFortsMin = pSyncopesTempsFortsMin;
	}

	public int getpSyncopesTempsFortsMax() {
		return pSyncopesTempsFortsMax;
	}

	public void setpSyncopesTempsFortsMax(int pSyncopesTempsFortsMax) {
		this.pSyncopesTempsFortsMax = pSyncopesTempsFortsMax;
	}

	public int getpContreTempsMin() {
		return pContreTempsMin;
	}

	public void setpContreTempsMin(int pContreTempsMin) {
		this.pContreTempsMin = pContreTempsMin;
	}

	public int getpContreTempsMax() {
		return pContreTempsMax;
	}

	public void setpContreTempsMax(int pContreTempsMax) {
		this.pContreTempsMax = pContreTempsMax;
	}

	public int getpDissonanceMin() {
		return pDissonanceMin;
	}

	public void setpDissonanceMin(int pDissonanceMin) {
		this.pDissonanceMin = pDissonanceMin;
	}

	public int getpDissonanceMax() {
		return pDissonanceMax;
	}

	public void setpDissonanceMax(int pDissonanceMax) {
		this.pDissonanceMax = pDissonanceMax;
	}

	public int getpIrregularitesMin() {
		return pIrregularitesMin;
	}

	public void setpIrregularitesMin(int pIrregularitesMin) {
		this.pIrregularitesMin = pIrregularitesMin;
	}

	public int getpIrregularitesMax() {
		return pIrregularitesMax;
	}

	public void setpIrregularitesMax(int pIrregularitesMax) {
		this.pIrregularitesMax = pIrregularitesMax;
	}

	public int getbBinaireTernaireMin() {
		return bBinaireTernaireMin;
	}

	public void setbBinaireTernaireMin(int bBinaireTernaireMin) {
		this.bBinaireTernaireMin = bBinaireTernaireMin;
	}

	public int getbBinaireTernaireMax() {
		return bBinaireTernaireMax;
	}

	public void setbBinaireTernaireMax(int bBinaireTernaireMax) {
		this.bBinaireTernaireMax = bBinaireTernaireMax;
	}

	public int getpChanceSupSameRythmMin() {
		return pChanceSupSameRythmMin;
	}

	public void setpChanceSupSameRythmMin(int pChanceSupSameRythmMin) {
		this.pChanceSupSameRythmMin = pChanceSupSameRythmMin;
	}

	public int getpChanceSupSameRythmMax() {
		return pChanceSupSameRythmMax;
	}

	public void setpChanceSupSameRythmMax(int pChanceSupSameRythmMax) {
		this.pChanceSupSameRythmMax = pChanceSupSameRythmMax;
	}

	public int getNumberNotesSimultaneouslyMin() {
		return numberNotesSimultaneouslyMin;
	}

	public void setNumberNotesSimultaneouslyMin(int numberNotesSimultaneouslyMin) {
		this.numberNotesSimultaneouslyMin = numberNotesSimultaneouslyMin;
	}

	public int getNumberNotesSimultaneouslyMax() {
		return numberNotesSimultaneouslyMax;
	}

	public void setNumberNotesSimultaneouslyMax(int numberNotesSimultaneouslyMax) {
		this.numberNotesSimultaneouslyMax = numberNotesSimultaneouslyMax;
	}

	public int getProbabilityRythmMin() {
		return probabilityRythmMin;
	}

	public void setProbabilityRythmMin(int probabilityRythmMin) {
		this.probabilityRythmMin = probabilityRythmMin;
	}

	public int getProbabilityRythmMax() {
		return probabilityRythmMax;
	}

	public void setProbabilityRythmMax(int probabilityRythmMax) {
		this.probabilityRythmMax = probabilityRythmMax;
	}

	public int getNumberRythmMin() {
		return numberRythmMin;
	}

	public void setNumberRythmMin(int numberRythmMin) {
		this.numberRythmMin = numberRythmMin;
	}

	public int getNumberRythmMax() {
		return numberRythmMax;
	}

	public void setNumberRythmMax(int numberRythmMax) {
		this.numberRythmMax = numberRythmMax;
	}

	public int getWeightRythmMin() {
		return weightRythmMin;
	}

	public void setWeightRythmMin(int weightRythmMin) {
		this.weightRythmMin = weightRythmMin;
	}

	public int getWeightRythmMax() {
		return weightRythmMax;
	}

	public void setWeightRythmMax(int weightRythmMax) {
		this.weightRythmMax = weightRythmMax;
	}

	public int getpChanceSameNoteInMotifMin() {
		return pChanceSameNoteInMotifMin;
	}

	public void setpChanceSameNoteInMotifMin(int pChanceSameNoteInMotifMin) {
		this.pChanceSameNoteInMotifMin = pChanceSameNoteInMotifMin;
	}

	public int getpChanceSameNoteInMotifMax() {
		return pChanceSameNoteInMotifMax;
	}

	public void setpChanceSameNoteInMotifMax(int pChanceSameNoteInMotifMax) {
		this.pChanceSameNoteInMotifMax = pChanceSameNoteInMotifMax;
	}

	public int getpChanceStartByTonaleAsBassMin() {
		return pChanceStartByTonaleAsBassMin;
	}

	public void setpChanceStartByTonaleAsBassMin(int pChanceStartByTonaleAsBassMin) {
		this.pChanceStartByTonaleAsBassMin = pChanceStartByTonaleAsBassMin;
	}

	public int getpChanceStartByTonaleAsBassMax() {
		return pChanceStartByTonaleAsBassMax;
	}

	public void setpChanceStartByTonaleAsBassMax(int pChanceStartByTonaleAsBassMax) {
		this.pChanceStartByTonaleAsBassMax = pChanceStartByTonaleAsBassMax;
	}

	public int getpChanceStayAroundNoteRefMin() {
		return pChanceStayAroundNoteRefMin;
	}

	public void setpChanceStayAroundNoteRefMin(int pChanceStayAroundNoteRefMin) {
		this.pChanceStayAroundNoteRefMin = pChanceStayAroundNoteRefMin;
	}

	public int getpChanceStayAroundNoteRefMax() {
		return pChanceStayAroundNoteRefMax;
	}

	public void setpChanceStayAroundNoteRefMax(int pChanceStayAroundNoteRefMax) {
		this.pChanceStayAroundNoteRefMax = pChanceStayAroundNoteRefMax;
	}

	public int getpChancePlayChordMin() {
		return pChancePlayChordMin;
	}

	public void setpChancePlayChordMin(int pChancePlayChordMin) {
		this.pChancePlayChordMin = pChancePlayChordMin;
	}

	public int getpChancePlayChordMax() {
		return pChancePlayChordMax;
	}

	public void setpChancePlayChordMax(int pChancePlayChordMax) {
		this.pChancePlayChordMax = pChancePlayChordMax;
	}

	public EnumDurationNote getPreferedRythm() {
		return preferedRythm;
	}

	public void setPreferedRythm(EnumDurationNote preferedRythm) {
		this.preferedRythm = preferedRythm;
	}

	public void setInstruments(List<EnumInstruments> instruments) {
		this.instruments = instruments;
	}

}
