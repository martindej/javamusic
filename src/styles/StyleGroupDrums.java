package styles;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import enums.EnumDurationChord;
import enums.EnumDurationNote;

public class StyleGroupDrums {

	private long id;
	private long styleId;
	private String name;
	
	private List<StyleGroupDrumsIns> drumsIns;
	
	private int rhythm;
	private int pourcentagePlayingTempsFortMin;
	private int pourcentagePlayingTempsFortMax;
	private int pourcentagePlayingTempsFaibleMin;
	private int pourcentagePlayingTempsFaibleMax;
	
	private int pourcentagePlayingTempsFort;
	private int pourcentagePlayingTempsFaible;
	
	private int pourcentagePlayingTemps1Min;
	private int pourcentagePlayingTemps1Max;
	private int pourcentagePlayingTemps1;
	
	private int pourcentagePlayingTemps2Min;
	private int pourcentagePlayingTemps2Max;
	private int pourcentagePlayingTemps2;
	
	private int pourcentagePlayingTemps3Min;
	private int pourcentagePlayingTemps3Max;
	private int pourcentagePlayingTemps3;
	
	private int pourcentagePlayingTemps4Min;
	private int pourcentagePlayingTemps4Max;
	private int pourcentagePlayingTemps4;
	
	private transient Random RANDOM = new Random();
	
	public StyleGroupDrums() {}
	
	public StyleGroupDrums(
			long id,
			long styleId,
			String name,
			int rhythm,
			int pourcentagePlayingTempsFortMin,
			int pourcentagePlayingTempsFortMax,
			int pourcentagePlayingTempsFaibleMin,
			int pourcentagePlayingTempsFaibleMax,
			int pourcentagePlayingTemps1Min,
			int pourcentagePlayingTemps1Max,
			int pourcentagePlayingTemps2Min,
			int pourcentagePlayingTemps2Max,
			int pourcentagePlayingTemps3Min,
			int pourcentagePlayingTemps3Max,
			int pourcentagePlayingTemps4Min,
			int pourcentagePlayingTemps4Max,
			List<StyleGroupDrumsIns> drumsIns) {
		
		this.id = id;
		this.styleId = styleId;
		this.name = name;
		this.rhythm = rhythm;
		this.pourcentagePlayingTempsFortMin = pourcentagePlayingTempsFortMin;
		this.pourcentagePlayingTempsFortMax = pourcentagePlayingTempsFortMax;
		this.pourcentagePlayingTempsFaibleMin = pourcentagePlayingTempsFaibleMin;
		this.pourcentagePlayingTempsFaibleMax = pourcentagePlayingTempsFaibleMax;
		
		this.pourcentagePlayingTemps1Min = pourcentagePlayingTemps1Min;
		this.pourcentagePlayingTemps1Max = pourcentagePlayingTemps1Max;
		
		this.pourcentagePlayingTemps2Min = pourcentagePlayingTemps2Min;
		this.pourcentagePlayingTemps2Max = pourcentagePlayingTemps2Max;
		
		this.pourcentagePlayingTemps3Min = pourcentagePlayingTemps3Min;
		this.pourcentagePlayingTemps3Max = pourcentagePlayingTemps3Max;
		
		this.pourcentagePlayingTemps4Min = pourcentagePlayingTemps4Min;
		this.pourcentagePlayingTemps4Max = pourcentagePlayingTemps4Max;
		
		this.drumsIns = drumsIns;
		
		this.pourcentagePlayingTempsFort = this.randomCustom(pourcentagePlayingTempsFortMax - pourcentagePlayingTempsFortMin) + pourcentagePlayingTempsFortMin;
		this.pourcentagePlayingTempsFaible = this.randomCustom(pourcentagePlayingTempsFaibleMax - pourcentagePlayingTempsFaibleMin) + pourcentagePlayingTempsFaibleMin;
		
		this.pourcentagePlayingTemps1 = this.randomCustom(pourcentagePlayingTemps1Max - pourcentagePlayingTemps1Min) + pourcentagePlayingTemps1Min; 
		this.pourcentagePlayingTemps2 = this.randomCustom(pourcentagePlayingTemps2Max - pourcentagePlayingTemps2Min) + pourcentagePlayingTemps2Min; 
		this.pourcentagePlayingTemps3 = this.randomCustom(pourcentagePlayingTemps3Max - pourcentagePlayingTemps3Min) + pourcentagePlayingTemps3Min; 
		this.pourcentagePlayingTemps4 = this.randomCustom(pourcentagePlayingTemps4Max - pourcentagePlayingTemps4Min) + pourcentagePlayingTemps4Min; 
	}
	
	private int randomCustom(int value) {
		if(value == 0) {
			return 0;
		}
		else {
			return RANDOM.nextInt(value+1);
		}
	}
	
	public void random(EnumDurationChord chordDuration) {
		
		EnumDurationNote duration = EnumDurationNote.randomEnumDurationPercussion(chordDuration);
		
		this.id = 0;
		this.styleId = 0;
		this.name = "random";
		this.rhythm = duration.getIndex();
		this.pourcentagePlayingTempsFortMin = 0;
		this.pourcentagePlayingTempsFortMax = 100;
		this.pourcentagePlayingTempsFaibleMin = 0;
		this.pourcentagePlayingTempsFaibleMax = 100;
		
		this.pourcentagePlayingTemps1Min = 0;
		this.pourcentagePlayingTemps1Max = 100;
		
		this.pourcentagePlayingTemps2Min = 0;
		this.pourcentagePlayingTemps2Max = 100;
		
		this.pourcentagePlayingTemps3Min = 0;
		this.pourcentagePlayingTemps3Max = 100;
		
		this.pourcentagePlayingTemps4Min = 0;
		this.pourcentagePlayingTemps4Max = 100;

		this.pourcentagePlayingTempsFort = this.randomCustom(pourcentagePlayingTempsFortMax - pourcentagePlayingTempsFortMin) + pourcentagePlayingTempsFortMin;
		this.pourcentagePlayingTempsFaible = this.randomCustom(pourcentagePlayingTempsFaibleMax - pourcentagePlayingTempsFaibleMin) + pourcentagePlayingTempsFaibleMin;
		
		this.pourcentagePlayingTemps1 = this.randomCustom(pourcentagePlayingTemps1Max - pourcentagePlayingTemps1Min) + pourcentagePlayingTemps1Min; 
		this.pourcentagePlayingTemps2 = this.randomCustom(pourcentagePlayingTemps2Max - pourcentagePlayingTemps2Min) + pourcentagePlayingTemps2Min; 
		this.pourcentagePlayingTemps3 = this.randomCustom(pourcentagePlayingTemps3Max - pourcentagePlayingTemps3Min) + pourcentagePlayingTemps3Min; 
		this.pourcentagePlayingTemps4 = this.randomCustom(pourcentagePlayingTemps4Max - pourcentagePlayingTemps4Min) + pourcentagePlayingTemps4Min; 
		
		this.drumsIns = new ArrayList<StyleGroupDrumsIns>();
		
		int ran = RANDOM.nextInt(4) + 1;
		
		for(int i=0;i<ran;i++) {
			StyleGroupDrumsIns sGDI = new StyleGroupDrumsIns();
			sGDI.random();
			this.drumsIns.add(sGDI);
		}
	}
	
	public long getStyleId() {
		return styleId;
	}
	public void setStyleId(long styleId) {
		this.styleId = styleId;
	}
	public int getRhythm() {
		return rhythm;
	}
	public void setRhythm(int rhythm) {
		this.rhythm = rhythm;
	}
	public int getPourcentagePlayingTempsFortMin() {
		return pourcentagePlayingTempsFortMin;
	}
	public void setPourcentagePlayingTempsFortMin(int pourcentagePlayingTempsFortMin) {
		this.pourcentagePlayingTempsFortMin = pourcentagePlayingTempsFortMin;
	}
	public int getPourcentagePlayingTempsFortMax() {
		return pourcentagePlayingTempsFortMax;
	}
	public void setPourcentagePlayingTempsFortMax(int pourcentagePlayingTempsFortMax) {
		this.pourcentagePlayingTempsFortMax = pourcentagePlayingTempsFortMax;
	}
	public int getPourcentagePlayingTempsFaibleMin() {
		return pourcentagePlayingTempsFaibleMin;
	}
	public void setPourcentagePlayingTempsFaibleMin(int pourcentagePlayingTempsFaibleMin) {
		this.pourcentagePlayingTempsFaibleMin = pourcentagePlayingTempsFaibleMin;
	}
	public int getPourcentagePlayingTempsFaibleMax() {
		return pourcentagePlayingTempsFaibleMax;
	}
	public void setPourcentagePlayingTempsFaibleMax(int pourcentagePlayingTempsFaibleMax) {
		this.pourcentagePlayingTempsFaibleMax = pourcentagePlayingTempsFaibleMax;
	}
	public int getPourcentagePlayingTemps1Min() {
		return pourcentagePlayingTemps1Min;
	}
	public void setPourcentagePlayingTemps1Min(int pourcentagePlayingTemps1Min) {
		this.pourcentagePlayingTemps1Min = pourcentagePlayingTemps1Min;
	}
	public int getPourcentagePlayingTemps1Max() {
		return pourcentagePlayingTemps1Max;
	}
	public void setPourcentagePlayingTemps1Max(int pourcentagePlayingTemps1Max) {
		this.pourcentagePlayingTemps1Max = pourcentagePlayingTemps1Max;
	}
	public int getPourcentagePlayingTemps2Min() {
		return pourcentagePlayingTemps2Min;
	}
	public void setPourcentagePlayingTemps2Min(int pourcentagePlayingTemps2Min) {
		this.pourcentagePlayingTemps2Min = pourcentagePlayingTemps2Min;
	}
	public int getPourcentagePlayingTemps2Max() {
		return pourcentagePlayingTemps2Max;
	}
	public void setPourcentagePlayingTemps2Max(int pourcentagePlayingTemps2Max) {
		this.pourcentagePlayingTemps2Max = pourcentagePlayingTemps2Max;
	}
	public int getPourcentagePlayingTemps3Min() {
		return pourcentagePlayingTemps3Min;
	}
	public void setPourcentagePlayingTemps3Min(int pourcentagePlayingTemps3Min) {
		this.pourcentagePlayingTemps3Min = pourcentagePlayingTemps3Min;
	}
	public int getPourcentagePlayingTemps3Max() {
		return pourcentagePlayingTemps3Max;
	}
	public void setPourcentagePlayingTemps3Max(int pourcentagePlayingTemps3Max) {
		this.pourcentagePlayingTemps3Max = pourcentagePlayingTemps3Max;
	}
	public int getPourcentagePlayingTemps4Min() {
		return pourcentagePlayingTemps4Min;
	}
	public void setPourcentagePlayingTemps4Min(int pourcentagePlayingTemps4Min) {
		this.pourcentagePlayingTemps4Min = pourcentagePlayingTemps4Min;
	}
	public int getPourcentagePlayingTemps4Max() {
		return pourcentagePlayingTemps4Max;
	}
	public void setPourcentagePlayingTemps4Max(int pourcentagePlayingTemps4Max) {
		this.pourcentagePlayingTemps4Max = pourcentagePlayingTemps4Max;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public List<StyleGroupDrumsIns> getDrumsIns() {
		return drumsIns;
	}

	public void setDrumsIns(List<StyleGroupDrumsIns> drumsIns) {
		this.drumsIns = drumsIns;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPourcentagePlayingTempsFort() {
		return pourcentagePlayingTempsFort;
	}

	public void setPourcentagePlayingTempsFort(int pourcentagePlayingTempsFort) {
		this.pourcentagePlayingTempsFort = pourcentagePlayingTempsFort;
	}

	public int getPourcentagePlayingTempsFaible() {
		return pourcentagePlayingTempsFaible;
	}

	public void setPourcentagePlayingTempsFaible(int pourcentagePlayingTempsFaible) {
		this.pourcentagePlayingTempsFaible = pourcentagePlayingTempsFaible;
	}

	public int getPourcentagePlayingTemps1() {
		return pourcentagePlayingTemps1;
	}

	public void setPourcentagePlayingTemps1(int pourcentagePlayingTemps1) {
		this.pourcentagePlayingTemps1 = pourcentagePlayingTemps1;
	}

	public int getPourcentagePlayingTemps2() {
		return pourcentagePlayingTemps2;
	}

	public void setPourcentagePlayingTemps2(int pourcentagePlayingTemps2) {
		this.pourcentagePlayingTemps2 = pourcentagePlayingTemps2;
	}

	public int getPourcentagePlayingTemps3() {
		return pourcentagePlayingTemps3;
	}

	public void setPourcentagePlayingTemps3(int pourcentagePlayingTemps3) {
		this.pourcentagePlayingTemps3 = pourcentagePlayingTemps3;
	}

	public int getPourcentagePlayingTemps4() {
		return pourcentagePlayingTemps4;
	}

	public void setPourcentagePlayingTemps4(int pourcentagePlayingTemps4) {
		this.pourcentagePlayingTemps4 = pourcentagePlayingTemps4;
	}
}

