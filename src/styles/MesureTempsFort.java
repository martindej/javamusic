package styles;

import java.util.ArrayList;
import java.util.List;

import enums.EnumDurationChord;
import enums.EnumTempsForts;

public class MesureTempsFort {
	
	private long id;
	
	private long styleId;
	private EnumDurationChord mesure;
	private List<EnumTempsForts> tempsFort;
	private boolean etOu;
	
	public MesureTempsFort(
		long id,
		long styleId,
		int mesure,
		List<Integer> tempsFort,
		boolean etOu) {
		
		this.id = id;
		this.styleId = styleId;
		this.setMesure(EnumDurationChord.getByIndex(mesure));
		this.etOu = etOu;
		
		this.tempsFort = new ArrayList<EnumTempsForts>();
		for(Integer i:tempsFort) {
			this.tempsFort.add(EnumTempsForts.getByIndex(i));
		}
	}

	public MesureTempsFort() {
		// TODO Auto-generated constructor stub
	}

	public long getStyleId() {
		return styleId;
	}

	public void setStyleId(long styleId) {
		this.styleId = styleId;
	}

	public boolean isEtOu() {
		return etOu;
	}

	public void setEtOu(boolean etOu) {
		this.etOu = etOu;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return this.id;
	}

	public EnumDurationChord getMesure() {
		return mesure;
	}

	public void setMesure(EnumDurationChord mesure) {
		this.mesure = mesure;
	}
	
	public List<EnumTempsForts> getTempsFort() {
		return this.tempsFort;
	}

}