package musicRandom;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import enums.EnumBrickSentence;
import enums.EnumChordTension;
import enums.EnumDurationNote;
import enums.EnumNote;
import enums.EnumNoteTonality;
import enums.EnumSilence;
import enums.EnumStyle;
import enums.EnumTempsForts;
import enums.EnumVelocity;
import enums.EnumVelocityVariation;
import exceptions.MyException;

public class BassLigne {

	private List<Note> listNotes;
	private int size;
	private Map<Integer, Motif> placementMotifsMelody;
	private EnumStyle style; // TODO
	private List<EnumTempsForts> tempsFortTension;
	private EnumVelocity velocity;
	private EnumVelocityVariation velocityVariation;

	int count = 0;
	int maxTries = 10;

	private IdentityInstrument idInstrument;

	private Random RANDOM = new Random();

	BassLigne(IdentityInstrument idInst, BassLigne bass, Map<EnumDurationNote, Integer> allowedRythmesOfRefInstrument)
			throws MyException {

		this.idInstrument = idInst;
		this.tempsFortTension = bass.getTempsFortTension();
		this.size = this.idInstrument.getChordsList().size();

		while (true) {
			try {
				this.listNotes = new ArrayList<>();
				generateBassLigneWithRef(bass, allowedRythmesOfRefInstrument);
				break;
			} catch (MyException e) {
				if (++count == maxTries)
					throw e;
			}
		}
	}

	BassLigne(IdentityInstrument idInst, List<EnumTempsForts> tempsFortTension, EnumVelocity velocity,
			EnumVelocityVariation velocityVariation) throws MyException {

		this.idInstrument = idInst;
		this.tempsFortTension = tempsFortTension;
		this.size = this.idInstrument.getChordsList().size();
		this.velocity = velocity;
		this.velocityVariation = velocityVariation;

		while (true) {
			try {
				this.listNotes = new ArrayList<>();
				generateBass();
				break;
			} catch (MyException e) {
				if (++count == maxTries)
					throw e;
			}
		}
	}

	BassLigne(BassLigne b) {
		this.listNotes = b.listNotes;
		this.size = b.size;
		this.placementMotifsMelody = b.placementMotifsMelody;
		this.style = b.style;
		this.tempsFortTension = b.tempsFortTension;
		this.velocity = b.velocity;
		this.velocityVariation = b.velocityVariation;
		this.idInstrument = b.idInstrument;
	}

	private void generateBass() throws MyException {

		Map<Integer, Integer> mapOfDegres = new HashMap<>();
		List<Map<Integer, Integer>> listOctaveDegres = new LinkedList<>();
		Note previousNote = null;
		Chord currentChord;
		EnumChordTension currentTension;
		int index = 0;
		int volumeRef;
		int velocityDifRef;
		int volume;
		boolean lastChord = false;
		boolean silence = false;
		boolean play = false;
		int endPart = this.size * this.idInstrument.getChordDuration().getDuration() * 24; // 24 beat by black note

		// TODO add some variation in the bass at importants time
		for (Map<Chord, EnumChordTension> entry : this.idInstrument.getChordsList()) {

			// get the first element of map
			currentChord = entry.entrySet().iterator().next().getKey();
			currentTension = entry.entrySet().iterator().next().getValue();
			index = 0;

			if (this.idInstrument.getChordsList().indexOf(entry) == (this.size - 1)) {
				lastChord = true;
			}

			if (!lastChord) {

				for (int i = 0; i < this.idInstrument.getChordDuration().getDuration(); i = i
						+ this.idInstrument.getRepetedItem().get(EnumBrickSentence.REPETEDITEM1).get(0).getSize()) {

					volumeRef = this.estimateVelocity(previousNote, endPart);
					velocityDifRef = volumeRef - this.idInstrument.getRepetedItem().get(EnumBrickSentence.REPETEDITEM1)
							.get(0).getListNotes().get(0).getVolume();

					for (Note not : this.idInstrument.getRepetedItem().get(EnumBrickSentence.REPETEDITEM1).get(0)
							.getListNotes()) {

						if (not.getSilenceOrPlaying().equals(EnumSilence.SILENCE)) {
							silence = true;
							play = false;
						} else {
							silence = false;
							play = true;
						}

						volume = not.getVolume() + velocityDifRef;

						listOctaveDegres = getDegresAndOctaves(index, not, this.idInstrument.getRepetedItem()
								.get(EnumBrickSentence.REPETEDITEM1).get(0).getListNotes());

						previousNote = new Note(previousNote, null, this.idInstrument.getNoteRef(), currentChord,
								this.idInstrument.getTonality(), this.idInstrument.getAllowedRythme(),
								this.idInstrument.getGapBetweenNotes(), this.idInstrument.getStyle(),
								this.idInstrument.getInstrument(), this.idInstrument.getPourcentageSilences(),
								this.idInstrument.getChordDuration(), this.idInstrument.getTempsForts(),
								not.getNoteDuration().get(0), this.idInstrument.getSentence(), endPart, lastChord,
								silence, play, listOctaveDegres, currentTension, tempsFortTension,
								this.idInstrument.getNumberNotesPlayedSimultaneously(), volume,
								this.idInstrument.getStyleGroupIns());
						this.listNotes.add(previousNote);

						index++;
					}

					index = 0;
				}

			} else {

				mapOfDegres.clear();
				listOctaveDegres.clear();

				mapOfDegres.put(0, -1);
				listOctaveDegres.add(mapOfDegres);

				currentChord = getChordByCursorOfNote(previousNote);
				currentTension = getTensionByCursorOfNote(previousNote);
				previousNote = new Note(previousNote, null, this.idInstrument.getNoteRef(), currentChord,
						this.idInstrument.getTonality(), this.idInstrument.getAllowedRythme(),
						this.idInstrument.getGapBetweenNotes(), this.idInstrument.getStyle(),
						this.idInstrument.getInstrument(), this.idInstrument.getPourcentageSilences(),
						this.idInstrument.getChordDuration(), this.idInstrument.getTempsForts(), null,
						this.idInstrument.getSentence(), endPart, lastChord, silence, play, listOctaveDegres,
						currentTension, tempsFortTension, this.idInstrument.getNumberNotesPlayedSimultaneously(), -1,
						this.idInstrument.getStyleGroupIns());

				this.listNotes.add(previousNote);

				while (previousNote.getCursor() < endPart) {
					List<EnumDurationNote> noteDuration = new LinkedList<>();
					noteDuration.add(EnumDurationNote.getLongestNoteByDuration(endPart - previousNote.getCursor()));
					previousNote = new Note(previousNote, previousNote.getListNoteNum(), noteDuration,
							EnumSilence.SILENCE, this.idInstrument.getChordDuration(), currentChord, 0);
					this.listNotes.add(previousNote);
				}
			}
		}

	}

	public void generateBassLigneWithRef(BassLigne bass, Map<EnumDurationNote, Integer> allowedRythmesOfRefInstrument)
			throws MyException {

		Note previousNote = null;
		int index = 0;
		int dev;
		boolean silence;
		boolean play;
		Chord currentChord;
		List<Map<Integer, Integer>> listOctaveDegres;
		Map<EnumDurationNote, Integer> rythmsInstrumentCanPlay = this.idInstrument
				.removeRythmInstrumentCantPlay(allowedRythmesOfRefInstrument);
		int endPart = this.size * this.idInstrument.getChordDuration().getDuration() * 24;

		// choose randomly if the instrument will play tierce, quinte or the same line
		// as the ref
		// +1 : tierce, +2 : quinte, 0 same
		dev = RANDOM.nextInt(3);

		for (Note not : bass.getListNotes()) {

			if (not.getSilenceOrPlaying().equals(EnumSilence.SILENCE)) {
				silence = true;
				play = false;
			} else {
				silence = false;
				play = true;
			}

			currentChord = getChordByCursorOfNote(previousNote);

			listOctaveDegres = getDegresAndOctaves(index, not, bass.getListNotes(), currentChord, dev);

			previousNote = new Note(previousNote, null, this.idInstrument.getNoteRef(), currentChord,
					this.idInstrument.getTonality(), rythmsInstrumentCanPlay, this.idInstrument.getGapBetweenNotes(),
					this.idInstrument.getStyle(), this.idInstrument.getInstrument(),
					this.idInstrument.getPourcentageSilences(), this.idInstrument.getChordDuration(),
					this.idInstrument.getTempsForts(), not.getNoteDuration().get(0), this.idInstrument.getSentence(),
					endPart, false, silence, play, listOctaveDegres, null, null,
					this.idInstrument.getNumberNotesPlayedSimultaneously(), not.getVolume(),
					this.idInstrument.getStyleGroupIns());
			this.listNotes.add(previousNote);

			index++;
		}

	}

	private int estimateVelocity(Note previousNote, int endPart) {

		double ratio = 0;
		int velocityStart;
		int velocityEnd;

		velocityStart = this.velocity.getValue();
		velocityEnd = this.velocity.getVelocityAfterVariation(this.velocityVariation).getValue();

		if (previousNote != null) {
			ratio = (double) previousNote.getCursor() / (double) endPart;
		}

		return (int) (ratio * (velocityEnd - velocityStart) + velocityStart);
	}

	private List<Map<Integer, Integer>> getDegresAndOctaves(int index, Note not, List<Note> listNotes) {

		int degre;
		int octave;
		Map<Integer, Integer> mapOfDegres = new HashMap<>();
		List<Map<Integer, Integer>> listOctaveDegres = new LinkedList<>();

		for (EnumNoteTonality n : not.getListNoteNum()) {

			mapOfDegres.clear();

			if (index > 0) {
				// difference octave entre nouvelle note et premiere note precedente de la liste
				octave = n.getOctave().getIndex()
						- listNotes.get(index - 1).getListNoteNum().get(0).getOctave().getIndex();
			} else {
				octave = 0;
			}
			degre = this.idInstrument.getTonality().getDegreOfNoteInChord(n.getNote(), this.idInstrument.getRefChord());

			mapOfDegres.put(octave, degre);
			listOctaveDegres.add(mapOfDegres);
		}

		return listOctaveDegres;
	}

	private List<Map<Integer, Integer>> getDegresAndOctaves(int index, Note not, List<Note> listNotes,
			Chord currentChord, int dev) {

		int degre;
		int octave;
		EnumNote note;
		Map<Integer, Integer> mapOfDegres = new HashMap<>();
		List<Map<Integer, Integer>> listOctaveDegres = new LinkedList<>();

		for (EnumNoteTonality n : not.getListNoteNum()) {

			note = this.idInstrument.getTonality().getNoteWithDeviation(n, dev, currentChord);
			mapOfDegres.clear();

			if (index > 0) {
				// difference octave entre nouvelle note et premiere note precedente de la liste
				octave = n.getOctave().getIndex()
						- listNotes.get(index - 1).getListNoteNum().get(0).getOctave().getIndex();
			} else {
				octave = 0;
			}
			degre = this.idInstrument.getTonality().getDegreOfNoteInChord(note, currentChord);

			mapOfDegres.put(octave, degre);
			listOctaveDegres.add(mapOfDegres);
		}

		return listOctaveDegres;
	}

	private Chord getChordByCursorOfNote(Note note) {

		int index = 0;

		if (note != null) {
			index = note.getCursor() / (this.idInstrument.getChordDuration().getDuration() * 24);
		}

		return this.idInstrument.getChordsList().get(index).entrySet().iterator().next().getKey();
	}

	private EnumChordTension getTensionByCursorOfNote(Note note) {

		int index = 0;

		if (note != null) {
			index = note.getCursor() / (this.idInstrument.getChordDuration().getDuration() * 24);
		}

		return this.idInstrument.getChordsList().get(index).entrySet().iterator().next().getValue();
	}

	public List<EnumTempsForts> getTempsFortTension() {
		return this.tempsFortTension;
	}

	public List<Note> getListNotes() {
		return this.listNotes;
	}

	public EnumVelocity getVelocity() {
		return velocity;
	}

	public void setVelocity(EnumVelocity velocity) {
		this.velocity = velocity;
	}

	public EnumVelocityVariation getVelocityVariation() {
		return velocityVariation;
	}

	public void setVelocityVariation(EnumVelocityVariation velocityVariation) {
		this.velocityVariation = velocityVariation;
	}

}
