package musicRandom;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

import enums.EnumBrickSentence;
import enums.EnumChordTension;
import enums.EnumDurationNote;
import enums.EnumNote;
import enums.EnumNoteTonality;
import enums.EnumSilence;
import enums.EnumStyle;
import enums.EnumTempsForts;
import enums.EnumVelocity;
import enums.EnumVelocityVariation;
import exceptions.MyException;

public class Melody {

	private List<Note> listNotes;
	private int size;
	private Map<Integer, Motif> placementMotifsMelody;
	private EnumStyle style; // TODO
	private List<EnumTempsForts> tempsFortTension;
	private EnumVelocity velocity;
	private EnumVelocityVariation velocityVariation;

	private IdentityInstrument idInstrument;

	int count = 0;
	int maxTries = 10;

	private Random random = new Random();

	Melody(IdentityInstrument idInst, Melody melRef, Map<EnumDurationNote, Integer> allowedRythmesOfRefInstrument)
			throws MyException {

		this.idInstrument = idInst;
		this.tempsFortTension = melRef.getTempsFortTension();
		this.size = this.idInstrument.getChordsList().size();

		while (true) {
			try {
				this.listNotes = new ArrayList<>();
				generateMelodyWithRef(melRef, allowedRythmesOfRefInstrument);
				break;
			} catch (MyException e) {
				if (++count == maxTries)
					throw e;
			}
		}

	}

	Melody(IdentityInstrument idInst, List<EnumTempsForts> tempsFortTension, EnumVelocity velocity,
			EnumVelocityVariation velocityVariation) throws MyException {

		this.idInstrument = idInst;
		this.tempsFortTension = tempsFortTension;
		this.size = this.idInstrument.getChordsList().size();
		this.velocity = velocity;
		this.velocityVariation = velocityVariation;

		while (true) {
			try {
				this.listNotes = new ArrayList<>();
				generatePlacementMotifs();
				generateMelody();
				break;
			} catch (MyException e) {
				if (++count == maxTries)
					throw e;
			}
		}
	}

	public Melody(IdentityInstrument identityInstrument, List<EnumTempsForts> tempsFortTension, EnumVelocity velocity,
			EnumVelocityVariation velocityVariation, Map<Integer, Motif> placementMotifsMelody) throws MyException {

		this.idInstrument = identityInstrument;
		this.tempsFortTension = tempsFortTension;
		this.size = this.idInstrument.getChordsList().size();
		this.velocity = velocity;
		this.velocityVariation = velocityVariation;
		generatePlacementMotifsWithRef(placementMotifsMelody);

		while (true) {
			try {
				this.listNotes = new ArrayList<>();
				generateMelody();
				break;
			} catch (MyException e) {
				if (++count == maxTries)
					throw e;
			}
		}
	}

	Melody(Melody m) {
		this.listNotes = m.listNotes;
		this.size = m.size;
		this.placementMotifsMelody = m.placementMotifsMelody;
		this.style = m.style;
		this.tempsFortTension = m.tempsFortTension;
		this.velocity = m.velocity;
		this.velocityVariation = m.velocityVariation;
	}

	private void generateMelody() throws MyException {

		Map<Integer, Integer> mapOfDegres = new HashMap<>();
		List<Map<Integer, Integer>> listOctaveDegres = new LinkedList<>();
		Note previousNote = null;
		Chord currentChord;
		EnumChordTension currentTension;
		int chordIndex;
		int timeUntilNextSmallMotif;
		int volume;
		int volumeRef;
		int velocityDifRef;
		int index = 0;
		int endPart = this.size * this.idInstrument.getChordDuration().getDuration() * 24; // 24 beat by black note
		boolean lastChord = false;
		boolean silence = false;
		boolean play = false;

		do {

			if (previousNote != null) {
				chordIndex = previousNote.getCursor() / (this.idInstrument.getChordDuration().getDuration() * 24);
			} else {
				chordIndex = 0;
			}

			timeUntilNextSmallMotif = getTimeUntilNextMotif(this.placementMotifsMelody, chordIndex, endPart);

			// TODO : change that,the last chord isn't silent in all cases
			if (lastChord) {
				silence = true;
			}

			lastChord = chordIndex + 1 == this.idInstrument.getChordsList().size();

			if (this.placementMotifsMelody.containsKey(chordIndex)) {

				index = 0;
				volumeRef = this.estimateVelocity(previousNote, endPart);
				velocityDifRef = 0;

				for (Note a : this.placementMotifsMelody.get(chordIndex).getListNotes()) {

					if (index == 0) {
						velocityDifRef = volumeRef - a.getVolume();
					}

					currentChord = getChordByCursorOfNote(previousNote);
					currentTension = getTensionByCursorOfNote(previousNote);

					if (a.getSilenceOrPlaying().equals(EnumSilence.SILENCE)) {
						silence = true;
						play = false;
					} else {
						silence = false;
						play = true;
					}

					if (this.placementMotifsMelody.get(chordIndex).isSameNotes()) {
						listOctaveDegres = getDegresAndOctaves(index, a,
								this.placementMotifsMelody.get(chordIndex).getListNotes(),
								this.idInstrument.getRefChord(), 0);
					} else {
						mapOfDegres.clear();
						listOctaveDegres.clear();

						mapOfDegres.put(0, -1);
						listOctaveDegres.add(mapOfDegres);
					}

					volume = a.getVolume() + velocityDifRef;

					previousNote = new Note(previousNote, null, this.idInstrument.getNoteRef(), currentChord,
							this.idInstrument.getTonality(), this.idInstrument.getAllowedRythme(),
							this.idInstrument.getGapBetweenNotes(), this.idInstrument.getStyle(),
							this.idInstrument.getInstrument(), this.idInstrument.getPourcentageSilences(),
							this.idInstrument.getChordDuration(), this.idInstrument.getTempsForts(),
							a.getNoteDuration().get(0), this.idInstrument.getSentence(), endPart, lastChord, silence,
							play, listOctaveDegres, currentTension, tempsFortTension,
							this.idInstrument.getNumberNotesPlayedSimultaneously(), volume,
							this.idInstrument.getStyleGroupIns());
					this.listNotes.add(previousNote);

					index++;
				}
			} else {

				mapOfDegres.clear();
				listOctaveDegres.clear();

				mapOfDegres.put(0, -1);
				listOctaveDegres.add(mapOfDegres);

				volume = this.estimateVelocity(previousNote, endPart);

				currentChord = getChordByCursorOfNote(previousNote);
				currentTension = getTensionByCursorOfNote(previousNote);
				previousNote = new Note(previousNote, null, this.idInstrument.getNoteRef(), currentChord,
						this.idInstrument.getTonality(), this.idInstrument.getAllowedRythme(),
						this.idInstrument.getGapBetweenNotes(), this.idInstrument.getStyle(),
						this.idInstrument.getInstrument(), this.idInstrument.getPourcentageSilences(),
						this.idInstrument.getChordDuration(), this.idInstrument.getTempsForts(), null,
						this.idInstrument.getSentence(), timeUntilNextSmallMotif, lastChord, silence, play,
						listOctaveDegres, currentTension, tempsFortTension,
						this.idInstrument.getNumberNotesPlayedSimultaneously(), volume,
						this.idInstrument.getStyleGroupIns());
				this.listNotes.add(previousNote);
			}

		} while (previousNote.getCursor() < endPart);
	}

	public void generateMelodyWithRef(Melody mel, Map<EnumDurationNote, Integer> allowedRythmesOfRefInstrument)
			throws MyException {

		Note previousNote = null;
		int dev;
		int speed;
		int index = 0;
		int noteDurationReplaced = 0;
		int endPart = this.size * this.idInstrument.getChordDuration().getDuration() * 24;
		boolean silence;
		boolean play;
		Chord currentChord;
		List<Map<Integer, Integer>> listOctaveDegres;
		Map<EnumDurationNote, Integer> rythmsInstrumentCanPlay = this.idInstrument
				.removeRythmInstrumentCantPlay(allowedRythmesOfRefInstrument);

		// choose randomly if the instrument will play tierce, quinte or the same line
		// as the ref
		// +1 : tierce, +2 : quinte, 0 same
		dev = random.nextInt(3);

		// choose randomly if the instrument will play 2X plus or 2X less or same speed
		// as the ref
		// -1 : 2X less, 0 : same, +1 2X plus
		speed = random.nextInt(3) - 1;

		for (Note not : mel.getListNotes()) {

			noteDurationReplaced = noteDurationReplaced + not.getNoteDuration().get(0).getDuration();
			EnumDurationNote durationNoteR = EnumDurationNote.getDurationNoteByClosestDuration(noteDurationReplaced);

			// on joue si noteDurationReplaced est authoris� ou bien si il ne l'est pas
			// mais que la prochaine note l'est
			if (this.idInstrument.getAllowedRythme().containsKey(durationNoteR)
					&& this.idInstrument.getAllowedRythme().get(durationNoteR) != 0
					|| (!this.idInstrument.getAllowedRythme().containsKey(durationNoteR)
							|| this.idInstrument.getAllowedRythme().containsKey(durationNoteR)
									&& this.idInstrument.getAllowedRythme().get(durationNoteR) == 0)
							&& (mel.getListNotes().size() >= index + 1 || mel.getListNotes().size() < index + 1
									&& this.idInstrument.getAllowedRythme()
											.containsKey(mel.getListNotes().get(index + 1).getNoteDuration().get(0))
									&& this.idInstrument.getAllowedRythme()
											.get(mel.getListNotes().get(index + 1).getNoteDuration().get(0)) != 0)) {

				if (not.getSilenceOrPlaying().equals(EnumSilence.SILENCE)) {
					silence = true;
					play = false;
				} else {
					silence = false;
					play = true;
				}

				currentChord = getChordByCursorOfNote(previousNote);

				listOctaveDegres = getDegresAndOctaves(index, not, mel.getListNotes(), currentChord, dev);
				previousNote = new Note(previousNote, null, this.idInstrument.getNoteRef(), currentChord,
						this.idInstrument.getTonality(), rythmsInstrumentCanPlay,
						this.idInstrument.getGapBetweenNotes(), this.idInstrument.getStyle(),
						this.idInstrument.getInstrument(), this.idInstrument.getPourcentageSilences(),
						this.idInstrument.getChordDuration(), this.idInstrument.getTempsForts(),
						not.getNoteDuration().get(0), this.idInstrument.getSentence(), endPart, false, silence, play,
						listOctaveDegres, null, null, this.idInstrument.getNumberNotesPlayedSimultaneously(),
						not.getVolume(), this.idInstrument.getStyleGroupIns());
				this.listNotes.add(previousNote);

				noteDurationReplaced = 0;
			}

			index++;
		}

	}

	private int estimateVelocity(Note previousNote, int endPart) {

		double ratio = 0;
		int velocityStart = this.velocity.getValue();
		int velocityEnd = this.velocity.getVelocityAfterVariation(this.velocityVariation).getValue();

		if (previousNote != null) {
			ratio = (double) previousNote.getCursor() / (double) endPart;
		}

		return (int) (ratio * (velocityEnd - velocityStart) + velocityStart);
	}

	private List<Map<Integer, Integer>> getDegresAndOctaves(int index, Note not, List<Note> listNotes,
			Chord currentChord, int dev) {

		int degre;
		int octave;
		EnumNote note;
		Map<Integer, Integer> mapOfDegres = new HashMap<>();
		// linkedList to keep the order
		List<Map<Integer, Integer>> listOctaveDegres = new LinkedList<>();

		for (EnumNoteTonality n : not.getListNoteNum()) {

			note = this.idInstrument.getTonality().getNoteWithDeviation(n, dev);
			mapOfDegres.clear();

			if (index > 0) {
				// difference octave entre nouvelle note et premiere note precedente de la liste
				octave = n.getOctave().getIndex()
						- listNotes.get(index - 1).getListNoteNum().get(0).getOctave().getIndex();
			} else {
				octave = 0;
			}
			degre = this.idInstrument.getTonality().getDegreOfNoteInChord(note, currentChord);

			mapOfDegres.put(octave, degre);
			listOctaveDegres.add(mapOfDegres);
		}

		return listOctaveDegres;
	}

	private int getTimeUntilNextMotif(Map<Integer, Motif> smallMotifStarts, int chordIndex, int endPart) {

		for (Map.Entry<Integer, Motif> entry : smallMotifStarts.entrySet()) {

			if (entry.getKey() > chordIndex) {
				return entry.getKey() * this.idInstrument.getChordDuration().getDuration() * 24;
			}
		}

		return endPart;

	}

	private Chord getChordByCursorOfNote(Note note) {

		int index = 0;

		if (note != null) {
			index = note.getCursor() / (this.idInstrument.getChordDuration().getDuration() * 24);
		}

		return this.idInstrument.getChordsList().get(index).entrySet().iterator().next().getKey();
	}

	private EnumChordTension getTensionByCursorOfNote(Note note) {

		int index = 0;

		if (note != null) {
			index = note.getCursor() / (this.idInstrument.getChordDuration().getDuration() * 24);
		}

		return this.idInstrument.getChordsList().get(index).entrySet().iterator().next().getValue();
	}

	private void generatePlacementMotifs() {

		int numberOfMotifs;
		int sizeMotif = 0;
		int a;
		int maxMotifs;

		List<Motif> listFinalMotif = generateFinalMotif();

		// use of treeMap in order to sort by key automatically
		this.placementMotifsMelody = new TreeMap<>();

		for (Motif m : listFinalMotif) {
			sizeMotif = sizeMotif + m.getSize();
		}

		// choice of how many times the small motif will be in the melody
		// TODO : must depend on style and other parameters
		maxMotifs = this.idInstrument.getChordsList().size() / sizeMotif;
		numberOfMotifs = random.nextInt(maxMotifs - 1) + 1;

		int nMotif = 0;

		do {
			nMotif = random.nextInt(maxMotifs) * sizeMotif;

			if (!this.placementMotifsMelody.containsKey(nMotif)) {

				a = 0;
				for (Motif m : listFinalMotif) {
					this.placementMotifsMelody.put(nMotif + a, m);
					a = a + m.getSize();
				}
			}
		} while (this.placementMotifsMelody.size() < numberOfMotifs * listFinalMotif.size());

	}

	private void generatePlacementMotifsWithRef(Map<Integer, Motif> placementMotifsMel) {

		EnumBrickSentence brick;
		Motif motif;

		// use of treeMap in order to sort by key automatically
		this.placementMotifsMelody = new TreeMap<>();

		for (Map.Entry<Integer, Motif> entry : placementMotifsMel.entrySet()) {

			brick = entry.getValue().getBrick();
			motif = this.idInstrument.getMotifs().get(brick).get(0);

			this.placementMotifsMelody.put(entry.getKey(), motif);
		}

	}

	private List<Motif> generateFinalMotif() {

		Motif m;
		int sizeTest = 0;
		int j;
		int newNumber;
		List<Integer> numberOfMotifs;
		List<Motif> listFinalMotif = new ArrayList<>();

		do {
			sizeTest = 0;
			numberOfMotifs = new ArrayList<>();

			newNumber = random.nextInt(3);
			numberOfMotifs.add(newNumber);
			sizeTest = sizeTest
					+ newNumber * this.idInstrument.getMotifs().get(EnumBrickSentence.MOTIF1).get(0).getSize();

			newNumber = random.nextInt(3);
			numberOfMotifs.add(newNumber);
			sizeTest = sizeTest
					+ newNumber * this.idInstrument.getMotifs().get(EnumBrickSentence.MOTIF2).get(0).getSize();

		} while (sizeTest == 0 || sizeTest > (this.idInstrument.getChordsList().size() / 2)); // check if we can at
																								// least place 2 times
																								// ths motif in the
																								// sentence

		for (j = 0; j < numberOfMotifs.get(0); j++) {
			m = this.idInstrument.getMotifs().get(EnumBrickSentence.MOTIF1).get(0);
			listFinalMotif.add(m);
		}
		for (j = 0; j < numberOfMotifs.get(1); j++) {
			m = this.idInstrument.getMotifs().get(EnumBrickSentence.MOTIF2).get(0);
			listFinalMotif.add(m);
		}

		return listFinalMotif;
	}

	public List<EnumTempsForts> getTempsFortTension() {
		return this.tempsFortTension;
	}

	public List<Note> getListNotes() {
		return this.listNotes;
	}

	public EnumVelocity getVelocity() {
		return velocity;
	}

	public void setVelocity(EnumVelocity velocity) {
		this.velocity = velocity;
	}

	public EnumVelocityVariation getVelocityVariation() {
		return velocityVariation;
	}

	public void setVelocityVariation(EnumVelocityVariation velocityVariation) {
		this.velocityVariation = velocityVariation;
	}

	public Map<Integer, Motif> getPlacementMotifsMelody() {
		return this.placementMotifsMelody;
	}

}
