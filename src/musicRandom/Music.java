package musicRandom;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import enums.EnumDurationChord;
import enums.EnumInstrumentCategories;
import enums.EnumInstruments;
import enums.EnumPart;
import enums.EnumTempsForts;
import enums.EnumTonalite;
import exceptions.MyException;
import styles.Style;
import styles.StyleGroupDrums;
import styles.StyleGroupIns;

public class Music {

	private EnumTonalite tonality;
	private int rythme;
	private int numberParts;
	private EnumDurationChord accordsDuration;
	private Map<EnumInstruments, StyleGroupIns> allInstruments;
	private Map<EnumInstruments, StyleGroupDrums> allDrums;
	private List<Instrument> instruments;
	private int numberBat;
	private List<Part> listParts;
	private List<EnumTempsForts> tempsForts;
	private Style musicStyle;
	private List<IdentityInstrument> listIdentityInstruments;
	private List<IdentityDrum> listIdentityDrum;

	private static Random random = new Random();

	// just to generate paramaters by style
	public Music(Style mStyle) {
		this.musicStyle = mStyle;
		this.rythme = mStyle.getRythm();
		this.chooseAllInstrumentsByStyle();
		this.tonality = this.getRandomTonality();
		this.numberBat = this.musicStyle.getNumberPercussion();
	}

	public Music(EnumTonalite Mtonalite, int MnbreParts, EnumDurationChord Mduree_accords,
			Map<EnumInstruments, StyleGroupIns> aInstruments, List<EnumInstruments> instruments, int Mnbre_bat,
			List<EnumTempsForts> MtempsForts, Style mStyle, int swing) throws MyException {
		this.tonality = Mtonalite;
		this.numberParts = MnbreParts;
		this.allInstruments = aInstruments;
		this.numberBat = Mnbre_bat;
		this.musicStyle = mStyle;
		this.rythme = mStyle.getRythm();
		this.allDrums = new EnumMap<>(EnumInstruments.class);

		if (this.tonality == null) {
			this.tonality = getRandomTonality();
		}
		if (this.numberParts == -1) {
			this.numberParts = random.nextInt(2) + 2;
		}

		this.accordsDuration = this.musicStyle.getMesureTempsFort().getMesure();
		this.tempsForts = EnumTempsForts.getFromStyle(this.musicStyle.getMesureTempsFort());

		if (this.numberBat == -1) {
			this.numberBat = this.musicStyle.getNumberPercussion();
		}
		if (this.allInstruments == null && instruments == null) {
			chooseAllInstrumentsByStyle();
		} else if (instruments != null) {
			affectAllInstrumentsToStyle(instruments);
		}

		if (swing != -1) {
			this.ajustSwing(swing);
		}

		// assign new list of StyleGroupInstrument after changing it assigning the
		// instruments
		this.musicStyle.assignNewStyleGroupIns(this.allInstruments);
		this.musicStyle.generateAllowedRythms(this.accordsDuration);

		createIdentitiesInstruments();
		createIdentitiesDrums();

		this.listParts = createParts();

		createListInstruments();
	}

	private void ajustSwing(int swing) {
		int ran;
		int sign;
		int averageSwing;

		List<StyleGroupIns> allStyleGroupIns = new ArrayList<>();

		for (Entry<EnumInstruments, StyleGroupIns> entry : this.allInstruments.entrySet()) {
			allStyleGroupIns.add(entry.getValue());
		}

		averageSwing = this.averageSwing(allStyleGroupIns);

		do {
			ran = random.nextInt(this.allInstruments.size());
			sign = (int) Math.signum(swing - averageSwing);

			allStyleGroupIns.get(ran).ajustSwing(sign);

			averageSwing = this.averageSwing(allStyleGroupIns);
		} while (Math.abs(swing - averageSwing) > 1);
	}

	private int averageSwing(List<StyleGroupIns> allStyleGroupIns) {

		int swingLocal = 0;

		for (StyleGroupIns sgIns : allStyleGroupIns) {
			swingLocal = swingLocal + sgIns.getPourcentageSil() + sgIns.getpSyncopes() + sgIns.getpSyncopettes()
					+ sgIns.getpSyncopesTempsForts() + sgIns.getpContreTemps() + sgIns.getpDissonance()
					+ sgIns.getpIrregularites() + sgIns.getpChanceSupSameRythm();
		}

		return swingLocal / (this.allInstruments.size() * 8);
	}

	private EnumTonalite getRandomTonality() {

		List<EnumTonalite> allChoices = new ArrayList<>();
		int nombre;

		for (EnumTonalite ton : this.musicStyle.getTonalities()) {

			nombre = 8 - Math.abs(ton.getMidiIndex());
			for (int i = 0; i < nombre; i++) {
				allChoices.add(ton);
			}
		}

		return allChoices.get(random.nextInt(allChoices.size()));
	}

	public String getListInstrumentsAsString() {
		String r = null;

		for (EnumInstruments ins : this.allInstruments.keySet()) {
			if (ins.isNotBassInstrument()) {
				if (r == null || r.isEmpty()) {
					r = ins.getName();
				} else {
					r = r + ", " + ins.getName();
				}
			}
		}

		return r;
	}

	private void createIdentitiesInstruments() {

		int rand;
		int refNumber = 0;
		EnumInstruments ins;
		StyleGroupIns styleGroupIns;
		IdentityInstrument idIns;

		this.listIdentityInstruments = new ArrayList<IdentityInstrument>();

		for (Map.Entry<EnumInstruments, StyleGroupIns> entry : this.allInstruments.entrySet()) {

			ins = entry.getKey();
			styleGroupIns = entry.getValue();

			if (ins.getCategory().equals(EnumInstrumentCategories.MEDIUM)) {

				rand = random.nextInt(101);
				if (rand > 80 && rand < 90) {
					idIns = new IdentityInstrument(null, null, null, null, null, 0, 0, null, this.listParts,
							this.tonality, accordsDuration, this.tempsForts, this.rythme, ins,
							EnumInstrumentCategories.HIGHPITCHED, refNumber, -1, this.musicStyle, styleGroupIns);
					this.listIdentityInstruments.add(idIns);
					refNumber++;
				} else if (rand > 90) {
					idIns = new IdentityInstrument(null, null, null, null, null, 0, 0, null, this.listParts,
							this.tonality, accordsDuration, this.tempsForts, this.rythme, ins,
							EnumInstrumentCategories.BASS, refNumber, -1, this.musicStyle, styleGroupIns);
					this.listIdentityInstruments.add(idIns);
					refNumber++;
				} else {
					idIns = new IdentityInstrument(null, null, null, null, null, 0, 0, null, this.listParts,
							this.tonality, accordsDuration, this.tempsForts, this.rythme, ins,
							EnumInstrumentCategories.MEDIUM, refNumber, -1, this.musicStyle, styleGroupIns);
					this.listIdentityInstruments.add(idIns);
					refNumber++;
				}
			} else if (ins.getCategory().equals(EnumInstrumentCategories.BASS)) {

				rand = random.nextInt(101);
				if (rand > 80) {
					idIns = new IdentityInstrument(null, null, null, null, null, 0, 0, null, this.listParts,
							this.tonality, accordsDuration, this.tempsForts, this.rythme, ins,
							EnumInstrumentCategories.MEDIUM, refNumber, -1, this.musicStyle, styleGroupIns);
					this.listIdentityInstruments.add(idIns);
					refNumber++;
				} else {
					idIns = new IdentityInstrument(null, null, null, null, null, 0, 0, null, this.listParts,
							this.tonality, accordsDuration, this.tempsForts, this.rythme, ins,
							EnumInstrumentCategories.BASS, refNumber, -1, this.musicStyle, styleGroupIns);
					this.listIdentityInstruments.add(idIns);
					refNumber++;
				}
			} else if (ins.getCategory().equals(EnumInstrumentCategories.HIGHPITCHED)) {

				rand = random.nextInt(101);
				if (rand > 80) {
					idIns = new IdentityInstrument(null, null, null, null, null, 0, 0, null, this.listParts,
							this.tonality, accordsDuration, this.tempsForts, this.rythme, ins,
							EnumInstrumentCategories.MEDIUM, refNumber, -1, this.musicStyle, styleGroupIns);
					this.listIdentityInstruments.add(idIns);
					refNumber++;
				} else {
					idIns = new IdentityInstrument(null, null, null, null, null, 0, 0, null, this.listParts,
							this.tonality, accordsDuration, this.tempsForts, this.rythme, ins,
							EnumInstrumentCategories.HIGHPITCHED, refNumber, -1, this.musicStyle, styleGroupIns);
					this.listIdentityInstruments.add(idIns);
					refNumber++;
				}
			} else {
				idIns = new IdentityInstrument(null, null, null, null, null, 0, 0, null, this.listParts, this.tonality,
						accordsDuration, this.tempsForts, this.rythme, ins, EnumInstrumentCategories.PERCUSSION,
						refNumber, -1, this.musicStyle, styleGroupIns);
				this.listIdentityInstruments.add(idIns);
				refNumber++;
			}
		}
	}

	private void createIdentitiesDrums() {

		EnumInstruments ins;
		StyleGroupDrums styleGroupDrums;

		this.listIdentityDrum = new ArrayList<>();

		for (Map.Entry<EnumInstruments, StyleGroupDrums> entry : this.allDrums.entrySet()) {

			ins = entry.getKey();
			styleGroupDrums = entry.getValue();

			IdentityDrum idDrum = new IdentityDrum(this.listParts, this.tonality, accordsDuration, this.tempsForts,
					this.rythme, ins, EnumInstrumentCategories.PERCUSSION, this.musicStyle, styleGroupDrums);
			this.listIdentityDrum.add(idDrum);
		}
	}

	public List<EnumTempsForts> getTempsForts() {
		return this.tempsForts;
	}

	public EnumTonalite getTonality() {
		return this.tonality;
	}

	public List<Instrument> getListInstruments() {
		return this.instruments;
	}

	private void createListInstruments() {

		this.instruments = new ArrayList<>();

		for (IdentityInstrument idIns : this.listIdentityInstruments) {
			Instrument inst = new Instrument(idIns.getInstrument(), this.listParts, idIns, null, this.tonality,
					this.rythme, null);
			this.instruments.add(inst);
		}

		for (IdentityDrum idDrum : this.listIdentityDrum) {
			Instrument inst = new Instrument(idDrum.getInstrument(), this.listParts, null, idDrum, this.tonality,
					this.rythme, null);
			this.instruments.add(inst);
		}
	}

	private void affectAllInstrumentsToStyle(List<EnumInstruments> instrus) {

		int numberInst = this.musicStyle.getNumberInstrument();
		StyleGroupIns sIns;
		int ran;
		int i;

		this.allInstruments = new EnumMap<>(EnumInstruments.class);
		this.allDrums = new EnumMap<>(EnumInstruments.class);
		List<StyleGroupDrums> styleGroupInsPercussion = null;
		List<StyleGroupIns> styleGroupIns = this.getListStyleGroupInsWithoutBass();
		List<Integer> stylesAvailable;
		List<EnumInstruments> temp;
		StyleGroupIns tempStyleGroupIns;
		StyleGroupDrums tempStyleGroupDrums;

		for (EnumInstruments ins : instrus) {

			stylesAvailable = new ArrayList<>();
			for (i = 0; i < styleGroupIns.size(); i++) {
				sIns = styleGroupIns.get(i);
				if (sIns.getInstruments().contains(ins)) {
					stylesAvailable.add(i);
				}
			}

			if (stylesAvailable.isEmpty()) {
				ran = random.nextInt(styleGroupIns.size());
			} else {
				ran = stylesAvailable.get(random.nextInt(stylesAvailable.size()));
			}
			sIns = styleGroupIns.get(ran);

			// randomize style for each instrument even if they share the same
			this.allInstruments.put(ins, new StyleGroupIns(sIns));

			if (ins.getBassInstrument() != null) {
				StyleGroupIns bassStyleGroupIns = findStyleGroupMotherId(sIns);

				if (bassStyleGroupIns != null) {
					this.allInstruments.put(ins.getBassInstrument(), new StyleGroupIns(bassStyleGroupIns));
					numberInst++;
				} else {
					this.allInstruments.put(ins.getBassInstrument(), new StyleGroupIns(sIns));
					numberInst++;
				}
			}
		}

		i = 0;

		while (this.allInstruments.size() < numberInst) {

			if (i < styleGroupIns.size()) {
				tempStyleGroupIns = styleGroupIns.get(i);
				temp = tempStyleGroupIns.getInstruments();
			} else {
				temp = EnumInstruments.getAllInstruments();
				tempStyleGroupIns = this.musicStyle.getStyleGroupInstruments()
						.get(random.nextInt(this.musicStyle.getStyleGroupInstruments().size())); // random styleGroup if
																									// not defined
			}
			EnumInstruments ins = temp.get(random.nextInt(temp.size()));

			this.allInstruments.put(ins, new StyleGroupIns(tempStyleGroupIns));

			if (ins.getBassInstrument() != null) {
				StyleGroupIns bassStyleGroupIns = findStyleGroupMotherId(tempStyleGroupIns);

				if (bassStyleGroupIns != null) {
					this.allInstruments.put(ins.getBassInstrument(), new StyleGroupIns(bassStyleGroupIns));
					numberInst++;
				} else {
					this.allInstruments.put(ins.getBassInstrument(), new StyleGroupIns(tempStyleGroupIns));
					numberInst++;
				}
			}
			i++;
		}

		if (this.numberBat > 0) {

			i = 0;
			styleGroupInsPercussion = this.musicStyle.getStyleGroupDrums();

			while (i < this.numberBat) {

				if (i < styleGroupInsPercussion.size()) {
					tempStyleGroupDrums = styleGroupInsPercussion.get(i);
				} else {
					tempStyleGroupDrums = new StyleGroupDrums();
					tempStyleGroupDrums.random(this.accordsDuration);
				}

				this.allDrums.put(EnumInstruments.PERCUSSIONS, tempStyleGroupDrums);

				i++;
			}
		}

	}

	private void chooseAllInstrumentsByStyle() {

		int numberInst = this.musicStyle.getNumberInstrument();
		int i = 0;

		this.allInstruments = new EnumMap<>(EnumInstruments.class);
		List<StyleGroupDrums> styleGroupInsPercussion = null;
		List<StyleGroupIns> styleGroupIns = this.getListStyleGroupInsWithoutBass();
		List<EnumInstruments> temp;
		StyleGroupIns tempStyleGroupIns;
		StyleGroupDrums tempStyleGroupDrums;
		EnumInstruments ins;

		while (this.allInstruments.size() < numberInst) {

			if (i < styleGroupIns.size()) {
				tempStyleGroupIns = styleGroupIns.get(i);
				temp = tempStyleGroupIns.getInstruments();
			} else {
				tempStyleGroupIns = this.musicStyle.getStyleGroupInstruments()
						.get(random.nextInt(this.musicStyle.getStyleGroupInstruments().size())); // random styleGroup if
																									// not defined
				temp = tempStyleGroupIns.getInstruments();
			}

			ins = temp.get(random.nextInt(temp.size()));

			this.allInstruments.put(ins, new StyleGroupIns(tempStyleGroupIns));

			if (ins.getBassInstrument() != null) {
				StyleGroupIns bassStyleGroupIns = findStyleGroupMotherId(tempStyleGroupIns);

				if (bassStyleGroupIns != null) {
					this.allInstruments.put(ins.getBassInstrument(), new StyleGroupIns(bassStyleGroupIns));
					numberInst++;
				} else {
					this.allInstruments.put(ins.getBassInstrument(), new StyleGroupIns(tempStyleGroupIns));
					numberInst++;
				}
			}
			i++;
		}

		if (this.numberBat > 0) {

			i = 0;
			styleGroupInsPercussion = this.musicStyle.getStyleGroupDrums();

			while (i < this.numberBat) {

				if (i < styleGroupInsPercussion.size()) {
					tempStyleGroupDrums = styleGroupInsPercussion.get(i);
				} else {
					tempStyleGroupDrums = new StyleGroupDrums();
					tempStyleGroupDrums.random(this.accordsDuration);
				}

				this.allDrums.put(EnumInstruments.PERCUSSIONS, tempStyleGroupDrums);
				i++;
			}
		}

	}

	private List<StyleGroupIns> getListStyleGroupInsWithoutBass() {

		List<StyleGroupIns> styleGroup = new ArrayList<>();

		for (StyleGroupIns stg : this.musicStyle.getStyleGroupInstruments()) {
			if (stg.getMotherId() == 0) {
				styleGroup.add(stg);
			}
		}

		return styleGroup;
	}

	private StyleGroupIns findStyleGroupMotherId(StyleGroupIns origin) {

		for (StyleGroupIns stg : this.musicStyle.getStyleGroupInstruments()) {
			if (stg.getMotherId() == origin.getId()) {
				return stg;
			}
		}
		return null;

	}

	public Map<EnumInstruments, StyleGroupIns> getListEnumInstruments() {
		return this.allInstruments;
	}

	public int getRythme() {
		return this.rythme;
	}

	public EnumDurationChord getAccordsDuration() {
		return this.accordsDuration;
	}

	private List<IdentityInstrument> createCopyOfListIdInstruments() {

		List<IdentityInstrument> copy = new ArrayList<>();

		for (IdentityInstrument id : this.listIdentityInstruments) {
			copy.add(new IdentityInstrument(id));
		}

		return copy;
	}

	private List<IdentityDrum> createCopyOfListIdDrums() {

		List<IdentityDrum> copy = new ArrayList<>();

		for (IdentityDrum id : this.listIdentityDrum) {
			copy.add(new IdentityDrum(id));
		}

		return copy;
	}

	// TODO improve must depend on music style
	public List<Part> createParts() throws MyException {

		List<Part> parts = new ArrayList<Part>();
		int number = random.nextInt(2) + 3;

		if (random.nextInt(2) == 1) {
			parts.add(new Part(EnumPart.INTRODUCTION, null, 0, 0, null, null, createCopyOfListIdInstruments(),
					createCopyOfListIdDrums(), this.musicStyle, this));
		}

		if (numberParts == 2 && number == 3) {

			Part a = new Part(EnumPart.PART, null, 0, 0, null, null, createCopyOfListIdInstruments(),
					createCopyOfListIdDrums(), this.musicStyle, this);

			parts.add(a);
			if (random.nextInt(2) == 1) {
				parts.add(new Part(EnumPart.SOLO, null, 0, 0, null, null, createCopyOfListIdInstruments(),
						createCopyOfListIdDrums(), this.musicStyle, this));
			}
			parts.add(new Part(EnumPart.REFRAIN, null, 0, 0, null, null, createCopyOfListIdInstruments(),
					createCopyOfListIdDrums(), this.musicStyle, this));
			parts.add(a);
		}

		else if (numberParts == 2 && number == 4) {
			Part a = new Part(EnumPart.PART, null, 0, 0, null, null, createCopyOfListIdInstruments(),
					createCopyOfListIdDrums(), this.musicStyle, this);
			parts.add(a);
			if (random.nextInt(2) == 1) {
				parts.add(new Part(EnumPart.SOLO, null, 0, 0, null, null, createCopyOfListIdInstruments(),
						createCopyOfListIdDrums(), this.musicStyle, this));
			}
			Part refrain = new Part(EnumPart.REFRAIN, null, 0, 0, null, null, createCopyOfListIdInstruments(),
					createCopyOfListIdDrums(), this.musicStyle, this);
			parts.add(refrain);
			parts.add(a);
			parts.add(refrain);
		}

		else if (numberParts == 3 && number == 3) {
			parts.add(new Part(EnumPart.PART, null, 0, 0, null, null, createCopyOfListIdInstruments(),
					createCopyOfListIdDrums(), this.musicStyle, this));
			if (random.nextInt(2) == 1) {
				parts.add(new Part(EnumPart.SOLO, null, 0, 0, null, null, createCopyOfListIdInstruments(),
						createCopyOfListIdDrums(), this.musicStyle, this));
			}
			Part refrain = new Part(EnumPart.REFRAIN, null, 0, 0, null, null, createCopyOfListIdInstruments(),
					createCopyOfListIdDrums(), this.musicStyle, this);
			parts.add(refrain);
			parts.add(new Part(EnumPart.PART, null, 0, 0, null, null, createCopyOfListIdInstruments(),
					createCopyOfListIdDrums(), this.musicStyle, this));
			parts.add(refrain);
		}

		else if (numberParts == 3 && number == 4) {
			parts.add(new Part(EnumPart.PART, null, 0, 0, null, null, createCopyOfListIdInstruments(),
					createCopyOfListIdDrums(), this.musicStyle, this));
			if (random.nextInt(2) == 1) {
				parts.add(new Part(EnumPart.SOLO, null, 0, 0, null, null, createCopyOfListIdInstruments(),
						createCopyOfListIdDrums(), this.musicStyle, this));
			}
			Part refrain1 = new Part(EnumPart.REFRAIN, null, 0, 0, null, null, createCopyOfListIdInstruments(),
					createCopyOfListIdDrums(), this.musicStyle, this);
			parts.add(refrain1);
			parts.add(new Part(EnumPart.PART, null, 0, 0, null, null, createCopyOfListIdInstruments(),
					createCopyOfListIdDrums(), this.musicStyle, this));
			parts.add(refrain1);
		}

		return parts;

	}

	public int getNumberBat() {
		return this.numberBat;
	}

	public Map<EnumInstruments, StyleGroupDrums> getAllDrums() {
		return allDrums;
	}

	public void setAllDrums(Map<EnumInstruments, StyleGroupDrums> allDrums) {
		this.allDrums = allDrums;
	}

	public List<IdentityDrum> getListIdentityDrum() {
		return listIdentityDrum;
	}

	public void setListIdentityDrum(List<IdentityDrum> listIdentityDrum) {
		this.listIdentityDrum = listIdentityDrum;
	}

	public List<Part> getListParts() {
		return this.listParts;
	}
}
