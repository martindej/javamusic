package musicRandom;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import enums.EnumDurationNote;
import enums.EnumPart;
import enums.EnumSentence;
import enums.EnumStyle;
import exceptions.MyException;
import styles.Style;

public class Part {
	
	private EnumPart p;	
	private List<Sentence> sentences;
	private List<Chord> availableAccords;
	private EnumStyle style;
	private Style musicStyle;
	
	private IdentityPart idPart;

	int numberSentences;

	private List<EnumDurationNote> allowedRythmes;
	private int gapBetweenNotes;
	private int pourcentageSilences;
	
	private List<IdentityInstrument> listIdentityInstruments;
	private List<IdentityDrum> listIdentityDrums;
	
	private Music myMusic;
	
	private Random RANDOM = new Random();
	
	Part(EnumPart part, List<EnumDurationNote> allowedR, int gapNotes, int pourcentageSilences, EnumStyle st, IdentityPart idP, List<IdentityInstrument> listIdentityI, List<IdentityDrum> listIdentityDrums, Style musicStyle, Music m) throws MyException {
		this.p = part;
		this.style = st;
		this.musicStyle = musicStyle;
		this.availableAccords = musicStyle.getChoosedChords();
		this.allowedRythmes = allowedR;
		this.gapBetweenNotes = gapNotes;
		this.pourcentageSilences = pourcentageSilences;
		this.listIdentityInstruments = listIdentityI;
		this.listIdentityDrums = listIdentityDrums;
		this.myMusic = m;
		
		if(idP == null) {
			this.idPart = new IdentityPart(this.allowedRythmes,this.gapBetweenNotes,this.pourcentageSilences, this.style, this.myMusic, this.listIdentityInstruments, this.listIdentityDrums, null, null);
		}
		else {
			this.idPart = idP;
		}
		
		generateSentences();
	}

	private void generateSentences() throws MyException {

		sentences = new ArrayList<Sentence>();
	
		switch(this.p) {
		
			case INTERLUDE:
				int durationInterlude = RANDOM.nextInt(4) + 4;
				sentences.add(new Sentence(EnumSentence.INTERLUDE, durationInterlude, this.availableAccords, new IdentityPart(this.idPart), 0, 1, this.musicStyle));
				break;
				
			case SOLO:
				int durationSolo = RANDOM.nextInt(12) + 4;
				sentences.add(new Sentence(EnumSentence.SOLO, durationSolo, this.availableAccords, new IdentityPart(this.idPart), 0, 1, this.musicStyle));
				break;
				
			case REFRAIN:
				int durationRefrain = RANDOM.nextInt(14) + 4;
				sentences.add(new Sentence(EnumSentence.REFRAIN, durationRefrain, this.availableAccords, new IdentityPart(this.idPart), 0, 1, this.musicStyle));
				break;
				
			case INTRODUCTION:
				int durationIntroduction = RANDOM.nextInt(8) + 4;
				sentences.add(new Sentence(EnumSentence.INTRODUCTION, durationIntroduction, this.availableAccords, new IdentityPart(this.idPart), 0, 1, this.musicStyle));
				break;
				
			case PART:
				
				numberSentences = RANDOM.nextInt(2) + 2;
				
				sentences.add(new Sentence(EnumSentence.SENTENCE, RANDOM.nextInt(12) + 4, this.availableAccords, new IdentityPart(this.idPart), 0, numberSentences, this.musicStyle));
				sentences.add(new Sentence(EnumSentence.SENTENCE, RANDOM.nextInt(12) + 4, this.availableAccords, new IdentityPart(this.idPart), 1, numberSentences, this.musicStyle));
				if(numberSentences == 3) {
					sentences.add(new Sentence(EnumSentence.SENTENCE, RANDOM.nextInt(12) + 4, this.availableAccords, new IdentityPart(this.idPart), 2, numberSentences, this.musicStyle));
				}

				break;
				
			default:
				break;
		}
		
		this.numberSentences = sentences.size();
		
	}

	public IdentityPart getIdentityPart() {
		return this.idPart;
	}

	public List<Sentence> getSentences() {
		return this.sentences;
	}
	
	public EnumPart getEnumPart() {
		return this.p;
	}

	public List<IdentityDrum> getListIdentityDrums() {
		return listIdentityDrums;
	}

	public void setListIdentityDrums(List<IdentityDrum> listIdentityDrums) {
		this.listIdentityDrums = listIdentityDrums;
	}

}

