package musicRandom;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import enums.EnumBrickSentence;
import enums.EnumSentence;
import enums.EnumStyle;
import enums.EnumVelocity;
import enums.EnumVelocityVariation;
import exceptions.MyException;

public class Motif {
	
	private List<Note> listNotes;
	private int size;
	private EnumStyle style; // TODO
	private EnumVelocity velocity;
	private EnumVelocityVariation velocityVariation;
	private EnumBrickSentence brick;
	private boolean sameNotes;
	
	int count = 0;
	int maxTries = 10;
	boolean succes = false;
	
	private IdentityInstrument idInstrument;
	
	private Random RANDOM = new Random();
	
	Motif(int sizeMotif, IdentityInstrument idInst, EnumVelocity vel, EnumVelocityVariation velVariation, EnumBrickSentence brick) throws MyException {
		
		this.size = sizeMotif;
		this.idInstrument = idInst;
		this.velocity = vel;
		this.velocityVariation = velVariation;
		this.brick = brick;
		if(this.velocity == null) {
			this.velocity = EnumVelocity.randomEnumVelocity();
		}
		
		if(this.velocityVariation == null) {
			this.velocityVariation = EnumVelocityVariation.randomEnumVelocityVariation();
		}
		
		if(this.size == -1) {
			this.size = RANDOM.nextInt(2) + 1;
		}

		while(true) {
		    try {
		    	this.sameNotes = RANDOM.nextBoolean();
		    	this.listNotes = new ArrayList<Note>();
		    	generateMotif();
		    	break;
		    } catch (MyException e) {
		    	System.out.println("problem with generateMotif");
		        if (++count == maxTries) throw e;
		    }
		}
	}
	
	Motif(Motif m) {
		this.listNotes = m.listNotes;
		this.size = m.size;
		this.velocity = m.velocity;
		this.style = m.style;
		this.velocityVariation = m.velocityVariation;
		this.sameNotes = m.sameNotes;
		this.brick = m.brick;
		
		this.idInstrument = m.idInstrument;
	}
	
	private void generateMotif() throws MyException {
		
		Note previousNote = null;
		List<Map<Integer, Integer>> listOctaveDegres = new LinkedList<Map<Integer, Integer>>();
		int endPart = this.size * this.idInstrument.getChordDuration().getDuration() * 24; // 24 beat by black note
		
		int velocityValue;

		do {
			velocityValue = estimateVelocity(previousNote, endPart);
			previousNote = new Note(previousNote, this.listNotes, this.idInstrument.getNoteRef(), this.idInstrument.getRefChord(), this.idInstrument.getTonality(), this.idInstrument.getAllowedRythme(), this.idInstrument.getGapBetweenNotes(), this.idInstrument.getStyle(), this.idInstrument.getInstrument(), this.idInstrument.getPourcentageSilences(), this.idInstrument.getChordDuration(), this.idInstrument.getTempsForts(), null, EnumSentence.SENTENCE, endPart, false, false, false, listOctaveDegres, null, null, this.idInstrument.getNumberNotesPlayedSimultaneously(), velocityValue, this.idInstrument.getStyleGroupIns());
			this.listNotes.add(previousNote);
		} 
		while(previousNote.getCursor() < endPart);
	}
	
	private int estimateVelocity(Note previousNote, int endPart) {
		
		double ratio = 0;
		int velocityStart, velocityEnd;
		
		velocityStart = this.velocity.getValue();
		velocityEnd = this.velocity.getVelocityAfterVariation(this.velocityVariation).getValue();
		
		if(previousNote != null) {
			ratio = (double)previousNote.getCursor()/(double)endPart;
		}
		
		return (int) (ratio * (velocityEnd - velocityStart) + velocityStart);
	}
	
	public int getSize() {
		return this.size;
	}
	
	public List<Note> getListNotes() {
		return this.listNotes;
	}

	public boolean isSameNotes() {
		return sameNotes;
	}

	public void setSameNotes(boolean sameNotes) {
		this.sameNotes = sameNotes;
	}

	public EnumBrickSentence getBrick() {
		return brick;
	}

	public void setBrick(EnumBrickSentence brick) {
		this.brick = brick;
	}

}
