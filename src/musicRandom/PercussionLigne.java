package musicRandom;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import enums.EnumBrickSentence;
import enums.EnumStyle;
import enums.EnumTempsForts;
import enums.EnumVelocity;
import enums.EnumVelocityVariation;

public class PercussionLigne {
	
	private List<Note> listNotes;
	private int size;
	private Map<Integer, Motif> placementMotifsMelody;
	private EnumStyle style; // TODO
	private List<EnumTempsForts> tempsFortTension;
	private EnumVelocity velocity;
	private EnumVelocityVariation velocityVariation;
	
	private IdentityInstrument idInstrument;
	private IdentityDrum idDrum;
	
	PercussionLigne(IdentityInstrument idInst, List<EnumTempsForts> tempsFortTension) {
		
		this.idInstrument = idInst;
		this.tempsFortTension = tempsFortTension;
		this.size = this.idInstrument.getChordsList().size();
		
		this.listNotes = new ArrayList<Note>();
		
		generatePercussionIns();
	}
	
	public PercussionLigne(IdentityDrum identityDrum, List<EnumTempsForts> tempsFortTension, EnumVelocity velocity, EnumVelocityVariation velocityVariation) {
		this.idDrum = identityDrum;
		this.tempsFortTension = tempsFortTension;
		this.size = this.idDrum.getChordsList().size();
		this.velocity = velocity;
		this.velocityVariation = velocityVariation;
		
		this.listNotes = new ArrayList<Note>();
		
		generatePercussionDrum();
	}
	
	PercussionLigne(PercussionLigne p) {
		this.listNotes = p.listNotes;
		this.size = p.size;
		this.placementMotifsMelody = p.placementMotifsMelody;
		this.style = p.style;
		this.tempsFortTension = p.tempsFortTension;
		this.velocity = p.velocity;
		this.velocityVariation = p.velocityVariation;
		this.idInstrument = p.idInstrument;
	}

	private void generatePercussionIns() {
				
		Note previousNote = null;
		
		for(int i=0;i<this.idInstrument.getChordsList().size();i++) {
			for(Note not:this.idInstrument.getRepetedItem().get(EnumBrickSentence.REPETEDITEM1).get(0).getListNotes()) {
				previousNote = new Note(previousNote, not.getListNoteNum() , not.getNoteDuration(), not.getSilenceOrPlaying(), this.idInstrument.getChordDuration(), new Chord(), not.getVolume());
				this.listNotes.add(previousNote);
			}
		}
	}
	
	private void generatePercussionDrum() {
				
		Note previousNote = null;
		int volumeRef, velocityDifRef, velocityFirstNote, volume;
		
		int endPart = this.idDrum.getChordsList().size()*this.idDrum.getAccordsDuration().getDuration() * 24;
		
		velocityFirstNote = this.idDrum.getRepetedItem().get(EnumBrickSentence.REPETEDITEM1).get(0).getListNotes().get(0).getVolume();
		
		for(int i=0;i<this.idDrum.getChordsList().size();i++) {
			
			volumeRef = this.estimateVelocity(previousNote, endPart);
			velocityDifRef = volumeRef - velocityFirstNote;
			
			for(Note not:this.idDrum.getRepetedItem().get(EnumBrickSentence.REPETEDITEM1).get(0).getListNotes()) {
				
				volume = not.getVolume() + velocityDifRef;
				previousNote = new Note(previousNote, not.getListNoteNum() , not.getNoteDuration(), not.getSilenceOrPlaying(), this.idDrum.getAccordsDuration(), new Chord(), volume);
				this.listNotes.add(previousNote);
			}
		}
	}
	
	private int estimateVelocity(Note previousNote, int endPart) {
		
		double ratio = 0;
		int velocityStart, velocityEnd;
		
		velocityStart = this.velocity.getValue();
		velocityEnd = this.velocity.getVelocityAfterVariation(this.velocityVariation).getValue();
		
		if(previousNote != null) {
			ratio = (double)previousNote.getCursor()/(double)endPart;
		}
		
		return (int) (ratio * (velocityEnd - velocityStart) + velocityStart);
	}
	
	public List<EnumTempsForts> getTempsFortTension() {
		return this.tempsFortTension;
	}
	
	public List<Note> getListNotes() {
		return this.listNotes;
	}

	public IdentityDrum getIdDrum() {
		return idDrum;
	}

	public void setIdDrum(IdentityDrum idDrum) {
		this.idDrum = idDrum;
	}

	public EnumVelocity getVelocity() {
		return velocity;
	}

	public void setVelocity(EnumVelocity velocity) {
		this.velocity = velocity;
	}

	public EnumVelocityVariation getVelocityVariation() {
		return velocityVariation;
	}

	public void setVelocityVariation(EnumVelocityVariation velocityVariation) {
		this.velocityVariation = velocityVariation;
	}

}
