package musicRandom;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import enums.EnumBrickSentence;
import enums.EnumChordTension;
import enums.EnumDurationChord;
import enums.EnumInstrumentCategories;
import enums.EnumInstruments;
import enums.EnumSentence;
import enums.EnumTempsForts;
import enums.EnumTonalite;
import enums.EnumVelocity;
import enums.EnumVelocityVariation;
import styles.Style;
import styles.StyleGroupDrums;

public class IdentityDrum {
	
	private List<Part> listParts;
	private Map<EnumBrickSentence,List<RepetedItem>> repetedItem;
	private Map<EnumBrickSentence,List<PercussionLigne>> percussionLignes;
	private EnumTonalite tonality;
	private EnumDurationChord accordsDuration;
	private int rythme;
	private List<EnumTempsForts> tempsForts;
	private EnumInstruments instrument;
	private EnumInstrumentCategories instrumentsCategory;
	private List<Map<Chord,EnumChordTension>> chordsList;
	private EnumSentence sentence;
	private Style musicStyle;
	private StyleGroupDrums styleGroupDrums;
	
	private IdentitySentence distributionInstrument;
	
	private int refNumber;
	
	private Random RANDOM = new Random();
	
	public IdentityDrum(List<Part> listParts, EnumTonalite tonality, EnumDurationChord accordsDuration, List<EnumTempsForts> tempsForts, int rythme, EnumInstruments ins, EnumInstrumentCategories percussion, Style musicStyle, StyleGroupDrums styleGroupDrums) {
		this.listParts = listParts;
		this.tonality = tonality;
		this.accordsDuration = accordsDuration;
		this.instrument = ins;
		this.rythme = rythme;
		this.instrumentsCategory = percussion;
		this.musicStyle = musicStyle;
		this.styleGroupDrums = styleGroupDrums;
		this.tempsForts = tempsForts;
		
		this.repetedItem = new HashMap<EnumBrickSentence,List<RepetedItem>>();
		this.percussionLignes = new HashMap<EnumBrickSentence, List<PercussionLigne>>();
	}

	public IdentityDrum(IdentityDrum id) {
		this.listParts = id.getListParts();
		this.tonality = id.getTonality();
		this.accordsDuration = id.getAccordsDuration();
		this.instrument = id.getInstrument();
		this.rythme = id.getRythme();
		this.instrumentsCategory = id.getInstrumentsCategory();
		this.musicStyle = id.getMusicStyle();
		this.chordsList = id.getChordsList();
		this.tempsForts = id.getTempsForts();
		this.percussionLignes = id.createCopyPercussions();
		this.repetedItem = id.getRepetedItem();
		this.styleGroupDrums = id.getStyleGroupDrums();
	}
	
	public Map<EnumBrickSentence, List<RepetedItem>> getRepetedItem() {
		return this.repetedItem;
	}
	
	public void generatePercussionLigne(List<EnumTempsForts> tempsFortTension, EnumBrickSentence brick, EnumVelocity velocity, EnumVelocityVariation velocityVariation) {
		
		ArrayList<PercussionLigne> percussionLignes = new ArrayList<PercussionLigne>();
		percussionLignes.add(new PercussionLigne(this, tempsFortTension, velocity, velocityVariation));
		
		this.percussionLignes.put(brick, percussionLignes);
	}

	public void generateRepetedItem() {
		this.repetedItem = new HashMap<EnumBrickSentence, List<RepetedItem>>();
		
		RepetedItem newRepetedItem = new RepetedItem(this,null, null);
		ArrayList<RepetedItem> repetedIt = new ArrayList<RepetedItem>();
		repetedIt.add(newRepetedItem);
		this.repetedItem.put(EnumBrickSentence.REPETEDITEM1, repetedIt);
	}
	
	private Map<EnumBrickSentence,List<PercussionLigne>> createCopyPercussions() {
		
		Map<EnumBrickSentence,List<PercussionLigne>> copy = new HashMap<EnumBrickSentence,List<PercussionLigne>>();
		List<PercussionLigne> listCopy = new ArrayList<PercussionLigne>();
		
		for(Map.Entry<EnumBrickSentence,List<PercussionLigne>> list:this.percussionLignes.entrySet()) {
			
			listCopy = new ArrayList<PercussionLigne>();
			for(PercussionLigne per:list.getValue()) {
				listCopy.add(new PercussionLigne(per));
			}
			copy.put(list.getKey(), listCopy);
		}
		
		return copy;
	}

	public List<Part> getListParts() {
		return listParts;
	}
	public void setListParts(List<Part> listParts) {
		this.listParts = listParts;
	}
	public Map<EnumBrickSentence,List<PercussionLigne>> getPercussionLignes() {
		return percussionLignes;
	}
	public void setPercussionLignes(Map<EnumBrickSentence,List<PercussionLigne>> percussionLignes) {
		this.percussionLignes = percussionLignes;
	}
	public EnumTonalite getTonality() {
		return tonality;
	}
	public void setTonality(EnumTonalite tonality) {
		this.tonality = tonality;
	}
	public EnumDurationChord getAccordsDuration() {
		return accordsDuration;
	}
	public void setAccordsDuration(EnumDurationChord accordsDuration) {
		this.accordsDuration = accordsDuration;
	}
	public int getRythme() {
		return rythme;
	}
	public void setRythme(int rythme) {
		this.rythme = rythme;
	}
	public List<EnumTempsForts> getTempsForts() {
		return tempsForts;
	}
	public void setTempsForts(List<EnumTempsForts> tempsForts) {
		this.tempsForts = tempsForts;
	}
	public EnumInstruments getInstrument() {
		return instrument;
	}
	public void setInstrument(EnumInstruments instrument) {
		this.instrument = instrument;
	}
	public EnumInstrumentCategories getInstrumentsCategory() {
		return instrumentsCategory;
	}
	public void setInstrumentsCategory(EnumInstrumentCategories instrumentsCategory) {
		this.instrumentsCategory = instrumentsCategory;
	}
	public List<Map<Chord,EnumChordTension>> getChordsList() {
		return chordsList;
	}
	public void setChordsList(List<Map<Chord,EnumChordTension>> chordsList) {
		this.chordsList = chordsList;
	}
	public EnumSentence getSentence() {
		return sentence;
	}
	public void setSentence(EnumSentence sentence) {
		this.sentence = sentence;
	}

	public Style getMusicStyle() {
		return musicStyle;
	}

	public void setMusicStyle(Style musicStyle) {
		this.musicStyle = musicStyle;
	}

	public StyleGroupDrums getStyleGroupDrums() {
		return styleGroupDrums;
	}

	public void setStyleGroupDrums(StyleGroupDrums styleGroupDrums) {
		this.styleGroupDrums = styleGroupDrums;
	}

	public IdentitySentence getDistributionInstrument() {
		return distributionInstrument;
	}

	public void setDistributionInstrument(IdentitySentence distributionInstrument) {
		this.distributionInstrument = distributionInstrument;
	}

	public int getRefNumber() {
		return refNumber;
	}

	public void setRefNumber(int refNumber) {
		this.refNumber = refNumber;
	}

}
