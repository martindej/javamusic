package musicRandom;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import enums.EnumDurationChord;
import enums.EnumDurationNote;
import enums.EnumStyle;
import enums.EnumTempsForts;
import enums.EnumTonalite;
import enums.EnumVelocity;
import enums.EnumVelocityVariation;

public class IdentityPart {

	private List<EnumDurationNote> allowedRythmes;

	private int gapBetweenNotes;
	private int pourcentageSilences;
	private EnumStyle style;
	private EnumTonalite tonality;
	private EnumDurationChord accordsDuration;
	private int rythme;
	private List<EnumTempsForts> tempsForts;
	private EnumVelocity velocity;
	private EnumVelocityVariation velocityVariation;

	private Music myMusic;
	private List<IdentityInstrument> listIdentityInstruments;
	private List<IdentityDrum> listIdentityDrums;

	int sizeSmallMotif;

	private static Random random = new Random();

	IdentityPart(List<EnumDurationNote> allowedR, int gapNotes, int pourcentageSilences, EnumStyle style, Music m,
			List<IdentityInstrument> listIdentityI, List<IdentityDrum> listIdentityDrums, EnumVelocity velocity,
			EnumVelocityVariation velocityVariation) {

		this.myMusic = m;
		this.tonality = m.getTonality();
		this.allowedRythmes = allowedR;
		this.gapBetweenNotes = gapNotes;
		this.pourcentageSilences = pourcentageSilences;
		this.style = style;
		this.accordsDuration = m.getAccordsDuration();
		this.tempsForts = m.getTempsForts();
		this.rythme = m.getRythme();
		this.listIdentityInstruments = listIdentityI;
		this.listIdentityDrums = listIdentityDrums;
		this.velocity = velocity;
		this.velocityVariation = velocityVariation;

		if (this.velocity == null) {
			this.velocity = EnumVelocity.randomEnumVelocity();
		}

		if (this.velocityVariation == null) {
			this.velocityVariation = EnumVelocityVariation.randomEnumVelocityVariation();
		}

		if (this.style == null) {
			this.style = EnumStyle.randomEnumStyle();
		}

		if (this.gapBetweenNotes == 0) {
			this.gapBetweenNotes = random.nextInt(13);
		}

		if (this.getPourcentageSilences() == 0) {
			this.pourcentageSilences = random.nextInt(40);
		}

	}

	// used to clone without references
	IdentityPart(IdentityPart idPart) {

		this.allowedRythmes = idPart.allowedRythmes;
		this.gapBetweenNotes = idPart.gapBetweenNotes;
		this.pourcentageSilences = idPart.pourcentageSilences;
		this.style = idPart.style;
		this.tonality = idPart.tonality;
		this.accordsDuration = idPart.accordsDuration;
		this.rythme = idPart.rythme;
		this.tempsForts = idPart.tempsForts;
		this.myMusic = idPart.myMusic;
		this.listIdentityInstruments = idPart.listIdentityInstruments;
		this.listIdentityDrums = idPart.listIdentityDrums;
		this.sizeSmallMotif = idPart.sizeSmallMotif;
		this.velocity = idPart.velocity;
		this.velocityVariation = idPart.velocityVariation;
	}

	private List<IdentityInstrument> createCopyOfListIdInstruments() {

		List<IdentityInstrument> copy = new ArrayList<>();

		for (IdentityInstrument id : this.listIdentityInstruments) {
			copy.add(new IdentityInstrument(id));
		}

		return copy;
	}

	private List<IdentityDrum> createCopyOfListIdDrums() {
		List<IdentityDrum> copy = new ArrayList<>();

		for (IdentityDrum id : this.listIdentityDrums) {
			copy.add(new IdentityDrum(id));
		}

		return copy;
	}

	public List<IdentityInstrument> getCopyOfListIdentityInstruments() {
		return createCopyOfListIdInstruments();
	}

	public List<IdentityDrum> getCopyOfListIdentityDrums() {
		return createCopyOfListIdDrums();
	}

	public List<IdentityInstrument> getListIdentityInstruments() {
		return this.listIdentityInstruments;
	}

	public List<EnumTempsForts> getTempsForts() {
		return this.tempsForts;
	}

	public int getRythme() {
		return this.rythme;
	}

	public EnumDurationChord getChordDuration() {
		return this.accordsDuration;
	}

	public int getPourcentageSilences() {
		return pourcentageSilences;
	}

	public EnumTonalite getTonality() {
		return this.tonality;
	}

	public List<EnumDurationNote> getAllowedRythme() {
		return this.allowedRythmes;
	}

	public int getGapBetweenNotes() {
		return this.gapBetweenNotes;
	}

	public EnumStyle getStyle() {
		return this.style;
	}

	public List<IdentityDrum> getListIdentityDrums() {
		return listIdentityDrums;
	}

	public void setListIdentityDrums(List<IdentityDrum> listIdentityDrums) {
		this.listIdentityDrums = listIdentityDrums;
	}

	public EnumVelocity getVelocity() {
		return velocity;
	}

	public void setVelocity(EnumVelocity velocity) {
		this.velocity = velocity;
	}

	public EnumVelocityVariation getVelocityVariation() {
		return velocityVariation;
	}

	public void setVelocityVariation(EnumVelocityVariation velocityVariation) {
		this.velocityVariation = velocityVariation;
	}
}
