package musicRandom;

import enums.EnumPlaySentence;

public class IdentitySentence {
	
	EnumPlaySentence style;
	
	//add volume to separate when it's a soliste playing 
	IdentitySentence(EnumPlaySentence s) {
		this.style = s;
	}
	
	public EnumPlaySentence getStyle() {
		return this.style;
	}

}