package musicRandom;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import enums.EnumBrickSentence;
import enums.EnumChordTension;
import enums.EnumDegre;
import enums.EnumInstrumentCategories;
import enums.EnumInstruments;
import enums.EnumPlaySentence;
import enums.EnumSentence;
import enums.EnumTempsForts;
import enums.EnumVelocity;
import enums.EnumVelocityVariation;
import exceptions.MyException;
import styles.Style;
import styles.StyleCoefDegres;

public class Sentence {

	private EnumSentence sentence;
	private int numberChords;
	private List<Chord> availableAccords;
	private List<Map<Chord, EnumChordTension>> chordsList;
	private IdentityPart idPart;
	private List<IdentityInstrument> listIdentityInstruments;
	private List<IdentityDrum> listIdentityDrums;
	private List<EnumTempsForts> tempsFortTension;
	private EnumVelocity velocity;
	private EnumVelocityVariation velocityVariation;
	IdentityInstrument idRefMainMelody = null;
	IdentityInstrument idRefMainMelodyBis = null;
	IdentityInstrument idRefBass = null;
	IdentityInstrument idRefBassBis = null;
	IdentityInstrument idRefPercussion = null;

	private Random random = new Random();

	Sentence(EnumSentence sentence, int numberAccords, List<Chord> availableAccords, IdentityPart idP,
			int indexSentence, int numberSentences, Style musicStyle) throws MyException {
		this.sentence = sentence;
		this.numberChords = numberAccords;
		this.availableAccords = availableAccords;
		this.idPart = idP;
		this.chordsList = generateListChords(musicStyle.getStyleCoefDegres());
		this.listIdentityInstruments = this.idPart.getCopyOfListIdentityInstruments();
		this.listIdentityDrums = this.idPart.getCopyOfListIdentityDrums();
		this.tempsFortTension = idP.getTempsForts();

		this.estimateVelocity(indexSentence, numberSentences);
		chooseDistributionInstruments();
	}

	public IdentityInstrument getIdentityInstrumentByRefNumber(int ref) {

		for (IdentityInstrument id : this.listIdentityInstruments) {
			if (id.getRefNumber() == ref) {
				return id;
			}
		}

		return null;
	}

	private void estimateVelocity(int indexSentence, int numberSentences) {

		if (indexSentence == 0) {
			this.velocity = this.idPart.getVelocity();
		} else {
			int volumeStart = this.idPart.getVelocity().getValue();
			int volumeEnd = this.idPart.getVelocity().getVelocityAfterVariation(this.idPart.getVelocityVariation())
					.getValue();

			int volume = (int) ((volumeEnd - volumeStart) * (indexSentence / (double) numberSentences)) + volumeStart;

			this.velocity = EnumVelocity.getClosestValue(volume);
		}

		double velocityVar = this.idPart.getVelocityVariation().getValue() / (double) numberSentences;

		this.velocityVariation = EnumVelocityVariation.getClosestValue(velocityVar);

	}

	private void generateBassLigne(EnumBrickSentence brick, IdentityInstrument idIns) throws MyException {

		idIns.generateRepetedItem();
		idIns.setChords(this.chordsList);
		idIns.generateBassLigne(this.tempsFortTension, brick, this.velocity, this.velocityVariation);
	}

	private void generateBassLigneWithRef(EnumBrickSentence brick, IdentityInstrument idIns, IdentityInstrument idRef)
			throws MyException {

		idIns.setChords(this.chordsList);
		idIns.generateBassLigneWithRef(idRef.getBassLignes().get(brick), brick, idRef.getAllowedRythme());
	}

	private void generatePercussionLigne(EnumBrickSentence brick, IdentityInstrument idIns) throws MyException {

		idIns.generateRepetedItem();
		idIns.setChords(this.chordsList);
		idIns.generatePercussionLigne(this.tempsFortTension, brick);
	}

	private void generatePercussionLigne(EnumBrickSentence brick, IdentityDrum idDrum) {

		idDrum.generateRepetedItem();
		idDrum.setChordsList(this.chordsList);
		idDrum.generatePercussionLigne(this.tempsFortTension, brick, this.velocity, this.velocityVariation);
	}

	private void generateMelody(EnumBrickSentence brick, IdentityInstrument idIns) throws MyException {

		idIns.generateMotifs();
		idIns.setChords(this.chordsList);
		idIns.generateMelody(this.tempsFortTension, brick, this.velocity, this.velocityVariation);
	}

	private void generateMelodyWithRef(EnumBrickSentence brick, IdentityInstrument idIns, IdentityInstrument idRef)
			throws MyException {

		idIns.setChords(this.chordsList);
		idIns.generateMainMelodyWithRef(idRef.getMelodies().get(brick), brick, idRef.getAllowedRythme());
	}

	private void generateMelodyWithMelodyRef(EnumBrickSentence brick, EnumBrickSentence melodyRef,
			IdentityInstrument idIns, IdentityInstrument idRefMainMelodyBis) throws MyException {

		idIns.generateMotifsWithRef(idRefMainMelodyBis.getMotifs());
		idIns.setChords(this.chordsList);
		idIns.generateMainMelodyWithMelodyRef(idRefMainMelodyBis.getMelodies().get(melodyRef), this.tempsFortTension,
				brick, this.velocity, this.velocityVariation);
	}

	public int getNumberChords() {
		return numberChords;
	}

	public EnumSentence getEnumSentence() {
		return sentence;
	}

	// TODO improve generation of chord list and personnalize depend on style
	private List<Map<Chord, EnumChordTension>> generateListChords(StyleCoefDegres styleCoefDegres) {

		Map<Chord, EnumChordTension> acc = new LinkedHashMap<>();
		// use of linkedList to keep the order
		List<Map<Chord, EnumChordTension>> listChords = new LinkedList<>();
		Chord currentChord;

		currentChord = new Chord(this.availableAccords);
		acc.put(currentChord, EnumChordTension.randomEnumTensionDependingOnPrevious(null));
		listChords.add(acc);

		for (int i = 1; i < this.numberChords - 2; i++) {
			currentChord = currentChord.getNextChord(this.availableAccords, styleCoefDegres);
			acc = new LinkedHashMap<>();
			acc.put(currentChord, EnumChordTension.randomEnumTensionDependingOnPrevious(
					listChords.get(i - 1).entrySet().iterator().next().getValue()));
			listChords.add(acc);
		}

		acc = new LinkedHashMap<>();
		acc.put(new Chord(this.availableAccords, EnumDegre.V), EnumChordTension.TENSION);
		listChords.add(acc);

		acc = new LinkedHashMap<>();
		acc.put(new Chord(this.availableAccords, EnumDegre.I), EnumChordTension.COMPLETERESOLUTION);
		listChords.add(acc);

		return listChords;
	}

	private List<IdentityInstrument> getRandomInstruments() {

		IdentityInstrument id;
		List<IdentityInstrument> returnList = new ArrayList<>();
		List<IdentityInstrument> in = new ArrayList<>();

		for (IdentityInstrument idIns : this.listIdentityInstruments) {
			if (!idIns.getInstrument().equals(EnumInstruments.PERCUSSIONS)) {
				in.add(idIns);
			}
		}

		int number = random.nextInt(in.size()) + 1;

		for (int i = 0; i < number; i++) {
			do {
				id = in.get(random.nextInt(number));
			} while (returnList.contains(id));

			returnList.add(id);
		}

		return returnList;
	}

	private IdentityInstrument getSoliste() {

		int maxPourcentageBassMelody = -100;
		IdentityInstrument maxId = null;

		for (IdentityInstrument idIns : this.listIdentityInstruments) {
			if (idIns.getStyleGroupIns().getBalanceBassMelody() > maxPourcentageBassMelody) {
				maxId = idIns;
				maxPourcentageBassMelody = idIns.getStyleGroupIns().getBalanceBassMelody();
			}
		}

		return maxId;
	}

	private void chooseDistributionInstruments() throws MyException {

		int randomNumber;
		int randomPlaying;
		int randomMelodyMelodyBis;
		List<IdentityInstrument> randomList;
		List<Integer> listIndexInstruments;
		int index;
		IdentityInstrument idIns;

		for (IdentityInstrument id : this.listIdentityInstruments) {
			id.setEnumSentence(this.sentence);
		}

		switch (this.sentence) {

		case INTERLUDE:

			randomList = getRandomInstruments();

			listIndexInstruments = getListIndexId();

			for (int i = 0; i < this.listIdentityInstruments.size(); i++) {

				index = listIndexInstruments.get(random.nextInt(listIndexInstruments.size()));
				listIndexInstruments.remove(Integer.valueOf(index));
				idIns = this.listIdentityInstruments.get(index);

				if (this.idRefBass == null) {
					this.idRefBass = idIns;
					generateBassLigne(EnumBrickSentence.BASSLIGNE1, idIns);
					idIns.setDistributionInstrument(new IdentitySentence(EnumPlaySentence.BASSLIGNE));
				} else if (this.idRefBassBis == null) {
					this.idRefBassBis = idIns;
					generateBassLigne(EnumBrickSentence.BASSLIGNE2, idIns);
					idIns.setDistributionInstrument(new IdentitySentence(EnumPlaySentence.BASSLIGNEBIS));
				} else {
					if (random.nextBoolean()) {
						generateBassLigneWithRef(EnumBrickSentence.BASSLIGNE1, idIns, this.idRefBass);
						idIns.setDistributionInstrument(new IdentitySentence(EnumPlaySentence.BASSLIGNE));
					} else {
						generateBassLigneWithRef(EnumBrickSentence.BASSLIGNE2, idIns, this.idRefBassBis);
						idIns.setDistributionInstrument(new IdentitySentence(EnumPlaySentence.BASSLIGNEBIS));
					}
				}
			}

			for (IdentityInstrument idInsBis : this.listIdentityInstruments) {
				if (!randomList.contains(idInsBis)) {
					idInsBis.setDistributionInstrument(new IdentitySentence(EnumPlaySentence.NOTPLAYING));
				} else if (idInsBis.getCategory().equals(EnumInstrumentCategories.PERCUSSION)) {
					generatePercussionLigne(EnumBrickSentence.DRUMS, idInsBis);
					idInsBis.setDistributionInstrument(new IdentitySentence(EnumPlaySentence.DRUMS));
				}
			}

			for (IdentityDrum idDrum : this.listIdentityDrums) {
				generatePercussionLigne(EnumBrickSentence.DRUMS, idDrum);
				idDrum.setDistributionInstrument(new IdentitySentence(EnumPlaySentence.DRUMS));
			}

			break;

		case SOLO:

			IdentityInstrument idInsSoliste = getSoliste();
			generateMelody(EnumBrickSentence.MELODY1, idInsSoliste);
			idInsSoliste.setDistributionInstrument(new IdentitySentence(EnumPlaySentence.MAINMELODY));

			listIndexInstruments = getListIndexId();

			for (int i = 0; i < this.listIdentityInstruments.size(); i++) {

				index = listIndexInstruments.get(random.nextInt(listIndexInstruments.size()));
				listIndexInstruments.remove(Integer.valueOf(index));
				idIns = this.listIdentityInstruments.get(index);

				if (!idInsSoliste.equals(idIns)) {
					randomNumber = random.nextInt(201) - 100;

					if (idIns.getStyleGroupIns().getBalanceBassMelody() <= randomNumber) {
						this.idRefBass = idIns;
						generateBassLigne(EnumBrickSentence.BASSLIGNE1, idIns);
						idIns.setDistributionInstrument(new IdentitySentence(EnumPlaySentence.BASSLIGNE));
					} else {
						idIns.setDistributionInstrument(new IdentitySentence(EnumPlaySentence.NOTPLAYING));
					}

					if (idIns.getCategory().equals(EnumInstrumentCategories.PERCUSSION)) {
						generatePercussionLigne(EnumBrickSentence.DRUMS, idIns);
						idIns.setDistributionInstrument(new IdentitySentence(EnumPlaySentence.DRUMS));
					}
				}

			}

			for (IdentityDrum idDrum : this.listIdentityDrums) {
				generatePercussionLigne(EnumBrickSentence.DRUMS, idDrum);
				idDrum.setDistributionInstrument(new IdentitySentence(EnumPlaySentence.DRUMS));
			}

			break;

		case REFRAIN:

			listIndexInstruments = getListIndexId();

			for (int i = 0; i < this.listIdentityInstruments.size(); i++) {

				index = listIndexInstruments.get(random.nextInt(listIndexInstruments.size()));
				listIndexInstruments.remove(Integer.valueOf(index));
				idIns = this.listIdentityInstruments.get(index);

				randomPlaying = random.nextInt(100);
				randomNumber = random.nextInt(201) - 100;

				if (idIns.getStyleGroupIns().getPourcentagePlaying() > randomPlaying || i == 0) {

					if (idIns.getCategory().equals(EnumInstrumentCategories.PERCUSSION)) {
						generatePercussionLigne(EnumBrickSentence.DRUMS, idIns);
						idIns.setDistributionInstrument(new IdentitySentence(EnumPlaySentence.DRUMS));
					} else {
						if (idIns.getStyleGroupIns().getBalanceBassMelody() <= randomNumber) {

							if (this.idRefBass == null) {
								this.idRefBass = idIns;
								generateBassLigne(EnumBrickSentence.BASSLIGNE1, idIns);
								idIns.setDistributionInstrument(new IdentitySentence(EnumPlaySentence.BASSLIGNE));
							} else {
								generateBassLigneWithRef(EnumBrickSentence.BASSLIGNE1, idIns, this.idRefBass);
								idIns.setDistributionInstrument(new IdentitySentence(EnumPlaySentence.BASSLIGNE));
							}
						} else if (idIns.getStyleGroupIns().getBalanceBassMelody() > randomNumber) {

							if (this.idRefMainMelody == null) {
								this.idRefMainMelody = idIns;
								generateMelody(EnumBrickSentence.MELODY1, idIns);
								idIns.setDistributionInstrument(new IdentitySentence(EnumPlaySentence.MAINMELODY));
							} else {
								generateMelodyWithRef(EnumBrickSentence.MELODY1, idIns, this.idRefMainMelody);
								idIns.setDistributionInstrument(new IdentitySentence(EnumPlaySentence.MAINMELODY));
							}
						}
					}
				} else {
					idIns.setDistributionInstrument(new IdentitySentence(EnumPlaySentence.NOTPLAYING));
				}
			}

			for (IdentityDrum idDrum : this.listIdentityDrums) {
				generatePercussionLigne(EnumBrickSentence.DRUMS, idDrum);
				idDrum.setDistributionInstrument(new IdentitySentence(EnumPlaySentence.DRUMS));
			}

			break;

		case INTRODUCTION:

			randomList = getRandomInstruments();

			listIndexInstruments = getListIndexId();

			for (int i = 0; i < this.listIdentityInstruments.size(); i++) {

				index = listIndexInstruments.get(random.nextInt(listIndexInstruments.size()));
				listIndexInstruments.remove(Integer.valueOf(index));
				idIns = this.listIdentityInstruments.get(index);

				if (this.idRefBass == null) {
					this.idRefBass = idIns;
					generateBassLigne(EnumBrickSentence.BASSLIGNE1, idIns);
					idIns.setDistributionInstrument(new IdentitySentence(EnumPlaySentence.BASSLIGNE));
				} else if (this.idRefBassBis == null) {
					this.idRefBassBis = idIns;
					generateBassLigne(EnumBrickSentence.BASSLIGNE2, idIns);
					idIns.setDistributionInstrument(new IdentitySentence(EnumPlaySentence.BASSLIGNEBIS));
				} else {
					if (random.nextBoolean()) {
						generateBassLigneWithRef(EnumBrickSentence.BASSLIGNE1, idIns, this.idRefBass);
						idIns.setDistributionInstrument(new IdentitySentence(EnumPlaySentence.BASSLIGNE));
					} else {
						generateBassLigneWithRef(EnumBrickSentence.BASSLIGNE2, idIns, this.idRefBassBis);
						idIns.setDistributionInstrument(new IdentitySentence(EnumPlaySentence.BASSLIGNEBIS));
					}
				}
			}

			for (IdentityInstrument idInsBis : this.listIdentityInstruments) {
				if (!randomList.contains(idInsBis)) {
					idInsBis.setDistributionInstrument(new IdentitySentence(EnumPlaySentence.NOTPLAYING));
				} else if (idInsBis.getCategory().equals(EnumInstrumentCategories.PERCUSSION)) {
					generatePercussionLigne(EnumBrickSentence.DRUMS, idInsBis);
					idInsBis.setDistributionInstrument(new IdentitySentence(EnumPlaySentence.DRUMS));
				}
			}

			for (IdentityDrum idDrum : this.listIdentityDrums) {
				generatePercussionLigne(EnumBrickSentence.DRUMS, idDrum);
				idDrum.setDistributionInstrument(new IdentitySentence(EnumPlaySentence.DRUMS));
			}

			break;

		case SENTENCE:

			listIndexInstruments = getListIndexId();

			for (int i = 0; i < this.listIdentityInstruments.size(); i++) {

				index = listIndexInstruments.get(random.nextInt(listIndexInstruments.size()));
				listIndexInstruments.remove(Integer.valueOf(index));
				idIns = this.listIdentityInstruments.get(index);

				randomPlaying = random.nextInt(100);
				randomNumber = random.nextInt(201) - 100;

				if (idIns.getStyleGroupIns().getPourcentagePlaying() > randomPlaying || i == 0) {

					if (idIns.getCategory().equals(EnumInstrumentCategories.PERCUSSION)) {
						generatePercussionLigne(EnumBrickSentence.DRUMS, idIns);
						idIns.setDistributionInstrument(new IdentitySentence(EnumPlaySentence.DRUMS));
					} else {
						if (idIns.getStyleGroupIns().getBalanceBassMelody() <= randomNumber) {

							if (this.idRefBass == null) {
								this.idRefBass = idIns;
								generateBassLigne(EnumBrickSentence.BASSLIGNE1, idIns);
								idIns.setDistributionInstrument(new IdentitySentence(EnumPlaySentence.BASSLIGNE));
							} else if (this.idRefBassBis == null) {
								this.idRefBassBis = idIns;
								generateBassLigne(EnumBrickSentence.BASSLIGNE2, idIns);
								idIns.setDistributionInstrument(new IdentitySentence(EnumPlaySentence.BASSLIGNEBIS));
							} else {
								if (random.nextBoolean()) {
									generateBassLigneWithRef(EnumBrickSentence.BASSLIGNE1, idIns, this.idRefBass);
									idIns.setDistributionInstrument(new IdentitySentence(EnumPlaySentence.BASSLIGNE));
								} else {
									generateBassLigneWithRef(EnumBrickSentence.BASSLIGNE2, idIns, this.idRefBassBis);
									idIns.setDistributionInstrument(
											new IdentitySentence(EnumPlaySentence.BASSLIGNEBIS));
								}
							}
						} else if (idIns.getStyleGroupIns().getBalanceBassMelody() > randomNumber) {

							randomMelodyMelodyBis = random.nextInt(201) - 100;

							if (idIns.getStyleGroupIns().getBalanceMelodyMelodyBis() <= randomMelodyMelodyBis) {
								if (this.idRefMainMelodyBis == null) {

									this.idRefMainMelodyBis = idIns;
									if (this.idRefMainMelody == null) {
										generateMelody(EnumBrickSentence.MELODY2, idIns);
									} else {
										generateMelodyWithMelodyRef(EnumBrickSentence.MELODY2,
												EnumBrickSentence.MELODY1, idIns, this.idRefMainMelody);
									}
									idIns.setDistributionInstrument(
											new IdentitySentence(EnumPlaySentence.MAINMELODYBIS));
								} else {
									generateMelodyWithRef(EnumBrickSentence.MELODY2, idIns, this.idRefMainMelodyBis);
									idIns.setDistributionInstrument(
											new IdentitySentence(EnumPlaySentence.MAINMELODYBIS));
								}
							} else {
								if (this.idRefMainMelody == null) {

									this.idRefMainMelody = idIns;
									if (this.idRefMainMelodyBis == null) {
										generateMelody(EnumBrickSentence.MELODY1, idIns);
									} else {
										generateMelodyWithMelodyRef(EnumBrickSentence.MELODY1,
												EnumBrickSentence.MELODY2, idIns, this.idRefMainMelodyBis);
									}
									idIns.setDistributionInstrument(new IdentitySentence(EnumPlaySentence.MAINMELODY));

								} else {
									generateMelodyWithRef(EnumBrickSentence.MELODY1, idIns, this.idRefMainMelody);
									idIns.setDistributionInstrument(new IdentitySentence(EnumPlaySentence.MAINMELODY));
								}
							}
						}
					}
				} else {
					idIns.setDistributionInstrument(new IdentitySentence(EnumPlaySentence.NOTPLAYING));
				}
			}

			for (IdentityDrum idDrum : this.listIdentityDrums) {
				generatePercussionLigne(EnumBrickSentence.DRUMS, idDrum);
				idDrum.setDistributionInstrument(new IdentitySentence(EnumPlaySentence.DRUMS));
			}

			break;

		default:
			break;
		}

	}

	private List<Integer> getListIndexId() {

		List<Integer> listIndexInstruments = new ArrayList<>();

		for (int i = 0; i < this.listIdentityInstruments.size(); i++) {
			listIndexInstruments.add(i);
		}

		return listIndexInstruments;
	}

	public List<Map<Chord, EnumChordTension>> getChordsList() {
		return this.chordsList;
	}

	public List<IdentityDrum> getListIdentityDrums() {
		return listIdentityDrums;
	}

	public void setListIdentityDrums(List<IdentityDrum> listIdentityDrums) {
		this.listIdentityDrums = listIdentityDrums;
	}

	public EnumVelocity getVelocity() {
		return velocity;
	}

	public void setVelocity(EnumVelocity velocity) {
		this.velocity = velocity;
	}

	public EnumVelocityVariation getVelocityVariation() {
		return velocityVariation;
	}

	public void setVelocityVariation(EnumVelocityVariation velocityVariation) {
		this.velocityVariation = velocityVariation;
	}
}
