package musicRandom;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import enums.EnumBrickSentence;
import enums.EnumDurationNote;
import enums.EnumInstruments;
import enums.EnumNoteTonality;
import enums.EnumPlaySentence;
import enums.EnumSilence;
import enums.EnumTonalite;
import midi.MidiListNote;

public class Instrument {

	private EnumInstruments instru;
	private List<Part> listParts;
	private IdentityInstrument identityInstrument;
	private IdentityDrum identityDrum;
	private List<Note> notes;
	private Map<Part, List<Note>> partNotes;
	private EnumTonalite tonality;
	private EnumNoteTonality noteRef;
	private int rythme;

	Instrument(EnumInstruments inst, List<Part> listP, IdentityInstrument id, IdentityDrum idDrum,
			EnumTonalite tonality, int rythme, EnumNoteTonality noteR) {
		this.instru = inst;
		this.listParts = listP;
		this.identityInstrument = id;
		this.identityDrum = idDrum;
		this.tonality = tonality;
		this.rythme = rythme;

		if (noteR != null) {
			this.noteRef = noteR;
		} else {
			this.noteRef = instru.getNoteRef(instru.getCategory());
		}

		generateNotes();
	}

	// generate all the sentences and small motifs for each instruments
	private void generateMidi() {
		new MidiListNote(
				this.identityInstrument.getRepetedItem().get(EnumBrickSentence.REPETEDITEM1).get(0).getListNotes(),
				"RepetedItem" + this.instru.getName(), this.instru, this.rythme,
				this.identityInstrument.getChordDuration(), this.tonality);
		new MidiListNote(this.identityInstrument.getMotifs().get(EnumBrickSentence.MOTIF1).get(0).getListNotes(),
				"smallMotif1" + this.instru.getName(), this.instru, this.rythme,
				this.identityInstrument.getChordDuration(), this.tonality);
		new MidiListNote(this.identityInstrument.getMotifs().get(EnumBrickSentence.MOTIF2).get(0).getListNotes(),
				"smallMotif2" + this.instru.getName(), this.instru, this.rythme,
				this.identityInstrument.getChordDuration(), this.tonality);
		new MidiListNote(this.identityInstrument.getMelodies().get(EnumBrickSentence.MELODY1).get(0).getListNotes(),
				"mainMelody" + this.instru.getName(), this.instru, this.rythme,
				this.identityInstrument.getChordDuration(), this.tonality);
	}

	public List<Note> getListNotes() {
		return this.notes;
	}

	public EnumInstruments getInstrument() {
		return this.instru;
	}

	// TODO: find a better solution to generate notes depending on sentence's play
	// style
	private void generateNotes() {

		this.notes = new ArrayList<>();
		this.partNotes = new HashMap<>();
		List<Note> no;
		Note previousNote = null;
		IdentitySentence identitySentence = null;
		List<EnumDurationNote> noteDuration;
		Chord currentChord;

		for (Part p : listParts) {

			if (this.partNotes != null && this.partNotes.containsKey(p)) {
				this.notes.addAll(partNotes.get(p));
			} else {
				no = new ArrayList<>();

				for (Sentence s : p.getSentences()) {

					if (this.identityInstrument != null) {
						this.identityInstrument = s
								.getIdentityInstrumentByRefNumber(this.identityInstrument.getRefNumber());
						identitySentence = this.identityInstrument.getIdentitySentence();

						if (identitySentence.getStyle().equals(EnumPlaySentence.NOTPLAYING)) {

							List<EnumNoteTonality> listNoteNum = new ArrayList<>();
							listNoteNum.add(this.noteRef);

							for (int i = 0; i < s.getNumberChords(); i++) {
								currentChord = s.getChordsList().get(i).entrySet().iterator().next().getKey();
								noteDuration = new LinkedList<>();
								noteDuration.add(EnumDurationNote.getLongestNoteByDuration(
										this.identityInstrument.getChordDuration().getDuration() * 24));
								previousNote = new Note(previousNote, listNoteNum, noteDuration, EnumSilence.SILENCE,
										this.identityInstrument.getChordDuration(), currentChord, 0);
								this.notes.add(previousNote);
								no.add(previousNote);
							}
						} else if (identitySentence.getStyle().equals(EnumPlaySentence.BASSLIGNE)) {

							previousNote = null;
							int cursor = 0;

							for (Note not : this.identityInstrument.getBassLignes().get(EnumBrickSentence.BASSLIGNE1)
									.get(0).getListNotes()) {
								currentChord = getChordByCursorOfNote(not, s, this.identityInstrument);
								previousNote = new Note(previousNote, not.getListNoteNum(), not.getNoteDuration(),
										not.getSilenceOrPlaying(), this.identityInstrument.getChordDuration(),
										currentChord, not.getVolume());
								this.notes.add(previousNote);
								no.add(previousNote);
								cursor = cursor + previousNote.getDuration();
							}

						} else if (identitySentence.getStyle().equals(EnumPlaySentence.BASSLIGNEBIS)) {

							previousNote = null;
							int cursor = 0;

							for (Note not : this.identityInstrument.getBassLignes().get(EnumBrickSentence.BASSLIGNE2)
									.get(0).getListNotes()) {
								currentChord = getChordByCursorOfNote(not, s, this.identityInstrument);
								previousNote = new Note(previousNote, not.getListNoteNum(), not.getNoteDuration(),
										not.getSilenceOrPlaying(), this.identityInstrument.getChordDuration(),
										currentChord, not.getVolume());
								this.notes.add(previousNote);
								no.add(previousNote);
								cursor = cursor + previousNote.getDuration();
							}

						} else if (identitySentence.getStyle().equals(EnumPlaySentence.DRUMS)) {

							previousNote = null;

							for (Note not : this.identityInstrument.getPercussionLignes().get(EnumBrickSentence.DRUMS)
									.get(0).getListNotes()) {
								currentChord = getChordByCursorOfNote(not, s, this.identityInstrument);
								previousNote = new Note(previousNote, not.getListNoteNum(), not.getNoteDuration(),
										not.getSilenceOrPlaying(), this.identityInstrument.getChordDuration(),
										currentChord, not.getVolume());
								this.notes.add(previousNote);
								no.add(previousNote);
							}

						} else if (identitySentence.getStyle().equals(EnumPlaySentence.MAINMELODY)) {

							previousNote = null;
							int cursor = 0;

							for (Note not : this.identityInstrument.getMelodies().get(EnumBrickSentence.MELODY1).get(0)
									.getListNotes()) {
								currentChord = getChordByCursorOfNote(not, s, this.identityInstrument);
								previousNote = new Note(previousNote, not.getListNoteNum(), not.getNoteDuration(),
										not.getSilenceOrPlaying(), this.identityInstrument.getChordDuration(),
										currentChord, not.getVolume());
								this.notes.add(previousNote);
								no.add(previousNote);
								cursor = cursor + previousNote.getDuration();
							}

						} else if (identitySentence.getStyle().equals(EnumPlaySentence.MAINMELODYBIS)) {

							previousNote = null;

							for (Note not : this.identityInstrument.getMelodies().get(EnumBrickSentence.MELODY2).get(0)
									.getListNotes()) {
								currentChord = getChordByCursorOfNote(not, s, this.identityInstrument);
								previousNote = new Note(previousNote, not.getListNoteNum(), not.getNoteDuration(),
										not.getSilenceOrPlaying(), this.identityInstrument.getChordDuration(),
										currentChord, not.getVolume());
								this.notes.add(previousNote);
								no.add(previousNote);
							}

						}
					} else {

						this.identityDrum = s.getListIdentityDrums().get(0);

						for (Note not : this.identityDrum.getPercussionLignes().get(EnumBrickSentence.DRUMS).get(0)
								.getListNotes()) {
							currentChord = new Chord();
							previousNote = new Note(previousNote, not.getListNoteNum(), not.getNoteDuration(),
									not.getSilenceOrPlaying(), this.identityDrum.getAccordsDuration(), currentChord,
									not.getVolume());
							this.notes.add(previousNote);
							no.add(previousNote);
						}
					}

				}

				this.partNotes.put(p, no);
			}
		}
	}

	private Chord getChordByCursorOfNote(Note note, Sentence sentence, IdentityInstrument idIns) {

		int index = 0;

		if (note != null) {
			index = (note.getCursor() - note.getDuration()) / (idIns.getChordDuration().getDuration() * 24);
		}

		return sentence.getChordsList().get(index).entrySet().iterator().next().getKey();
	}

	public int getNumberMeasure() {

		int chordDuration;

		if (this.identityInstrument != null) {
			chordDuration = this.identityInstrument.getChordDuration().getDuration();
		} else {
			chordDuration = this.identityDrum.getAccordsDuration().getDuration();
		}
		int sum = 0;

		for (Note n : this.getListNotes()) {
			sum = sum + n.getDuration();
		}
		return sum / (chordDuration * 24);
	}

	public IdentityInstrument getIdentityInstrument() {
		return identityInstrument;
	}

	public void setIdentityInstrument(IdentityInstrument identityInstrument) {
		this.identityInstrument = identityInstrument;
	}

	public IdentityDrum getIdentityDrum() {
		return identityDrum;
	}

	public void setIdentityDrum(IdentityDrum identityDrum) {
		this.identityDrum = identityDrum;
	}

}
