package musicRandom;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import enums.EnumDurationNote;
import enums.EnumNoteTonality;
import enums.EnumSentence;
import enums.EnumSilence;
import enums.EnumStyle;
import enums.EnumTempsForts;
import enums.EnumVelocity;
import enums.EnumVelocityVariation;
import exceptions.MyException;
import styles.StyleGroupDrumsIns;

public class RepetedItem {

	private List<Note> listNotes;
	private int size;
	private EnumStyle style; // TODO
	private EnumVelocity velocity;
	private EnumVelocityVariation velocityVariation;

	private IdentityInstrument idInstrument;
	private IdentityDrum idDrum;

	int count = 0;
	int maxTries = 10;

	private Random random = new Random();

	RepetedItem(IdentityInstrument idInst, EnumVelocity vel, EnumVelocityVariation velVariation) throws MyException {

		this.size = idInst.getChordDuration().getDuration(); // TODO change size,must be able to be on one beat, 2 beat,
																// one measure or 2 measure, it will change the
																// regularity of music
		this.idInstrument = idInst;
		this.idDrum = null;
		this.velocity = vel;
		this.velocityVariation = velVariation;

		if (this.velocity == null) {
			this.velocity = EnumVelocity.randomEnumVelocity();
		}

		if (this.velocityVariation == null) {
			this.velocityVariation = EnumVelocityVariation.randomEnumVelocityVariation();
		}

		while (true) {
			try {
				this.listNotes = new ArrayList<Note>();
				generateRepetedItemIns();
				break;
			} catch (MyException e) {
				if (++count == maxTries)
					throw e;
			}
		}
	}

	public RepetedItem(IdentityDrum identityDrum, EnumVelocity vel, EnumVelocityVariation velVariation) {

		this.size = identityDrum.getAccordsDuration().getDuration(); // TODO change size,must be able to be on one beat,
																		// 2 beat, one measure or 2 measure, it will
																		// change the regularity of music
		this.idDrum = identityDrum;
		this.idInstrument = null;
		this.velocity = vel;
		this.velocityVariation = velVariation;

		if (this.velocity == null) {
			this.velocity = EnumVelocity.randomEnumVelocity();
		}

		if (this.velocityVariation == null) {
			this.velocityVariation = EnumVelocityVariation.randomEnumVelocityVariation();
		}

		this.listNotes = new ArrayList<>();

		generateRepetedItemDrum();
	}

	RepetedItem(RepetedItem r) {
		this.listNotes = r.listNotes;
		this.size = r.size;
		this.velocity = r.velocity;
		this.style = r.style;
		this.velocityVariation = r.velocityVariation;

		this.idInstrument = r.idInstrument;
		this.idDrum = r.idDrum;
	}

	private void generateRepetedItemDrum() {

		EnumDurationNote durationRhythm = EnumDurationNote.getByIndex(this.idDrum.getStyleGroupDrums().getRhythm());
		int numberTics = this.idDrum.getAccordsDuration().getDuration() * 24 / durationRhythm.getDuration();
		Note previousNote = null;
		List<EnumDurationNote> noteDuration = new ArrayList<>();
		List<EnumNoteTonality> noteNum;
		EnumSilence silenceOrPlaying;
		boolean isTempsFort;
		int beatNumber;

		noteDuration.add(durationRhythm);

		for (int i = 0; i < numberTics; i++) {

			isTempsFort = isTempsFort(i, durationRhythm);
			beatNumber = getBeatNumber(i, durationRhythm);
			silenceOrPlaying = this.getSilenceOrPlaying(isTempsFort, beatNumber);

			if (silenceOrPlaying.equals(EnumSilence.PLAY)) {
				noteNum = this.getListNoteNumDrum(isTempsFort, beatNumber);
			} else {
				noteNum = new ArrayList<>();
			}

			if (noteNum.isEmpty()) {
				silenceOrPlaying = EnumSilence.SILENCE;
			}

			previousNote = new Note(previousNote, noteNum, noteDuration, silenceOrPlaying,
					this.idDrum.getAccordsDuration(), new Chord(), 64);
			this.listNotes.add(previousNote);
		}
	}

	private int getBeatNumber(int i, EnumDurationNote durationRhythm) {

		int beatNumber = 0;
		int reste;

		if (durationRhythm.getDuration() < 24 && i > 0) {
			reste = (durationRhythm.getDuration() * i) % 24;
			if (reste > 0) {
				beatNumber = reste / durationRhythm.getDuration();
			}

		}

		return beatNumber;
	}

	private EnumSilence getSilenceOrPlaying(boolean isTempsFort, int beatNumber) {

		int ran = random.nextInt(101);

		// on teste d'abord que l'on joue bien sur le temps fort ou faible
		if ((isTempsFort && ran > this.idDrum.getStyleGroupDrums().getPourcentagePlayingTempsFort())
				|| (!isTempsFort && ran > this.idDrum.getStyleGroupDrums().getPourcentagePlayingTempsFaible())) {

			return EnumSilence.SILENCE;
		}

		ran = random.nextInt(101);

		switch (beatNumber) {
		case 0:
			if (ran > this.idDrum.getStyleGroupDrums().getPourcentagePlayingTemps1()) {
				return EnumSilence.SILENCE;
			}
			break;
		case 1:
			if (ran > this.idDrum.getStyleGroupDrums().getPourcentagePlayingTemps2()) {
				return EnumSilence.SILENCE;
			}
			break;
		case 2:
			if (ran > this.idDrum.getStyleGroupDrums().getPourcentagePlayingTemps3()) {
				return EnumSilence.SILENCE;
			}
			break;
		case 3:
			if (ran > this.idDrum.getStyleGroupDrums().getPourcentagePlayingTemps4()) {
				return EnumSilence.SILENCE;
			}
			break;
		default:
			return EnumSilence.SILENCE;
		}

		return EnumSilence.PLAY;
	}

	private EnumSilence getSilenceOrPlaying(boolean isTempsFort, int beatNumber, StyleGroupDrumsIns sGDI) {

		int ran = random.nextInt(101);

		// on teste d'abord que l'on joue bien sur le temps fort ou faible
		if ((isTempsFort && ran > sGDI.getPourcentagePlayingTempsFort())
				|| (!isTempsFort && ran > sGDI.getPourcentagePlayingTempsFaible())) {

			return EnumSilence.SILENCE;
		}

		ran = random.nextInt(101);

		switch (beatNumber) {
		case 0:
			if (ran > sGDI.getPourcentagePlayingTemps1()) {
				return EnumSilence.SILENCE;
			}
			break;
		case 1:
			if (ran > sGDI.getPourcentagePlayingTemps2()) {
				return EnumSilence.SILENCE;
			}
			break;
		case 2:
			if (ran > sGDI.getPourcentagePlayingTemps3()) {
				return EnumSilence.SILENCE;
			}
			break;
		case 3:
			if (ran > sGDI.getPourcentagePlayingTemps4()) {
				return EnumSilence.SILENCE;
			}
			break;
		default:
			return EnumSilence.SILENCE;
		}

		return EnumSilence.PLAY;
	}

	private List<EnumNoteTonality> getListNoteNumDrum(boolean isTempsFort, int beatNumber) {

		int indexNote;
		List<EnumNoteTonality> noteNum = new ArrayList<>();

		for (StyleGroupDrumsIns sGDI : this.idDrum.getStyleGroupDrums().getDrumsIns()) {
			if (getSilenceOrPlaying(isTempsFort, beatNumber, sGDI).equals(EnumSilence.PLAY)) {

				indexNote = sGDI.getInstruments().get(random.nextInt(sGDI.getInstruments().size()));
				noteNum.add(EnumNoteTonality.getByIndex(indexNote));
			}
		}

		return noteNum;
	}

	private boolean isTempsFort(int i, EnumDurationNote durationRhythm) {
		int beatNumber;

		if (i > 0) {
			beatNumber = (int) (durationRhythm.getDuration() * i / 24.0);
		} else {
			beatNumber = 0;
		}

		return this.idDrum.getTempsForts().contains(EnumTempsForts.getByIndex(beatNumber));
	}

	private void generateRepetedItemIns() throws MyException {

		Note previousNote = null;
		Map<Integer, Integer> mapOfDegres = new HashMap<>();
		List<Map<Integer, Integer>> listOctaveDegres = new LinkedList<>();
		int velocityValue;
		int endPart = this.size * 24; // 24 beat by black note //
										// this.idInstrument.getChordDuration().getDuration() *

		// TODO, la basse joue aussi beaucoup sur le V eme degre
		do {
			mapOfDegres.clear();
			listOctaveDegres.clear();

			if (previousNote == null && this.playTonale()) {
				mapOfDegres.put(0, 1); // octave, degre : la basse joue g�n�ralement la tonale comme premiere note
			} else {
				mapOfDegres.put(0, -1);
			}
			listOctaveDegres.add(mapOfDegres);

			velocityValue = estimateVelocity(previousNote, endPart);

			previousNote = new Note(previousNote, this.listNotes, this.idInstrument.getNoteRef(),
					this.idInstrument.getRefChord(), this.idInstrument.getTonality(),
					this.idInstrument.getAllowedRythme(), this.idInstrument.getGapBetweenNotes(),
					this.idInstrument.getStyle(), this.idInstrument.getInstrument(),
					this.idInstrument.getPourcentageSilences(), this.idInstrument.getChordDuration(),
					this.idInstrument.getTempsForts(), null, EnumSentence.SENTENCE, endPart, false, false, false,
					listOctaveDegres, null, null, this.idInstrument.getNumberNotesPlayedSimultaneously(), velocityValue,
					this.idInstrument.getStyleGroupIns());
			this.listNotes.add(previousNote);
		} while (previousNote.getCursor() < endPart);
	}

	private boolean playTonale() {

		int pChanceTonale = 0;

		if (this.idInstrument != null) {
			pChanceTonale = this.idInstrument.getStyleGroupIns().getpChanceStartByTonaleAsBass();
		}

		return this.random.nextInt(100) < pChanceTonale;
	}

	private int estimateVelocity(Note previousNote, int endPart) {

		double ratio = 0;
		int velocityStart = this.velocity.getValue();
		int velocityEnd = this.velocity.getVelocityAfterVariation(this.velocityVariation).getValue();

		if (previousNote != null) {
			ratio = (double) previousNote.getCursor() / (double) endPart;
		}

		return (int) (ratio * (velocityEnd - velocityStart) + velocityStart);
	}

	public int getSize() {
		return this.size;
	}

	public List<Note> getListNotes() {
		return this.listNotes;
	}

	public IdentityDrum getIdDrum() {
		return idDrum;
	}

	public void setIdDrum(IdentityDrum idDrum) {
		this.idDrum = idDrum;
	}

}
