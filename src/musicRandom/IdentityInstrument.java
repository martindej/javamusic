package musicRandom;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import enums.EnumBrickSentence;
import enums.EnumChordNumber;
import enums.EnumChordTension;
import enums.EnumDegre;
import enums.EnumDurationChord;
import enums.EnumDurationNote;
import enums.EnumInstrumentCategories;
import enums.EnumInstruments;
import enums.EnumNoteTonality;
import enums.EnumSentence;
import enums.EnumStyle;
import enums.EnumTempsForts;
import enums.EnumTonalite;
import enums.EnumVelocity;
import enums.EnumVelocityVariation;
import exceptions.MyException;
import styles.Style;
import styles.StyleGroupIns;

public class IdentityInstrument {

	private Map<EnumDurationNote, Integer> allowedRythmes;
	private List<Part> listParts;
	private Map<EnumBrickSentence, List<Motif>> motifs;
	private Map<EnumBrickSentence, List<RepetedItem>> repetedItem;

	private Map<EnumBrickSentence, List<Melody>> melodies;
	private Map<EnumBrickSentence, List<BassLigne>> bassLignes;
	private Map<EnumBrickSentence, List<PercussionLigne>> percussionLignes;
	private int gapBetweenNotes;
	private int pourcentageSilences;
	private EnumStyle style;
	private EnumTonalite tonality;
	private EnumDurationChord accordsDuration;
	private Chord refChord;
	private int rythme;
	private List<EnumTempsForts> tempsForts;
	private EnumInstruments instrument;
	private EnumNoteTonality noteRef;
	private EnumInstrumentCategories instrumentsCategory;
	// TODO change that Map in List or multimap,currently the order is not sure and
	// there isn't duplicate keys
	private List<Map<Chord, EnumChordTension>> chordsList;
	private EnumSentence sentence;
	private int numberNotesSimultaneously;

	private IdentitySentence distributionInstrument;

	private int refNumber;

	private Style myStyle;

	private StyleGroupIns styleGroupIns;

	IdentityInstrument(Map<EnumDurationNote, Integer> allowedR, Map<EnumBrickSentence, List<Motif>> mots,
			Map<EnumBrickSentence, List<RepetedItem>> repetedI, Map<EnumBrickSentence, List<Melody>> mainM,
			Map<EnumBrickSentence, List<BassLigne>> bass, int pourcentageSilences, EnumStyle style,
			List<Part> listParts, EnumTonalite tonality, EnumDurationChord accordsDuration, List<EnumTempsForts> tForts,
			int rythme, EnumInstruments inst, EnumInstrumentCategories iCategory, int refN, int nNotesSimultaneously,
			Style s, StyleGroupIns styleGroupIns) {

		this.tonality = tonality;
		this.allowedRythmes = allowedR;
		this.motifs = mots;
		this.repetedItem = repetedI;
		this.melodies = mainM;
		this.bassLignes = bass;
		this.style = style;
		this.accordsDuration = accordsDuration;
		this.tempsForts = tForts;
		this.rythme = rythme;
		this.instrument = inst;
		this.instrumentsCategory = iCategory;
		this.noteRef = inst.getNoteRef(this.instrumentsCategory);
		this.refNumber = refN;
		this.numberNotesSimultaneously = nNotesSimultaneously;
		this.myStyle = s;
		this.styleGroupIns = styleGroupIns;

		this.refChord = new Chord(EnumChordNumber.ACCORD_I, EnumDegre.I);

		if (this.numberNotesSimultaneously == -1) {
			if (this.instrument.getMultipleNotes()) {
				this.numberNotesSimultaneously = this.styleGroupIns.getNumberNotesSimultaneously();
			} else {
				this.numberNotesSimultaneously = 1;
			}
		}

		if (this.allowedRythmes == null) {
			chooseAllowedRythmes();
		}

		if (this.style == null) {
			this.style = EnumStyle.randomEnumStyle();
		}

		this.gapBetweenNotes = this.styleGroupIns.getgBetweenNotes();
		this.pourcentageSilences = this.styleGroupIns.getPourcentageSil();

		if (this.motifs == null || (this.motifs != null && this.motifs.isEmpty())) {
			this.motifs = new EnumMap<>(EnumBrickSentence.class);
		}

		if (this.repetedItem == null || (this.repetedItem != null && this.repetedItem.isEmpty())) {
			this.repetedItem = new EnumMap<>(EnumBrickSentence.class);
		}

		if (this.melodies == null || (this.melodies != null && this.melodies.isEmpty())) {
			this.melodies = new EnumMap<>(EnumBrickSentence.class);
		}

		if (this.bassLignes == null || (this.bassLignes != null && this.bassLignes.isEmpty())) {
			this.bassLignes = new EnumMap<>(EnumBrickSentence.class);
		}

		this.percussionLignes = new EnumMap<>(EnumBrickSentence.class);
	}

	public void generateRepetedItem() throws MyException {
		this.repetedItem = new EnumMap<>(EnumBrickSentence.class);

		RepetedItem newRepetedItem = new RepetedItem(this, null, null);
		ArrayList<RepetedItem> repetedIt = new ArrayList<>();
		repetedIt.add(newRepetedItem);
		this.repetedItem.put(EnumBrickSentence.REPETEDITEM1, repetedIt);
	}

	public void generateMotifs() throws MyException {

		this.motifs = new EnumMap<>(EnumBrickSentence.class);

		Motif newMotif = new Motif(-1, this, null, null, EnumBrickSentence.MOTIF1);
		ArrayList<Motif> motis = new ArrayList<>();
		motis.add(newMotif);
		this.motifs.put(EnumBrickSentence.MOTIF1, motis);

		newMotif = new Motif(-1, this, null, null, EnumBrickSentence.MOTIF2);
		motis = new ArrayList<>();
		motis.add(newMotif);
		this.motifs.put(EnumBrickSentence.MOTIF2, motis);
	}

	public void generateMotifsWithRef(Map<EnumBrickSentence, List<Motif>> map) throws MyException {

		int sizeOne = map.get(EnumBrickSentence.MOTIF1).get(0).getSize();
		int sizeTwo = map.get(EnumBrickSentence.MOTIF2).get(0).getSize();

		this.motifs = new EnumMap<>(EnumBrickSentence.class);

		Motif newMotif = new Motif(sizeOne, this, null, null, EnumBrickSentence.MOTIF1);
		ArrayList<Motif> motis = new ArrayList<>();
		motis.add(newMotif);
		this.motifs.put(EnumBrickSentence.MOTIF1, motis);

		newMotif = new Motif(sizeTwo, this, null, null, EnumBrickSentence.MOTIF2);
		motis = new ArrayList<>();
		motis.add(newMotif);
		this.motifs.put(EnumBrickSentence.MOTIF2, motis);
	}

	// used to clone without references
	IdentityInstrument(IdentityInstrument idIns) {

		this.myStyle = idIns.myStyle;
		this.chordsList = idIns.chordsList;
		this.allowedRythmes = idIns.allowedRythmes;
		this.listParts = idIns.listParts;
		this.motifs = idIns.createCopyMotifs(Motif.class);
		this.repetedItem = idIns.createCopyRepetedItems();

		this.melodies = idIns.createCopyMelodies();
		this.bassLignes = idIns.createCopyBass();
		this.percussionLignes = idIns.createCopyPercussions();
		this.gapBetweenNotes = idIns.gapBetweenNotes;
		this.pourcentageSilences = idIns.pourcentageSilences;
		this.style = idIns.style;
		this.tonality = idIns.tonality;
		this.accordsDuration = idIns.accordsDuration;
		this.refChord = idIns.refChord;
		this.rythme = idIns.rythme;
		this.tempsForts = idIns.tempsForts;
		this.instrument = idIns.instrument;
		this.noteRef = idIns.noteRef;
		this.instrumentsCategory = idIns.instrumentsCategory;
		this.refNumber = idIns.refNumber;
		this.numberNotesSimultaneously = idIns.numberNotesSimultaneously;

		this.styleGroupIns = idIns.styleGroupIns;

		this.distributionInstrument = idIns.distributionInstrument;
	}

//	private Map<EnumBrickSentence, List<Melody>> createCopyMelodies() {
//
//		Map<EnumBrickSentence, List<Melody>> copy = new EnumMap<>(EnumBrickSentence.class);
//		List<Melody> listCopy;
//
//		for (Map.Entry<EnumBrickSentence, List<Melody>> list : this.melodies.entrySet()) {
//
//			listCopy = new ArrayList<>();
//			for (Melody mel : list.getValue()) {
//				listCopy.add(new Melody(mel));
//			}
//			copy.put(list.getKey(), listCopy);
//		}
//
//		return copy;
//	}
//
//	private Map<EnumBrickSentence, List<Motif>> createCopyMotifs() {
//
//		Map<EnumBrickSentence, List<Motif>> copy = new EnumMap<>(EnumBrickSentence.class);
//		List<Motif> listCopy;
//
//		for (Map.Entry<EnumBrickSentence, List<Motif>> list : this.motifs.entrySet()) {
//
//			listCopy = new ArrayList<>();
//			for (Motif mot : list.getValue()) {
//				listCopy.add(new Motif(mot));
//			}
//			copy.put(list.getKey(), listCopy);
//		}
//
//		return copy;
//	}
//
//	private Map<EnumBrickSentence, List<RepetedItem>> createCopyRepetedItems() {
//
//		Map<EnumBrickSentence, List<RepetedItem>> copy = new EnumMap<>(EnumBrickSentence.class);
//		List<RepetedItem> listCopy;
//
//		for (Map.Entry<EnumBrickSentence, List<RepetedItem>> list : this.repetedItem.entrySet()) {
//
//			listCopy = new ArrayList<>();
//			for (RepetedItem rep : list.getValue()) {
//				listCopy.add(new RepetedItem(rep));
//			}
//			copy.put(list.getKey(), listCopy);
//		}
//
//		return copy;
//	}
//
//	private Map<EnumBrickSentence, List<BassLigne>> createCopyBass() {
//
//		Map<EnumBrickSentence, List<BassLigne>> copy = new EnumMap<>(EnumBrickSentence.class);
//		List<BassLigne> listCopy;
//
//		for (Map.Entry<EnumBrickSentence, List<BassLigne>> list : this.bassLignes.entrySet()) {
//
//			listCopy = new ArrayList<>();
//			for (BassLigne bas : list.getValue()) {
//				listCopy.add(new BassLigne(bas));
//			}
//			copy.put(list.getKey(), listCopy);
//		}
//
//		return copy;
//	}

	public <T> void insertData(Class<T> clazz, String fileName) {
		List<T> newList = new ArrayList<>();
	}

	private Map<EnumBrickSentence, List<T>> createCopy(Class<T> classOfCopy) {

		Map<EnumBrickSentence, List<T>> copy = new EnumMap<>(EnumBrickSentence.class);
		List<BassLigne> listCopy;

		for (Map.Entry<EnumBrickSentence, List<BassLigne>> list : this.bassLignes.entrySet()) {

			listCopy = new ArrayList<>();
			for (BassLigne bas : list.getValue()) {
				listCopy.add(new BassLigne(bas));
			}
			copy.put(list.getKey(), listCopy);
		}

		return copy;
	}

//	private Map<EnumBrickSentence, List<PercussionLigne>> createCopyPercussions() {
//
//		Map<EnumBrickSentence, List<PercussionLigne>> copy = new HashMap<EnumBrickSentence, List<PercussionLigne>>();
//		List<PercussionLigne> listCopy = new ArrayList<PercussionLigne>();
//
//		for (Map.Entry<EnumBrickSentence, List<PercussionLigne>> list : this.percussionLignes.entrySet()) {
//
//			listCopy = new ArrayList<PercussionLigne>();
//			for (PercussionLigne per : list.getValue()) {
//				listCopy.add(new PercussionLigne(per));
//			}
//			copy.put(list.getKey(), listCopy);
//		}
//
//		return copy;
//	}

	public int getNumberNotesPlayedSimultaneously() {
		return this.numberNotesSimultaneously;
	}

	public void setEnumSentence(EnumSentence s) {
		this.sentence = s;
	}

	public EnumInstrumentCategories getCategory() {
		return this.instrumentsCategory;
	}

	public Style getStyleMusic() {
		return this.myStyle;
	}

	public void setDistributionInstrument(IdentitySentence idS) {
		this.distributionInstrument = idS;
	}

	public IdentitySentence getIdentitySentence() {
		return this.distributionInstrument;
	}

	public void setChords(List<Map<Chord, EnumChordTension>> ac) {
		this.chordsList = ac;
	}

	public void generateMelody(List<EnumTempsForts> tempsFortTension, EnumBrickSentence brick, EnumVelocity velocity,
			EnumVelocityVariation velocityVariation) throws MyException {

		ArrayList<Melody> melodiesGenerated = new ArrayList<>();
		melodiesGenerated.add(new Melody(this, tempsFortTension, velocity, velocityVariation));

		this.melodies.put(brick, melodiesGenerated);
	}

	public void generateBassLigne(List<EnumTempsForts> tempsFortTension, EnumBrickSentence brick, EnumVelocity velocity,
			EnumVelocityVariation velocityVariation) throws MyException {

		ArrayList<BassLigne> bassLignesGenerated = new ArrayList<>();
		bassLignesGenerated.add(new BassLigne(this, tempsFortTension, velocity, velocityVariation));

		this.bassLignes.put(brick, bassLignesGenerated);
	}

	public void generatePercussionLigne(List<EnumTempsForts> tempsFortTension, EnumBrickSentence brick) {

		ArrayList<PercussionLigne> percussionLignesGenerated = new ArrayList<>();
		percussionLignesGenerated.add(new PercussionLigne(this, tempsFortTension));

		this.percussionLignes.put(brick, percussionLignesGenerated);
	}

	public void generateMainMelodyWithRef(List<Melody> list, EnumBrickSentence brick,
			Map<EnumDurationNote, Integer> allowedRythmesOfRefInstrument) throws MyException {

		ArrayList<Melody> melodies = new ArrayList<Melody>();
		melodies.add(new Melody(this, list.get(0), allowedRythmesOfRefInstrument));

		this.melodies.put(brick, melodies);
	}

	public void generateBassLigneWithRef(List<BassLigne> list, EnumBrickSentence brick,
			Map<EnumDurationNote, Integer> allowedRythmesOfRefInstrument) throws MyException {

		ArrayList<BassLigne> bassLignes = new ArrayList<BassLigne>();
		bassLignes.add(new BassLigne(this, list.get(0), allowedRythmesOfRefInstrument));

		this.bassLignes.put(brick, bassLignes);
	}

	public void generateMainMelodyWithMelodyRef(List<Melody> list, List<EnumTempsForts> tempsFortTension,
			EnumBrickSentence brick, EnumVelocity velocity, EnumVelocityVariation velocityVariation)
			throws MyException {

		ArrayList<Melody> melodies = new ArrayList<Melody>();
		melodies.add(new Melody(this, tempsFortTension, velocity, velocityVariation,
				list.get(0).getPlacementMotifsMelody()));

		this.melodies.put(brick, melodies);

	}

	public EnumInstruments getInstrument() {
		return this.instrument;
	}

	public Chord getRefChord() {
		return this.refChord;
	}

	public EnumDurationChord getChordDuration() {
		return this.accordsDuration;
	}

	public Map<EnumBrickSentence, List<Motif>> getMotifs() {
		return this.motifs;
	}

	public Map<EnumBrickSentence, List<RepetedItem>> getRepetedItem() {
		return this.repetedItem;
	}

	public Map<EnumBrickSentence, List<Melody>> getMelodies() {
		return this.melodies;
	}

	public Map<EnumBrickSentence, List<BassLigne>> getBassLignes() {
		return this.bassLignes;
	}

	public Map<EnumBrickSentence, List<PercussionLigne>> getPercussionLignes() {
		return this.percussionLignes;
	}

	public Map<EnumDurationNote, Integer> removeRythmInstrumentCantPlay(
			Map<EnumDurationNote, Integer> allowedRythmesOfRefInstrument) {

		Map<EnumDurationNote, Integer> allowedRythmesOfRefInstrumentUpdated = new HashMap<EnumDurationNote, Integer>();

		for (Map.Entry<EnumDurationNote, Integer> entry : allowedRythmesOfRefInstrument.entrySet()) {
			if (entry.getKey().getCoefRythme() * this.rythme < this.instrument.getRythmeMax()) {
				allowedRythmesOfRefInstrumentUpdated.put(entry.getKey(), entry.getValue());
			}
		}
		return allowedRythmesOfRefInstrumentUpdated;
	}

	// TODO : change this selection depend on style, and choosing matching rythmes
	private void chooseAllowedRythmes() {

		this.allowedRythmes = new EnumMap<>(EnumDurationNote.class);
		int rythmeNumber;
		int weightRythm;

		List<EnumDurationNote> choosedRythms = new ArrayList<>();
		List<EnumDurationNote> allPossibilities = getListRythmInstrumentCanPlay();

		rythmeNumber = this.styleGroupIns.getNumberRythm();

		if (rythmeNumber > allPossibilities.size()) {
			rythmeNumber = allPossibilities.size();
		}

		do {
			EnumDurationNote e = EnumDurationNote.randomEnumDurationWithExclusions(choosedRythms, this.accordsDuration);
			choosedRythms.add(e);

			if (allPossibilities.contains(e)) {
				if (!this.allowedRythmes.containsKey(e)) {

					weightRythm = this.styleGroupIns.getRandomWeight();
					weightRythm = (int) (weightRythm * this.styleGroupIns.getAllowedR().get(e));

					if (e.isTernaire()) {
						weightRythm = (int) (weightRythm * (100 - this.styleGroupIns.getbBinaireTernaire()) / 100.0);
					} else {
						weightRythm = (int) (weightRythm * this.styleGroupIns.getbBinaireTernaire() / 100.0);
					}
					this.allowedRythmes.put(e, weightRythm);
				}
			} else {
				this.allowedRythmes.put(e, 0);
			}
		} while (this.allowedRythmes.size() < EnumDurationNote.getList(this.accordsDuration).size());

		if (this.allowedRythmes.get(EnumDurationNote.CROCHEPOINTEE) != 0
				&& this.allowedRythmes.get(EnumDurationNote.DOUBLECROCHE) == 0) {
			this.allowedRythmes.put(EnumDurationNote.CROCHEPOINTEE, 0);
		}

		if (this.allowedRythmes.get(EnumDurationNote.NOIREPOINTEE) != 0
				&& this.allowedRythmes.get(EnumDurationNote.DOUBLECROCHE) == 0
				&& this.allowedRythmes.get(EnumDurationNote.CROCHE) == 0) {
			this.allowedRythmes.put(EnumDurationNote.NOIREPOINTEE, 0);
		}

	}

	private List<EnumDurationNote> getListRythmInstrumentCanPlay() {

		List<EnumDurationNote> all = new ArrayList<>();

		for (EnumDurationNote duration : EnumDurationNote.getList(this.accordsDuration)) {
			if (duration.getCoefRythme() * this.rythme < this.instrument.getRythmeMax()) {
				all.add(duration);
			}
		}
		return all;
	}

	public int getPourcentageSilences() {
		return pourcentageSilences;
	}

	public EnumTonalite getTonality() {
		return this.tonality;
	}

	public Map<EnumDurationNote, Integer> getAllowedRythme() {
		return this.allowedRythmes;
	}

	public int getGapBetweenNotes() {
		return this.gapBetweenNotes;
	}

	public EnumStyle getStyle() {
		return this.style;
	}

	public int getRefNumber() {
		return this.refNumber;
	}

	public EnumNoteTonality getNoteRef() {
		return this.noteRef;
	}

	public int getRythm() {
		return this.rythme;
	}

	public List<EnumTempsForts> getTempsForts() {
		return this.tempsForts;
	}

	public List<Map<Chord, EnumChordTension>> getChordsList() {
		return this.chordsList;
	}

	public EnumSentence getSentence() {
		return this.sentence;
	}

	public StyleGroupIns getStyleGroupIns() {
		return this.styleGroupIns;
	}

	public void setStyleGroupIns(StyleGroupIns styleGroupIns) {
		this.styleGroupIns = styleGroupIns;
	}
}
