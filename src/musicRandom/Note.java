package musicRandom;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import enums.EnumChordTension;
import enums.EnumDurationChord;
import enums.EnumDurationNote;
import enums.EnumInstruments;
import enums.EnumNoteTonality;
import enums.EnumSentence;
import enums.EnumSilence;
import enums.EnumStyle;
import enums.EnumTempsForts;
import enums.EnumTonalite;
import enums.EnumVelocity;
import exceptions.MyException;
import styles.StyleGroupIns;

public class Note {

	private List<EnumNoteTonality> listNoteNum;
	private List<EnumDurationNote> noteDuration;// the note duration can be multiple duration on the row
	private EnumSilence silenceOrPlaying;
	private EnumSentence sentenceType;
	private List<EnumTempsForts> tempsForts;
	private EnumDurationChord accordsDuration;
	private EnumInstruments instrument;
	private EnumNoteTonality noteRef;
	private Map<EnumDurationNote, Integer> allowedRythmesForThisInstrument;
	private StyleGroupIns styleGroupIns;
	private int cursor;
	private int cursorMidi;
	private EnumChordTension tension;
	private List<EnumTempsForts> tempsFortTension;
	private int volume;
	private int numberNotesSimultaneously;
	private boolean chordPlaying;
	private Chord currentChord;
	private boolean dissonant;
	private List<Map<Integer, Integer>> listOctaveDegres;

	private Note previousNote;

	private Random random = new Random();

	public Note(Note previousNote, List<EnumNoteTonality> noteNum, List<EnumDurationNote> noteDuration,
			EnumSilence silenceOrPlaying, EnumDurationChord aDuration, Chord currentChord, int vol) {

		this.listNoteNum = noteNum;
		this.noteDuration = noteDuration;
		this.silenceOrPlaying = silenceOrPlaying;
		this.accordsDuration = aDuration;
		this.currentChord = currentChord;
		this.volume = vol;
		this.dissonant = false;
		this.previousNote = previousNote;

		setCursor(previousNote);
	}

	Note(Note previousNote, List<Note> listNotes, EnumNoteTonality noteR, Chord currentChord,
			EnumTonalite currentTonality, Map<EnumDurationNote, Integer> allowedRythmes, int gapBetweenNotes,
			EnumStyle style, EnumInstruments instru, int pourcentageSilences, EnumDurationChord aDuration,
			List<EnumTempsForts> tForts, EnumDurationNote nDuration, EnumSentence sType, int endPart, boolean lastChord,
			boolean silence, boolean play, List<Map<Integer, Integer>> lOctaveDegres, EnumChordTension tens,
			List<EnumTempsForts> tempsFortTension, int nNotesSimultaneously, int vol, StyleGroupIns styleGroupIns)
			throws MyException {

		this.sentenceType = sType;
		this.tempsForts = tForts;
		this.accordsDuration = aDuration;
		this.instrument = instru;
		this.noteRef = noteR;
		this.tension = tens;
		this.tempsFortTension = tempsFortTension;
		this.numberNotesSimultaneously = nNotesSimultaneously;
		this.listOctaveDegres = lOctaveDegres;
		this.volume = vol;
		this.styleGroupIns = styleGroupIns;
		this.currentChord = currentChord;
		this.dissonant = false;

		this.noteDuration = new LinkedList<>();

		this.previousNote = previousNote;

		if (this.volume == -1) {
			this.volume = EnumVelocity.MF.getValue();
		}

		this.allowedRythmesForThisInstrument = allowedRythmes;

		int onBeatValue = onBeat();
		boolean contreTemps = false;

		if (onBeatValue == 0) {
			int randomValue = random.nextInt(101);

			if (randomValue < this.styleGroupIns.getpContreTemps()) {
				contreTemps = true;
			}
		}
		chooseRythme(previousNote, noteFinal(previousNote, endPart), lastChord, nDuration, onBeatValue, contreTemps);

		if (this.numberNotesSimultaneously > 1) {
			chooseIfChordPlaying();
			if (!this.chordPlaying) {
				this.numberNotesSimultaneously = 1;
			}
		}

		boolean finalNote = noteFinal(previousNote, endPart) == 0;
		chooseNote(style, currentTonality, pourcentageSilences, finalNote, listNotes);

		chooseSilenceOrPlay(pourcentageSilences, play, silence, nDuration, contreTemps);
	}

	private void chooseIfChordPlaying() {
		// plus le rythme est lent, plus il y a de chances d'avoir un accord

		boolean onBeat = this.onBeat() == 0;

		if (onBeat || (this.previousNote != null && this.previousNote.chordPlaying)) {
			// cas ou la note precedente n'est pas sur le temps et est un accord => on joue
			// un accord
			if (this.previousNote != null && this.previousNote.chordPlaying && this.previousNote.onBeat() != 0) {
				this.chordPlaying = true;
			} else {
				this.chordPlaying = random.nextInt(100) < this.styleGroupIns.getpChancePlayChord();
			}
		} else {
			this.chordPlaying = false;
		}
	}

	public int getDuration() {
		int duration = 0;

		for (EnumDurationNote d : this.noteDuration) {
			duration = duration + d.getDuration();
		}

		return duration;
	}

	private void chooseSilenceOrPlay(int pourcentageSilences, boolean play, boolean silence, EnumDurationNote nDuration,
			boolean contreTemps) {
		// in the refrain, there is less silences
		if (this.sentenceType.equals(EnumSentence.REFRAIN)) {
			pourcentageSilences = pourcentageSilences / 2;
		}

		this.silenceOrPlaying = EnumSilence.getRandomEnumSilenceByPourcentage(pourcentageSilences);

		if ((this.sentenceType.equals(EnumSentence.REFRAIN) && this.getDuration() > 48) || play
				|| (!contreTemps && timeRemainingUntilTempsFort(this.previousNote) == 0)) {
			this.silenceOrPlaying = EnumSilence.PLAY;
		}

		if (((!allowedRythmesForThisInstrument.containsKey(nDuration)
				|| allowedRythmesForThisInstrument.containsKey(nDuration)
						&& allowedRythmesForThisInstrument.get(nDuration) == 0)
				&& this.noteDuration.get(0) == nDuration) || silence
				|| (contreTemps && this.getDuration() < EnumDurationNote.NOIRE.getDuration())) {
			this.silenceOrPlaying = EnumSilence.SILENCE;
		}
	}

	private int noteFinal(Note previousNote, int endPart) {
		if (previousNote == null) {
			return endPart;
		} else {
			return (endPart - previousNote.cursor);
		}
	}

	public void resetCursor() {
		this.cursor = 0;
	}

	private void chooseNote(EnumStyle style, EnumTonalite tonality, int pourcentageSilences, boolean finalNote,
			List<Note> listNotes) throws MyException {

		boolean onBeat = false;
		boolean isTempsFort = false;

		if (onBeat() == 0) {
			onBeat = true;
		}

		isTempsFort = this.isTensionTempsFort(previousNote);

		this.listNoteNum = new ArrayList<>();
		EnumNoteTonality noteChoosed;
		EnumNoteTonality previous;
		Map.Entry<Integer, Integer> entry;
		int degre;
		int octaveDif;

		if (this.previousNote == null) {
			previous = this.noteRef;
		} else {
			previous = previousNote.getListNoteNum().get(0);
		}

		// TODO change that loop to oblige to have the number of note in chord
		for (int i = 0; i < this.numberNotesSimultaneously; i++) {

			if (!this.listOctaveDegres.isEmpty() && this.listOctaveDegres.size() > i) {
				// get the first element in the map
				entry = this.listOctaveDegres.get(i).entrySet().iterator().next();
				degre = entry.getValue();
				octaveDif = entry.getKey();
			} else {
				degre = -1;
				octaveDif = 0;
			}

			noteChoosed = previous.getNextNoteRandom(previousNote, this.noteRef, this.styleGroupIns.getgBetweenNotes(),
					this.styleGroupIns.getDispersionNotes(), style, this.instrument, tonality, currentChord, onBeat,
					isTempsFort, canBeDissonant(), degre, octaveDif, this.tension, isTensionTempsFort(previousNote),
					this.styleGroupIns.getpChanceStayAroundNoteRef(), listNotes,
					this.styleGroupIns.getpChanceSameNoteInMotif());
			if (!this.listNoteNum.contains(noteChoosed)) {
				this.listNoteNum.add(noteChoosed);
			}

			if (!currentChord.getListNotes(tonality).contains(noteChoosed.getNote())) {
				this.dissonant = true;
			}
		}

	}

	private boolean canBeDissonant() {

		int dissonanceRandom = random.nextInt(101);
		return this.styleGroupIns.getpDissonance() >= dissonanceRandom;
	}

	private void chooseRythme(Note previousNote, int toEnd, boolean lastChord, EnumDurationNote nDuration,
			int onBeatValue, boolean contreTemps) throws MyException {

		boolean ternaire = ternaireMode(previousNote, onBeatValue);
		int toNextBeat = 24 - onBeatValue;
		EnumDurationNote durationNote = null;

		if (nDuration != null) {
			durationNote = nDuration;
		} else if (previousNote == null || onBeatValue == 0) {
			durationNote = EnumDurationNote.randomSelectedEnumDuration(this.accordsDuration,
					this.allowedRythmesForThisInstrument, toEnd, timeRemainingUntilTempsFort(previousNote),
					this.styleGroupIns.getpSyncopes(), contreTemps);
		} else if (lastChord) {
			durationNote = EnumDurationNote.randomSelectedEnumDurationLastChord(this.accordsDuration,
					this.allowedRythmesForThisInstrument, toNextBeat, toEnd, timeRemainingUntilTempsFort(previousNote));
		} else if (ternaire) {
			durationNote = EnumDurationNote.randomSelectedOutOfBeatTernaire(this.accordsDuration,
					this.allowedRythmesForThisInstrument, toNextBeat, toEnd, timeRemainingUntilTempsFort(previousNote),
					this.styleGroupIns.getpSyncopes(), this.styleGroupIns.getpSyncopesTempsForts(),
					this.styleGroupIns.getpSyncopettes());
		} else {
			durationNote = EnumDurationNote.randomSelectedOutOfBeatBinaire(this.accordsDuration,
					this.allowedRythmesForThisInstrument, toNextBeat, toEnd, timeRemainingUntilTempsFort(previousNote),
					this.styleGroupIns.getpSyncopes(), this.styleGroupIns.getpSyncopesTempsForts(),
					this.styleGroupIns.getpSyncopettes());
		}

		if (durationNote == null) {
			throw new MyException();
		}
		this.noteDuration.add(durationNote);

		setCursor(previousNote);
	}

	private void setCursor(Note previousNote) {

		if (previousNote == null) {
			this.cursor = this.getDuration();
			this.cursorMidi = this.getDuration();
		} else {
			this.cursor = previousNote.getCursor() + this.getDuration();
			this.cursorMidi = previousNote.getCursor() + this.getDuration();
		}
	}

	private int onBeat() {
		if (this.previousNote == null) {
			return 0;
		} else {
			return this.previousNote.getCursor() % 24;
		}
	}

	private boolean ternaireMode(Note previousNote, int onBeatValue) {
		if (onBeatValue != 0 && previousNote.getNoteDuration().get(0).isTernaire()) {
			return true;
		} else {
			return false;
		}
	}

	private boolean isLastNote(Note previousNote) {

		int indexNote;
		int indexPreviousNote;

		indexNote = this.cursor / (this.accordsDuration.getDuration() * 24);

		if (previousNote == null) {
			indexPreviousNote = 0;
		} else {
			indexPreviousNote = previousNote.getCursor() / (this.accordsDuration.getDuration() * 24);
		}

		if (indexNote > indexPreviousNote) {
			return true;
		} else {
			return false;
		}

	}

	// TODO: prendre un temps aleatoire dans la mesure et se forcer a le jouer a
	// toute les mesures
	private boolean isTensionTempsFort(Note previousNote) {

		int temp;

		if (previousNote == null) {
			temp = 0;
		} else {
			temp = previousNote.cursor % (this.accordsDuration.getDuration() * 24);
		}

		if (this.tempsFortTension != null) {
			for (EnumTempsForts eTF : this.tempsFortTension) {
				if (eTF.getCursor() == temp) {
					return true;
				}
			}
			return false;

		} else {
			return false;
		}
	}

	private int timeRemainingUntilTempsFort(Note previousNote) {

		int temp = 0;
		int remaining = 999;

		if (previousNote == null) {
			temp = 0;
		} else {
			temp = previousNote.cursor % (this.accordsDuration.getDuration() * 24);
		}

		for (EnumTempsForts i : this.tempsForts) {
			if ((i.getCursor() - temp) > 0 && (i.getCursor() - temp) < remaining) {
				remaining = i.getCursor() - temp;
			}
			if ((i.getCursor() + this.accordsDuration.getDuration() * 24 - temp) > 0
					&& (i.getCursor() + this.accordsDuration.getDuration() * 24 - temp) < remaining) {
				remaining = i.getCursor() + this.accordsDuration.getDuration() * 24 - temp;
			}
		}

		return remaining;
	}

	private int onTempsFort(Note previousNote) {

		int temp = 0;
		int remaining = 999;

		if (previousNote == null) {
			temp = 0;
		} else {
			temp = previousNote.cursor % (this.accordsDuration.getDuration() * 24);
		}

		for (EnumTempsForts i : this.tempsForts) {
			if ((i.getCursor() - temp) >= 0 && (i.getCursor() - temp) < remaining) {
				remaining = i.getCursor() - temp;
			}
			if ((i.getCursor() + this.accordsDuration.getDuration() * 24 - temp) > 0
					&& (i.getCursor() + this.accordsDuration.getDuration() * 24 - temp) < remaining) {
				remaining = i.getCursor() + this.accordsDuration.getDuration() * 24 - temp;
			}
		}

		return remaining;
	}

	public List<EnumNoteTonality> getListNoteNum() {
		return listNoteNum;
	}

	public void setNoteNum(List<EnumNoteTonality> notes) {
		this.listNoteNum = notes;
	}

	public List<EnumDurationNote> getNoteDuration() {
		return this.noteDuration;
	}

	public EnumSilence getSilenceOrPlaying() {
		return this.silenceOrPlaying;
	}

	public int getVolume() {
		return this.volume;
	}

	public int getCursor() {
		return this.cursor;
	}

	public int getCursorMidi() {
		return this.cursorMidi;
	}

	public Note getPreviousNote() {
		return this.previousNote;
	}

	public Chord getCurrentChord() {
		return currentChord;
	}

	public void setCurrentChord(Chord currentChord) {
		this.currentChord = currentChord;
	}

	public boolean isDissonant() {
		return this.dissonant;
	}

}
