package musicRandom;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import enums.EnumChordNumber;
import enums.EnumDegre;
import enums.EnumNote;
import enums.EnumTonalite;
import styles.StyleCoefDegres;

public class Chord {

	private EnumChordNumber number;
	private EnumDegre degre;
	
	private Random RANDOM = new Random();
	
	public Chord() {
		this.degre = EnumDegre.randomEnumDegre();
		this.number = EnumChordNumber.randomEnumNumberAccord();
	}
	
	Chord(List<Chord> allowedChords) {
		
		Chord c = allowedChords.get(RANDOM.nextInt(allowedChords.size()));
		this.degre = c.getDegre();
		this.number = c.getNumberAccord();
	}
	
	public Chord(EnumChordNumber number, EnumDegre degre) {
		this.number = number;
		this.degre = degre ;
	}
	
	public Chord(int complexityChordMin, int complexityChordMax, List<EnumDegre> allDegres, List<Chord> allChords, StyleCoefDegres styleCoefDegres) {
		
		do {
			if(!allDegres.contains(EnumDegre.I)) {
				this.degre = EnumDegre.I ;
			}
			else {
				this.degre = allDegres.get(allDegres.size()-1).getDegreRandom(styleCoefDegres);
			}
			this.number = EnumChordNumber.randomEnumNumberAccordWithBoundaries(complexityChordMin, complexityChordMax);
		}
		while(chordExist(allChords));
		
	}
	
	private boolean chordExist(List<Chord> allChords) {
		for(Chord c:allChords) {
			if(c.degre.equals(this.degre) && c.number.equals(this.number)) {
				return true;
			}
		}
		return false;
	}

	public Chord(List<Chord> availableAccords, EnumDegre degre) {
		
		List<EnumChordNumber> chordNumbers = new ArrayList<EnumChordNumber>();
		for(Chord c:availableAccords) {
			chordNumbers.add(c.getNumberAccord());
		}
		
		this.degre = degre ;
		this.number = chordNumbers.get(RANDOM.nextInt(chordNumbers.size()));
	}

	public Chord getNextChord(List<Chord> allowedChords, StyleCoefDegres styleCoefDegres) {
		
		Chord nextChord;
		
		EnumDegre deg = this.degre.getDegreRandomByReference(allowedChords, styleCoefDegres);
		
		if(deg != null) {
			EnumChordNumber accordNumber = EnumChordNumber.randomEnumNumberAccordByReference(allowedChords, deg);
			nextChord = new Chord(accordNumber, deg);
		}
		else {
			nextChord = new Chord();
		}

		return nextChord;
	}

	public EnumDegre getDegre() {
		return degre;
	}

	public EnumChordNumber getNumberAccord() {
		return number;
	}
	
	public List<EnumNote> getListNotes(EnumTonalite tonality) {
		return tonality.getNotesAccord(this.number,this.degre);
	}
	
	public EnumNote getFundamentalNote(EnumTonalite tonality) {
		return tonality.getFundamentalNote(this.number,this.degre);
	}
}
