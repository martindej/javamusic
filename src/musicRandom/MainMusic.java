package musicRandom;

import java.util.ArrayList;
import java.util.List;

import analyse.AnalyseMusic;
import enums.EnumDurationChord;
import enums.EnumInstruments;
import enums.EnumTonalite;
import exceptions.MyException;
import styles.MesureTempsFort;
import styles.Style;
import styles.StyleCoefDegres;
import styles.StyleGroupDrums;
import styles.StyleGroupDrumsIns;
import styles.StyleGroupIns;

public class MainMusic {

	public static void main(String[] args) {

		EnumTonalite tonality = null;
		EnumDurationChord dChord = null;

		List<StyleCoefDegres> listCoefDegres = new ArrayList<>();

		StyleCoefDegres styleCoefDegres = new StyleCoefDegres(1, 1, 1, 3, 3, 1, 1, 0, 0, 0, 1, 2, 0, 2, 0, 1, 0, 3, 1,
				3, 0, 3, 1, 0, 0, 3, 1, 1, 4, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, "");
		listCoefDegres.add(styleCoefDegres);

		List<Integer> ins1 = new ArrayList<>();
		ins1.add(75);
		ins1.add(1);

		List<Integer> insDrum = new ArrayList<>();
		insDrum.add(75);

		StyleGroupDrumsIns drumsIns = new StyleGroupDrumsIns(0, 0, 0, "test", 8, 100, 100, 100, 100, 100, 100, 100, 100,
				100, 100, 100, 100, insDrum);

		List<StyleGroupDrumsIns> listDrumsIns = new ArrayList<>();
		listDrumsIns.add(drumsIns);

		StyleGroupDrums drums = new StyleGroupDrums(0, 0, "test", 8, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100,
				100, 100, listDrumsIns);

		List<StyleGroupDrums> listDrums = new ArrayList<>();
//		listDrums.add(drums);

		StyleGroupIns group1 = new StyleGroupIns(2, "flute", 0, ins1, 100, 100, 100, 100, -100, 100, 2, 5, 3, 6, 0, 5,
				500, 40, 0, 100, 2, 8, 2, 8, 2, 8, 2, 8, 0, 100, 50, 50, 0, 100, 1, 4, 1, 100, 3, 6, 1, 6, 0, 100, 80,
				100, 50, 80, 0, 100);

		List<Integer> ins2 = new ArrayList<Integer>();
		ins2.add(64);
		ins2.add(65);
		ins2.add(66);
		ins2.add(67);

		StyleGroupIns group2 = new StyleGroupIns(1, "test", 0, ins2, 80, 100, -100, 100, -100, 100, 0, 10, 3, 6, 0, 50,
				300, 40, 0, 100, 0, 100, 0, 100, 0, 100, 0, 100, 0, 100, -100, -100, 0, 100, 1, 4, 1, 100, 3, 6, 1, 6,
				0, 100, 80, 100, 50, 80, 0, 100);

		List<StyleGroupIns> groupIns = new ArrayList<StyleGroupIns>();
		groupIns.add(group1);
//		groupIns.add(group2);

		List<Integer> availableDur = new ArrayList<Integer>();
		availableDur.add(0);
		availableDur.add(1);
		availableDur.add(2);

		List<Integer> availableTon = new ArrayList<Integer>();
		availableTon.add(0);
		availableTon.add(1);
		availableTon.add(2);
		availableTon.add(3);
		availableTon.add(4);
		availableTon.add(5);
		availableTon.add(6);
		availableTon.add(7);
		availableTon.add(8);
		availableTon.add(9);
		availableTon.add(10);
		availableTon.add(11);
		availableTon.add(12);
		availableTon.add(13);
		availableTon.add(14);
		availableTon.add(15);
		availableTon.add(16);
		availableTon.add(17);
		availableTon.add(18);
		availableTon.add(19);
		availableTon.add(20);
		availableTon.add(21);
		availableTon.add(22);
		availableTon.add(23);
		availableTon.add(24);
		availableTon.add(25);
		availableTon.add(26);
		availableTon.add(27);
		availableTon.add(28);
		availableTon.add(29);

		List<Integer> tempsF = new ArrayList<Integer>();
		tempsF.add(0);
//		tempsF.add(1);
		tempsF.add(2);
//		tempsF.add(3);
//		tempsF.add(4);

		List<MesureTempsFort> listMesureTempsFort = new ArrayList<MesureTempsFort>();

		MesureTempsFort mesureTempsFort = new MesureTempsFort(1, 0, 2, tempsF, true);

		listMesureTempsFort.add(mesureTempsFort);

		Style st = new Style("test", availableTon, 0, 200, 40, 2, 2, 1, 1, 1, 1, 30, 30, listCoefDegres,
				listMesureTempsFort, groupIns, listDrums);

		List<EnumInstruments> instruments = new ArrayList<EnumInstruments>();
		instruments.add(EnumInstruments.AGOGO);
		instruments.add(EnumInstruments.CUIVRESSYNTHEUN);

		Music essai;
		try {
			essai = new Music(tonality, -1, dChord, null, null, -1, null, st, 10);
			AnalyseMusic a = new AnalyseMusic(essai);
		} catch (MyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

//		a.compute();
//		new MidiWhitoutJavax(essai);

//		new MidiFileOutput(essai);
	}

}
