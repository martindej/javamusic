package analyse;

import java.util.HashMap;
import java.util.Map;

import enums.EnumDurationNote;
import enums.EnumInstruments;

public class AnalyseGroupInstrument {
	
	private EnumInstruments instrument;
	
	private int pourcentagePlaying; 
	private int balanceBassMelody; 
	private int balanceMelodyMelodyBis; 
	private int balanceBassBassBis; 
	private double gBetweenNotesMean; 
	private int gapBetweenNotesMin;
	private int gapBetweenNotesMax;
	private Map<Integer, Integer> gBetweenNotesMap;
	private int distanceNoteRefMin;
	private int distanceNoteRefMax;
	private double distanceNoteRefMean;
	private Map<Integer, Integer> distanceNoteRefMap = new HashMap<Integer, Integer>();
	private int dispersionNotes;
	private double pourcentageSil; 
	private int speed; 
	private Map<EnumDurationNote, Double> allowedR; 
	private double pSyncopes; 
	private int pSyncopettes; 
	private double pSyncopesTempsForts; 
	private double pContreTemps; 
	private double pDissonance; 
	private double pIrregularites; 
	private int bBinaireTernaire; 
	private int pChanceSupSameRythm;
	private int numberNotesSimultaneously;
	private int numberRythm;
	private int pChanceSameNoteInMotif;
	private int pChanceStartByTonaleAsBass;
	private int pChanceStayAroundNoteRef;
	private int pChancePlayChord;
	
	AnalyseGroupInstrument() {}
	
	public AnalyseGroupInstrument(EnumInstruments instrument2) {
		this.instrument = instrument2;
	}

	public EnumInstruments getInstrument() {
		return instrument;
	}
	public void setInstrument(EnumInstruments instrument) {
		this.instrument = instrument;
	}
	public int getPourcentagePlaying() {
		return pourcentagePlaying;
	}
	public void setPourcentagePlaying(int pourcentagePlaying) {
		this.pourcentagePlaying = pourcentagePlaying;
	}
	public int getBalanceBassMelody() {
		return balanceBassMelody;
	}
	public void setBalanceBassMelody(int balanceBassMelody) {
		this.balanceBassMelody = balanceBassMelody;
	}
	public int getBalanceMelodyMelodyBis() {
		return balanceMelodyMelodyBis;
	}
	public void setBalanceMelodyMelodyBis(int balanceMelodyMelodyBis) {
		this.balanceMelodyMelodyBis = balanceMelodyMelodyBis;
	}
	public double getgBetweenNotesMean() {
		return gBetweenNotesMean;
	}
	public void setgBetweenNotesMean(double gBetweenNotes) {
		this.gBetweenNotesMean = gBetweenNotes;
	}
	public int getDispersionNotes() {
		return dispersionNotes;
	}
	public void setDispersionNotes(int dispersionNotes) {
		this.dispersionNotes = dispersionNotes;
	}
	public double getPourcentageSil() {
		return pourcentageSil;
	}
	public void setPourcentageSil(double d) {
		this.pourcentageSil = d;
	}
	public int getSpeed() {
		return speed;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	public Map<EnumDurationNote, Double> getAllowedR() {
		return allowedR;
	}
	public void setAllowedR(Map<EnumDurationNote, Double> allowedR) {
		this.allowedR = allowedR;
	}
	public double getpSyncopes() {
		return pSyncopes;
	}
	public void setpSyncopes(double d) {
		this.pSyncopes = d;
	}
	public int getpSyncopettes() {
		return pSyncopettes;
	}
	public void setpSyncopettes(int pSyncopettes) {
		this.pSyncopettes = pSyncopettes;
	}
	public double getpSyncopesTempsForts() {
		return pSyncopesTempsForts;
	}
	public void setpSyncopesTempsForts(double d) {
		this.pSyncopesTempsForts = d;
	}
	public double getpContreTemps() {
		return pContreTemps;
	}
	public void setpContreTemps(double pContreTemps) {
		this.pContreTemps = pContreTemps;
	}
	public double getpDissonance() {
		return pDissonance;
	}
	public void setpDissonance(double pDissonance) {
		this.pDissonance = pDissonance;
	}
	public double getpIrregularites() {
		return pIrregularites;
	}
	public void setpIrregularites(double d) {
		this.pIrregularites = d;
	}
	public int getbBinaireTernaire() {
		return bBinaireTernaire;
	}
	public void setbBinaireTernaire(int bBinaireTernaire) {
		this.bBinaireTernaire = bBinaireTernaire;
	}
	public int getpChanceSupSameRythm() {
		return pChanceSupSameRythm;
	}
	public void setpChanceSupSameRythm(int pChanceSupSameRythm) {
		this.pChanceSupSameRythm = pChanceSupSameRythm;
	}
	public int getNumberNotesSimultaneously() {
		return numberNotesSimultaneously;
	}
	public void setNumberNotesSimultaneously(int numberNotesSimultaneously) {
		this.numberNotesSimultaneously = numberNotesSimultaneously;
	}
	public int getNumberRythm() {
		return numberRythm;
	}
	public void setNumberRythm(int numberRythm) {
		this.numberRythm = numberRythm;
	}
	public int getpChanceSameNoteInMotif() {
		return pChanceSameNoteInMotif;
	}
	public void setpChanceSameNoteInMotif(int pChanceSameNoteInMotif) {
		this.pChanceSameNoteInMotif = pChanceSameNoteInMotif;
	}
	public int getpChanceStartByTonaleAsBass() {
		return pChanceStartByTonaleAsBass;
	}
	public void setpChanceStartByTonaleAsBass(int pChanceStartByTonaleAsBass) {
		this.pChanceStartByTonaleAsBass = pChanceStartByTonaleAsBass;
	}
	public int getpChanceStayAroundNoteRef() {
		return pChanceStayAroundNoteRef;
	}
	public void setpChanceStayAroundNoteRef(int pChanceStayAroundNoteRef) {
		this.pChanceStayAroundNoteRef = pChanceStayAroundNoteRef;
	}
	public int getpChancePlayChord() {
		return pChancePlayChord;
	}
	public void setpChancePlayChord(int pChancePlayChord) {
		this.pChancePlayChord = pChancePlayChord;
	}

	public void setBalanceBassBassBis(int bBassBassBis) {
		this.balanceBassBassBis = bBassBassBis;
	}
	
	public int getBalanceBassBassBis() {
		return this.balanceBassBassBis;
	}

	public Map<Integer, Integer> getgBetweenNotesMap() {
		return gBetweenNotesMap;
	}

	public void setgBetweenNotesMap(Map<Integer, Integer> gBetweenNotesMap) {
		this.gBetweenNotesMap = gBetweenNotesMap;
	}

	public int getGapBetweenNotesMin() {
		return gapBetweenNotesMin;
	}

	public void setGapBetweenNotesMin(int gapBetweenNotesMin) {
		this.gapBetweenNotesMin = gapBetweenNotesMin;
	}

	public int getGapBetweenNotesMax() {
		return gapBetweenNotesMax;
	}

	public void setGapBetweenNotesMax(int gapBetweenNotesMax) {
		this.gapBetweenNotesMax = gapBetweenNotesMax;
	}

	public int getDistanceNoteRefMin() {
		return distanceNoteRefMin;
	}

	public void setDistanceNoteRefMin(int distanceNoteRefMin) {
		this.distanceNoteRefMin = distanceNoteRefMin;
	}

	public int getDistanceNoteRefMax() {
		return distanceNoteRefMax;
	}

	public void setDistanceNoteRefMax(int distanceNoteRefMax) {
		this.distanceNoteRefMax = distanceNoteRefMax;
	}

	public double getDistanceNoteRefMean() {
		return distanceNoteRefMean;
	}

	public void setDistanceNoteRefMean(double distanceNoteRefMean) {
		this.distanceNoteRefMean = distanceNoteRefMean;
	}

	public Map<Integer, Integer> getDistanceNoteRefMap() {
		return distanceNoteRefMap;
	}

	public void setDistanceNoteRefMap(Map<Integer, Integer> distanceNoteRefMap) {
		this.distanceNoteRefMap = distanceNoteRefMap;
	}

}
