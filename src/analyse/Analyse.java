package analyse;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import enums.EnumInstruments;
import exceptions.MyException;
import musicRandom.Music;
import styles.Style;
import styles.StyleGroupIns;

public class Analyse {

	private Style style;
	private Map<Integer, AnalyseMusic> mapAnalyseMusic;

	public Analyse(Style mStyle, String parameter) throws NoSuchFieldException, SecurityException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {

		Music m = null;
		AnalyseMusic a = new AnalyseMusic();
		;
		int paramMin;
		int paramMax;
		Method setterMethod;
		int maxCount = 10;
		int index = 0;
		boolean succes = false;
		this.setStyle(mStyle);
		Map<EnumInstruments, StyleGroupIns> aInstruments = null;

		mapAnalyseMusic = new HashMap<>();

		paramMin = findParamValue(mStyle.getStyleGroupInstruments().get(0), parameter + "Min");
		paramMax = findParamValue(mStyle.getStyleGroupInstruments().get(0), parameter + "Max");
		setterMethod = findSetterMethod(mStyle.getStyleGroupInstruments().get(0), parameter);

		// on genere une premiere fois la musique pour recuperer les parametres
		while (index < maxCount && !succes) {
			try {
				m = new Music(null, -1, null, aInstruments, null, -1, null, mStyle, -1);
				succes = true;
			} catch (MyException e) {
				// handle exception
				System.out.println("problem with music");
			}
			index++;

		}

		succes = false;

		aInstruments = m.getListEnumInstruments();

		for (int i = paramMin; i <= paramMax; i++) {

			for (Map.Entry<EnumInstruments, StyleGroupIns> entry : aInstruments.entrySet()) {
				setterMethod.invoke(entry.getValue(), i);
			}

			index = 0;

			while (index < maxCount && !succes) {
				try {
					m = new Music(null, -1, null, aInstruments, null, -1, null, mStyle, -1);
					a = new AnalyseMusic(m);
					succes = true;
				} catch (MyException e) {
					a = new AnalyseMusic();
				}
				index++;
			}

			mapAnalyseMusic.put(i, a);

			index = 0;
			succes = false;
		}

	}

	private int findParamValue(StyleGroupIns mStyle, String param) {

		Method[] methods = mStyle.getClass().getDeclaredMethods();

		for (Method method : methods) {
			if (isGetter(method)) {
				try {
					if (Pattern.compile(Pattern.quote(param), Pattern.CASE_INSENSITIVE).matcher(method.getName())
							.find()) {
						return (int) method.invoke(mStyle);
					}
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return 0;
	}

	private Method findSetterMethod(StyleGroupIns styleGroupIns, String param) {

		Method[] methods = styleGroupIns.getClass().getDeclaredMethods();

		for (Method method : methods) {
			if (isSetter(method) && Pattern.compile(Pattern.quote(param), Pattern.CASE_INSENSITIVE)
					.matcher(method.getName()).find()) {
				return method;
			}
		}

		return null;
	}

	private static boolean isGetter(Method method) {
		// identify get methods
		return (method.getName().startsWith("get") || method.getName().startsWith("is"))
				&& method.getParameterCount() == 0 && !method.getReturnType().equals(void.class);
	}

	private static boolean isSetter(Method method) {
		// identify set methods
		return (method.getName().startsWith("set") && method.getParameterCount() == 1
				&& method.getReturnType().equals(void.class));
	}

	public Map<Integer, AnalyseMusic> getMapAnalyseMusic() {
		return mapAnalyseMusic;
	}

	public void setMapAnalyseMusic(Map<Integer, AnalyseMusic> mapAnalyseMusic) {
		this.mapAnalyseMusic = mapAnalyseMusic;
	}

	public Style getStyle() {
		return style;
	}

	public void setStyle(Style style) {
		this.style = style;
	}
}
