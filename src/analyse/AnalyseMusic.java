package analyse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import enums.EnumDurationNote;
import enums.EnumInstrumentCategories;
import enums.EnumPlaySentence;
import enums.EnumSilence;
import enums.EnumTempsForts;
import musicRandom.IdentityInstrument;
import musicRandom.IdentitySentence;
import musicRandom.Instrument;
import musicRandom.Music;
import musicRandom.Note;
import musicRandom.Part;
import musicRandom.Sentence;

public class AnalyseMusic {
		
	private List<AnalyseGroupInstrument> listAnalyseGroupInstrument;

	public AnalyseMusic(Music m) {
		this.listAnalyseGroupInstrument = new ArrayList<AnalyseGroupInstrument>();
		this.compute(m);
	}
	
	public AnalyseMusic() {
		this.listAnalyseGroupInstrument = new ArrayList<AnalyseGroupInstrument>();
	}
	
	public void compute(Music music) {
		
		IdentityInstrument idIns;
		IdentitySentence identitySentence;
		AnalyseGroupInstrument analyseGroupInstrument;
		
		int nSentence = 0, nSentenceDrum = 0, nSentenceNotPlaying = 0, nSentenceMainMelody = 0, nSentenceMainMelodyBis = 0, nSentenceBass = 0, nSentenceBassBis = 0;
		int pPlaying, bBassMelody, bMelodyMelodyBis, bBassBassBis;
		int nNotes = 0, nSilences = 0, nRythme, gapBetweenNotesTotal = 0, gapBetweenNotes, gapBetweenNotesSum = 0, gapBetweenNotesMin = 99, gapBetweenNotesMax = 0;
		int nRythmsRegulier, nRythmsNotRegulier, nSyncoppe, nSyncopette, nSyncoppeTempsFort, nDissonance, nContreTemps;
		int distanceMeanNoteRef, distanceMinNoteRef, distanceMaxNoteRef, distanceTotalNoteRef, distanceNoteRef, nDistance;
		Map<Integer, Integer> gBetweenNotesMap = new HashMap<Integer, Integer>();
		Map<Integer, Integer> distanceNotesNoteRefMap = new HashMap<Integer, Integer>();
		Map<EnumDurationNote, Double> mapRythmes = new HashMap<EnumDurationNote, Double>();
		
		double gapBetweenNotesMean = 0;
		
		for(Instrument ins:music.getListInstruments()) {
			
			if(!ins.getInstrument().getCategory().equals(EnumInstrumentCategories.PERCUSSION)) {
				analyseGroupInstrument = new AnalyseGroupInstrument(ins.getInstrument());
				nSilences = 0;
				gapBetweenNotesTotal = 0;
				nSentence = 0; 
				nSentenceDrum = 0; 
				nSentenceNotPlaying = 0; 
				nSentenceMainMelody = 0; 
				nSentenceMainMelodyBis = 0; 
				nSentenceBass = 0; 
				nSentenceBassBis = 0;
				nRythmsRegulier = 0;
				nRythmsNotRegulier = 0;
				nSyncoppe = 0;
				nSyncoppeTempsFort = 0;
				nSyncopette = 0;
				nDissonance = 0;
				nContreTemps = 0;
				distanceMeanNoteRef = 0;
				distanceTotalNoteRef = 0;
				distanceMinNoteRef = 999;
				distanceMaxNoteRef = 0;
				distanceNoteRef = 0;
				mapRythmes = new HashMap<EnumDurationNote, Double>();
				distanceNotesNoteRefMap = new HashMap<Integer, Integer>();
				gBetweenNotesMap = new HashMap<Integer, Integer>();
				
				for(Part p:music.getListParts()) {
					for(Sentence s:p.getSentences()) {
						
						idIns = s.getIdentityInstrumentByRefNumber(ins.getIdentityInstrument().getRefNumber());
						identitySentence = idIns.getIdentitySentence();
						
						nSentence++;
						
						if(identitySentence.getStyle().equals(EnumPlaySentence.NOTPLAYING)) {
							nSentenceNotPlaying++;
						}
						else if(identitySentence.getStyle().equals(EnumPlaySentence.BASSLIGNE)) {
							nSentenceBass++;
						}
						else if(identitySentence.getStyle().equals(EnumPlaySentence.BASSLIGNEBIS)) {
							nSentenceBassBis++;
						}
						else if(identitySentence.getStyle().equals(EnumPlaySentence.DRUMS)) {
							nSentenceDrum++;
						}
						else if(identitySentence.getStyle().equals(EnumPlaySentence.MAINMELODY)) {
							nSentenceMainMelody++;
						}
						else if(identitySentence.getStyle().equals(EnumPlaySentence.MAINMELODYBIS)) {
							nSentenceMainMelodyBis++;
						}
					}
				}
				
				pPlaying = (int)(((double)(nSentence - nSentenceNotPlaying) / (double)nSentence) * 100.0);
				analyseGroupInstrument.setPourcentagePlaying(pPlaying);
				
				bBassMelody = (int)((((double)(nSentenceBass + nSentenceBassBis) / (double)(nSentence - nSentenceNotPlaying)) * 200.0) - 100.0);
				analyseGroupInstrument.setBalanceBassMelody(bBassMelody);
				
				if(nSentenceMainMelody + nSentenceMainMelodyBis > 0) {
					bMelodyMelodyBis = (int)((((double)nSentenceMainMelody / (double)(nSentenceMainMelody + nSentenceMainMelodyBis)) * 200.0) - 100.0);
					analyseGroupInstrument.setBalanceMelodyMelodyBis(bMelodyMelodyBis);
				}
				else {
					analyseGroupInstrument.setBalanceMelodyMelodyBis(0);
				}
				
				if(nSentenceBass + nSentenceBassBis > 0) {
					bBassBassBis = (int)((((double)nSentenceBass / (double)(nSentenceBass + nSentenceBassBis)) * 200.0) - 100.0);
					analyseGroupInstrument.setBalanceBassBassBis(bBassBassBis);
				}
				else {
					analyseGroupInstrument.setBalanceBassBassBis(0);
				}
				
				nNotes = ins.getListNotes().size();
				
				for(Note n:ins.getListNotes()) {
					
					if(n.getPreviousNote() != null) {
						gapBetweenNotes = n.getListNoteNum().get(0).getIndex() - n.getPreviousNote().getListNoteNum().get(0).getIndex();
						gapBetweenNotesTotal = gapBetweenNotesTotal + Math.abs(gapBetweenNotes);
						
						if(Math.abs(gapBetweenNotes) < gapBetweenNotesMin) {
							gapBetweenNotesMin = Math.abs(gapBetweenNotes);
						}
						if(Math.abs(gapBetweenNotes) > gapBetweenNotesMax) {
							gapBetweenNotesMax = Math.abs(gapBetweenNotes);
						}
						
						if(gBetweenNotesMap.containsKey(gapBetweenNotes)) {
							gapBetweenNotesSum = gBetweenNotesMap.get(gapBetweenNotes) + 1;
						}
						else {
							gapBetweenNotesSum = 1;
						}
						gBetweenNotesMap.put(gapBetweenNotes, gapBetweenNotesSum);
					}
					
					if(n.getSilenceOrPlaying().equals(EnumSilence.SILENCE)) {
						nSilences++;
					}
					
					if(mapRythmes.containsKey(n.getNoteDuration().get(0))) {
						nRythme = (int) (mapRythmes.get(n.getNoteDuration().get(0)) + 1);
					}
					else {
						nRythme = 1;
					}
					mapRythmes.put(n.getNoteDuration().get(0), (double)nRythme);
					
					if(n.getNoteDuration().get(0).isRregular()) {
						nRythmsRegulier++;
					}
					else {
						nRythmsNotRegulier++;
					}
									
					if(isSyncopeTempsFort(n, music)) {
						nSyncoppeTempsFort++;
					}
					else if(isSyncope(n)) {
						nSyncoppe++;
					}
					
					if(isContreTemps(n)) {
						nContreTemps++;
					}
					
					if(n.isDissonant()) {
						nDissonance++;
					}
					
					distanceNoteRef = ins.getIdentityInstrument().getNoteRef().getIndex() - n.getListNoteNum().get(0).getIndex();
					distanceTotalNoteRef = distanceTotalNoteRef + Math.abs(distanceNoteRef);
					
					if(Math.abs(distanceNoteRef) > distanceMaxNoteRef) {
						distanceMaxNoteRef = Math.abs(distanceNoteRef);
					}
					if(Math.abs(distanceNoteRef) < distanceMinNoteRef) {
						distanceMinNoteRef = Math.abs(distanceNoteRef);
					}
					
					if(distanceNotesNoteRefMap.containsKey(distanceNoteRef)) {
						nDistance = (int) (distanceNotesNoteRefMap.get(distanceNoteRef) + 1);
					}
					else {
						nDistance = 1;
					}
					distanceNotesNoteRefMap.put(distanceNoteRef, nDistance);
					
				}
				
				gapBetweenNotesMean = (double)gapBetweenNotesTotal / (double)nNotes;
				analyseGroupInstrument.setgBetweenNotesMean(gapBetweenNotesMean);
				analyseGroupInstrument.setgBetweenNotesMap(gBetweenNotesMap);
				analyseGroupInstrument.setGapBetweenNotesMin(gapBetweenNotesMin);
				analyseGroupInstrument.setGapBetweenNotesMax(gapBetweenNotesMax);
				analyseGroupInstrument.setPourcentageSil(((double)nSilences / (double)nNotes) * 100.0);
				analyseGroupInstrument.setAllowedR(mapRythmes);
				analyseGroupInstrument.setpIrregularites(((double)nRythmsNotRegulier / (double)nNotes) * 100.0);
				analyseGroupInstrument.setpSyncopes(((double)nSyncoppe / (double)nNotes) * 100.0);
				analyseGroupInstrument.setpSyncopesTempsForts(((double)nSyncoppeTempsFort / (double)nNotes) * 100.0);
				analyseGroupInstrument.setpContreTemps(((double)nContreTemps / (double)nNotes) * 100.0);
				analyseGroupInstrument.setpDissonance(((double)nDissonance / (double)nNotes) * 100.0);
				analyseGroupInstrument.setDistanceNoteRefMin(distanceMinNoteRef);
				analyseGroupInstrument.setDistanceNoteRefMax(distanceMaxNoteRef);
				analyseGroupInstrument.setDistanceNoteRefMean((double)distanceTotalNoteRef / (double)nNotes);
				analyseGroupInstrument.setDistanceNoteRefMap(distanceNotesNoteRefMap);
				
				
				listAnalyseGroupInstrument.add(analyseGroupInstrument);
			}
		}
	}
	
	private boolean isSyncope(Note n) {
		int durationAfterTime = n.getCursor()%24;
		boolean onBeat = (n.getCursor()-n.getDuration())%24 == 0;
		
		if(n.getDuration() > durationAfterTime && !onBeat) {
			return true;
		}
		else {
			return false;
		}
	}
	
	private boolean isSyncopeTempsFort(Note n, Music music) {
		
		int durationFromTempsFort = 999;
		int durationInMesure = n.getCursor()%(music.getAccordsDuration().getDuration() * 24);
		int durationInMesurePlusOneMesure = durationInMesure + music.getAccordsDuration().getDuration() * 24;
		
		for(EnumTempsForts tF:music.getTempsForts()) {
			if(n.getCursor() > tF.getCursor() && durationInMesure > tF.getCursor() && durationFromTempsFort > (durationInMesure - tF.getCursor())) {
				durationFromTempsFort = durationInMesure - tF.getCursor();
			}
			else if(n.getCursor() > tF.getCursor() && durationInMesurePlusOneMesure > tF.getCursor() && durationFromTempsFort > (durationInMesurePlusOneMesure - tF.getCursor())) {
				durationFromTempsFort = durationInMesurePlusOneMesure - tF.getCursor();
			}
		}
		
		if(n.getDuration() > durationFromTempsFort) {
			return true;
		}
		else {
			return false;
		}
	}
	
	//TODO
	private boolean isContreTemps(Note n) {
		return false;
	}
	
	public List<AnalyseGroupInstrument> getListAnalyseGroupInstrument() {
		return listAnalyseGroupInstrument;
	}

	public void setListAnalyseGroupInstrument(List<AnalyseGroupInstrument> listAnalyseGroupInstrument) {
		this.listAnalyseGroupInstrument = listAnalyseGroupInstrument;
	}

}
