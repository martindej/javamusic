package enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public enum EnumChordTension {

	TENSION 			("Tension", 			EnumIntervals.SECONDMINOR, EnumIntervals.SEVENTH),
	PARTIALTENSION 		("Partial tension", 	EnumIntervals.SECOND, EnumIntervals.SEVENTHMINOR),
	WITHOUTEFFECT 		("Without effect", 		EnumIntervals.FIFTHDIM),
	PARTIALRESOLUTION 	("Partial resolution",	EnumIntervals.THIRDMINOR, EnumIntervals.THIRD, EnumIntervals.SIXTH, EnumIntervals.SIXTHMINOR),
	COMPLETERESOLUTION 	("Complete resolution",	EnumIntervals.NONE, EnumIntervals.FOURTH, EnumIntervals.EIGHT, EnumIntervals.FIFTH);
	
	private String name = "";
	private List<EnumIntervals> intervals;
	private static final List<EnumChordTension> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
	private static final int SIZE = VALUES.size();
	private static Random RANDOM = new Random();
   
	EnumChordTension(String name, EnumIntervals...inters){
		this.name = name;
		this.intervals = Arrays.asList(inters);
	}
   
	public String getName(){
		return this.name;
	}
	
	public List<EnumNote> getListAllowedNotesForThisChord(List<EnumNote> chordNotes, EnumTonalite tonality) {
		
		List<EnumNote> allowedNotes = new ArrayList<EnumNote>();
		EnumNoteTonality tonale = EnumNoteTonality.getByNoteAndOctave(tonality.getTonale(), EnumOctave.OCTAVE2);
		EnumNote noteTest;
		
		for(EnumIntervals interval:this.intervals) {
			noteTest = EnumNoteTonality.getByIndex(tonale.getIndex() + interval.getDifferenceWithTonale()).getNote();
			
			if(chordNotes.contains(noteTest)) {
				allowedNotes.add(noteTest);
			}
		}
		return allowedNotes;
	}

	public static EnumChordTension randomEnumTension()  {
		return VALUES.get(RANDOM.nextInt(SIZE));
	}
	
	public static EnumChordTension randomEnumTensionWithoutCompleteResolution()  {
		return VALUES.subList(0, SIZE-1).get(RANDOM.nextInt(SIZE-1));
	}
	
	public static EnumChordTension randomEnumTensionDependingOnPrevious(EnumChordTension previousTension)  {
		
		// TODO precise conditions 
		if(previousTension == null) {
			return VALUES.subList(0, SIZE-2).get(RANDOM.nextInt(SIZE-2));
		}
		else {
			return VALUES.subList(0, SIZE-1).get(RANDOM.nextInt(SIZE-1));
		}
	}
}
