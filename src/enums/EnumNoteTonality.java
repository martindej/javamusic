package enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import exceptions.MyException;
import musicRandom.Chord;
import musicRandom.Note;

public enum EnumNoteTonality {

	DO_TON1("Do Tonalite 1", EnumOctave.OCTAVE1, EnumNote.DO, 0),
	DODIESE_TON1("Do diese Tonalite 1", EnumOctave.OCTAVE1, EnumNote.DODIESE, 1),
	RE_TON1("Re Tonalite 1", EnumOctave.OCTAVE1, EnumNote.RE, 2),
	REDIESE_TON1("Re diese Tonalite 1", EnumOctave.OCTAVE1, EnumNote.REDIESE, 3),
	MI_TON1("Mi Tonalite 1", EnumOctave.OCTAVE1, EnumNote.MI, 4),
	FA_TON1("Fa Tonalite 1", EnumOctave.OCTAVE1, EnumNote.FA, 5),
	FADIESE_TON1("Fa diese Tonalite 1", EnumOctave.OCTAVE1, EnumNote.FADIESE, 6),
	SOL_TON1("Sol Tonalite 1", EnumOctave.OCTAVE1, EnumNote.SOL, 7),
	SOLDIESE_TON1("Sol diese Tonalite 1", EnumOctave.OCTAVE1, EnumNote.SOLDIESE, 8),
	LA_TON1("La Tonalite 1", EnumOctave.OCTAVE1, EnumNote.LA, 9),
	LADIESE_TON1("La diese Tonalite 1", EnumOctave.OCTAVE1, EnumNote.LADIESE, 10),
	SI_TON1("Si Tonalite 1", EnumOctave.OCTAVE1, EnumNote.SI, 11),

	DO_TON2("Do Tonalite 2", EnumOctave.OCTAVE2, EnumNote.DO, 12),
	DODIESE_TON2("Do diese Tonalite 2", EnumOctave.OCTAVE2, EnumNote.DODIESE, 13),
	RE_TON2("Re Tonalite 2", EnumOctave.OCTAVE2, EnumNote.RE, 14),
	REDIESE_TON2("Re diese Tonalite 2", EnumOctave.OCTAVE2, EnumNote.REDIESE, 15),
	MI_TON2("Mi Tonalite 2", EnumOctave.OCTAVE2, EnumNote.MI, 16),
	FA_TON2("Fa Tonalite 2", EnumOctave.OCTAVE2, EnumNote.FA, 17),
	FADIESE_TON2("Fa diese Tonalite 2", EnumOctave.OCTAVE2, EnumNote.FADIESE, 18),
	SOL_TON2("Sol Tonalite 2", EnumOctave.OCTAVE2, EnumNote.SOL, 19),
	SOLDIESE_TON2("Sol diese Tonalite 2", EnumOctave.OCTAVE2, EnumNote.SOLDIESE, 20),
	LA_TON2("La Tonalite 2", EnumOctave.OCTAVE2, EnumNote.LA, 21),
	LADIESE_TON2("La diese Tonalite 2", EnumOctave.OCTAVE2, EnumNote.LADIESE, 22),
	SI_TON2("Si Tonalite 2", EnumOctave.OCTAVE2, EnumNote.SI, 23),

	DO_TON3("Do Tonalite 3", EnumOctave.OCTAVE3, EnumNote.DO, 24),
	DODIESE_TON3("Do diese Tonalite 3", EnumOctave.OCTAVE3, EnumNote.DODIESE, 25),
	RE_TON3("Re Tonalite 3", EnumOctave.OCTAVE3, EnumNote.RE, 26),
	REDIESE_TON3("Re diese Tonalite 3", EnumOctave.OCTAVE3, EnumNote.REDIESE, 27),
	MI_TON3("Mi Tonalite 3", EnumOctave.OCTAVE3, EnumNote.MI, 28),
	FA_TON3("Fa Tonalite 3", EnumOctave.OCTAVE3, EnumNote.FA, 29),
	FADIESE_TON3("Fa diese Tonalite 3", EnumOctave.OCTAVE3, EnumNote.FADIESE, 30),
	SOL_TON3("Sol Tonalite 3", EnumOctave.OCTAVE3, EnumNote.SOL, 31),
	SOLDIESE_TON3("Sol diese Tonalite 3", EnumOctave.OCTAVE3, EnumNote.SOLDIESE, 32),
	LA_TON3("La Tonalite 3", EnumOctave.OCTAVE3, EnumNote.LA, 33),
	LADIESE_TON3("La diese Tonalite 3", EnumOctave.OCTAVE3, EnumNote.LADIESE, 34),
	SI_TON3("Si Tonalite 3", EnumOctave.OCTAVE3, EnumNote.SI, 35),

	DO_TON4("Do Tonalite 4", EnumOctave.OCTAVE4, EnumNote.DO, 36),
	DODIESE_TON4("Do diese Tonalite 4", EnumOctave.OCTAVE4, EnumNote.DODIESE, 37),
	RE_TON4("Re Tonalite 4", EnumOctave.OCTAVE4, EnumNote.RE, 38),
	REDIESE_TON4("Re diese Tonalite 4", EnumOctave.OCTAVE4, EnumNote.REDIESE, 39),
	MI_TON4("Mi Tonalite 4", EnumOctave.OCTAVE4, EnumNote.MI, 40),
	FA_TON4("Fa Tonalite 4", EnumOctave.OCTAVE4, EnumNote.FA, 41),
	FADIESE_TON4("Fa diese Tonalite 4", EnumOctave.OCTAVE4, EnumNote.FADIESE, 42),
	SOL_TON4("Sol Tonalite 4", EnumOctave.OCTAVE4, EnumNote.SOL, 43),
	SOLDIESE_TON4("Sol diese Tonalite 4", EnumOctave.OCTAVE4, EnumNote.SOLDIESE, 44),
	LA_TON4("La Tonalite 4", EnumOctave.OCTAVE4, EnumNote.LA, 45),
	LADIESE_TON4("La diese Tonalite 4", EnumOctave.OCTAVE4, EnumNote.LADIESE, 46),
	SI_TON4("Si Tonalite 4", EnumOctave.OCTAVE4, EnumNote.SI, 47),

	DO_TON5("Do Tonalite 5", EnumOctave.OCTAVE5, EnumNote.DO, 48),
	DODIESE_TON5("Do diese Tonalite 5", EnumOctave.OCTAVE5, EnumNote.DODIESE, 49),
	RE_TON5("Re Tonalite 5", EnumOctave.OCTAVE5, EnumNote.RE, 50),
	REDIESE_TON5("Re diese Tonalite 5", EnumOctave.OCTAVE5, EnumNote.REDIESE, 51),
	MI_TON5("Mi Tonalite 5", EnumOctave.OCTAVE5, EnumNote.MI, 52),
	FA_TON5("Fa Tonalite 5", EnumOctave.OCTAVE5, EnumNote.FA, 53),
	FADIESE_TON5("Fa diese Tonalite 5", EnumOctave.OCTAVE5, EnumNote.FADIESE, 54),
	SOL_TON5("Sol Tonalite 5", EnumOctave.OCTAVE5, EnumNote.SOL, 55),
	SOLDIESE_TON5("Sol diese Tonalite 5", EnumOctave.OCTAVE5, EnumNote.SOLDIESE, 56),
	LA_TON5("La Tonalite 5", EnumOctave.OCTAVE5, EnumNote.LA, 57),
	LADIESE_TON5("La diese Tonalite 5", EnumOctave.OCTAVE5, EnumNote.LADIESE, 58),
	SI_TON5("Si Tonalite 5", EnumOctave.OCTAVE5, EnumNote.SI, 59),

	DO_TON6("Do Tonalite 6", EnumOctave.OCTAVE6, EnumNote.DO, 60), // medium C
	DODIESE_TON6("Do diese Tonalite 6", EnumOctave.OCTAVE6, EnumNote.DODIESE, 61),
	RE_TON6("Re Tonalite 6", EnumOctave.OCTAVE6, EnumNote.RE, 62),
	REDIESE_TON6("Re diese Tonalite 6", EnumOctave.OCTAVE6, EnumNote.REDIESE, 63),
	MI_TON6("Mi Tonalite 6", EnumOctave.OCTAVE6, EnumNote.MI, 64),
	FA_TON6("Fa Tonalite 6", EnumOctave.OCTAVE6, EnumNote.FA, 65),
	FADIESE_TON6("Fa diese Tonalite 6", EnumOctave.OCTAVE6, EnumNote.FADIESE, 66),
	SOL_TON6("Sol Tonalite 6", EnumOctave.OCTAVE6, EnumNote.SOL, 67),
	SOLDIESE_TON6("Sol diese Tonalite 6", EnumOctave.OCTAVE6, EnumNote.SOLDIESE, 68),
	LA_TON6("La Tonalite 6", EnumOctave.OCTAVE6, EnumNote.LA, 69),
	LADIESE_TON6("La diese Tonalite 6", EnumOctave.OCTAVE6, EnumNote.LADIESE, 70),
	SI_TON6("Si Tonalite 6", EnumOctave.OCTAVE6, EnumNote.SI, 71),

	DO_TON7("Do Tonalite 7", EnumOctave.OCTAVE7, EnumNote.DO, 72),
	DODIESE_TON7("Do diese Tonalite 7", EnumOctave.OCTAVE7, EnumNote.DODIESE, 73),
	RE_TON7("Re Tonalite 7", EnumOctave.OCTAVE7, EnumNote.RE, 74),
	REDIESE_TON7("Re diese Tonalite 7", EnumOctave.OCTAVE7, EnumNote.REDIESE, 75),
	MI_TON7("Mi Tonalite 7", EnumOctave.OCTAVE7, EnumNote.MI, 76),
	FA_TON7("Fa Tonalite 7", EnumOctave.OCTAVE7, EnumNote.FA, 77),
	FADIESE_TON7("Fa diese Tonalite 7", EnumOctave.OCTAVE7, EnumNote.FADIESE, 78),
	SOL_TON7("Sol Tonalite 7", EnumOctave.OCTAVE7, EnumNote.SOL, 79),
	SOLDIESE_TON7("Sol diese Tonalite 7", EnumOctave.OCTAVE7, EnumNote.SOLDIESE, 80),
	LA_TON7("La Tonalite 7", EnumOctave.OCTAVE7, EnumNote.LA, 81),
	LADIESE_TON7("La diese Tonalite 7", EnumOctave.OCTAVE7, EnumNote.LADIESE, 82),
	SI_TON7("Si Tonalite 7", EnumOctave.OCTAVE7, EnumNote.SI, 83),

	DO_TON8("Do Tonalite 8", EnumOctave.OCTAVE8, EnumNote.DO, 84),
	DODIESE_TON8("Do diese Tonalite 8", EnumOctave.OCTAVE8, EnumNote.DODIESE, 85),
	RE_TON8("Re Tonalite 8", EnumOctave.OCTAVE8, EnumNote.RE, 86),
	REDIESE_TON8("Re diese Tonalite 8", EnumOctave.OCTAVE8, EnumNote.REDIESE, 87),
	MI_TON8("Mi Tonalite 8", EnumOctave.OCTAVE8, EnumNote.MI, 88),
	FA_TON8("Fa Tonalite 8", EnumOctave.OCTAVE8, EnumNote.FA, 89),
	FADIESE_TON8("Fa diese Tonalite 8", EnumOctave.OCTAVE8, EnumNote.FADIESE, 90),
	SOL_TON8("Sol Tonalite 8", EnumOctave.OCTAVE8, EnumNote.SOL, 91),
	SOLDIESE_TON8("Sol diese Tonalite 8", EnumOctave.OCTAVE8, EnumNote.SOLDIESE, 92),
	LA_TON8("La Tonalite 8", EnumOctave.OCTAVE8, EnumNote.LA, 93),
	LADIESE_TON8("La diese Tonalite 8", EnumOctave.OCTAVE8, EnumNote.LADIESE, 94),
	SI_TON8("Si Tonalite 8", EnumOctave.OCTAVE8, EnumNote.SI, 95),

	DO_TON9("Do Tonalite 9", EnumOctave.OCTAVE9, EnumNote.DO, 96),
	DODIESE_TON9("Do diese Tonalite 9", EnumOctave.OCTAVE9, EnumNote.DODIESE, 97),
	RE_TON9("Re Tonalite 9", EnumOctave.OCTAVE9, EnumNote.RE, 98),
	REDIESE_TON9("Re diese Tonalite 9", EnumOctave.OCTAVE9, EnumNote.REDIESE, 99),
	MI_TON9("Mi Tonalite 9", EnumOctave.OCTAVE9, EnumNote.MI, 100),
	FA_TON9("Fa Tonalite 9", EnumOctave.OCTAVE9, EnumNote.FA, 101),
	FADIESE_TON9("Fa diese Tonalite 9", EnumOctave.OCTAVE9, EnumNote.FADIESE, 102),
	SOL_TON9("Sol Tonalite 9", EnumOctave.OCTAVE9, EnumNote.SOL, 103),
	SOLDIESE_TON9("Sol diese Tonalite 9", EnumOctave.OCTAVE9, EnumNote.SOLDIESE, 104),
	LA_TON9("La Tonalite 9", EnumOctave.OCTAVE9, EnumNote.LA, 105),
	LADIESE_TON9("La diese Tonalite 9", EnumOctave.OCTAVE9, EnumNote.LADIESE, 106),
	SI_TON9("Si Tonalite 9", EnumOctave.OCTAVE9, EnumNote.SI, 107),

	DO_TON10("Do Tonalite 10", EnumOctave.OCTAVE10, EnumNote.DO, 108),
	DODIESE_TON10("Do diese Tonalite 10", EnumOctave.OCTAVE10, EnumNote.DODIESE, 109),
	RE_TON10("Re Tonalite 10", EnumOctave.OCTAVE10, EnumNote.RE, 110),
	REDIESE_TON10("Re diese Tonalite 10", EnumOctave.OCTAVE10, EnumNote.REDIESE, 111),
	MI_TON10("Mi Tonalite 10", EnumOctave.OCTAVE10, EnumNote.MI, 112),
	FA_TON10("Fa Tonalite 10", EnumOctave.OCTAVE10, EnumNote.FA, 113),
	FADIESE_TON10("Fa diese Tonalite 10", EnumOctave.OCTAVE10, EnumNote.FADIESE, 114),
	SOL_TON10("Sol Tonalite 10", EnumOctave.OCTAVE10, EnumNote.SOL, 115),
	SOLDIESE_TON10("Sol diese Tonalite 10", EnumOctave.OCTAVE10, EnumNote.SOLDIESE, 116),
	LA_TON10("La Tonalite 10", EnumOctave.OCTAVE10, EnumNote.LA, 117),
	LADIESE_TON10("La diese Tonalite 10", EnumOctave.OCTAVE10, EnumNote.LADIESE, 118),
	SI_TON10("Si Tonalite 10", EnumOctave.OCTAVE10, EnumNote.SI, 119),

	DO_TON11("Do Tonalite 11", EnumOctave.OCTAVE11, EnumNote.DO, 120),
	DODIESE_TON11("Do diese Tonalite 11", EnumOctave.OCTAVE11, EnumNote.DODIESE, 121),
	RE_TON11("Re Tonalite 11", EnumOctave.OCTAVE11, EnumNote.RE, 122),
	REDIESE_TON11("Re diese Tonalite 11", EnumOctave.OCTAVE11, EnumNote.REDIESE, 123),
	MI_TON11("Mi Tonalite 11", EnumOctave.OCTAVE11, EnumNote.MI, 124),
	FA_TON11("Fa Tonalite 11", EnumOctave.OCTAVE11, EnumNote.FA, 125),
	FADIESE_TON11("Fa diese Tonalite 11", EnumOctave.OCTAVE11, EnumNote.FADIESE, 126),
	SOL_TON11("Sol Tonalite 11", EnumOctave.OCTAVE11, EnumNote.SOL, 127);

	private String name = "";
	private EnumNote note;
	private int index;
	private EnumOctave octave;

	private static final List<EnumNoteTonality> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
	private static final int SIZE = VALUES.size();
	private static Random random = new Random();

	EnumNoteTonality(String name, EnumOctave octave, EnumNote note, int index) {
		this.name = name;
		this.note = note;
		this.octave = octave;
		this.index = index;
	}

	public EnumOctave getOctave() {
		return this.octave;
	}

	public static EnumNoteTonality randomEnumNoteTonality() {
		return VALUES.get(random.nextInt(SIZE));
	}

	public static EnumNoteTonality randomNoteBetween(EnumNoteTonality note1, EnumNoteTonality note2) {

		List<EnumNoteTonality> rangeList;
		int variance;
		int mediane;
		int rand;

		rangeList = VALUES.subList(Math.min(note2.getIndex(), note1.getIndex()),
				Math.max(note2.getIndex(), note1.getIndex()));
		variance = Math.abs(note2.getIndex() - note1.getIndex()) / 2;
		mediane = variance; // vu que index commence a z�ro

		do {
			rand = (int) (random.nextGaussian() * Math.sqrt(variance - 1) + mediane);
		} while (rand < 0 || rand >= rangeList.size());

		return rangeList.get(rand);
	}

	// TODO case ascending and descending and gap between notes
	public EnumNoteTonality getNextNoteRandom(Note previousNote, EnumNoteTonality noteRef, int gapBetweenNotes,
			int dispersionNotes, EnumInstruments instrument, EnumTonalite tonality, Chord currentChord, boolean onBeat,
			boolean isTempsFort, boolean dissonant, int degre, int octaveDif, EnumChordTension tension,
			boolean tensionNote, int chanceStayAroundNoteRef, List<Note> listNotes, int pChanceSameNoteInMotif)
			throws MyException {

		EnumNoteTonality noteReturn = null;

		if (tension != null && tensionNote) {
			noteReturn = getNoteWithTension(tonality, currentChord, instrument, octaveDif, tension);
		}

		if (noteReturn != null) {
			return noteReturn;
		} else if (degre != -1) {
			return getNoteWithRef(tonality, currentChord, onBeat, instrument, degre, octaveDif);
		} else {
			int randomSign;
			int indexNote = 0;
			int i = 0;

			do {
				i++;

				if (random.nextInt(100) < chanceStayAroundNoteRef) {
					randomSign = Integer.signum(noteRef.index - this.index);
				} else {
					randomSign = Integer.signum(this.index - noteRef.index);
				}
				indexNote = this.index + gapBetweenNotes * randomSign; // add a little variation randomly

				if (i > 500) {
					throw new MyException();
				}
			} while (indexNote > instrument.getNoteMax().getIndex() || indexNote < instrument.getNoteMin().getIndex());

			return getRandomNoteByIndex(tonality, currentChord, indexNote, onBeat, isTempsFort, dissonant, instrument,
					dispersionNotes, listNotes, pChanceSameNoteInMotif);
		}

	}

	private EnumNoteTonality getNoteWithTension(EnumTonalite tonality, Chord currentChord, EnumInstruments instrument,
			int octaveDif, EnumChordTension tension) {

		List<EnumNote> allowedNotes;
		List<EnumNote> chordNotes = currentChord.getListNotes(tonality);
		allowedNotes = tension.getListAllowedNotesForThisChord(chordNotes, tonality);

		if (!allowedNotes.isEmpty()) {

			int i;
			int indexOctave = this.octave.getIndex() + octaveDif;

			EnumOctave newOctave = EnumOctave.getOctaveByIndex(indexOctave);

			EnumNote noteAllowed = allowedNotes.get(random.nextInt(allowedNotes.size()));
			EnumNoteTonality noteReturn = EnumNoteTonality.getByNoteAndOctave(noteAllowed, newOctave);

			if (noteReturn.index > instrument.getNoteMax().getIndex()) {
				i = 1;
				do {
					newOctave = EnumOctave.getOctaveByIndex(indexOctave - i);
					noteReturn = EnumNoteTonality.getByNoteAndOctave(noteAllowed, newOctave);
					i++;
				} while (noteReturn.index > instrument.getNoteMax().getIndex());
			} else if (noteReturn.index < instrument.getNoteMin().getIndex()) {
				i = 1;
				do {
					newOctave = EnumOctave.getOctaveByIndex(indexOctave + i);
					noteReturn = EnumNoteTonality.getByNoteAndOctave(noteAllowed, newOctave);
					i++;
				} while (noteReturn.index < instrument.getNoteMin().getIndex());
			}

			return noteReturn;
		} else {
			return null;
		}
	}

	private EnumNoteTonality getNoteWithRef(EnumTonalite tonality, Chord currentChord, boolean onBeat,
			EnumInstruments instrument, int degre, int octaveDif) {

		EnumOctave newOctave = null;
		EnumNote noteR = null;
		EnumNoteTonality noteReturn;
		List<EnumNote> chordNotes = currentChord.getListNotes(tonality);
		int indexOctave;
		int i;

		noteR = tonality.getNoteByDegreInChord(degre, currentChord);

		indexOctave = this.octave.getIndex() + octaveDif;

		// indexOctave cannot be less than 0 and more than 10
		indexOctave = Math.min(10, indexOctave);
		indexOctave = Math.max(0, indexOctave);

		newOctave = EnumOctave.getOctaveByIndex(indexOctave);

		noteReturn = EnumNoteTonality.getByNoteAndOctave(noteR, newOctave);

		if ((chordNotes.contains(noteReturn.getNote()) && onBeat
				|| tonality.getAvailableNotes().contains(noteReturn.getNote()) && !onBeat)
				&& (noteReturn.index <= instrument.getNoteMax().getIndex()
						&& noteReturn.index >= instrument.getNoteMin().getIndex())) {
			return noteReturn;
		} else if (noteReturn.index > instrument.getNoteMax().getIndex()) {
			i = 1;
			do {
				newOctave = EnumOctave.getOctaveByIndex(indexOctave - i);
				noteReturn = EnumNoteTonality.getByNoteAndOctave(noteR, newOctave);
				i++;
			} while (noteReturn.index > instrument.getNoteMax().getIndex());

			return noteReturn;
		} else if (noteReturn.index < instrument.getNoteMin().getIndex()) {
			i = 1;
			do {
				newOctave = EnumOctave.getOctaveByIndex(indexOctave + i);
				noteReturn = EnumNoteTonality.getByNoteAndOctave(noteR, newOctave);
				i++;
			} while (noteReturn.index < instrument.getNoteMin().getIndex());

			return noteReturn;
		} else {
			return null;
		}

	}

	public static EnumNoteTonality getByNoteAndOctave(EnumNote note, EnumOctave octave) {

		for (EnumNoteTonality no : VALUES) {
			if (no.note.equals(note) && no.octave.equals(octave)) {
				return no;
			}
		}
		return null;
	}

	private EnumNoteTonality getRandomNoteByIndex(EnumTonalite tonality, Chord currentChord, int index, boolean inChord,
			boolean isTempsFort, boolean dissonant, EnumInstruments instrument, int dispersionNotes,
			List<Note> listNotes, int pChanceSameNoteInMotif) {

		if (instrument.equals(EnumInstruments.PERCUSSIONS)) {
			int indexNote = random.nextInt(instrument.getNoteMax().getIndex() - instrument.getNoteMin().getIndex())
					+ instrument.getNoteMin().getIndex();

			return EnumNoteTonality.getByIndex(indexNote);
		} else {
			List<EnumNote> chordNotes = currentChord.getListNotes(tonality);

			List<EnumNoteTonality> availableNotes = new ArrayList<>();
			List<EnumNoteTonality> previousNotes = new ArrayList<>();
			List<EnumNoteTonality> availablePreviousNotes = new ArrayList<>();
			List<EnumNoteTonality> availableNotPreviousNotes = new ArrayList<>();
			List<EnumNoteTonality> finalNotes = new ArrayList<>();

			for (int i = 0; i <= dispersionNotes; i++) {

				availableNotes = this.addNoteIfAvailable(availableNotes, instrument, index + i, dispersionNotes - i,
						chordNotes, tonality, inChord, isTempsFort, dissonant);
				availableNotes = this.addNoteIfAvailable(availableNotes, instrument, index - i, dispersionNotes - i,
						chordNotes, tonality, inChord, isTempsFort, dissonant);

				// add more derivation in the case of we don't find available note around with
				// the current derivation
				if (i == dispersionNotes && availableNotes.isEmpty()) {
					dispersionNotes++;
				}
			}

			if (listNotes != null && !listNotes.isEmpty()) {

				for (Note pNote : listNotes) {
					previousNotes.addAll(pNote.getListNoteNum());
				}

				for (EnumNoteTonality noteAvailable : availableNotes) {
					if (!previousNotes.contains(noteAvailable)) {
						availableNotPreviousNotes.add(noteAvailable);
					} else {
						availablePreviousNotes.add(noteAvailable);
					}
				}

				// we create a list with a proportion of notes belonging to the previous notes
				// or not
				for (int i = 0; i < 100; i++) {

					if (i < pChanceSameNoteInMotif) {
						finalNotes.addAll(availablePreviousNotes);
					} else {
						finalNotes.addAll(availableNotPreviousNotes);
					}
				}

				if (finalNotes.isEmpty()) {
					if (availablePreviousNotes.isEmpty()) {
						finalNotes.addAll(availableNotPreviousNotes);
					} else {
						finalNotes.addAll(availablePreviousNotes);
					}
				}
			} else {
				finalNotes = availableNotes;
			}

			return finalNotes.get(random.nextInt(finalNotes.size()));
		}
	}

	// TODO verifier qu'il ne peut pas y avoir de notes dissonantes sur temps fort
	private List<EnumNoteTonality> addNoteIfAvailable(List<EnumNoteTonality> availableNotes, EnumInstruments instrument,
			int index, int numberIteration, List<EnumNote> chordNotes, EnumTonalite tonality, boolean inChord,
			boolean isTempsFort, boolean dissonant) {

		EnumNoteTonality noteIndex;

		if (instrument != null && index < instrument.getNoteMax().getIndex()
				&& index > instrument.getNoteMin().getIndex()) {
			noteIndex = getByIndex(index);
		} else {
			noteIndex = null;
		}

		if (noteIndex != null) {
			if (((chordNotes.contains(noteIndex.getNote()))
					|| (tonality.getAvailableNotes().contains(noteIndex.getNote()) && (!inChord || dissonant)))) {
				availableNotes.add(noteIndex);

				// on donne 2x plus de poids quand on est hors temps
				for (int j = 0; j < 2 * numberIteration; j++) {
					availableNotes.add(noteIndex);
				}
			}
			// cas ou on a une note hors accords sur un temps
			else if (tonality.getAvailableNotes().contains(noteIndex.getNote()) && (inChord && !isTempsFort)) {

				for (int j = 0; j < numberIteration; j++) {
					availableNotes.add(noteIndex);
				}
			}
		}

		return availableNotes;
	}

	public String getName() {
		return name;
	}

	public EnumNote getNote() {
		return note;
	}

	public int getIndex() {
		return index;
	}

	public static EnumNoteTonality getByIndex(int index) {
		return VALUES.get(index);
	}
}
