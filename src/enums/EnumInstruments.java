package enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public enum EnumInstruments {
	
	// DO tonalit� 6 correspond au do milleu 
	
	GRANDPIANOBASS 				("Acoustic Grand Piano bass",		300,	EnumInstrumentCategories.BASS,	EnumNoteTonality.DO_TON5,	EnumNoteTonality.DO_TON6,	EnumNoteTonality.DO_TON4,	0, true,	null,	"F",	4,	EnumMusicStyle.ALLRANDOM),
	PIANOACOUSTIQUEBASS 		("Bright Acoustic Piarandomo bass",	400,	EnumInstrumentCategories.BASS,	EnumNoteTonality.DO_TON7,	EnumNoteTonality.DO_TON9,	EnumNoteTonality.DO_TON6,	1, true,	null,	"F",	4,	EnumMusicStyle.ALLRANDOM),
	PIANOELECBASS 				("Electric Grand Piano bass",		400,	EnumInstrumentCategories.BASS,	EnumNoteTonality.DO_TON7,	EnumNoteTonality.MI_TON9,	EnumNoteTonality.DO_TON6,	2, true, 	null,	"F",	4,	EnumMusicStyle.ALLRANDOM),
	PIANOBASTRIGUEBASS 			("Honky-tonk Piano bass", 			400,	EnumInstrumentCategories.BASS,	EnumNoteTonality.DO_TON7,	EnumNoteTonality.MI_TON9,	EnumNoteTonality.DO_TON6,	3, true,	null,	"F",	4,	EnumMusicStyle.ALLRANDOM),
	PIANOELECTRIQUEBASS 		("Electric Piano 1 bass", 			400,	EnumInstrumentCategories.BASS,	EnumNoteTonality.DO_TON7,	EnumNoteTonality.MI_TON9,	EnumNoteTonality.DO_TON6,	4, true,	null,	"F",	4,	EnumMusicStyle.ALLRANDOM),
	PIANOCHORUSBASS 			("Electric Piano 2 bass", 			400,	EnumInstrumentCategories.BASS,	EnumNoteTonality.DO_TON7,	EnumNoteTonality.MI_TON9,	EnumNoteTonality.DO_TON6,	5, true,	null,	"F",	4,	EnumMusicStyle.ALLRANDOM),
	CLAVECINBASS 				("Harpsichord bass", 				300,	EnumInstrumentCategories.BASS,	EnumNoteTonality.DO_TON7,	EnumNoteTonality.MI_TON9,	EnumNoteTonality.DO_TON6,	6, true,	null,	"F",	4,	EnumMusicStyle.ALLRANDOM),
	CLAVINETBASS 				("Clavi bass", 						300,	EnumInstrumentCategories.BASS,	EnumNoteTonality.DO_TON7,	EnumNoteTonality.MI_TON9,	EnumNoteTonality.DO_TON6,	7, true,	null,	"F",	4,	EnumMusicStyle.ALLRANDOM),
	CELESTABASS 				("Celesta bass", 					600,	EnumInstrumentCategories.BASS,	EnumNoteTonality.DO_TON7,	EnumNoteTonality.MI_TON9,	EnumNoteTonality.DO_TON6,	8, true,	null,	"F",	4,	EnumMusicStyle.ALLRANDOM),
	VIBRAPHONEBASS 				("Vibraphone bass", 				600,	EnumInstrumentCategories.BASS,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.SI_TON8,	EnumNoteTonality.MI_TON5,	11, true,	null,	"F",	4,	EnumMusicStyle.ALLRANDOM),
	MARIMBABASS 				("Marimba bass", 					600,	EnumInstrumentCategories.BASS,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.SI_TON8,	EnumNoteTonality.MI_TON5,	12, true,	null,	"F",	4,	EnumMusicStyle.ALLRANDOM),
	XYLOPHONEBASS 				("Xylophone bass", 					600,	EnumInstrumentCategories.BASS,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.SI_TON8,	EnumNoteTonality.MI_TON5,	13, true,	null,	"F",	4,	EnumMusicStyle.ALLRANDOM),
	CLOCHESTUBULAIRESBASS 		("Tubular Bells bass", 				600,	EnumInstrumentCategories.BASS,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.SI_TON8,	EnumNoteTonality.MI_TON5,	14, true,	null,	"F",	4,	EnumMusicStyle.ALLRANDOM),
	TYMPANONBASS 				("Dulcimer bass", 					600,	EnumInstrumentCategories.BASS,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.SI_TON8,	EnumNoteTonality.MI_TON5,	15, true,	null,	"F",	4,	EnumMusicStyle.ALLRANDOM),
	ORGUEHAMMONDBASS 			("Drawbar Organ bass", 				300,	EnumInstrumentCategories.BASS,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.SI_TON8,	EnumNoteTonality.MI_TON5,	16, true,	null,	"F",	4,	EnumMusicStyle.ALLRANDOM),
	ORGUEAPERCUSSIONBASS 		("Percussive Organ bass", 			300,	EnumInstrumentCategories.BASS,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.SI_TON8,	EnumNoteTonality.MI_TON5,	17, true,	null,	"F",	4,	EnumMusicStyle.ALLRANDOM),
	ORGUEROCKBASS 				("Rock Organ bass", 				300,	EnumInstrumentCategories.BASS,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.SI_TON8,	EnumNoteTonality.MI_TON5,	18, true,	null,	"F",	4,	EnumMusicStyle.ALLRANDOM),
	GRANDESORGUESBASS 			("Church Organ bass", 				100,	EnumInstrumentCategories.BASS,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.SI_TON8,	EnumNoteTonality.MI_TON5,	19, true,	null,	"F",	4,	EnumMusicStyle.ALLRANDOM),
	HARMONIUMBASS 				("Reed Organ bass",		 			100,	EnumInstrumentCategories.BASS,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.SI_TON8,	EnumNoteTonality.MI_TON5,	20, true,	null,	"F",	4,	EnumMusicStyle.ALLRANDOM),
	ACCORDEONBASS 				("Accordion bass", 					400,	EnumInstrumentCategories.BASS,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.SI_TON8,	EnumNoteTonality.DO_TON4,	21, true,	null,	"F",	4,	EnumMusicStyle.ALLRANDOM),
	HARMONICABASS 				("Harmonica bass", 					200,	EnumInstrumentCategories.BASS,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.SI_TON8,	EnumNoteTonality.MI_TON5,	22, true,	null,	"F",	4,	EnumMusicStyle.ALLRANDOM),
	ACCORDEONTANNGOBASS 		("Tango Accordion bass", 			400,	EnumInstrumentCategories.BASS,	EnumNoteTonality.SI_TON7,	EnumNoteTonality.FA_TON9,	EnumNoteTonality.SI_TON5,	23, true,	null,	"F",	4,	EnumMusicStyle.ALLRANDOM),
	
	GRANDPIANO 					("Acoustic Grand Piano",			600,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.DO_TON7,	EnumNoteTonality.DO_TON9,	EnumNoteTonality.DO_TON6,	0, false,	EnumInstruments.GRANDPIANOBASS,		"G",	2,	EnumMusicStyle.ALLRANDOM, EnumMusicStyle.MELANCOLIQUECLASSIQUE, EnumMusicStyle.TEST, EnumMusicStyle.TIERCEN),
	PIANOACOUSTIQUE 			("Bright Acoustic Piarandomo",		600,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.DO_TON7,	EnumNoteTonality.DO_TON9,	EnumNoteTonality.DO_TON6,	1, true,	EnumInstruments.PIANOACOUSTIQUEBASS,"G",	2,	EnumMusicStyle.ALLRANDOM, EnumMusicStyle.MELANCOLIQUECLASSIQUE),
	PIANOELEC 					("Electric Grand Piano",			600,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.DO_TON7,	EnumNoteTonality.MI_TON9,	EnumNoteTonality.DO_TON6,	2, true, 	EnumInstruments.PIANOELECBASS,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	PIANOBASTRIGUE 				("Honky-tonk Piano", 				600,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.DO_TON7,	EnumNoteTonality.MI_TON9,	EnumNoteTonality.DO_TON6,	3, true,	EnumInstruments.PIANOBASTRIGUEBASS,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	PIANOELECTRIQUE 			("Electric Piano 1", 				600,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.DO_TON7,	EnumNoteTonality.MI_TON9,	EnumNoteTonality.DO_TON6,	4, true,	EnumInstruments.PIANOELECTRIQUEBASS,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	PIANOCHORUS 				("Electric Piano 2", 				600,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.DO_TON7,	EnumNoteTonality.MI_TON9,	EnumNoteTonality.DO_TON6,	5, true,	EnumInstruments.PIANOCHORUSBASS,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	CLAVECIN 					("Harpsichord", 					300,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.DO_TON7,	EnumNoteTonality.FA_TON8,	EnumNoteTonality.DO_TON6,	6, true,	EnumInstruments.CLAVECINBASS,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	CLAVINET 					("Clavi", 							300,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.DO_TON7,	EnumNoteTonality.MI_TON9,	EnumNoteTonality.DO_TON6,	7, true,	EnumInstruments.CLAVINETBASS,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	CELESTA 					("Celesta", 						600,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.DO_TON7,	EnumNoteTonality.MI_TON9,	EnumNoteTonality.DO_TON6,	8, true,	EnumInstruments.CELESTABASS,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	GLOCKENSPIEL 				("Glockenspiel", 					600,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.DO_TON8,	EnumNoteTonality.LA_TON8,	EnumNoteTonality.DO_TON7,	9, true,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	BOITEAMUSIQUE 				("Music Box", 						600,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.DO_TON9,	EnumNoteTonality.DO_TON10,	EnumNoteTonality.DO_TON8,	10, true,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	VIBRAPHONE 					("Vibraphone", 						600,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.SI_TON8,	EnumNoteTonality.MI_TON5,	11, true,	EnumInstruments.VIBRAPHONEBASS,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	MARIMBA 					("Marimba", 						600,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.SI_TON8,	EnumNoteTonality.MI_TON5,	12, true,	EnumInstruments.MARIMBABASS,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	XYLOPHONE 					("Xylophone", 						600,	EnumInstrumentCategories.HIGHPITCHED,EnumNoteTonality.SI_TON6,	EnumNoteTonality.SI_TON8,	EnumNoteTonality.MI_TON5,	13, true,	EnumInstruments.XYLOPHONEBASS,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	CLOCHESTUBULAIRES 			("Tubular Bells", 					600,	EnumInstrumentCategories.HIGHPITCHED,EnumNoteTonality.LA_TON6,	EnumNoteTonality.SOL_TON7,	EnumNoteTonality.DO_TON6,	14, true,	EnumInstruments.CLOCHESTUBULAIRESBASS,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	TYMPANON 					("Dulcimer", 						600,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.LA_TON6,	EnumNoteTonality.RE_TON8,	EnumNoteTonality.MI_TON5,	15, true,	EnumInstruments.TYMPANONBASS,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	ORGUEHAMMOND 				("Drawbar Organ", 					300,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.SI_TON8,	EnumNoteTonality.MI_TON5,	16, true,	EnumInstruments.ORGUEHAMMONDBASS,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	ORGUEAPERCUSSION 			("Percussive Organ", 				300,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.SI_TON8,	EnumNoteTonality.MI_TON5,	17, true,	EnumInstruments.ORGUEAPERCUSSIONBASS,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	ORGUEROCK 					("Rock Organ", 						300,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.SI_TON8,	EnumNoteTonality.MI_TON5,	18, true,	EnumInstruments.ORGUEROCKBASS,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	GRANDESORGUES 				("Church Organ", 					100,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.SI_TON8,	EnumNoteTonality.MI_TON5,	19, true,	EnumInstruments.GRANDESORGUESBASS,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	HARMONIUM 					("Reed Organ",		 				100,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.SI_TON8,	EnumNoteTonality.MI_TON5,	20, true,	EnumInstruments.HARMONIUMBASS,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	ACCORDEON 					("Accordion", 						400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.SI_TON8,	EnumNoteTonality.DO_TON4,	21, false,	EnumInstruments.ACCORDEONBASS,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	HARMONICA 					("Harmonica", 						200,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.SI_TON8,	EnumNoteTonality.MI_TON5,	22, false,	EnumInstruments.HARMONICABASS,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	ACCORDEONTANNGO 			("Tango Accordion", 				400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON7,	EnumNoteTonality.FA_TON9,	EnumNoteTonality.SI_TON5,	23, false,	EnumInstruments.ACCORDEONTANNGOBASS,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	GUITARECLASSIQUE 			("Acoustic Guitar (nylon)", 		400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON5,	EnumNoteTonality.REDIESE_TON6,EnumNoteTonality.MI_TON5,	24, true,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	GUITARESECHE 				("Acoustic Guitar (steel)", 		400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON5,	EnumNoteTonality.SI_TON7,	EnumNoteTonality.SI_TON3,	25, true,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	GUITAREELECTRIQUEJAZZ 		("Electric Guitar (jazz)", 			400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON5,	EnumNoteTonality.SI_TON7,	EnumNoteTonality.FA_TON4,	26, true,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	GUITAREELECTRIQUESONCLAIR 	("Electric Guitar (clean)", 		400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.DO_TON5,	EnumNoteTonality.RE_TON7,	EnumNoteTonality.FA_TON4,	27, true,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	GUITAREELECTRIQUESOURDINE 	("Electric Guitar (muted)", 		400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON5,	EnumNoteTonality.SI_TON7,	EnumNoteTonality.MI_TON5,	28, true,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	GUITARESATUREE 				("Overdriven Guitar", 				400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON5,	EnumNoteTonality.SI_TON7,	EnumNoteTonality.MI_TON5,	29, true,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	GUITAREDISTORSION			("Distortion Guitar", 				400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON5,	EnumNoteTonality.RE_TON8,	EnumNoteTonality.MI_TON5,	30, true,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	HARMONIQUESGUITARE			("Guitar harmonics", 				400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON5,	EnumNoteTonality.DO_TON8,	EnumNoteTonality.MI_TON5,	31, true,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	BASSEACOUSTIQUESANSFRETE 	("Acoustic Bass", 					400,	EnumInstrumentCategories.BASS,		EnumNoteTonality.SI_TON4,	EnumNoteTonality.RE_TON6,	EnumNoteTonality.MI_TON3,	32, true,	null,		"G",	2, 									EnumMusicStyle.VALSE,EnumMusicStyle.ALLRANDOM, EnumMusicStyle.MELANCOLIQUECLASSIQUE),   // dif de 1 octave
	BASSEELECTRIQUE				("Electric Bass (finger)", 			400,	EnumInstrumentCategories.BASS,		EnumNoteTonality.SI_TON4,	EnumNoteTonality.MI_TON6,	EnumNoteTonality.SI_TON2,	33, true,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),	// dif de 1 octave
	BASSEELECTRIQUEMEDIATOR		("Electric Bass (pick)", 			400,	EnumInstrumentCategories.BASS,		EnumNoteTonality.SI_TON4,	EnumNoteTonality.MI_TON6,	EnumNoteTonality.MI_TON3,	34, true,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),	// dif de 1 octave
	BASSESANSFRETTES			("Fretless Bass", 					400,	EnumInstrumentCategories.BASS,		EnumNoteTonality.SI_TON4,	EnumNoteTonality.MI_TON6,	EnumNoteTonality.MI_TON3,	35, true,	null,		"G",	2, 									EnumMusicStyle.VALSE, EnumMusicStyle.ALLRANDOM),	// dif de 1 octave
	BASSESLAPUN					("Slap Bass 1",	 					400,	EnumInstrumentCategories.BASS,		EnumNoteTonality.SI_TON4,	EnumNoteTonality.FA_TON6,	EnumNoteTonality.MI_TON3,	36, true,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),	// dif de 1 octave
	BASSESLAPDEUX				("Slap Bass 2", 					400,	EnumInstrumentCategories.BASS,		EnumNoteTonality.SOL_TON4,	EnumNoteTonality.RE_TON6,	EnumNoteTonality.FA_TON3,	37, true,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),	// dif de 1 octave
	BASSESYNTHEUN				("Synth Bass 1", 					200,	EnumInstrumentCategories.BASS,		EnumNoteTonality.DO_TON5,	EnumNoteTonality.DO_TON9,	EnumNoteTonality.DO_TON4,	38, true,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),	// dif de 1 octave
	BASSESYNTHEDEUX				("Synth Bass 2", 					400,	EnumInstrumentCategories.BASS,		EnumNoteTonality.DO_TON5,	EnumNoteTonality.DO_TON9,	EnumNoteTonality.DO_TON4,	39, true,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),	// dif de 1 octave
	VIOLON						("Violin", 							400,	EnumInstrumentCategories.HIGHPITCHED,	EnumNoteTonality.SI_TON7,	EnumNoteTonality.DO_TON9,	EnumNoteTonality.SOL_TON5,	40, false,	null,		"G",	2, 									EnumMusicStyle.VALSE, EnumMusicStyle.ALLRANDOM, EnumMusicStyle.MELANCOLIQUECLASSIQUE),
	VIOLONALTO					("Viola", 							200,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.DO_TON6,	EnumNoteTonality.DO_TON8,	EnumNoteTonality.SOL_TON4,	41, false,	null,		"G",	2, 									EnumMusicStyle.VALSE, EnumMusicStyle.ALLRANDOM, EnumMusicStyle.MELANCOLIQUECLASSIQUE),
	VIOLONCELLE					("Cello", 							200,	EnumInstrumentCategories.BASS,		EnumNoteTonality.FA_TON5,	EnumNoteTonality.SOL_TON6,	EnumNoteTonality.LA_TON3,	42, false,	null,		"G",	2, 									EnumMusicStyle.VALSE, EnumMusicStyle.ALLRANDOM, EnumMusicStyle.MELANCOLIQUECLASSIQUE),
	CONTREBASSE					("Contrabass", 						300,	EnumInstrumentCategories.BASS,		EnumNoteTonality.MI_TON4,	EnumNoteTonality.SOL_TON5,	EnumNoteTonality.MI_TON3,	43, false,	null,		"G",	2, 									EnumMusicStyle.VALSE, EnumMusicStyle.ALLRANDOM, EnumMusicStyle.MELANCOLIQUECLASSIQUE),    // dif de 1 octave
	CORDESTREMOLO				("Tremolo Strings", 				100,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.RE_TON5,	EnumNoteTonality.RE_TON6,	EnumNoteTonality.RE_TON4,	44, false,	null,		"G",	2, 									EnumMusicStyle.VALSE, EnumMusicStyle.ALLRANDOM, EnumMusicStyle.MELANCOLIQUECLASSIQUE),
	CORDESPIZZICATO				("Pizzicato Strings", 				400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.RE_TON5,	EnumNoteTonality.RE_TON6,	EnumNoteTonality.RE_TON4,	45, false,	null,		"G",	2, 									EnumMusicStyle.VALSE, EnumMusicStyle.ALLRANDOM),
	HARPE						("Orchestral Harp", 				300,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.RE_TON5,	EnumNoteTonality.RE_TON8,	EnumNoteTonality.SI_TON3,	46, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	TIMBALES					("Timpani", 						400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.RE_TON5,	EnumNoteTonality.RE_TON6,	EnumNoteTonality.RE_TON4,	47, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	ENSASCOUSTIQUECORDEUN		("String Ensemble 1", 				200,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.RE_TON5,	EnumNoteTonality.RE_TON6,	EnumNoteTonality.RE_TON4,	48, false,	null,		"G",	2, 									EnumMusicStyle.VALSE, EnumMusicStyle.ALLRANDOM, EnumMusicStyle.MELANCOLIQUECLASSIQUE),
	ENSASCOUSTIQUECORDEDEUX		("String Ensemble 2", 				100,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.DO_TON7,	EnumNoteTonality.MI_TON8,	EnumNoteTonality.SOL_TON5,	49, false,	null,		"G",	2, 									EnumMusicStyle.VALSE, EnumMusicStyle.ALLRANDOM, EnumMusicStyle.MELANCOLIQUECLASSIQUE),
	CORDESSYNTHEUN				("SynthStrings 1", 					300,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.RE_TON5,	EnumNoteTonality.RE_TON6,	EnumNoteTonality.RE_TON4,	50, false,	null,		"G",	2, 									EnumMusicStyle.VALSE, EnumMusicStyle.ALLRANDOM),
	CORDESSYNTHEDEUX			("SynthStrings 2", 					300,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.DO_TON7,	EnumNoteTonality.DO_TON9,	EnumNoteTonality.SOL_TON5,	51, false,	null,		"G",	2, 									EnumMusicStyle.VALSE, EnumMusicStyle.ALLRANDOM),
	COEURAAH					("Choir Aahs", 						200,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.RE_TON5,	EnumNoteTonality.RE_TON6,	EnumNoteTonality.RE_TON4,	52, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM, EnumMusicStyle.MELANCOLIQUECLASSIQUE),
	COEUROOHH					("Voice Oohs", 						200,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.RE_TON5,	EnumNoteTonality.RE_TON6,	EnumNoteTonality.RE_TON4,	53, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM, EnumMusicStyle.MELANCOLIQUECLASSIQUE),
	VOIXSYNTHE					("Synth Voice", 					300,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.RE_TON5,	EnumNoteTonality.RE_TON6,	EnumNoteTonality.RE_TON4,	54, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	COUPORCHESTRE				("Orchestra Hit", 					300,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.RE_TON5,	EnumNoteTonality.RE_TON6,	EnumNoteTonality.RE_TON4,	55, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	TROMPETTE 					("Trumpet", 						300,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON4,	EnumNoteTonality.REDIESE_TON6,EnumNoteTonality.FA_TON3,		56, false,	null,		"G",	2, 								EnumMusicStyle.VALSE, EnumMusicStyle.ALLRANDOM),
	TROMBONE 					("Trombone", 						200,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.DODIESE_TON5,EnumNoteTonality.FADIESE_TON6,EnumNoteTonality.SI_TON3,	57, false,	null,		"G",	2, 								EnumMusicStyle.VALSE, EnumMusicStyle.ALLRANDOM),
	TUBA						("Tuba", 							100,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SOL_TON4,	EnumNoteTonality.MI_TON6,	EnumNoteTonality.FADIESE_TON3,	58, false,	null,		"G",	2, 								EnumMusicStyle.VALSE, EnumMusicStyle.ALLRANDOM),
	TROMPETTESOURDINE           ("Muted Trumpet", 					300,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.LA_TON6,	EnumNoteTonality.SI_TON7,	EnumNoteTonality.FA_TON5,		59, false,	null,		"G",	2, 								EnumMusicStyle.VALSE, EnumMusicStyle.ALLRANDOM),
	CORHARMONIE					("French Horn", 					200,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.DO_TON6,	EnumNoteTonality.SI_TON7,	EnumNoteTonality.RE_TON4,	60, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	SECTIONCUIVRES				("Brass Section", 					200,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SOL_TON4,	EnumNoteTonality.MI_TON6,	EnumNoteTonality.FADIESE_TON3,	61, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	CUIVRESSYNTHEUN				("SynthBrass 1", 					200,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SOL_TON4,	EnumNoteTonality.MI_TON6,	EnumNoteTonality.FADIESE_TON3,	62, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	CUIVRESSYNTHEDEUX			("SynthBrass 2", 					200,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SOL_TON4,	EnumNoteTonality.MI_TON6,	EnumNoteTonality.FADIESE_TON3,	63, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	SAXSOPRANO					("Soprano Sax", 					400,	EnumInstrumentCategories.HIGHPITCHED,EnumNoteTonality.LA_TON6,	EnumNoteTonality.MI_TON8,	EnumNoteTonality.SOL_TON5,		64, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	SAXALTO						("Alto Sax", 						400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.RE_TON6,	EnumNoteTonality.LA_TON7,	EnumNoteTonality.RE_TON5,		65, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	SAXTENOR					("Tenor Sax", 						400,	EnumInstrumentCategories.BASS,		EnumNoteTonality.RE_TON6,	EnumNoteTonality.MI_TON7,	EnumNoteTonality.LA_TON4,		66, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	SAXBARYTON					("Baritone Sax", 					300,	EnumInstrumentCategories.BASS,		EnumNoteTonality.MI_TON5,	EnumNoteTonality.LA_TON6,	EnumNoteTonality.RE_TON4,		67, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	HAUTBOIS					("Oboe", 							300,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.RE_TON6,	EnumNoteTonality.SOLDIESE_TON7,EnumNoteTonality.SI_TON4,	68, false,	null,		"G",	2, 								EnumMusicStyle.VALSE, EnumMusicStyle.ALLRANDOM, EnumMusicStyle.MELANCOLIQUECLASSIQUE),
	CORANGLAIS					("English Horn", 					300,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.DO_TON7,	EnumNoteTonality.SOL_TON8,	EnumNoteTonality.DO_TON6,		69, false,	null,		"G",	2, 								EnumMusicStyle.VALSE, EnumMusicStyle.ALLRANDOM),
	BASSON						("Bassoon", 						300,	EnumInstrumentCategories.BASS,		EnumNoteTonality.SOL_TON5,	EnumNoteTonality.DO_TON7,	EnumNoteTonality.SI_TON3,		70, false,	null,		"G",	2, 								EnumMusicStyle.VALSE, EnumMusicStyle.ALLRANDOM, EnumMusicStyle.MELANCOLIQUECLASSIQUE),
	CLARINETTE					("Clarinet", 						400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.LA_TON4,	EnumNoteTonality.FADIESE_TON6,EnumNoteTonality.DODIESE_TON3,71, false,	null,		"G",	2, 								EnumMusicStyle.VALSE, EnumMusicStyle.ALLRANDOM, EnumMusicStyle.MELANCOLIQUECLASSIQUE),
	PICOLO						("Piccolo", 						400,	EnumInstrumentCategories.HIGHPITCHED,EnumNoteTonality.SI_TON7,	EnumNoteTonality.MI_TON9,	EnumNoteTonality.FA_TON6,	72, false,	null,		"G",	2, 									EnumMusicStyle.VALSE, EnumMusicStyle.ALLRANDOM),
	FLUTE						("Flute", 							400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON4,	EnumNoteTonality.SI_TON5,	EnumNoteTonality.SI_TON3,		73, false,	null,		"G",	2, 								EnumMusicStyle.VALSE, EnumMusicStyle.ALLRANDOM),
	FLUTEABEC					("Recorder", 						200,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SOL_TON5,	EnumNoteTonality.RE_TON6,	EnumNoteTonality.FA_TON4,		74, false,	null,		"G",	2, 								EnumMusicStyle.VALSE, EnumMusicStyle.ALLRANDOM),
	FLUTEDEPAN					("Pan Flute", 						400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SOL_TON6,	EnumNoteTonality.FADIESE_TON7,EnumNoteTonality.SOL_TON5,		75, false,	null,		"G",	2, 							EnumMusicStyle.VALSE, EnumMusicStyle.ALLRANDOM),
	BOUTEILLESOUFLE				("Blown Bottle", 					300,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SOL_TON7,	EnumNoteTonality.SOL_TON8,	EnumNoteTonality.LA_TON6,	76, false,	null,		"G",	2, 									EnumMusicStyle.VALSE, EnumMusicStyle.ALLRANDOM),
	SHAKUHACHI					("Shakuhachi", 						300,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SOL_TON7,	EnumNoteTonality.SOL_TON8,EnumNoteTonality.LA_TON6,	77, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	SIFFLET						("Whistle", 						400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.RE_TON8,	EnumNoteTonality.RE_TON9,	EnumNoteTonality.MI_TON7,	78, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	OCARINA						("Ocarina", 						400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SOL_TON6,	EnumNoteTonality.FADIESE_TON7,EnumNoteTonality.SOL_TON5,	79, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	SIGNALCARRE					("Lead 1 (square)", 				400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.DO_TON9,	EnumNoteTonality.DO_TON4,	80, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	SIGNALDENTDESCIE			("Lead 2 (sawtooth)", 				200,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.DO_TON9,	EnumNoteTonality.DO_TON4,	81, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	ORGUEAVAPEUR				("Lead 3 (calliope)", 				400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.DO_TON9,	EnumNoteTonality.DO_TON4,	82, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	CHIFFRER					("Lead 4 (chiff)", 					400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.SI_TON9,	EnumNoteTonality.DO_TON4,	83, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	CHARANG						("Lead 5 (charang)", 				400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON5,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.DO_TON5,	84, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	VOIXSOLO					("Lead 6 (voice)", 					400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.DO_TON9,	EnumNoteTonality.DO_TON4,	85, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	SIGNALDENTDESCIEQUITE		("Lead 7 (fifths)", 				400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.DO_TON9,	EnumNoteTonality.DO_TON4,	86, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	SIGNALBASSESOLO				("Lead 8 (bass + lead)", 			200,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.DO_TON9,	EnumNoteTonality.DO_TON4,	87, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	FANTAISIE					("Pad 1 (new age)", 				400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.DO_TON9,	EnumNoteTonality.DO_TON4,	88, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	SONCHALEUREUX				("Pad 2 (warm)", 					200,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.DO_TON9,	EnumNoteTonality.DO_TON4,	89, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	POLYSYNTHE					("Pad 3 (polysynth)", 				400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.DO_TON9,	EnumNoteTonality.DO_TON4,	90, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	CHOEUR						("Pad 4 (choir)", 					200,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.DO_TON9,	EnumNoteTonality.DO_TON4,	91, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	ARCHET						("Pad 5 (bowed)", 					200,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.DO_TON9,	EnumNoteTonality.DO_TON4,	92, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	METALLIQUE					("Pad 6 (metallic)", 				400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.DO_TON9,	EnumNoteTonality.DO_TON4,	93, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	HALO						("Pad 7 (halo)", 					150,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.DO_TON9,	EnumNoteTonality.DO_TON4,	94, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	BALAI						("Pad 8 (sweep)", 					400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.DO_TON9,	EnumNoteTonality.DO_TON4,	95, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	PLUIEDEGLACE				("FX 1 (rain)", 					400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.DO_TON9,	EnumNoteTonality.DO_TON4,	96, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	TRAMESSONORES				("FX 2 (soundtrack)", 				400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.DO_TON9,	EnumNoteTonality.DO_TON4,	97, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	CRISTAL						("FX 3 (crystal)", 					400,	EnumInstrumentCategories.HIGHPITCHED,	EnumNoteTonality.FA_TON6,	EnumNoteTonality.DO_TON9,	EnumNoteTonality.DO_TON4,	98, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	ATMOSPHERE					("FX 4 (atmosphere)", 				400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.FA_TON6,	EnumNoteTonality.DO_TON9,	EnumNoteTonality.DO_TON4,	99, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	BRILLANCE					("FX 5 (brightness)", 				400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.DO_TON9,	EnumNoteTonality.DO_TON4,	100, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	GOBELINS					("FX 6 (goblins)", 					100,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.DO_TON9,	EnumNoteTonality.DO_TON4,	101, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	ECHOS						("FX 7 (echoes)", 					400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.DO_TON9,	EnumNoteTonality.DO_TON4,	102, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	ESPACESCIFI					("FX 8 (sci-fi)", 					400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.DO_TON9,	EnumNoteTonality.DO_TON4,	103, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	SITAR						("Sitar", 							400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.DO_TON6,	EnumNoteTonality.DO_TON8,	EnumNoteTonality.DO_TON4,	104, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	BANJO						("Banjo", 							400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.DO_TON6,	EnumNoteTonality.MI_TON7,	EnumNoteTonality.SOL_TON4,	105, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	SHAMISEN					("Shamisen", 						400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SOL_TON6,	EnumNoteTonality.SI_TON7,	EnumNoteTonality.RE_TON5,	106, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	KOTO						("Koto", 							400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.DO_TON9,	EnumNoteTonality.DO_TON4,	107, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	KALIMBA						("Kalimba", 						400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.DO_TON9,	EnumNoteTonality.DO_TON4,	108, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	CORNEMUSE					("Bag pipe", 						400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.DO_TON7,	EnumNoteTonality.LA_TON7,	EnumNoteTonality.SOL_TON6,	109, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	VIOLE						("Fiddle", 							300,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.FA_TON7,	EnumNoteTonality.LA_TON8,	EnumNoteTonality.MI_TON6,	110, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	SHEHNAI						("Shanai", 							400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON6,	EnumNoteTonality.SI_TON7,	EnumNoteTonality.RE_TON6,	111, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	CLOCHETTES					("Tinkle Bell", 					400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON4,	EnumNoteTonality.DO_TON7,	EnumNoteTonality.DO_TON1,	112, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	AGOGO						("Agogo", 							400,	EnumInstrumentCategories.MEDIUM,	EnumNoteTonality.SI_TON4,	EnumNoteTonality.DO_TON7,	EnumNoteTonality.DO_TON1,	113, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	BATTERIEMETALLIQUE			("Steel Drums", 					400,	EnumInstrumentCategories.PERCUSSION,EnumNoteTonality.DO_TON6,	EnumNoteTonality.MI_TON8,	EnumNoteTonality.DO_TON4,	114, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	PLANCHETTES					("Woodblock", 						400,	EnumInstrumentCategories.PERCUSSION,EnumNoteTonality.SI_TON4,	EnumNoteTonality.DO_TON7,	EnumNoteTonality.DO_TON1,	115, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	TIMBALLES					("Taiko Drum", 						400,	EnumInstrumentCategories.PERCUSSION,EnumNoteTonality.SI_TON2,	EnumNoteTonality.REDIESE_TON4,EnumNoteTonality.LA_TON1,	116, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	TOMMELODIQUE				("Melodic Tom", 					400,	EnumInstrumentCategories.BASS,		EnumNoteTonality.DO_TON6,	EnumNoteTonality.MI_TON7,	EnumNoteTonality.MI_TON4,	117, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	TAMBOURSYNTHETIQUE			("Synth Drum", 						400,	EnumInstrumentCategories.PERCUSSION,EnumNoteTonality.SI_TON4,	EnumNoteTonality.DO_TON7,	EnumNoteTonality.DO_TON1,	118, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	CYMBALE						("Reverse Cymbal", 					400,	EnumInstrumentCategories.PERCUSSION,EnumNoteTonality.SI_TON4,	EnumNoteTonality.DO_TON7,	EnumNoteTonality.DO_TON1,	119, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	GUITAREBRUITDEFRETTE		("Guitar Fret Noise", 				400,	EnumInstrumentCategories.PERCUSSION,EnumNoteTonality.SI_TON4,	EnumNoteTonality.DO_TON7,	EnumNoteTonality.DO_TON1,	120, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	RESPIRATION					("Breath Noise", 					400,	EnumInstrumentCategories.PERCUSSION,EnumNoteTonality.SI_TON4,	EnumNoteTonality.DO_TON7,	EnumNoteTonality.DO_TON1,	121, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	RIVAGE						("Seashore", 						100,	EnumInstrumentCategories.PERCUSSION,EnumNoteTonality.SI_TON4,	EnumNoteTonality.DO_TON7,	EnumNoteTonality.DO_TON1,	122, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	GAZOUILLI					("Bird Tweet", 						400,	EnumInstrumentCategories.PERCUSSION,EnumNoteTonality.SI_TON4,	EnumNoteTonality.DO_TON7,	EnumNoteTonality.DO_TON1,	123, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	SONNERIETEL					("Telephone Ring", 					400,	EnumInstrumentCategories.PERCUSSION,EnumNoteTonality.SI_TON4,	EnumNoteTonality.DO_TON7,	EnumNoteTonality.DO_TON1,	124, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	HELICOPTERE					("Helicopter", 						100,	EnumInstrumentCategories.PERCUSSION,EnumNoteTonality.SI_TON4,	EnumNoteTonality.DO_TON7,	EnumNoteTonality.DO_TON1,	125, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	APPLAUDISSEMENTS			("Applause", 						100,	EnumInstrumentCategories.PERCUSSION,EnumNoteTonality.SI_TON4,	EnumNoteTonality.DO_TON7,	EnumNoteTonality.DO_TON1,	126, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	COUPDEFEU					("Gunshot", 						400,	EnumInstrumentCategories.PERCUSSION,EnumNoteTonality.SI_TON4,	EnumNoteTonality.DO_TON7,	EnumNoteTonality.DO_TON1,	127, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM),
	
	PERCUSSIONS					("Percussions", 					600,	EnumInstrumentCategories.PERCUSSION,EnumNoteTonality.SI_TON5,	EnumNoteTonality.DO_TON8,	EnumNoteTonality.REDIESE_TON3,	0, false,	null,		"G",	2,	EnumMusicStyle.ALLRANDOM);
	
	private final String name;
    private final int rythmeMax; // rythm max for a black note
    private final EnumInstrumentCategories category;
    private final EnumNoteTonality noteRef;
    private final EnumNoteTonality noteMax;
    private final EnumNoteTonality noteMin;
    private String signMusicXml;
    private int lineMusicXml;
    private final int numInstrument;
    private final EnumInstruments bassInstrument;
    private boolean multipleNotes; 
    private List<EnumMusicStyle> listMusicStyle;
    
    private static final List<EnumInstruments> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
	private static final int SIZE = VALUES.size();
	private static Random RANDOM = new Random();

    private EnumInstruments(String name, int rythmeMax, EnumInstrumentCategories category, EnumNoteTonality noteRef, EnumNoteTonality noteMax, EnumNoteTonality noteMin, int numInstrument, boolean mNotes, EnumInstruments bassIns, String sign, int line, EnumMusicStyle...musicStyles) {
        this.name = name;
    	this.rythmeMax = rythmeMax;
        this.noteRef = noteRef;
        this.noteMax = noteMax;
        this.noteMin = noteMin;
        this.numInstrument = numInstrument;
        this.category = category;
        this.bassInstrument = bassIns;
        this.signMusicXml = sign;
        this.lineMusicXml = line;
        this.multipleNotes = mNotes;
        this.listMusicStyle = Arrays.asList(musicStyles);
    }
    
    public boolean getMultipleNotes() {
    	return this.multipleNotes;
    }
    
    public EnumInstruments getBassInstrument() {
    	return this.bassInstrument;
    }
    
    public String getName() {
        return name;
    }

    public int getRythmeMax() {
        return rythmeMax;
    }

    public EnumNoteTonality getNoteRef(EnumInstrumentCategories cat) {

    	if(cat != null && this.category.equals(cat)) {
    		return EnumNoteTonality.randomNoteBetween(this.noteMin, this.noteMax);
    	}
    	else if(cat != null && cat.getIndex() > this.category.getIndex()) {
    		return EnumNoteTonality.randomNoteBetween(this.noteRef, this.noteMax);
    	}
    	else if(cat != null && cat.getIndex() < this.category.getIndex()) {
    		return EnumNoteTonality.randomNoteBetween(this.noteMin, this.noteRef);
    	}
    	else {
    		return noteRef;
    	}
        
    }

    public EnumNoteTonality getNoteMax() {
        return noteMax;
    }
    
    public EnumNoteTonality getNoteMin() {
        return noteMin;
    }
    
    public EnumInstrumentCategories getCategory() {
    	return category;
    }
    
    public List<EnumMusicStyle> getListMusicStyle() {
    	return this.listMusicStyle;
    }
    
    public int getNnumInstrument() {
        return numInstrument;
    }
    
    public static EnumInstruments randomEnumInstruments()  {
		return VALUES.get(RANDOM.nextInt(SIZE));
	}
    
    public static List<EnumInstruments> getAllInstruments()  {
		return VALUES.subList(22, SIZE);
	}
    
    public boolean isNotBassInstrument() {
    	return getAllInstruments().contains(this);
    }
    
    public static List<EnumInstruments> getInstrumentsByCategory(EnumInstrumentCategories cat)  {
    	List<EnumInstruments> instruments = new ArrayList<EnumInstruments>();
    	
    	for(EnumInstruments ins:VALUES) {
    		if(ins.getCategory().equals(cat)) {
    			instruments.add(ins);
    		}
    	}
    	
    	return instruments;
	}
    
    public static List<EnumInstruments> getInstrumentsByStyle(EnumMusicStyle musicStyle)  {
    	List<EnumInstruments> instruments = new ArrayList<EnumInstruments>();
    	
    	for(EnumInstruments ins:VALUES) {
    		if(ins.getListMusicStyle().contains(musicStyle)) {
    			instruments.add(ins);
    		}
    	}
    	
    	return instruments;
	}
    
    public static List<EnumInstruments> getInstrumentsByStyleAndCategory(EnumMusicStyle musicStyle, EnumInstrumentCategories cat)  {
    	List<EnumInstruments> instruments = new ArrayList<EnumInstruments>();
    	
    	for(EnumInstruments ins:VALUES) {
    		if(ins.getListMusicStyle().contains(musicStyle) && ins.getCategory().equals(cat)) {
    			instruments.add(ins);
    		}
    	}
    	
    	return instruments;
	}
    
    public static EnumInstruments getByIndex(int index) {
    	return getAllInstruments().get(index);
    }
    
    public String getSignMusicXml() {
    	return this.signMusicXml;
    }
    
    public int getLineMusicXml() {
    	return this.lineMusicXml;
    }
}
