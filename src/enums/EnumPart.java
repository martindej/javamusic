package enums;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public enum EnumPart {

	INTRODUCTION ("Introduction"),
	PART ("Part"),
	REFRAIN ("Refrain"),
	INTERLUDE ("Interlude"),
	SOLO ("Solo");
	
	private String name = "";
	private static final List<EnumPart> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
	private static final int SIZE = VALUES.size();
	private static Random RANDOM = new Random();
   
	EnumPart(String name){
		this.name = name;
	}
   
	public String getName(){
		return name;
	}

	public static EnumPart randomEnumPart()  {
		return VALUES.get(RANDOM.nextInt(SIZE));
	}
}
