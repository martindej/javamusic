package enums;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public enum EnumPlaySentence {

	MAINMELODY ("Main melody"),
	MAINMELODYBIS ("Main melody bis"),
	NOTPLAYING ("Not playing"),
	ANSWERMELODY ("Answer to melody"),
	OTHERMELODY ("Other melody"),
	DRUMS ("Drums"),
	BASSLIGNE ("Bass ligne"),
	BASSLIGNEBIS ("Bass ligne bis");
	
	private String name = "";
	private static final List<EnumPlaySentence> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
	private static final int SIZE = VALUES.size();
	private static Random RANDOM = new Random();
   
	EnumPlaySentence(String name){
		this.name = name;
	}
   
	public String getName(){
		return name;
	}

	public static EnumPlaySentence randomEnumPlaySentence()  {
		return VALUES.get(RANDOM.nextInt(SIZE));
	}
}
