package enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;

public enum EnumDurationNote {

	RONDEPOINTEE("Ronde pointee", Constants.WHOLE, true, false, false, false, false, false, 0, 0, 144, 0.17),
	RONDE("Ronde", Constants.WHOLE, false, false, false, false, true, true, 0, 0, 96, 0.25),
	BLANCHEPOINTEE("Blanche pointee", Constants.HALF, true, false, false, false, false, false, 0, 0, 72, 0.33),
	BLANCHE("Blanche", Constants.HALF, false, false, false, false, true, true, 0, 0, 48, 0.5),
	NOIREPOINTEE("Noire pointee", Constants.QUARTER, true, false, false, false, false, false, 0, 0, 36, 0.66),
	NOIRE("Noire", Constants.QUARTER, false, false, false, false, true, true, 0, 0, 24, 1),
	CROCHEPOINTEE("Croche pointee", Constants.EIGHTH, true, false, true, false, false, false, 0, 0, 18, 1.25),
	NOIRETRIOLET("Noire triolet", Constants.QUARTER, false, true, false, true, true, true, 3, 2, 16, 1.5),
	CROCHE("Croche", Constants.EIGHTH, false, false, true, false, true, true, 0, 0, 12, 2),
	CROCHETRIOLET("Croche triolet", Constants.EIGHTH, false, true, true, true, true, true, 3, 2, 8, 3),
	DOUBLECROCHE("Double croche", Constants.STH, false, false, true, false, true, true, 0, 0, 6, 4),
	DOUBLECROCHETRIOLET("Double croche triolet", Constants.STH, false, true, true, true, true, true, 3, 2, 4, 6);

	private String name;
	private String nameMusicXml;
	private int duration;
	private boolean dot;
	private boolean timeModification;
	private boolean beam;
	private boolean ternaire;
	private boolean choicePercussion;
	private boolean regular;
	private int actualNote;
	private int normalNote;
	private double coefRythme;
	private static final List<EnumDurationNote> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
	private static Random random = new Random();

	EnumDurationNote(String name, String nameMusicXml, boolean dot, boolean timeModification, boolean beam,
			boolean ternaire, boolean choicePercussion, boolean regular, int actualNote, int normalNote, int duration,
			double coefRythme) {
		this.name = name;
		this.nameMusicXml = nameMusicXml;
		this.dot = dot;
		this.timeModification = timeModification;
		this.beam = beam;
		this.ternaire = ternaire;
		this.actualNote = actualNote;
		this.normalNote = normalNote;
		this.duration = duration;
		this.coefRythme = coefRythme;
		this.regular = regular;
		this.choicePercussion = choicePercussion;
	}

	private static class Constants {
		public static final String WHOLE = "Whole";
		public static final String HALF = "half";
		public static final String QUARTER = "quarter";
		public static final String EIGHTH = "eighth";
		public static final String STH = "16th";
	}

	public int getIndex() {
		return this.ordinal();
	}

	public static EnumDurationNote randomEnumDuration(EnumDurationChord chord) {
		return getList(chord).get(random.nextInt(getList(chord).size()));
	}

	public static EnumDurationNote randomEnumDurationPercussion(EnumDurationChord chord) {
		return getListPercussions(chord).get(random.nextInt(getListPercussions(chord).size()));
	}

	public static EnumDurationNote getEnumDurationByClosestCoefRythme(double coef, EnumDurationChord chord) {

		double min = 100;
		EnumDurationNote closestNote = null;

		for (EnumDurationNote d : getList(chord)) {
			if (Math.abs(coef - d.getCoefRythme()) < min) {
				min = Math.abs(coef - d.getCoefRythme());
				closestNote = d;
			}
		}

		return closestNote;
	}

	public static EnumDurationNote randomEnumDurationWithExclusions(List<EnumDurationNote> excludedRythms,
			EnumDurationChord chord) {

		List<EnumDurationNote> remainingRythms = getList(chord);
		remainingRythms.removeAll(excludedRythms);

		return remainingRythms.get(random.nextInt(remainingRythms.size()));
	}

	public static EnumDurationNote randomEnumDurationInList(List<EnumDurationNote> allPossibilities) {
		return allPossibilities.get(random.nextInt(allPossibilities.size()));
	}

	public static List<EnumDurationNote> getListPercussions(EnumDurationChord chord) {

		List<EnumDurationNote> valuesReturned = new ArrayList<>();

		for (EnumDurationNote d : VALUES) {
			if (d.isChoicePercussion()) {
				valuesReturned.add(d);
			}
		}

		if (chord.equals(EnumDurationChord.TWOBEAT) || chord.equals(EnumDurationChord.THREEBEAT)) {
			valuesReturned.remove(EnumDurationNote.RONDE);
		}

		if (chord.equals(EnumDurationChord.THREEBEAT)) {
			valuesReturned.remove(EnumDurationNote.BLANCHE);
			valuesReturned.remove(EnumDurationNote.NOIRETRIOLET);
		}

		return valuesReturned;
	}

	public static List<EnumDurationNote> getList(EnumDurationChord chord) {

		List<EnumDurationNote> valuesReturned = new ArrayList<>();

		for (EnumDurationNote d : VALUES) {
			valuesReturned.add(d);
		}

		if (chord.equals(EnumDurationChord.TWOBEAT) || chord.equals(EnumDurationChord.THREEBEAT)) {
			valuesReturned.remove(EnumDurationNote.RONDEPOINTEE);
			valuesReturned.remove(EnumDurationNote.RONDE);
		}
		return valuesReturned;
	}

	public static EnumDurationNote randomSelectedEnumDuration(Map<EnumDurationNote, Integer> allowedRythmes, int toEnd,
			int untilTempsFort, int pourcSyncope, boolean contreTemps) {

		int i;
		double ratio;
		List<EnumDurationNote> notSyncope = new ArrayList<>();
		List<EnumDurationNote> syncope = new ArrayList<>();
		List<EnumDurationNote> all = new ArrayList<>();

		if (contreTemps) {
			for (Map.Entry<EnumDurationNote, Integer> n : allowedRythmes.entrySet()) {

				if (n.getKey().getDuration() < EnumDurationNote.NOIRE.duration && n.getKey().getDuration() <= toEnd) {
					for (i = 0; i < n.getValue(); i++) {
						all.add(n.getKey());
					}
				}
			}
		}

		if (!contreTemps || all.isEmpty()) {
			for (Map.Entry<EnumDurationNote, Integer> n : allowedRythmes.entrySet()) {

				if (n.getKey().getDuration() > untilTempsFort && n.getKey().getDuration() <= toEnd) {
					for (i = 0; i < n.getValue(); i++) {
						syncope.add(n.getKey());
					}
				}
				// not a syncope directly but cases where this rythm imply a syncope at the next
				// note
				else if (n.getKey().getDuration() < untilTempsFort && (!allowedRythmes
						.containsKey(getLongestNoteByDuration(untilTempsFort - n.getKey().getDuration()))
						|| allowedRythmes.get(getLongestNoteByDuration(untilTempsFort - n.getKey().getDuration())) == 0)
						&& n.getKey().getDuration() <= toEnd) {
					for (i = 0; i < n.getValue(); i++) {
						syncope.add(n.getKey());
					}
				} else if (n.getKey().getDuration() <= untilTempsFort && n.getKey().getDuration() <= toEnd) {
					for (i = 0; i < n.getValue(); i++) {
						notSyncope.add(n.getKey());
					}
				}
			}

			// TODO : precise the differents syncopes, and changes pourcentages chances
			// depend on situations
			if (!syncope.isEmpty() && !notSyncope.isEmpty()) {

				ratio = ((100 - pourcSyncope) * syncope.size()) / (double) (notSyncope.size() * 10);

				if (ratio < 1) {
					ratio = ((100 - pourcSyncope) * notSyncope.size()) / (double) (syncope.size() * 10);

					all.addAll(notSyncope);
					for (i = 0; i < ratio; i++) {
						all.addAll(syncope);
					}
				} else {
					all.addAll(syncope);
					for (i = 0; i < ratio; i++) {
						all.addAll(notSyncope);
					}
				}
			} else if (!syncope.isEmpty()) {
				all.addAll(syncope);
			} else if (!notSyncope.isEmpty()) {
				all.addAll(notSyncope);
			}
		}

		if (all.isEmpty()) {
			System.out.println("error : no rythms availables for this situation-- toEnd = " + toEnd);
			return getDurationNoteByDuration(toEnd);
		}

		return all.get(random.nextInt(all.size()));
	}

	public static EnumDurationNote randomSelectedEnumDurationLastChord(Map<EnumDurationNote, Integer> allowedRythmes,
			int duration, int toEnd) {

		int i;

		if (duration == 8 || duration == 16) {
			return EnumDurationNote.CROCHETRIOLET;
		} else {

			allowedRythmes.remove(EnumDurationNote.CROCHETRIOLET);

			List<EnumDurationNote> availableNotes = new ArrayList<>();

			for (Map.Entry<EnumDurationNote, Integer> n : allowedRythmes.entrySet()) {

				if (n.getKey().getDuration() <= toEnd) {
					for (i = 0; i < n.getValue(); i++) {
						availableNotes.add(n.getKey());
					}
				}
			}

			// TODO change that and put a silence if that append
			if (availableNotes.isEmpty()) {

				for (EnumDurationNote n : VALUES) {

					if (n.getDuration() <= toEnd && !n.equals(EnumDurationNote.CROCHETRIOLET)) {
						availableNotes.add(n);
					}
				}
			}

			if (availableNotes.isEmpty()) {
				return null;
			}

			return availableNotes.get(random.nextInt(availableNotes.size()));
		}
	}

	public static EnumDurationNote randomSelectedOutOfBeatBinaire(Map<EnumDurationNote, Integer> allowedRythmes,
			int duration, int toEnd, int untilTempsFort, int pourcSyncope, int pourcSyncopeTempsFort,
			int pourcSyncopette) {

		int i;
		List<EnumDurationNote> notSyncope = new ArrayList<>();
		List<EnumDurationNote> syncopeNotTempFort = new ArrayList<>();
		List<EnumDurationNote> syncopeTempFort = new ArrayList<>();
		List<EnumDurationNote> syncopette = new ArrayList<>();
		List<EnumDurationNote> allPourcSyncope = new ArrayList<>();
		List<EnumDurationNote> allPourcSyncopette = new ArrayList<>();
		List<EnumDurationNote> allPourcSyncopeTempsFort = new ArrayList<>();
		List<EnumDurationNote> all = new ArrayList<>();

		for (Map.Entry<EnumDurationNote, Integer> n : allowedRythmes.entrySet()) {

			if (!n.getKey().isTernaire()) {
				if (duration == 18 && n.getKey().equals(EnumDurationNote.CROCHE) && n.getKey().getDuration() <= toEnd) {
					for (i = 0; i < n.getValue(); i++) {
						syncopette.add(n.getKey());
					}
				}
				// list of note with short duration to get the next beat
				else if (n.getKey().getDuration() <= duration && n.getKey().getDuration() <= toEnd) {
					for (i = 0; i < n.getValue(); i++) {
						notSyncope.add(n.getKey());
					}
				} else if (n.getKey().getDuration() > duration && n.getKey().getDuration() <= untilTempsFort
						&& n.getKey().getDuration() <= toEnd) {
					for (i = 0; i < n.getValue(); i++) {
						syncopeNotTempFort.add(n.getKey());
					}
				}
				// not a syncope directly but cases where this rythm imply a syncope at the next
				// note
				else if (n.getKey().getDuration() < duration && n.getKey().getDuration() <= untilTempsFort
						&& (!allowedRythmes.containsKey(getLongestNoteByDuration(duration - n.getKey().getDuration()))
								|| allowedRythmes
										.get(getLongestNoteByDuration(duration - n.getKey().getDuration())) == 0)
						&& n.getKey().getDuration() <= toEnd) {
					for (i = 0; i < n.getValue(); i++) {
						syncopeNotTempFort.add(n.getKey());
					}
				} else if (n.getKey().getDuration() > untilTempsFort && n.getKey().getDuration() <= toEnd) {
					for (i = 0; i < n.getValue(); i++) {
						syncopeTempFort.add(n.getKey());
					}
				}
				// not a syncope directly but cases where this rythm imply a syncope at the next
				// note
				else if (n.getKey().getDuration() < untilTempsFort && (!allowedRythmes
						.containsKey(getLongestNoteByDuration(untilTempsFort - n.getKey().getDuration()))
						|| allowedRythmes.get(getLongestNoteByDuration(untilTempsFort - n.getKey().getDuration())) == 0)
						&& n.getKey().getDuration() <= toEnd) {
					for (i = 0; i < n.getValue(); i++) {
						syncopeTempFort.add(n.getKey());
					}
				}
			}
		}

		if (!notSyncope.isEmpty() && !syncopeNotTempFort.isEmpty()) {
			allPourcSyncope = createListProportionals(notSyncope, syncopeNotTempFort, pourcSyncope);
		}

		if (!notSyncope.isEmpty() && !syncopeTempFort.isEmpty()) {
			allPourcSyncopeTempsFort = createListProportionals(notSyncope, syncopeTempFort, pourcSyncopeTempsFort);
		}

		if (!notSyncope.isEmpty() && !syncopette.isEmpty()) {
			allPourcSyncopette = createListProportionals(notSyncope, syncopette, pourcSyncopette);
		}

		all.addAll(allPourcSyncope);
		all.addAll(allPourcSyncopette);
		all.addAll(allPourcSyncopeTempsFort);

		if (all.isEmpty()) {
			all.addAll(syncopeNotTempFort);
		}
		if (all.isEmpty()) {
			all.addAll(syncopeTempFort);
		}

		if (all.isEmpty()) {
			EnumDurationNote noteDurationReturn = getDurationNoteByDuration(toEnd);
			if (noteDurationReturn == null) {
				System.out.println("problem here randomSelectedOutOfBeatBinaire");
			}
			return noteDurationReturn;
		}

		return all.get(random.nextInt(all.size()));
	}

	private static List<EnumDurationNote> createListProportionals(List<EnumDurationNote> notSyncope,
			List<EnumDurationNote> syncope, int pourcSyncope) {

		int i;
		int j;
		int ratio;
		List<EnumDurationNote> allPourcSyncope = new ArrayList<>();

		if (syncope.size() <= notSyncope.size()) {

			ratio = notSyncope.size() / syncope.size();

			for (i = 0; i < 100; i++) {
				if (i < pourcSyncope) {
					for (j = 0; j < ratio; j++) {
						allPourcSyncope.addAll(syncope);
					}
				} else {
					allPourcSyncope.addAll(notSyncope);
				}
			}
		} else {
			ratio = syncope.size() / notSyncope.size();

			for (i = 0; i < 100; i++) {
				if (i < pourcSyncope) {
					allPourcSyncope.addAll(syncope);
				} else {
					for (j = 0; j < ratio; j++) {
						allPourcSyncope.addAll(notSyncope);
					}
				}
			}
		}

		return allPourcSyncope;
	}

	public static EnumDurationNote randomSelectedOutOfBeatTernaire(Map<EnumDurationNote, Integer> allowedRythmes,
			int duration, int toEnd, int untilTempsFort, int pourcSyncopette, int pourcSyncopeTempsFort) {

		int i;
		List<EnumDurationNote> notSyncope = new ArrayList<>();
		List<EnumDurationNote> syncopeNotTempFort = new ArrayList<>();
		List<EnumDurationNote> syncopeTempFort = new ArrayList<>();
		List<EnumDurationNote> syncopette = new ArrayList<>();
		List<EnumDurationNote> allPourcSyncopette = new ArrayList<>();
		List<EnumDurationNote> allPourcSyncopeTempsFort = new ArrayList<>();
		List<EnumDurationNote> all = new ArrayList<>();

		for (Map.Entry<EnumDurationNote, Integer> n : allowedRythmes.entrySet()) {

			if (n.getKey().isTernaire()) {
				// list of note with short duration to get the next beat
				if (n.getKey().getDuration() <= duration && n.getKey().getDuration() <= toEnd) {
					for (i = 0; i < n.getValue(); i++) {
						notSyncope.add(n.getKey());
					}
				} else if (n.getKey().getDuration() > duration && n.getKey().getDuration() <= untilTempsFort
						&& n.getKey().getDuration() <= toEnd) {
					for (i = 0; i < n.getValue(); i++) {
						syncopeNotTempFort.add(n.getKey());
					}
				} else if (n.getKey().getDuration() > untilTempsFort && n.getKey().getDuration() <= toEnd) {
					for (i = 0; i < n.getValue(); i++) {
						syncopeTempFort.add(n.getKey());
					}
				}
			}
		}

		if (!notSyncope.isEmpty() && !syncopeTempFort.isEmpty()) {
			allPourcSyncopeTempsFort = createListProportionals(notSyncope, syncopeTempFort, pourcSyncopeTempsFort);
		}

		if (!notSyncope.isEmpty() && !syncopette.isEmpty()) {
			allPourcSyncopette = createListProportionals(notSyncope, syncopette, pourcSyncopette);
		}

		all.addAll(notSyncope);
		all.addAll(allPourcSyncopette);
		all.addAll(allPourcSyncopeTempsFort);

		if (all.isEmpty()) {
			all.addAll(syncopeNotTempFort);
		}
		if (all.isEmpty()) {
			all.addAll(syncopeTempFort);
		}

		if (all.isEmpty()) {
			EnumDurationNote noteDurationReturn = getDurationNoteByDuration(toEnd);
			if (noteDurationReturn == null) {
				System.out.println("problem here randomSelectedOutOfBeatTernaire");
			}
			return noteDurationReturn;
		}

		return all.get(random.nextInt(all.size()));
	}

	public static EnumDurationNote getDurationNoteByDuration(int duration) {

		for (EnumDurationNote n : VALUES) {
			if (n.getDuration() == duration) {
				return n;
			}
		}
		return null;
	}

	public static EnumDurationNote getDurationNoteByClosestDuration(int duration) {

		int min = 999;
		EnumDurationNote minDuration = null;

		for (EnumDurationNote n : VALUES) {
			if (Math.abs(n.duration - duration) < min) {
				min = Math.abs(n.getDuration() - duration);
				minDuration = n;
			}
		}
		return minDuration;
	}

	public static EnumDurationNote getLongestNoteByDuration(int duration) {

		EnumDurationNote max = EnumDurationNote.DOUBLECROCHETRIOLET;

		for (EnumDurationNote n : VALUES) {
			if (duration >= n.getDuration() && n.getDuration() > max.duration) {
				max = n;
			}
		}
		return max;
	}

	public static EnumDurationNote getByIndex(int rhythm) {
		return VALUES.get(rhythm);
	}

	public int getDuration() {
		return duration;
	}

	public double getCoefRythme() {
		return coefRythme;
	}

	public String getName() {
		return name;
	}

	public String getNameMusicXml() {
		return nameMusicXml;
	}

	public boolean hasDot() {
		return this.dot;
	}

	public boolean hasTimeModification() {
		return this.timeModification;
	}

	public boolean hasBeam() {
		return this.beam;
	}

	public int getActualNote() {
		return this.actualNote;
	}

	public int getNormalNote() {
		return this.normalNote;
	}

	public boolean isTernaire() {
		return ternaire;
	}

	public boolean isChoicePercussion() {
		return choicePercussion;
	}

	public boolean isRregular() {
		return regular;
	}
}
