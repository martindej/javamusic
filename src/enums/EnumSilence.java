package enums;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public enum EnumSilence {

	PLAY ("Play"),
	SILENCE ("Silence");
	
	private String name = "";
	private static final List<EnumSilence> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
	private static final int SIZE = VALUES.size();
	private static Random RANDOM = new Random();
   
	EnumSilence(String name){
		this.name = name;
	}
   
	public String getName(){
		return name;
	}

	public static EnumSilence randomEnumSilence()  {
		return VALUES.get(RANDOM.nextInt(SIZE));
	}
	
	public static EnumSilence getRandomEnumSilenceByPourcentage(int pourcentageSilence)  {
		
		int random = RANDOM.nextInt(100) + 1;
		
		if(random > pourcentageSilence) {
			return PLAY;
		}
		else {
			return SILENCE;
		}
	}
}
