package enums;

public enum EnumNote {

	DO 			("Do", 			"C", 0,		"C", 0),
	DODIESE 	("Do diese", 	"C", 1,		"D", -1),
	RE 			("Re", 			"D", 0,		"D", 0),
	REDIESE 	("Re diese", 	"D", 1,		"E", -1),
	MI 			("Mi", 			"E", 0,		"E", 0),
	FA 			("Fa", 			"F", 0,		"F", 0),
	FADIESE 	("Fa diese", 	"F", 1,		"G", -1),
	SOL 		("Sol", 		"G", 0,		"G", 0),
	SOLDIESE 	("Sol diese", 	"G", 1,		"A", -1),
	LA 			("La", 			"A", 0,		"A", 0),
	LADIESE 	("La diese", 	"A", 1,		"B", -1),
	SI 			("Si", 			"B", 0,		"B", 0);
   
	private String name = "";
	private String americanName1 = "";
	private int alter1;
	private String americanName2 = "";
	private int alter2;

	EnumNote(String name, String americanName1, int alter1, String americanName2, int alter2) {
		this.name = name;
		this.americanName1 = americanName1;
		this.alter1 = alter1;
		this.americanName2 = americanName2;
		this.alter2 = alter2;
	}
   
	public String toString() {
		return name;
	}
	
	public String americanName(EnumTonalite tonality) {
		if(tonality.getAlteration().equals("sharp")) {
			return americanName1;
		}
		else {
			return americanName2;
		}
	}
	
	public int getAlter(EnumTonalite tonality) {
		if(tonality.getAlteration().equals("sharp")) {
			return alter1;
		}
		else {
			return alter2;
		}
	}
}
