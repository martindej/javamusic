package enums;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public enum EnumMajMin {
	
	MAJEURE ("major"),
	MINEURE ("minor");
   
	private String name = "";
	private static final List<EnumMajMin> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
	private static final int SIZE = VALUES.size();
	private static Random RANDOM = new Random();
   
	EnumMajMin(String name){
		this.name = name;
	}
   
	public String toString(){
		return name;
	}

	public static EnumMajMin randomEnumMajMin()  {
		return VALUES.get(RANDOM.nextInt(SIZE));
	}
}
