package enums;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public enum EnumInstrumentCategories {

	// TODO delete list of instruments
	BASS ("Bass"),
	MEDIUM ("Medium"),
	HIGHPITCHED ("Hight pitched"),
	PERCUSSION ("Percussion");
	   
	private String name = "";
	private static final List<EnumInstrumentCategories> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
	private static final int SIZE = VALUES.size();
	private static Random RANDOM = new Random();
   
	EnumInstrumentCategories(String name){
		this.name = name;
	}
   
	public String getName(){
		return name;
	}
	
	public int getIndex() {
		return VALUES.indexOf(this);
	}

	public static EnumInstrumentCategories randomCategory()  {
		return VALUES.get(RANDOM.nextInt(SIZE));
	}
}
