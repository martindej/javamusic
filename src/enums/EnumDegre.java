package enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import musicRandom.Chord;
import styles.StyleCoefDegres;

public enum EnumDegre {

	I("Degre I", 0),
	II("Degre II", 1),
	III("Degre III", 2),
	IV("Degre IV", 3),
	V("Degre V", 4),
	VI("Degre VI", 5),
	VII("Degre VII", 6);

	private String name;
	private int index;

	private static final List<EnumDegre> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
	private static final int SIZE = VALUES.size();
	private static Random random = new Random();

	EnumDegre(String name, int index) {
		this.name = name;
		this.index = index;
	}

	public String getName() {
		return name;
	}

	public int getIndex() {
		return index;
	}

	public EnumDegre getDegreRandomByReference(List<Chord> allowedChords, StyleCoefDegres styleCoefDegres) {

		List<EnumDegre> listDegreRandom = new ArrayList<>();
		int j = 0;

		for (Integer i : styleCoefDegres.getCMap().get(this)) {
			EnumDegre deg = getEnumDegreByIndex(j);

			if (listChordsContains(allowedChords, deg)) {
				for (int t = 0; t < i; t++) {
					listDegreRandom.add(deg);
				}
			}
			j++;
		}

		if (!listDegreRandom.isEmpty()) {
			return listDegreRandom.get(random.nextInt(listDegreRandom.size()));
		} else {
			return null;
		}
	}

	public EnumDegre getDegreRandom(StyleCoefDegres styleCoefDegres) {

		List<EnumDegre> listDegreRandom = new ArrayList<>();
		int j = 0;

		for (Integer i : styleCoefDegres.getCMap().get(this)) {

			EnumDegre deg = getEnumDegreByIndex(j);

			for (int t = 0; t < i; t++) {
				listDegreRandom.add(deg);
			}
			j++;
		}

		if (!listDegreRandom.isEmpty()) {
			return listDegreRandom.get(random.nextInt(listDegreRandom.size()));
		} else {
			return null;
		}
	}

	private boolean listChordsContains(List<Chord> allowedChords, EnumDegre degre) {

		for (Chord c : allowedChords) {
			if (c.getDegre().equals(degre)) {
				return true;
			}
		}
		return false;
	}

	public EnumDegre getEnumDegreByIndex(int i) {

		switch (i) {
		case 0:
			return EnumDegre.I;
		case 1:
			return EnumDegre.II;
		case 2:
			return EnumDegre.III;
		case 3:
			return EnumDegre.IV;
		case 4:
			return EnumDegre.V;
		case 5:
			return EnumDegre.VI;
		case 6:
			return EnumDegre.VII;
		default:
			return null;
		}
	}

	public static EnumDegre randomEnumDegre() {
		return VALUES.get(random.nextInt(SIZE));
	}
}
