package enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import musicRandom.Chord;

public enum EnumTonalite {

	DODMAJEUR("C# major", "Do # Majeur", -7, "LADMINEUR", Constants.SHARP, EnumMajMin.MAJEURE, EnumNote.DODIESE,
			EnumNote.REDIESE, EnumNote.FA, EnumNote.FADIESE, EnumNote.SOLDIESE, EnumNote.LADIESE, EnumNote.DO),
	FADMAJEUR("F# major", "Fa # Majeur", -6, "REDMINEUR", Constants.SHARP, EnumMajMin.MAJEURE, EnumNote.FADIESE,
			EnumNote.SOLDIESE, EnumNote.LADIESE, EnumNote.SI, EnumNote.DODIESE, EnumNote.REDIESE, EnumNote.FA),
	SIMAJEUR("B major", "Si Majeur", -5, "SOLDMINEUR", Constants.SHARP, EnumMajMin.MAJEURE, EnumNote.SI,
			EnumNote.DODIESE, EnumNote.REDIESE, EnumNote.MI, EnumNote.FADIESE, EnumNote.SOLDIESE, EnumNote.LADIESE),
	MIMAJEUR("E major", "Mi Majeur", -4, "DODMINEUR", Constants.SHARP, EnumMajMin.MAJEURE, EnumNote.MI,
			EnumNote.FADIESE, EnumNote.SOLDIESE, EnumNote.LA, EnumNote.SI, EnumNote.DODIESE, EnumNote.REDIESE),
	LAMAJEUR("A major", "La Majeur", -3, "FADMINEUR", Constants.SHARP, EnumMajMin.MAJEURE, EnumNote.LA, EnumNote.SI,
			EnumNote.DODIESE, EnumNote.RE, EnumNote.MI, EnumNote.FADIESE, EnumNote.SOLDIESE),
	REMAJEUR("D major", "R� Majeur", -2, "SIMINEUR", Constants.SHARP, EnumMajMin.MAJEURE, EnumNote.RE, EnumNote.MI,
			EnumNote.FADIESE, EnumNote.SOL, EnumNote.LA, EnumNote.SI, EnumNote.DODIESE),
	SOLMAJEUR("G major", "Sol Majeur", -1, "MIMINEUR", Constants.SHARP, EnumMajMin.MAJEURE, EnumNote.SOL, EnumNote.LA,
			EnumNote.SI, EnumNote.DO, EnumNote.RE, EnumNote.MI, EnumNote.FADIESE),
	DOMAJEUR("C major", "Do Majeur", 0, "LAMINEUR", Constants.SHARP, EnumMajMin.MAJEURE, EnumNote.DO, EnumNote.RE,
			EnumNote.MI, EnumNote.FA, EnumNote.SOL, EnumNote.LA, EnumNote.SI),
	FAMAJEUR("F major", "Fa Majeur", 1, "REMINEUR", Constants.FLAT, EnumMajMin.MAJEURE, EnumNote.FA, EnumNote.SOL,
			EnumNote.LA, EnumNote.LADIESE, EnumNote.DO, EnumNote.RE, EnumNote.MI),
	SIBMAJEUR("Bb major", "Si b Majeur", 2, "SOLMINEUR", Constants.FLAT, EnumMajMin.MAJEURE, EnumNote.LADIESE,
			EnumNote.DO, EnumNote.RE, EnumNote.REDIESE, EnumNote.FA, EnumNote.SOL, EnumNote.LA),
	MIBMAJEUR("Eb major", "Mi b Majeur", 3, "DOMINEUR", Constants.FLAT, EnumMajMin.MAJEURE, EnumNote.REDIESE,
			EnumNote.FA, EnumNote.SOL, EnumNote.SOLDIESE, EnumNote.LADIESE, EnumNote.DO, EnumNote.RE),
	LABMAJEUR("Ab major", "La b Majeur", 4, "FAMINEUR", Constants.FLAT, EnumMajMin.MAJEURE, EnumNote.SOLDIESE,
			EnumNote.LADIESE, EnumNote.DO, EnumNote.DODIESE, EnumNote.REDIESE, EnumNote.FA, EnumNote.SOL),
	REBMAJEUR("Db major", "Re b Majeur", 5, "SIBMINEUR", Constants.FLAT, EnumMajMin.MAJEURE, EnumNote.DODIESE,
			EnumNote.REDIESE, EnumNote.FA, EnumNote.FADIESE, EnumNote.SOLDIESE, EnumNote.LADIESE, EnumNote.DO),
	SOLBMAJEUR("Gb major", "Sol b Majeur", 6, "MIBMINEUR", Constants.FLAT, EnumMajMin.MAJEURE, EnumNote.FADIESE,
			EnumNote.SOLDIESE, EnumNote.LADIESE, EnumNote.SI, EnumNote.DODIESE, EnumNote.REDIESE, EnumNote.FA),
	DOBMAJEUR("Cb major", "Do b Majeur", 7, "LABMINEUR", Constants.FLAT, EnumMajMin.MAJEURE, EnumNote.SI,
			EnumNote.DODIESE, EnumNote.REDIESE, EnumNote.MI, EnumNote.FADIESE, EnumNote.SOLDIESE, EnumNote.LADIESE),

	LADMINEUR("A# minor", "La # mineur", -7, "DODMAJEUR", Constants.SHARP, EnumMajMin.MINEURE, EnumNote.LADIESE,
			EnumNote.DO, EnumNote.DODIESE, EnumNote.REDIESE, EnumNote.FA, EnumNote.FADIESE, EnumNote.LA),
	REDMINEUR("D# minor", "R� # mineur", -6, "FADMAJEUR", Constants.SHARP, EnumMajMin.MINEURE, EnumNote.REDIESE,
			EnumNote.FA, EnumNote.FADIESE, EnumNote.SOLDIESE, EnumNote.LADIESE, EnumNote.SI, EnumNote.RE),
	SOLDMINEUR("G# minor", "Sol # mineur", -5, "SIMAJEUR", Constants.SHARP, EnumMajMin.MINEURE, EnumNote.SOLDIESE,
			EnumNote.LADIESE, EnumNote.SI, EnumNote.DODIESE, EnumNote.REDIESE, EnumNote.MI, EnumNote.SOL),
	DODMINEUR("C# minor", "Do # mineur", -4, "MIMAJEUR", Constants.SHARP, EnumMajMin.MINEURE, EnumNote.DODIESE,
			EnumNote.REDIESE, EnumNote.MI, EnumNote.FADIESE, EnumNote.SOLDIESE, EnumNote.LA, EnumNote.DO),
	FADMINEUR("F# minor", "Fa # mineur", -3, "LAMAJEUR", Constants.SHARP, EnumMajMin.MINEURE, EnumNote.FADIESE,
			EnumNote.SOLDIESE, EnumNote.LA, EnumNote.SI, EnumNote.DODIESE, EnumNote.RE, EnumNote.FA),
	SIMINEUR("B minor", "Si mineur", -2, "REMAJEUR", Constants.SHARP, EnumMajMin.MINEURE, EnumNote.SI, EnumNote.DODIESE,
			EnumNote.RE, EnumNote.MI, EnumNote.FADIESE, EnumNote.SOL, EnumNote.LADIESE),
	MIMINEUR("E minor", "Mi mineur", -1, "SOLMAJEUR", Constants.SHARP, EnumMajMin.MINEURE, EnumNote.MI,
			EnumNote.FADIESE, EnumNote.SOL, EnumNote.LA, EnumNote.SI, EnumNote.DO, EnumNote.REDIESE),
	LAMINEUR("A minor", "La mineur", 0, "DOMAJEUR", Constants.FLAT, EnumMajMin.MINEURE, EnumNote.LA, EnumNote.SI,
			EnumNote.DO, EnumNote.RE, EnumNote.MI, EnumNote.FA, EnumNote.SOLDIESE),
	REMINEUR("D minor", "R� mineur", 1, "FAMAJEUR", Constants.FLAT, EnumMajMin.MINEURE, EnumNote.RE, EnumNote.MI,
			EnumNote.FA, EnumNote.SOL, EnumNote.LA, EnumNote.LADIESE, EnumNote.DODIESE),
	SOLMINEUR("G minor", "Sol mineur", 2, "SIBMAJEUR", Constants.FLAT, EnumMajMin.MINEURE, EnumNote.SOL, EnumNote.LA,
			EnumNote.LADIESE, EnumNote.DO, EnumNote.RE, EnumNote.REDIESE, EnumNote.FADIESE),
	DOMINEUR("C minor", "Do mineur", 3, "MIBMAJEUR", Constants.FLAT, EnumMajMin.MINEURE, EnumNote.DO, EnumNote.RE,
			EnumNote.REDIESE, EnumNote.FA, EnumNote.SOL, EnumNote.SOLDIESE, EnumNote.SI),
	FAMINEUR("F minor", "Fa mineur", 4, "LABMAJEUR", Constants.FLAT, EnumMajMin.MINEURE, EnumNote.FA, EnumNote.SOL,
			EnumNote.SOLDIESE, EnumNote.LADIESE, EnumNote.DO, EnumNote.DODIESE, EnumNote.MI),
	SIBMINEUR("Bb minor", "Si b mineur", 5, "REBMAJEUR", Constants.FLAT, EnumMajMin.MINEURE, EnumNote.LADIESE,
			EnumNote.DO, EnumNote.DODIESE, EnumNote.REDIESE, EnumNote.FA, EnumNote.FADIESE, EnumNote.LA),
	MIBMINEUR("Eb minor", "Mi b mineur", 6, "SOLBMAJEUR", Constants.FLAT, EnumMajMin.MINEURE, EnumNote.REDIESE,
			EnumNote.FA, EnumNote.FADIESE, EnumNote.SOLDIESE, EnumNote.LADIESE, EnumNote.SI, EnumNote.RE),
	LABMINEUR("Ab minor", "La b mineur", 7, "DOBMAJEUR", Constants.FLAT, EnumMajMin.MINEURE, EnumNote.SOLDIESE,
			EnumNote.LADIESE, EnumNote.SI, EnumNote.DODIESE, EnumNote.REDIESE, EnumNote.MI, EnumNote.SOL);

	private String tonality;
	private int midiIndex;
	private String relative;
	private EnumMajMin majMin;
	private String alteration;
	private String engName;
	private List<EnumNote> availableNotes;
	private static final List<EnumTonalite> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
	private static final int SIZE = VALUES.size();
	private static Random random = new Random();

	EnumTonalite(String engName, String tonality, int midiIndex, String relative, String alteration, EnumMajMin majMin,
			EnumNote... enumNotes) {
		this.engName = engName;
		this.tonality = tonality;
		this.midiIndex = midiIndex;
		this.relative = relative;
		this.alteration = alteration;
		this.majMin = majMin;
		this.availableNotes = Arrays.asList(enumNotes);
	}

	private static class Constants {
		public static final String SHARP = "sharp";
		public static final String FLAT = "flat";
	}

	public int getMidiIndex() {
		return midiIndex;
	}

	public int getMusicXmlIndex() {
		return -midiIndex;
	}

	public String getEngName() {
		return this.engName;
	}

	public int getMidiMajMin() {
		if (majMin.equals(EnumMajMin.MAJEURE)) {
			return 1;
		} else {
			return 0;
		}
	}

	public static EnumTonalite getByMidi(int majMin, int midiIndex) {

		for (EnumTonalite ton : VALUES) {
			if (ton.getMidiMajMin() == majMin && ton.getMidiIndex() == midiIndex) {
				return ton;
			}
		}
		return null;
	}

	public static EnumTonalite getByEngName(String engName) {

		for (EnumTonalite ton : VALUES) {
			if (ton.getEngName().equals(engName)) {
				return ton;
			}
		}
		return null;
	}

	public static EnumTonalite getByIndex(int index) {
		return VALUES.get(index);
	}

	public String getAlteration() {
		return this.alteration;
	}

	public String getTonality() {
		return this.tonality;
	}

	public EnumNote getTonale() {
		return this.getAvailableNotes().get(0);
	}

	public EnumMajMin getMajMin() {
		return this.majMin;
	}

	public EnumTonalite getRelative() {
		return EnumTonalite.valueOf(this.relative);
	}

	public List<EnumNote> getAvailableNotes() {
		return this.availableNotes;
	}

	public static EnumTonalite randomEnumTonality() {
		return VALUES.get(random.nextInt(SIZE));
	}

	public static List<EnumTonalite> getAllTonalities() {
		return VALUES;
	}

	public static List<EnumTonalite> getAllTonalitiesMajOrMin(EnumMajMin majOrMin) {

		List<EnumTonalite> all = new ArrayList<>();

		for (EnumTonalite ton : VALUES) {
			if (ton.majMin.equals(majOrMin)) {
				all.add(ton);
			}
		}
		return all;
	}

	public Chord getAccordByListNotes(List<EnumNote> listNote) {

		int dif = 0;
		int bestCandidate = 99;
		Chord chord = null;

		for (EnumChordNumber chordNumber : EnumChordNumber.values()) {
			for (EnumDegre degre : EnumDegre.values()) {
				List<EnumNote> list = getNotesAccord(chordNumber, degre);
				for (EnumNote note : listNote) {

					if (!list.contains(note)) {
						dif++;
					}
				}

				if (dif < bestCandidate) {
					bestCandidate = dif;
					chord = new Chord(chordNumber, degre);
				}

				dif = 0;
			}
		}

		return chord;
	}

	public List<EnumNote> getNotesAccord(EnumChordNumber accord, EnumDegre degre) {

		List<EnumNote> notesAccord = new ArrayList<>();
		int index;

		for (int indexNote : accord.getListNotes()) {
			index = (degre.getIndex() + indexNote) % 7;
			notesAccord.add(availableNotes.get(index));
		}

		return notesAccord;
	}

	public EnumNote getNoteChordByIndex(Chord chord, int indexNote) {

		int index;
		index = (chord.getDegre().getIndex() + indexNote) % 6;

		return this.availableNotes.get(index);
	}

	// get the degre of note in tonality
	public int getDegreOfNoteInTonality(EnumNote note) {

		for (int i = 0; i < this.availableNotes.size(); i++) {
			if (note.equals(this.availableNotes.get(i))) {
				return i;
			}
		}
		return -1;
	}

	// get the degre of note in chord
	public int getDegreOfNoteInChord(EnumNote note, Chord refChord) {

		List<EnumNote> notesChord = getNotesAccord(refChord.getNumberAccord(), refChord.getDegre());

		for (int i = 0; i < notesChord.size(); i++) {
			if (note.equals(notesChord.get(i))) {
				return i;
			}
		}
		return -1;
	}

	public EnumNote getNoteByDegreInChord(int degre, Chord refChord) {

		List<EnumNote> notesChord = getNotesAccord(refChord.getNumberAccord(), refChord.getDegre());

		if (notesChord.size() > degre) {
			return notesChord.get(degre);
		} else {
			degre = degre % notesChord.size();
			return notesChord.get(degre);
		}
	}

	public EnumNote getNoteWithDeviation(EnumNoteTonality n, int dev) {

		int index = this.availableNotes.indexOf(n.getNote());

		if (index == -1) {
			index = random.nextInt(7);
		}

		return this.availableNotes.get((index + dev * 2) % (this.availableNotes.size() - 1));
	}

	public EnumNote getFundamentalNote(EnumChordNumber accord, EnumDegre degre) {

		// la fondamentale est la premiere note de l'accord dans son etat fondamental
		int index = (degre.getIndex() + accord.getListNotes().get(0)) % 6;
		return this.availableNotes.get(index);
	}

	// TODO change to add cases where there is notes outside the tonality
	public static EnumTonalite getByListNote(List<EnumNote> listNoteTonality, List<EnumNote> firstBassNotes) {

		List<EnumTonalite> possibleTonalities = new ArrayList<>();
		int dif = 0;
		int minDif = 99;

		for (EnumTonalite t : VALUES) {
			dif = 0;
			for (EnumNote n : listNoteTonality) {
				if (!t.getAvailableNotes().contains(n)) {
					dif++;
				}
			}
			if (dif < minDif) {
				minDif = dif;
			}
		}

		for (EnumTonalite t : VALUES) {
			dif = 0;
			for (EnumNote n : listNoteTonality) {
				if (!t.getAvailableNotes().contains(n)) {
					dif++;
				}
			}
			if (dif == minDif) {
				possibleTonalities.add(t);
			}
		}

		// majeur or mineur
		if (!possibleTonalities.isEmpty()) {
			for (EnumTonalite t : possibleTonalities) {
				if (firstBassNotes.contains(t.getTonale())) {
					return t;
				}

				else if (firstBassNotes.contains(t.getRelative().getTonale())) {
					return t.getRelative();
				}

				else {
					return t;
				}
			}
		}
		return null;
	}
}
