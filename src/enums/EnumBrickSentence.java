package enums;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public enum EnumBrickSentence {

	MELODY1("Melody 1"),
	MELODY2("Melody 2"),
	MOTIF1("Motif 1"),
	MOTIF2("Motif 2"),
	BASSLIGNE1("Bass ligne 1"),
	BASSLIGNE2("Bass ligne 2"),
	REPETEDITEM1("Repeted item 1"),
	DRUMS("Drums");

	private String name = "";
	private static final List<EnumBrickSentence> VALUES = Collections.unmodifiableList(Arrays.asList(values()));

	EnumBrickSentence(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
