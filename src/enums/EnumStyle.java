package enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public enum EnumStyle {

	ASCENDING ("Ascending"),
	DESCENDING ("Descending"),
	NEUTRAL ("Neutral");
	
	private String name = "";
	private static final List<EnumStyle> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
	private static final int SIZE = VALUES.size();
	private static Random RANDOM = new Random();
   
	EnumStyle(String name){
		this.name = name;
	}
   
	public String getName(){
		return name;
	}
	
	public static EnumStyle detPosNeg(EnumStyle style,EnumInstruments instrument, EnumNoteTonality previousNote) {
		
		switch(style) {
			case ASCENDING:
				if(Math.abs(instrument.getNoteMax().getIndex() - previousNote.getIndex()) < 7) {
					return DESCENDING;
				}
				else {
					return ASCENDING;
				}
			case DESCENDING:
				if(Math.abs(instrument.getNoteMin().getIndex() - previousNote.getIndex()) < 7) {
					return ASCENDING;
				}
				else {
					return DESCENDING;
				}
			case NEUTRAL:
				return NEUTRAL;
			default:
				return NEUTRAL;
		}
	}

	public static EnumStyle randomEnumStyle()  {
		return VALUES.get(RANDOM.nextInt(SIZE));
	}
}
