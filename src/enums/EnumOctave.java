package enums;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public enum EnumOctave {

	OCTAVE1 ("Octave 1", 	-1),
	OCTAVE2 ("Octave 2", 	0),
	OCTAVE3 ("Octave 3", 	1),
	OCTAVE4 ("Octave 4", 	2),
	OCTAVE5 ("Octave 5", 	3),
	OCTAVE6 ("Octave 6", 	4),
	OCTAVE7 ("Octave 7", 	5),
	OCTAVE8 ("Octave 8", 	6),
	OCTAVE9 ("Octave 9", 	7),
	OCTAVE10 ("Octave 10", 	8),
	OCTAVE11 ("Octave 11", 	9);
	
	private String name = "";
	private int numberMusicXml;
	private static final List<EnumOctave> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
	private static final int SIZE = VALUES.size();
	private static Random RANDOM = new Random();
   
	EnumOctave(String name, int numberMusicXml){
		this.name = name;
		this.numberMusicXml = numberMusicXml;
	}
	
	public int getNumberMusicXml() {
		return this.numberMusicXml;
	}
   
	public String getName(){
		return name;
	}
	
	public int getIndex() {
		return VALUES.indexOf(this);
	}
	
	public static EnumOctave getOctaveByIndex(int index) {
		return VALUES.get(index);
	}
}

