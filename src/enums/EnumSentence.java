package enums;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public enum EnumSentence {

	INTRODUCTION	("Introduction"),
	SENTENCE 		("Sentence"),
	REFRAIN 		("Refrain"),
	SILENCE			("Silence"),
	INTERLUDE 		("Interlude"),
	SOLO 			("Solo");
	
	private String name = "";
	private static final List<EnumSentence> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
	private static final int SIZE = VALUES.size();
	private static Random RANDOM = new Random();
   
	EnumSentence(String name){
		this.name = name;
	}
   
	public String getName(){
		return name;
	}

	public static EnumSentence randomEnumSentence()  {
		return VALUES.get(RANDOM.nextInt(SIZE));
	}
}
