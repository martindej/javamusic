package enums;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public enum EnumIntervals {

	NONE ("None", 					0),
	SECONDMINOR ("Second minor",	1),
	SECOND ("Second",				2),
	THIRDMINOR ("Third Minor",		3),
	THIRD ("Third",					4),
	FOURTH ("Fourth",				5),
	FIFTHDIM ("Fith dim",			6),
	FIFTH ("Fifth",					7),
	SIXTHMINOR ("Sixth minor",		8),
	SIXTH ("Sixth",					9),
	SEVENTHMINOR ("Seventh minor",	10),
	SEVENTH ("SEVENTH",				11),
	EIGHT ("Eigth",					12);
	
	private String name = "";
	private int differenceWithTonale;
	private static final List<EnumIntervals> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
	private static final int SIZE = VALUES.size();
	private static Random RANDOM = new Random();
   
	EnumIntervals(String name, int dif){
		this.name = name;
		this.differenceWithTonale = dif;
	}
   
	public String getName(){
		return this.name;
	}
	
	public int getDifferenceWithTonale() {
		return this.differenceWithTonale;
	}

	public static EnumIntervals randomEnumInterval()  {
		return VALUES.get(RANDOM.nextInt(SIZE));
	}
}
