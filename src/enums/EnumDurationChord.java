package enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public enum EnumDurationChord {

	TWOBEAT("Two beat", 2, 2, 4, 2, EnumMusicStyle.ALLRANDOM),
	THREEBEAT("Three beat", 3, 3, 4, 2, EnumMusicStyle.ALLRANDOM, EnumMusicStyle.VALSE),
	FOURBEAT("Four beat", 4, 4, 4, 2, EnumMusicStyle.ALLRANDOM);

	private String name;
	private int duration;
	private int numerator;
	private int denominator;
	private int denominatorMidi;
	private List<EnumMusicStyle> listMusicStyle;

	private static final List<EnumDurationChord> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
	private static final int SIZE = VALUES.size();
	private static Random random = new Random();

	EnumDurationChord(String name, int duration, int numerator, int denominator, int denominatorMidi,
			EnumMusicStyle... musicStyles) {
		this.name = name;
		this.duration = duration;
		this.numerator = numerator;
		this.denominator = denominator;
		this.denominatorMidi = denominatorMidi;
		this.listMusicStyle = Arrays.asList(musicStyles);
	}

	public static EnumDurationChord randomEnumDuration() {
		return VALUES.get(random.nextInt(SIZE));
	}

	public static List<EnumDurationChord> getAllDuration() {
		return VALUES;
	}

	public static List<EnumDurationChord> getdurationsByStyle(EnumMusicStyle musicStyle) {
		List<EnumDurationChord> durationChord = new ArrayList<>();

		for (EnumDurationChord d : VALUES) {
			if (d.getListMusicStyle().contains(musicStyle)) {
				durationChord.add(d);
			}
		}

		return durationChord;
	}

	public static EnumDurationChord getByIndex(int index) {
		return VALUES.get(index);
	}

	public List<EnumMusicStyle> getListMusicStyle() {
		return this.listMusicStyle;
	}

	public int getDuration() {
		return this.duration;
	}

	public String getName() {
		return this.name;
	}

	public int getNumerator() {
		return this.numerator;
	}

	public int getDenominator() {
		return this.denominator;
	}

	public int getDenominatorMidi() {
		// 2 -> 4, 3 ->8 ...etc 2^dd
		return this.denominatorMidi;
	}

	public static EnumDurationChord getByNumAndDen(int num, int den) {
		for (EnumDurationChord d : VALUES) {
			if (d.numerator == num && d.denominatorMidi == den) {
				return d;
			}
		}
		return null;
	}
}
