package enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import musicRandom.Chord;

public enum EnumChordNumber {

	ACCORD_I(" ", 0, 2, 4, 0, 0),
	ACCORD_II("7", 0, 2, 4, 5, 0),
	ACCORD_III("9", 0, 2, 4, 6, 0),
	ACCORD_IV("11", 0, 2, 4, 6, 1);

	private String name;
	private int notesAccord1;
	private int notesAccord2;
	private int notesAccord3;
	private int notesAccord4;
	private int notesAccord5;

	private static final List<EnumChordNumber> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
	private static final int SIZE = VALUES.size();
	private static Random random = new Random();

	EnumChordNumber(String name, int note1, int note2, int note3, int note4, int note5) {
		this.name = name;
		this.notesAccord1 = note1;
		this.notesAccord2 = note2;
		this.notesAccord3 = note3;
		this.notesAccord4 = note4;
		this.notesAccord5 = note5;
	}

	public String getName() {
		return name;
	}

	public List<Integer> getListNotes() {
		List<Integer> allNotesAccord = new ArrayList<>();
		allNotesAccord.add(notesAccord1);
		allNotesAccord.add(notesAccord2);
		allNotesAccord.add(notesAccord3);
		if (notesAccord4 != 0) {
			allNotesAccord.add(notesAccord4);
		}
		if (notesAccord5 != 0) {
			allNotesAccord.add(notesAccord5);
		}

		return allNotesAccord;
	}

	public static EnumChordNumber randomEnumNumberAccord() {
		return VALUES.get(random.nextInt(SIZE));
	}

	public static EnumChordNumber randomEnumNumberAccordByReference(List<Chord> allowedChords, EnumDegre degre) {

		List<Chord> shortListChords = new ArrayList<>();

		for (Chord c : allowedChords) {
			if (c.getDegre().equals(degre)) {
				shortListChords.add(c);
			}
		}
		Chord result = shortListChords.get(random.nextInt(shortListChords.size()));

		return result.getNumberAccord();
	}

	public static EnumChordNumber randomEnumNumberAccordWithBoundaries(int complexityChordMin, int complexityChordMax) {
		List<EnumChordNumber> subList = VALUES.subList(complexityChordMin - 1, complexityChordMax);
		return subList.get(random.nextInt(subList.size()));
	}
}
