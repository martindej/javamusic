package enums;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public enum EnumVelocityVariation {

	MINUS3("minus 3", -3),
	MINUS2("minus 2", -2),
	MINUS1("minus 1", -1),
	NEUTRAL("neutral", 0),
	PLUS1("plus 1", 1),
	PLUS2("plus 2", 2),
	PLUS3("plus 3", 3);

	private String name = "";
	private int value;
	private static final List<EnumVelocityVariation> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
	private static final int SIZE = VALUES.size();
	private static Random random = new Random();

	EnumVelocityVariation(String name, int value) {
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return this.name;
	}

	public int getValue() {
		return this.value;
	}

	public static EnumVelocityVariation randomEnumVelocityVariation() {
		return VALUES.get(random.nextInt(SIZE));
	}

	public static EnumVelocityVariation getClosestValue(double velocityVar) {

		double min = 999;
		EnumVelocityVariation minValue = null;

		for (EnumVelocityVariation vVar : VALUES) {
			if (Math.abs(velocityVar - vVar.getValue()) < min) {
				minValue = vVar;
			}
		}

		return minValue;
	}

}
