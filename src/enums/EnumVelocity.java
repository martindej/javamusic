package enums;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public enum EnumVelocity {

	PPPP("pppp", 8),
	PPP("ppp", 20),
	PP("pp", 31),
	P("p", 42),
	MP("mp", 53),
	MF("mf", 64),
	F("f", 80),
	FF("ff", 96),
	FFF("fff", 112),
	FFFF("ffff", 127);

	private String name = "";
	private int value;
	private static final List<EnumVelocity> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
	private static final int SIZE = VALUES.size();
	private static Random random = new Random();

	EnumVelocity(String name, int value) {
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return this.name;
	}

	public int getValue() {
		return this.value;
	}

	public EnumVelocity getVelocityAfterVariation(EnumVelocityVariation variation) {

		int index = this.ordinal() + variation.getValue();

		if (index < 0) {
			index = 0;
		}

		if (index >= SIZE) {
			index = SIZE - 1;
		}

		return VALUES.get(index);
	}

	public static EnumVelocity randomEnumVelocity() {

		int variance;
		int mediane;
		int rand;

		variance = 3;
		mediane = 6; // correspond au forte

		rand = (int) (random.nextGaussian() * Math.sqrt(variance - 1) + mediane);

		if (rand >= VALUES.size()) {
			rand = VALUES.size() - 1;
		}
		if (rand < 0) {
			rand = 0;
		}
		return VALUES.get(rand);
	}

	public static EnumVelocity getClosestValue(int volume) {

		EnumVelocity closestVelocity = null;
		int min = 999;

		for (EnumVelocity v : VALUES) {

			if (Math.abs(v.getValue() - volume) < min) {
				min = Math.abs(v.getValue() - volume);
				closestVelocity = v;
			}
		}

		return closestVelocity;
	}
}
