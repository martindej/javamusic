package enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import styles.MesureTempsFort;

public enum EnumTempsForts {
		
	FIRST (1,0, EnumMusicStyle.ALLRANDOM, EnumMusicStyle.VALSE),
	SECOND (2,24, EnumMusicStyle.ALLRANDOM),
	THIRD (3,48, EnumMusicStyle.ALLRANDOM),
	FOURTH (4,72, EnumMusicStyle.ALLRANDOM),
	FIFTH (5,96, EnumMusicStyle.ALLRANDOM);
	
	private int number,cursor;
	private List<EnumMusicStyle> listMusicStyle;
	
	private static List<EnumTempsForts> tempsForts = new ArrayList<EnumTempsForts>();
	private static final List<EnumTempsForts> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
	private static final int SIZE = VALUES.size();
	private static Random RANDOM = new Random();
   
	EnumTempsForts(int number, int cursor, EnumMusicStyle...musicStyles){
		this.number = number;
		this.cursor = cursor;
		this.listMusicStyle = Arrays.asList(musicStyles);
	}
   
	public List<EnumTempsForts> getListTempsForts(){
		return tempsForts;
	}
	
	public static EnumTempsForts getByNumber(int index) {
		return VALUES.get(index - 1);
	}
	
	public static List<EnumTempsForts> getAll() {
		return VALUES;
	}
	
	public static EnumTempsForts getByIndex(int index) {
    	return VALUES.get(index);
    }

	public static List<EnumTempsForts> randomEnumTempsForts(EnumDurationChord chordDuration)  {
		
		// generate a random number centered on 1 with a derivation of the value of numerator
		//Double u = Math.abs(RANDOM.nextGaussian()*(chordDuration.getNumerator()-1)) + 1; 
		//int numberTempsForts = u.intValue();
		
		int numberTempsForts = RANDOM.nextInt(2) + 1;
		
		//generate the list of possibilities, the first beat is the most used
		List<EnumTempsForts> randomList = new ArrayList<EnumTempsForts>();
		for(int i=1;i<=chordDuration.getNumerator();i++) {
			randomList.add(EnumTempsForts.FIRST);
			randomList.add(EnumTempsForts.getByNumber(i));
		}

		do {
			EnumTempsForts temp = randomList.get(RANDOM.nextInt(randomList.size()));
			if(!tempsForts.contains(temp)) {
				tempsForts.add(temp);
			}
		} 
		while(tempsForts.size() < numberTempsForts);

		return tempsForts;
	}
	
	public static List<EnumTempsForts> getFromStyle(MesureTempsFort mesureTempsFort)  {

		if(mesureTempsFort.isEtOu()) {
			return mesureTempsFort.getTempsFort();
		}
		else {
			List<EnumTempsForts> randomList = new ArrayList<EnumTempsForts>();
			randomList.add(mesureTempsFort.getTempsFort().get(RANDOM.nextInt(mesureTempsFort.getTempsFort().size())));
			
			return randomList;
		}
	}

	public int getNumber() {
		return number;
	}

	public int getCursor() {
		return cursor;
	}
	
	public static List<EnumTempsForts> getTempsFortsByStyle(EnumMusicStyle musicStyle)  {
    	List<EnumTempsForts> tForts = new ArrayList<EnumTempsForts>();
    	
    	for(EnumTempsForts d:VALUES) {
    		if(d.getListMusicStyle().contains(musicStyle)) {
    			tForts.add(d);
    		}
    	}
    	
    	return tForts;
	}
	
	public List<EnumMusicStyle> getListMusicStyle() {
    	return this.listMusicStyle;
    }
}
