package enums;

import java.util.ArrayList;
import java.util.List;

public enum EnumMusicStyle {
	
	ALLRANDOM ("All random"),
	TEST ("Test"),
	TIERCEN ("Tiercen"),
	VALSE ("Valse"),
	MELANCOLIQUECLASSIQUE ("classique melancolique"),
	BLUES ("blues");
	
	private String name;
	
	EnumMusicStyle(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	public static String getListAsString() {
		String list="";
		
		for(EnumMusicStyle s:EnumMusicStyle.values()) {
			if(list.isEmpty()) {
				list = s.name;
			}
			else {
				list = list+","+s.name;
			}
		}
		
		return list;
	}
	
	public static List<String> getList() {
		
		List<String> list = new ArrayList<String>();
		
		for(EnumMusicStyle s:EnumMusicStyle.values()) {
			list.add(s.name);
		}
		
		return list;
	}

	public static EnumMusicStyle getByString(String style) {
		
		for(EnumMusicStyle s:EnumMusicStyle.values()) {
			if(s.toString().equals(style)) {
				return s;
			}
		}
		
		return null;
	}
	
	public static EnumMusicStyle getByName(String style) {
		
		for(EnumMusicStyle s:EnumMusicStyle.values()) {
			if(s.name.equals(style)) {
				return s;
			}
		}
		
		return null;
	}
}
