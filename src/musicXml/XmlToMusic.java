package musicXml;

import musicRandom.Music;

public class XmlToMusic {
	
	private String xml;
	private Music musicFromXml;
	
	XmlToMusic(String xml) {
		this.xml = xml;
	}
	
	public Music getMusicFromXml() {
		return musicFromXml;
		
	}

}
