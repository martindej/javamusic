package musicXml;

import java.io.File;
import java.io.IOException;

import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequence;
import javax.xml.parsers.ParserConfigurationException;

import org.jfugue.integration.MusicXmlParser;
import org.jfugue.midi.MidiParserListener;

import nu.xom.ParsingException;

public class XmlToMidi {

	String xmlFile;

	public XmlToMidi(String xmlFile) {
		this.xmlFile = xmlFile;
	}

	public void convertXmlToMidi() throws IOException, ParserConfigurationException, ParsingException {

		MusicXmlParser parser = new MusicXmlParser();
		MidiParserListener listener = new MidiParserListener();
		parser.addParserListener(listener);
		parser.parse(this.xmlFile);
		Sequence seq = listener.getSequence();
		MidiSystem.write(seq, 1, new File("temp.mid"));
	}
}
