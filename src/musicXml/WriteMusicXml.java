package musicXml;

import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import enums.EnumDurationNote;
import enums.EnumNoteTonality;
import enums.EnumSilence;
import enums.EnumVelocity;
import musicRandom.Chord;
import musicRandom.Instrument;
import musicRandom.Music;
import musicRandom.Note;

public class WriteMusicXml {

	Music m;
	String musicXml;

	private static final String SOUND = "sound";
	private static final String START = "start";
	private static final String CONTINUE = "continue";
	private static final String STOP = "stop";

	public WriteMusicXml(Music mu, String xml) {
		this.m = mu;
		write(xml);
	}

	public String getMusicXml() {
		return this.musicXml;
	}

	private void write(String xml) {

		// instance of a DocumentBuilderFactory
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.newDocument();

			// root element
			Element rootElement = doc.createElement("score-partwise");
			doc.appendChild(rootElement);

			// setting attribute to element
			Attr attr = doc.createAttribute("version");
			attr.setValue("3.1");
			rootElement.setAttributeNode(attr);

			writeHeader(doc, rootElement, xml);
			writePartList(doc, rootElement);
			writeParts(doc, rootElement);

			// write the content into xml file
			this.musicXml = toString(doc);
		} catch (Exception el) {
			el.printStackTrace();
		}
	}

	private String toString(Document doc) {
		try {
			StringWriter sw = new StringWriter();
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

			transformer.transform(new DOMSource(doc), new StreamResult(sw));
			return sw.toString();
		} catch (Exception ex) {
			throw new RuntimeException("Error converting to String", ex);
		}
	}

	private void writeHeader(Document doc, Element rootElement, String xml) {

		Attr attr;
		int workN = 1;

		Element work = doc.createElement("work");
		rootElement.appendChild(work);

		Element workNumber = doc.createElement("work-number");
		workNumber.appendChild(doc.createTextNode(String.valueOf(workN)));
		work.appendChild(workNumber);

		Element workTitle = doc.createElement("work-title");
		workTitle.appendChild(doc.createTextNode(xml));
		work.appendChild(workTitle);

		Element movementNumber = doc.createElement("movement-number");
		movementNumber.appendChild(doc.createTextNode(String.valueOf(workN)));
		rootElement.appendChild(movementNumber);

		Element movementTitle = doc.createElement("movement-title");
		movementTitle.appendChild(doc.createTextNode(xml));
		rootElement.appendChild(movementTitle);

		Element identification = doc.createElement("identification");
		rootElement.appendChild(identification);

		Element composer = doc.createElement("creator");
		attr = doc.createAttribute("type");
		attr.setValue("composer");
		composer.setAttributeNode(attr);
		composer.appendChild(doc.createTextNode("MurMure"));
		identification.appendChild(composer);

		Element rights = doc.createElement("rights");
		rights.appendChild(doc.createTextNode("Copyright � 2017 SASU CodeMure"));
		identification.appendChild(rights);

		Element encoding = doc.createElement("encoding");
		identification.appendChild(encoding);

		Element encoder = doc.createElement("encoder");
		encoder.appendChild(doc.createTextNode("MurMure.fr"));
		encoding.appendChild(encoder);

		Element software = doc.createElement("software");
		software.appendChild(doc.createTextNode("MurMure.fr"));
		encoding.appendChild(software);
	}

	private void writeParts(Document doc, Element rootElement) {

		Element part;
		Attr attr;
		int partNumber = 1;

		for (Instrument ins : this.m.getListInstruments()) {

			part = doc.createElement("part");
			rootElement.appendChild(part);

			// setting attribute to element
			attr = doc.createAttribute("id");
			attr.setValue("P" + partNumber);
			part.setAttributeNode(attr);

			writeMeasures(doc, part, ins, partNumber == 1);
			partNumber++;
		}

	}

	private void writeMeasures(Document doc, Element part, Instrument ins, boolean firstInstrument) {

		int measureNumber = 1;
		int durationInFirstMeasure;
		int durationInSecondMeasure;
		int cursor = 0;
		int iterator = 0;
		int velocity = 0;
		Element measure = null;
		Note nextNote = null;
		Note previousNote = null;
		Chord currentChord = null;

		for (Note note : ins.getListNotes()) {

			if (ins.getListNotes().size() > iterator + 1) {
				nextNote = ins.getListNotes().get(iterator + 1);
			} else {
				nextNote = null;
			}

			if (iterator > 0 && ins.getListNotes().size() > iterator - 1) {
				previousNote = ins.getListNotes().get(iterator - 1);
			} else {
				previousNote = null;
			}

			cursor = cursor + note.getDuration();
			currentChord = note.getCurrentChord();

			if (this.getMeasureNumber(cursor, note) + 1 == measureNumber) {
				measure = createMeasure(doc, part, ins, measureNumber, note, velocity);
				if (firstInstrument) {
					writeChord(doc, measure, currentChord);
				}
				if (measure.getElementsByTagName(SOUND).getLength() > 0) {
					velocity = Integer.parseInt(
							measure.getElementsByTagName(SOUND).item(0).getAttributes().item(0).getNodeValue());
				}
				measureNumber++;
			}

			if (this.onSameMeasure(cursor, note)) {
				writeNote(doc, measure, note, 0, "", onBeat(cursor, note), onEndBeat(cursor), nextNote, previousNote);
			} else {
				durationInFirstMeasure = this.getDurationInFirstMeasure(cursor, note);

				if (EnumDurationNote.getDurationNoteByDuration(durationInFirstMeasure) == null) {
					EnumDurationNote maxDuration = EnumDurationNote.getLongestNoteByDuration(durationInFirstMeasure);
					writeNote(doc, measure, note, maxDuration.getDuration(), START, onBeat(cursor, note),
							onEndBeat(cursor), nextNote, previousNote);
					writeNote(doc, measure, note, durationInFirstMeasure - maxDuration.getDuration(), CONTINUE,
							onBeat(cursor, note), onEndBeat(cursor), nextNote, previousNote);
				} else {
					writeNote(doc, measure, note, durationInFirstMeasure, START, onBeat(cursor, note),
							onEndBeat(cursor), nextNote, previousNote);
				}

				measure = createMeasure(doc, part, ins, measureNumber, note, velocity);
				if (firstInstrument) {
					writeChord(doc, measure, currentChord);
				}

				if (measure.getElementsByTagName(SOUND).getLength() > 0) {
					velocity = Integer.parseInt(
							measure.getElementsByTagName(SOUND).item(0).getAttributes().item(0).getNodeValue());
				}
				measureNumber++;

				durationInSecondMeasure = this.getDurationInSecondMeasure(cursor, note);

				if (EnumDurationNote.getDurationNoteByDuration(durationInSecondMeasure) == null) {
					EnumDurationNote maxDuration = EnumDurationNote.getLongestNoteByDuration(durationInSecondMeasure);
					writeNote(doc, measure, note, maxDuration.getDuration(), CONTINUE, onBeat(cursor, note),
							onEndBeat(cursor), nextNote, previousNote);
					writeNote(doc, measure, note, durationInSecondMeasure - maxDuration.getDuration(), STOP,
							onBeat(cursor, note), onEndBeat(cursor), nextNote, previousNote);
				} else {
					writeNote(doc, measure, note, durationInSecondMeasure, STOP, onBeat(cursor, note),
							onEndBeat(cursor), nextNote, previousNote);
				}

			}

			iterator++;
		}

	}

	private int getMeasureNumber(int cursor, Note note) {
		return (cursor - note.getDuration()) / (this.m.getAccordsDuration().getDuration() * 24);
	}

	private boolean onBeat(int cursor, Note note) {
		return (cursor - note.getDuration()) % 24 == 0;
	}

	private boolean onEndBeat(int cursor) {
		return cursor % 24 == 0;
	}

	private boolean onSameMeasure(int cursor, Note note) {

		int measureBeginNote = (cursor - note.getDuration()) / (this.m.getAccordsDuration().getDuration() * 24);
		int measureEndNote = cursor / (this.m.getAccordsDuration().getDuration() * 24);
		int resultEniereFraction = cursor % (this.m.getAccordsDuration().getDuration() * 24);

		return ((measureBeginNote == measureEndNote)
				|| ((measureBeginNote + 1 == measureEndNote) && resultEniereFraction == 0));
	}

	private int getDurationInFirstMeasure(int cursor, Note note) {

		int measureNumber = (cursor - note.getDuration()) / (this.m.getAccordsDuration().getDuration() * 24);
		return (measureNumber + 1) * this.m.getAccordsDuration().getDuration() * 24 - (cursor - note.getDuration());
	}

	private int getDurationInSecondMeasure(int cursor, Note note) {

		int measureNumber = (cursor - note.getDuration()) / (this.m.getAccordsDuration().getDuration() * 24);
		return cursor - (measureNumber + 1) * this.m.getAccordsDuration().getDuration() * 24;
	}

	private void writeNote(Document doc, Element measure, Note no, int durationN, String tieString, boolean onBeat,
			boolean onEndBeat, Note nextNote, Note previousNote) {

		int iterator = 0;
		int durationNote;

		if (durationN == 0) {
			durationNote = no.getDuration();
		} else {
			durationNote = durationN;
		}

		for (EnumNoteTonality noTonality : no.getListNoteNum()) {

			Element note = doc.createElement("note");
			measure.appendChild(note);

			if (iterator > 0) {
				Element chord = doc.createElement("chord");
				note.appendChild(chord);
			}

			if (no.getSilenceOrPlaying().equals(EnumSilence.SILENCE)) {

				Element rest = doc.createElement("rest");
				Attr attr = doc.createAttribute("measure");
				if (durationN == this.m.getAccordsDuration().getDuration() * 24) {
					attr.setValue(String.valueOf("yes"));
				} else {
					attr.setValue(String.valueOf("no"));
				}

				rest.setAttributeNode(attr);
				note.appendChild(rest);
			} else {
				Element pitch = doc.createElement("pitch");
				note.appendChild(pitch);

				Element step = doc.createElement("step");
				step.appendChild(doc.createTextNode(noTonality.getNote().americanName(this.m.getTonality())));
				pitch.appendChild(step);

				if (no.getListNoteNum().get(0).getNote().getAlter(this.m.getTonality()) != 0) {
					Element alter = doc.createElement("alter");
					alter.appendChild(
							doc.createTextNode(String.valueOf(noTonality.getNote().getAlter(this.m.getTonality()))));
					pitch.appendChild(alter);
				}

				Element octave = doc.createElement("octave");
				octave.appendChild(doc.createTextNode(String.valueOf(noTonality.getOctave().getNumberMusicXml())));
				pitch.appendChild(octave);
			}

			Element duration = doc.createElement("duration");
			duration.appendChild(doc.createTextNode(String.valueOf(durationNote)));
			note.appendChild(duration);

			if (durationN > 0 && no.getSilenceOrPlaying().equals(EnumSilence.PLAY)) {
				Element tie = doc.createElement("tie");
				Attr attr = doc.createAttribute("type");
				attr.setValue(String.valueOf(tieString));
				tie.setAttributeNode(attr);
				note.appendChild(tie);
			}

			Element voice = doc.createElement("voice");
			voice.appendChild(doc.createTextNode(String.valueOf(1)));
			note.appendChild(voice);

			Element type = doc.createElement("type");
			type.appendChild(
					doc.createTextNode(EnumDurationNote.getDurationNoteByDuration(durationNote).getNameMusicXml()));
			note.appendChild(type);

			if (EnumDurationNote.getDurationNoteByDuration(durationNote).hasDot()) {
				Element dot = doc.createElement("dot");
				note.appendChild(dot);
			}

			if (EnumDurationNote.getDurationNoteByDuration(durationNote).hasTimeModification()) {
				Element timeModification = doc.createElement("time-modification");
				note.appendChild(timeModification);

				Element actualNotes = doc.createElement("actual-notes");
				actualNotes.appendChild(doc.createTextNode(
						String.valueOf(EnumDurationNote.getDurationNoteByDuration(durationNote).getActualNote())));
				timeModification.appendChild(actualNotes);

				Element normalNotes = doc.createElement("normal-notes");
				normalNotes.appendChild(doc.createTextNode(
						String.valueOf(EnumDurationNote.getDurationNoteByDuration(durationNote).getNormalNote())));
				timeModification.appendChild(normalNotes);

				if (onBeat || onEndBeat) {
					Element notations = doc.createElement("notations");
					note.appendChild(notations);

					Element tuplet = doc.createElement("tuplet");
					Attr attr = doc.createAttribute("type");

					if (onBeat) {
						attr.setValue(String.valueOf(START));
						Attr attr2 = doc.createAttribute("bracket");
						attr2.setValue(String.valueOf("yes"));
						tuplet.setAttributeNode(attr2);
					} else if (onEndBeat) {
						attr.setValue(String.valueOf(STOP));
					}

					tuplet.setAttributeNode(attr);
					notations.appendChild(tuplet);
				}

			}

			if (nextNote != null && EnumDurationNote.getDurationNoteByDuration(durationNote).hasBeam()
					&& nextNote.getNoteDuration().get(0).hasBeam()
					|| previousNote != null && EnumDurationNote.getDurationNoteByDuration(durationNote).hasBeam()
							&& previousNote.getNoteDuration().get(0).hasBeam()) {

				Element beam = doc.createElement("beam");
				Attr attr = doc.createAttribute("number");
				attr.setValue(String.valueOf(1));
				beam.setAttributeNode(attr);
				if (onBeat) {
					beam.appendChild(doc.createTextNode("begin"));
				} else if (onEndBeat) {
					beam.appendChild(doc.createTextNode("end"));
				} else if (!onBeat && !onEndBeat) {
					beam.appendChild(doc.createTextNode(CONTINUE));
				}

				note.appendChild(beam);
			}

			if (durationN > 0 && no.getSilenceOrPlaying().equals(EnumSilence.PLAY)) {

				Element notations = doc.createElement("notations");
				note.appendChild(notations);

				Element tied = doc.createElement("tied");
				Attr attr = doc.createAttribute("type");
				attr.setValue(String.valueOf(tieString));
				tied.setAttributeNode(attr);
				notations.appendChild(tied);
			}

			iterator++;
		}
	}

	private Element createMeasure(Document doc, Element part, Instrument ins, int measureNumber, Note note,
			int velocity) {

		Element measure = doc.createElement("measure");
		part.appendChild(measure);

		// setting attribute to element
		Attr attr = doc.createAttribute("number");
		attr.setValue(String.valueOf(measureNumber));
		measure.setAttributeNode(attr);

		if (measureNumber == 1) {
			writeAttributesMeasure(doc, measure, ins);
		}

		if (!EnumVelocity.getClosestValue(velocity).equals(EnumVelocity.getClosestValue(note.getVolume()))) {
			writeDirection(doc, measure, measureNumber, note);
		}

		return measure;
	}

	private void writeDirection(Document doc, Element measure, int measureNumber, Note note) {

		Element direction = doc.createElement("direction");
		Attr attr = doc.createAttribute("placement");
		attr.setValue("below");
		direction.setAttributeNode(attr);
		measure.appendChild(direction);

		Element directionType = doc.createElement("direction-type");
		direction.appendChild(directionType);

		Element dynamics = doc.createElement("dynamics");
		attr = doc.createAttribute("default-x");
		attr.setValue("129");
		dynamics.setAttributeNode(attr);
		attr = doc.createAttribute("default-y");
		attr.setValue("-75");
		dynamics.setAttributeNode(attr);
		directionType.appendChild(dynamics);

		Element velocity = doc.createElement(EnumVelocity.getClosestValue(note.getVolume()).getName());
		dynamics.appendChild(velocity);

		Element sound = doc.createElement(SOUND);
		attr = doc.createAttribute("dynamics");
		attr.setValue(String.valueOf(note.getVolume()));
		sound.setAttributeNode(attr);
		if (measureNumber == 1) {
			attr = doc.createAttribute("tempo");
			attr.setValue(String.valueOf(this.m.getRythme()));
			sound.setAttributeNode(attr);
		}
		direction.appendChild(sound);
	}

	private void writeChord(Document doc, Element measure, Chord chord) {

		Element harmony = doc.createElement("harmony");
		Attr attr = doc.createAttribute("default-y");
		attr.setValue("100");
		harmony.setAttributeNode(attr);
		measure.appendChild(harmony);

		Element root = doc.createElement("root");
		harmony.appendChild(root);

		Element rootStep = doc.createElement("root-step");
		rootStep.appendChild(
				doc.createTextNode(chord.getFundamentalNote(this.m.getTonality()).americanName(this.m.getTonality())));
		root.appendChild(rootStep);

		Element kind = doc.createElement("kind");
		Attr attr3 = doc.createAttribute("halign");
		attr3.setValue("center");
		kind.setAttributeNode(attr3);

		Attr attr2 = doc.createAttribute("text");
		attr2.setValue("6");
		kind.setAttributeNode(attr2);
		kind.appendChild(doc.createTextNode(this.m.getTonality().getMajMin().toString()));

		harmony.appendChild(kind);

		Element degree = doc.createElement("degree");
		harmony.appendChild(degree);

		Element degreeValue = doc.createElement("degree-value");
		degreeValue.appendChild(doc.createTextNode(chord.getNumberAccord().getName()));
		degree.appendChild(degreeValue);

		Element degreeAlter = doc.createElement("degree-alter");
		degreeAlter.appendChild(doc.createTextNode("0"));
		degree.appendChild(degreeAlter);

		Element degreeType = doc.createElement("degree-type");
		Attr attr4 = doc.createAttribute("text");
		attr4.setValue("");
		degreeType.setAttributeNode(attr4);
		degreeType.appendChild(doc.createTextNode(""));
		degree.appendChild(degreeType);

	}

	private void writeAttributesMeasure(Document doc, Element measure, Instrument ins) {

		Element attributes = doc.createElement("attributes");
		measure.appendChild(attributes);

		Element divisions = doc.createElement("divisions");
		divisions.appendChild(doc.createTextNode("24"));
		attributes.appendChild(divisions);

		Element key = doc.createElement("key");
		attributes.appendChild(key);

		Element fifths = doc.createElement("fifths");
		fifths.appendChild(doc.createTextNode(String.valueOf(this.m.getTonality().getMusicXmlIndex())));
		key.appendChild(fifths);

		Element mode = doc.createElement("mode");
		mode.appendChild(doc.createTextNode(this.m.getTonality().getMajMin().toString()));
		key.appendChild(mode);

		Element time = doc.createElement("time");
		attributes.appendChild(time);

		Element beats = doc.createElement("beats");
		beats.appendChild(doc.createTextNode(String.valueOf(this.m.getAccordsDuration().getNumerator())));
		time.appendChild(beats);

		Element beatType = doc.createElement("beat-type");
		beatType.appendChild(doc.createTextNode(String.valueOf(this.m.getAccordsDuration().getDenominator())));
		time.appendChild(beatType);

		Element clef = doc.createElement("clef");
		attributes.appendChild(clef);

		Element sign = doc.createElement("sign");
		sign.appendChild(doc.createTextNode(ins.getInstrument().getSignMusicXml()));
		clef.appendChild(sign);

		Element line = doc.createElement("line");
		line.appendChild(doc.createTextNode(String.valueOf(ins.getInstrument().getLineMusicXml())));
		clef.appendChild(line);

	}

	private void writePartList(Document doc, Element rootElement) {

		Element scorePart;
		Element partName;
		Attr attr;
		int part = 1;
		Element partList = doc.createElement("part-list");
		rootElement.appendChild(partList);

		for (Instrument ins : this.m.getListInstruments()) {
			scorePart = doc.createElement("score-part");
			partList.appendChild(scorePart);

			// setting attribute to element
			attr = doc.createAttribute("id");
			attr.setValue("P" + part);
			scorePart.setAttributeNode(attr);

			partName = doc.createElement("part-name");
			partName.appendChild(doc.createTextNode(ins.getInstrument().getName()));
			scorePart.appendChild(partName);

			part++;
		}

	}

}
