package utils;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import enums.EnumBrickSentence;

public class Copy<T> {

	// Variable d'instance
	private Class<T> clazz;

	// Constructeur par d�faut
	public Copy() {
		this.clazz = null;
	}

	// Constructeur avec param�tre inconnu pour l'instant
	public Copy(Class<T> clazz) {
		this.clazz = clazz;
	}

	public T buildOne(T arg) throws InstantiationException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, NoSuchMethodException, SecurityException {
		return this.clazz.getDeclaredConstructor(this.clazz).newInstance(arg);
	}

	public Map<EnumBrickSentence, List<T>> createCopy(Map<EnumBrickSentence, List<T>> origin) {

		Map<EnumBrickSentence, List<T>> copy = new EnumMap<>(EnumBrickSentence.class);
		List<T> listCopy;

		for (Map.Entry<EnumBrickSentence, List<T>> list : origin.entrySet()) {

			listCopy = new ArrayList<>();
			for (T arg : list.getValue()) {

				listCopy.add(this.buildOne(arg));
			}
			copy.put(list.getKey(), listCopy);
		}

		return copy;
	}

	// D�finit la valeur avec le param�tre
	public void setValeur(T val) {
		this.valeur = val;
	}

	// Retourne la valeur d�j� � cast�e � par la signature de la m�thode !
	public T getValeur() {
		return this.valeur;
	}
}
