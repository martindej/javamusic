package midi;

public enum EnumMidiMessageType {
	/** The message type for an end of a tune playback. */
	END_OF_TRACK ((byte)0x2F),
	/** The message type for a tempo change. */ 
	TEMPO_CHANGE ((byte)0x51),
	/** The message type to flag which part of the abc notation
	 * is played during a tune playback.
	 * @deprecated use <TT>NOTE_INDEX_MARKER</TT>
	 * @see #NOTE_INDEX_MARKER */
	NOTATION_MARKER ((byte)0x30),
	/** The message type to give a reference to the note index in the
	 * score that is being played during a tune playback. */
	NOTE_INDEX_MARKER ((byte)0x40),
	
	MARKER ((byte)0x06);
	
	private byte message;

	EnumMidiMessageType(byte message) {
		this.message = message;
	}
	
	public byte getMessage() {
		return this.message;
	}

}
