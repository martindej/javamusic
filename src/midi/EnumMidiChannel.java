package midi;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import enums.EnumDurationNote;

public enum EnumMidiChannel {
	
	C0	("Channel 0", 	0xc0),	
	C1 	("Channel 1", 	0xc1),	
	C2 	("Channel 2", 	0xc2),
	C3 	("Channel 3",	0xc3),
	C4	("Channel 4", 	0xc4),
	C5	("Channel 5",	0xc5),
	C6	("Channel 6", 	0xc6),
	C7	("Channel 7",	0xc7),
	C8	("Channel 8",	0xc8),
	C9	("Channel 9",	0xc9);
	
	private String name;
	private int channel;
	private static final List<EnumMidiChannel> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
	
	EnumMidiChannel(String name, int chan) {
		this.name = name;
		this.channel = chan;
	}
	
	static public int getChannelByIndex(int index) {
		return VALUES.get(index).channel;
	}
	
	public int getChannel() {
		return this.channel;
	}

}

