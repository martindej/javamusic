package midi;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.sound.midi.Instrument;
import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MetaMessage;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Receiver;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;
import javax.sound.midi.Soundbank;
import javax.sound.midi.Track;
import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;

import com.sun.media.sound.AudioSynthesizer;

public final class MidiToWavTools {

	private final AudioSynthesizer synth;

	public MidiToWavTools() throws MidiUnavailableException, InvalidMidiDataException, IOException {
		try {
			synth = (AudioSynthesizer) MidiSystem.getSynthesizer();
		} catch (ClassCastException e) {
			throw new Error("Please make sure Gervill is included in the classpath: "
					+ "it should be de default synth. These are the currently installed synths: "
					+ Arrays.toString(MidiSystem.getMidiDeviceInfo()), e);
		}
	}

	private Soundbank loadSoundbank(final File soundbankFile) throws InvalidMidiDataException, IOException {
		return MidiSystem.getSoundbank(soundbankFile);
	}

	/**
	 * Creates a WAV file based on the Sequence, using the default soundbank.
	 * 
	 * @param sequence
	 * @param outputFile
	 * @throws MidiUnavailableException
	 * @throws InvalidMidiDataException
	 * @throws IOException
	 */
	public File createWavFile(final File soundbankFile, byte[] midiBytes)
			throws MidiUnavailableException, InvalidMidiDataException, IOException {
		/*
		 * Open synthesizer in pull mode in the format 96000hz 24 bit stereo using Sinc
		 * interpolation for highest quality. With 1024 as max polyphony.
		 */

		InputStream myInputStream = new ByteArrayInputStream(midiBytes);

		final Sequence sequence = MidiSystem.getSequence(myInputStream);
		final Sequencer sequencer = MidiSystem.getSequencer(false);
		sequencer.getTransmitter().setReceiver(synth.getReceiver());

		final AudioFormat format = new AudioFormat(96000, 24, 2, true, false);
		final Map<String, Object> map = new HashMap<>();
		map.put("interpolation", "sinc");
		map.put("max polyphony", "1024");
		AudioInputStream stream = synth.openStream(format, map);

		if (soundbankFile != null) {
			final Soundbank soundbank = loadSoundbank(soundbankFile);

			// Open the Synthesizer and load the requested instruments

			this.synth.unloadAllInstruments(soundbank);
			final Instrument[] instruments = soundbank.getInstruments();
			for (final Instrument instrument : instruments) {
				synth.loadInstrument(instrument);
			}
		}
		// Play Sequence into AudioSynthesizer Receiver.
		final double total = send(sequence, synth.getReceiver());

		final long len = (long) (stream.getFormat().getFrameRate() * (total + 1));
		stream = new AudioInputStream(stream, stream.getFormat(), len);

		File outputFile = File.createTempFile("temp", ".wav"); // create a file in temp folder

		AudioSystem.write(stream, AudioFileFormat.Type.WAVE, outputFile);

		stream.close();

		return outputFile;
	}

	/**
	 * Send entry MIDI Sequence into Receiver using time stamps.
	 * 
	 * @return The total length of the sequence.
	 */
	private double send(final Sequence seq, final Receiver recv) {
		assert seq.getDivisionType() == Sequence.PPQ;

		final float divtype = seq.getDivisionType();
		final Track[] tracks = seq.getTracks();

		final int[] trackspos = new int[tracks.length];
		int mpq = 500000;
		final int seqres = seq.getResolution();
		long lasttick = 0;
		long curtime = 0;
		while (true) {
			MidiEvent selevent = null;
			int seltrack = -1;
			for (int i = 0; i < tracks.length; i++) {
				final int trackpos = trackspos[i];
				final Track track = tracks[i];
				if (trackpos < track.size()) {
					final MidiEvent event = track.get(trackpos);
					if (selevent == null || event.getTick() < selevent.getTick()) {
						selevent = event;
						seltrack = i;
					}
				}
			}
			if (seltrack == -1) {
				break;
			}
			trackspos[seltrack]++;
			final long tick = selevent.getTick();
			if (divtype == Sequence.PPQ) {
				curtime += (tick - lasttick) * mpq / seqres;
			} else {
				curtime = (long) (tick * 1000000.0 * divtype / seqres);
			}
			lasttick = tick;
			final MidiMessage msg = selevent.getMessage();
			if (msg instanceof MetaMessage) {
				if (divtype == Sequence.PPQ && ((MetaMessage) msg).getType() == 0x51) {
					final byte[] data = ((MetaMessage) msg).getData();
					mpq = (data[0] & 0xff) << 16 | (data[1] & 0xff) << 8 | data[2] & 0xff;
				}
			} else if (recv != null) {
				recv.send(msg, curtime);
			}
		}
		return curtime / 1000000.0;
	}

}
