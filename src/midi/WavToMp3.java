package midi;

import java.io.File;
import java.io.IOException;

import it.sauronsoftware.jave.AudioAttributes;
import it.sauronsoftware.jave.Encoder;
import it.sauronsoftware.jave.EncoderException;
import it.sauronsoftware.jave.EncodingAttributes;

public class WavToMp3 {

	private WavToMp3() {
		throw new IllegalStateException("WavToMp3 class");
	}

	public static File encodeToMp3(File source) throws EncoderException {

		File target = null;
		try {
			target = File.createTempFile("temp", ".mp3");
		} catch (IOException e) {
			e.printStackTrace();
		} // create a file in temp folder
		AudioAttributes audio = new AudioAttributes();
		audio.setCodec("libmp3lame");
		audio.setBitRate(Integer.valueOf(128000));
		audio.setChannels(Integer.valueOf(2));
		audio.setSamplingRate(Integer.valueOf(44100));
		EncodingAttributes attrs = new EncodingAttributes();
		attrs.setFormat("mp3");
		attrs.setAudioAttributes(audio);
		Encoder encoder = new Encoder();
		encoder.encode(source, target, attrs);

		return target;
	}

}
