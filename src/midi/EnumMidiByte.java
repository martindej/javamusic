package midi;

public enum EnumMidiByte {

	FF((char) 0xff),
	A51((char) 0x51),
	A03((char) 0x03),
	A20((char) 0x20),
	A01((char) 0x01),
	A58((char) 0x58),
	A04((char) 0x04),
	A59((char) 0x59),
	A02((char) 0x02),
	A2F((char) 0x2f),
	ZERO((char) 0x00);

	private char byt;

	EnumMidiByte(char by) {
		this.byt = by;
	}

	public char getChar() {
		return byt;
	}

}
