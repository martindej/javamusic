package midi;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.List;

import enums.EnumDurationChord;
import enums.EnumInstruments;
import enums.EnumNoteTonality;
import enums.EnumSilence;
import enums.EnumTonalite;
import musicRandom.Instrument;
import musicRandom.Music;
import musicRandom.Note;

public class MidiWhitoutJavax {
	
	Music m;
	byte[] midiBytes = new byte[0];
		
	public MidiWhitoutJavax(Music mu) {
		
		this.m = mu;
		try {
			writeMidi();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public byte[] getMidiBytes() {
		return this.midiBytes;
	}
	
	private void addToMidiBytes(byte add) {

		byte[] destination = new byte[1];
		destination[0] = add;

		addToMidiBytes(destination);
	}
	
	private void addToMidiBytes(byte[] add) {
		// create a destination array that is the size of the two arrays
		byte[] destination = new byte[this.midiBytes.length + add.length];

		// copy ciphertext into start of destination (from pos 0, copy ciphertext.length bytes)
		System.arraycopy(this.midiBytes, 0, destination, 0, this.midiBytes.length);

		// copy mac into end of destination (from pos ciphertext.length, copy mac.length bytes)
		System.arraycopy(add, 0, destination, this.midiBytes.length, add.length);
		
		this.midiBytes = destination;
	}
	
	public static byte[] toByteArray(char array) {
		return toByteArray(array, Charset.defaultCharset());
	}
 
	public static byte[] toByteArray(char array, Charset charset) {
		CharBuffer cbuf = CharBuffer.allocate(1);
		cbuf.append(array);
		ByteBuffer bbuf = charset.encode(cbuf);
		
		return bbuf.array();
	}
	
	public static byte[] toByteArray(int array) {
		return ByteBuffer.allocate(4).putInt(array).array();
	}
	
	private void writeHeader() throws IOException {
		
		int length = 6;
		short formatType = 1;
		short nbreVoies = (short)(this.m.getListInstruments().size()+1);
		short PPQN = 240;
		
		// write midi MThd chunk	
		addToMidiBytes("MThd".getBytes());
		addToMidiBytes(fourToBytes(length));
		addToMidiBytes(shortToBytes(formatType));
		addToMidiBytes(shortToBytes(nbreVoies));
		addToMidiBytes(shortToBytes(PPQN));	
	}
	
	private void writeParameters() throws IOException {
		
		int length_bis = 25;
		
		int tempo = (int)((1000000.0 * 60.0)/((float)this.m.getRythme())); //microseconds/noire 
		
		// write midi MTrk chunks et options
		addToMidiBytes("MTrk".getBytes());
		addToMidiBytes(fourToBytes(length_bis));
		
		//tempo
		addToMidiBytes((byte)EnumMidiByte.zero.getChar());
		addToMidiBytes((byte)EnumMidiByte.FF.getChar());
		addToMidiBytes((byte)EnumMidiByte.a_51.getChar());
		addToMidiBytes((byte)EnumMidiByte.a_03.getChar());
		addToMidiBytes(threeToBytes(tempo));
		
		// time signature
		EnumDurationChord acc = this.m.getAccordsDuration();
		
	    char nn = (char)acc.getNumerator();
	    char dd = (char)(acc.getDenominatorMidi());
	    char midiClocksPerMetronomeTick = 24;//50;
	    char thirySecondNotesPer24MidiClocks = 8;//50;
		
		addToMidiBytes((byte)(EnumMidiByte.zero.getChar()));
		addToMidiBytes((byte)(EnumMidiByte.FF.getChar()));
		addToMidiBytes((byte)(EnumMidiByte.a_58.getChar()));
		addToMidiBytes((byte)(EnumMidiByte.a_04.getChar()));
		addToMidiBytes((byte)(nn));
		addToMidiBytes((byte)(dd));
		addToMidiBytes((byte)(midiClocksPerMetronomeTick));
		addToMidiBytes((byte)(thirySecondNotesPer24MidiClocks));

		//key signature
		EnumTonalite tonality = this.m.getTonality();
		byte mi = (byte)tonality.getMidiMajMin();
		byte sf = (byte) tonality.getMidiIndex();
		
		addToMidiBytes((byte)(EnumMidiByte.zero.getChar()));
		addToMidiBytes((byte)(EnumMidiByte.FF.getChar()));
		addToMidiBytes((byte)(EnumMidiByte.a_59.getChar()));
		addToMidiBytes((byte)(EnumMidiByte.a_02.getChar()));
		addToMidiBytes(sf);
		addToMidiBytes(mi);

		addToMidiBytes((byte)(EnumMidiByte.zero.getChar()));
		addToMidiBytes((byte)(EnumMidiByte.FF.getChar()));
		addToMidiBytes((byte)(EnumMidiByte.a_2f.getChar()));
		addToMidiBytes((byte)(EnumMidiByte.zero.getChar()));
	}
	
	private synchronized void writeVariableLengthIntBytes (int value) {
		int buffer = value & 0x7F;
		
	    while ((value >>= 7) != 0) {
			buffer <<= 8;
			buffer |= ((value & 0x7F) | 0x80);
	    }
	      
	    while (true) {
	    	addToMidiBytes((byte)(buffer & 0xff));
	    	
			if ((buffer & 0x80) != 0)
				buffer >>>= 8;
			else
				break;
	   }
	}
	
	private int variableLengthIntLength (int value) {
		
	    int length = 0;
	    int buffer = value & 0x7F;
	
	    while ((value >>= 7) != 0) {
			buffer <<= 8;
			buffer |= ((value & 0x7F) | 0x80);
	    }
	      
	    while (true) {
			length++;
			
			if ((buffer & 0x80) != 0)
				buffer >>>= 8;
			else
				break;
	    }

	    return length;
	}
	
	private int estimateSizeTrack(Instrument ins) {
		
		int size = 0, midiTime = 0;
		
		for(Note n:ins.getListNotes()) {
			if(n.getSilenceOrPlaying().equals(EnumSilence.PLAY)) {
				
				//note on				
				size = size + variableLengthIntLength(midiTime) + 3 + (n.getListNoteNum().size()-1) * 4; // estimate size of time key 
				
				// note off
				midiTime = n.getDuration()*10; 
				size = size + variableLengthIntLength(midiTime) + 3 + (n.getListNoteNum().size()-1) * 4; // estimate size of time key
				
				midiTime = 0;
			}
			else {
				midiTime = midiTime + n.getDuration()*10;
			}
			
		}
		
		size = size + 12;
		
		return size;
	}
	
	private void writeTrack(Instrument ins, int channel) throws IOException {
		
		int midiTime = 0, i = 0;

		for(Note n:ins.getListNotes()) {
			if(n.getSilenceOrPlaying().equals(EnumSilence.PLAY)) {
				
				//note on
				i = 0;
				
				if(n.getListNoteNum().isEmpty()) {
					System.out.println("problem problem problem problem problem problem standard");
				}

				for(EnumNoteTonality note:n.getListNoteNum()) {
					if(i != 0){
						midiTime = 0;
					}
					
					writeVariableLengthIntBytes(midiTime);
					addToMidiBytes((byte)(EnumNoteOnOff.getNoteOnByIndex(channel)));
					addToMidiBytes((byte)(note.getIndex())); //numéro de la note
					addToMidiBytes((byte)(n.getVolume())); //vélocité de la note
					i++;
				}
				
				// note off
				i = 0;
				midiTime = n.getDuration()*10;
				
				for(EnumNoteTonality note:n.getListNoteNum()) {
					if(i != 0){
						midiTime = 0;
					}
					writeVariableLengthIntBytes(midiTime);
					addToMidiBytes((byte)(EnumNoteOnOff.getNoteOffByIndex(channel)));
					addToMidiBytes((byte)(note.getIndex())); //numéro de la note
					addToMidiBytes((byte)(n.getVolume())); //vélocité de la note
					i++;
				}
				
				midiTime = 0;
			}
			else {
				midiTime = midiTime + n.getDuration()*10;
			}
		}
	}
	
	private void writeTrackMain(Instrument ins, char channel) throws IOException {
		
		int sizeTrack;
		
		sizeTrack = estimateSizeTrack(ins);

		addToMidiBytes("MTrk".getBytes());
		addToMidiBytes(fourToBytes(sizeTrack));
		addToMidiBytes((byte)(EnumMidiByte.zero.getChar()));
		addToMidiBytes((byte)(EnumMidiByte.FF.getChar()));
		addToMidiBytes((byte)(EnumMidiByte.a_20.getChar()));
		addToMidiBytes((byte)(EnumMidiByte.a_01.getChar()));
		addToMidiBytes((byte)(channel));
		
		if(channel == 10) {
			channel = 9;
		}

		addToMidiBytes((byte)(EnumMidiByte.zero.getChar()));
		addToMidiBytes((byte)(EnumMidiChannel.getChannelByIndex(channel)));
		addToMidiBytes((byte)(ins.getInstrument().getNnumInstrument()));
		
		writeTrack(ins, channel);

		addToMidiBytes((byte)(EnumMidiByte.zero.getChar()));
		addToMidiBytes((byte)(EnumMidiByte.FF.getChar())); 
		addToMidiBytes((byte)(EnumMidiByte.a_2f.getChar()));
		addToMidiBytes((byte)(EnumMidiByte.zero.getChar())); 
	}
	
	private void writeMidi() throws IOException {
				
		writeHeader();
		writeParameters();
				
		List<Instrument> instruments = this.m.getListInstruments();
		char channel = 1, tempChannel = 1;
				
		for(Instrument i:instruments) {
			
			System.out.println("writing of instrument "+i.getInstrument().getName());
			if(i.getInstrument().equals(EnumInstruments.PERCUSSIONS)) {
				channel = 10;
			}
			else {
				channel = tempChannel;
			}
			writeTrackMain(i, channel);
			tempChannel++;
		}
	}
	
	private byte[] fourToBytes(int x) {
	    ByteBuffer buffer = ByteBuffer.allocate(4);
	    buffer.putInt(x);
	    return buffer.array();
	}
	
	private byte[] threeToBytes(int x) {
	    ByteBuffer buffer = ByteBuffer.allocate(4);
	    ByteBuffer buffer2 = ByteBuffer.allocate(3);
	    buffer.putInt(x);
	    buffer2.array()[0] = buffer.array()[1];
	    buffer2.array()[1] = buffer.array()[2];
	    buffer2.array()[2] = buffer.array()[3];
	    return buffer2.array();
	}
	
	private byte[] shortToBytes(short x) {
	    ByteBuffer buffer = ByteBuffer.allocate(Short.BYTES);
	    buffer.putShort(x);
	    return buffer.array();
	}

}
