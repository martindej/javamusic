package midi;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import enums.EnumDurationNote;

public enum EnumNoteOnOff {
	
	N0	("Channel 1", 	0x90,	0x80,	0),	
	N1 	("Channel 2", 	0x91,	0x81,	1),	
	N2 	("Channel 3", 	0x92,	0x82,	2),
	N3 	("Channel 4",	0x93,	0x83,	3),
	N4	("Channel 5", 	0x94,	0x84,	4),
	N5	("Channel 6",	0x95,	0x85,	5),
	N6	("Channel 7", 	0x96,	0x86,	6),
	N7	("Channel 8",	0x97,	0x87,	7),
	N8	("Channel 9",	0x98,	0x88,	8),
	N9	("Channel 10",	0x99,	0x89,	9);
	
	private String name;
	private int noteOn;
	private int noteOff;
	private int index;
	private static final List<EnumNoteOnOff> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
	
	EnumNoteOnOff(String name, int noteOn, int noteOff, int index) {
		this.name = name;
		this.noteOn = noteOn;
		this.noteOff = noteOff;
		this.index  = index;
	}
	
	static public int getNoteOnByIndex(int index) {
		return VALUES.get(index).noteOn;
	}
	
	static public int getNoteOffByIndex(int index) {
		return VALUES.get(index).noteOff;
	}
	
	public int getNoteOn() {
		return this.noteOn;
	}
	
	public int getNoteOff() {
		return this.noteOff;
	}

}
