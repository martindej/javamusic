package midi;

import java.io.File;
import java.io.IOException;

import javax.sound.midi.*;

public class MidiToWav {
	
	public Synthesizer synth;
	private File wavFile;
	private byte[] wavBytes;

	public MidiToWav(byte[] midiBytes, File filesf2) {
		 
		 this.wavFile = new File("wavFile.wav");
		 
		 MidiToWavTools test = null;

		 try {
			 test = new MidiToWavTools();
			} catch (MidiUnavailableException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvalidMidiDataException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 try {
				this.wavFile = test.createWavFile(filesf2, midiBytes);
			} catch (MidiUnavailableException | InvalidMidiDataException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	}

	public byte[] getWavBytes() {
		return this.wavBytes;
	}
	
	public File getWavFile() {
		return this.wavFile;
	}
	
	public void deleteWavFile() {
		this.wavFile.delete();
	}
}
