package midi;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;

import enums.EnumDurationChord;
import enums.EnumInstruments;
import enums.EnumNoteTonality;
import enums.EnumSilence;
import enums.EnumTonalite;
import musicRandom.Note;

public class MidiListNote {

	List<Note> m;
	String name;
	EnumInstruments instrument;
	EnumTonalite tonality;
	int rythme;
	EnumDurationChord chordDuration;
	FileOutputStream file;

	public MidiListNote(List<Note> mu, String name, EnumInstruments instrument, int rythme,
			EnumDurationChord chordDuration, EnumTonalite tonality) {

		this.m = mu;
		this.name = name;
		this.instrument = instrument;
		this.rythme = rythme;
		this.chordDuration = chordDuration;
		this.tonality = tonality;

		try {
			writeMidi();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void writeHeader() throws IOException {

		int length = 6;
		short formatType = 1;
		short nbreVoies = 2;
		short ppqn = 240;

		// write midi MThd chunk
		this.file.write("MThd".getBytes());
		this.file.write(fourToBytes(length));
		this.file.write(shortToBytes(formatType));
		this.file.write(shortToBytes(nbreVoies));
		this.file.write(shortToBytes(ppqn));

	}

	private void writeParameters() throws IOException {

		int lengthBis = 25;

		int tempo = (int) ((1000000.0 * 60.0) / (this.rythme)); // microseconds/noire

		// write midi MTrk chunks et options
		this.file.write("MTrk".getBytes());
		this.file.write(fourToBytes(lengthBis));

		// tempo
		this.file.write(EnumMidiByte.ZERO.getChar());
		this.file.write(EnumMidiByte.FF.getChar());
		this.file.write(EnumMidiByte.A51.getChar());
		this.file.write(EnumMidiByte.A03.getChar());
		this.file.write(threeToBytes(tempo));

		// time signature
		EnumDurationChord acc = this.chordDuration;

		char nn = (char) acc.getNumerator();
		char dd = (char) (acc.getDenominatorMidi());
		char midiClocksPerMetronomeTick = 24;
		char thirySecondNotesPer24MidiClocks = 8;

		this.file.write(EnumMidiByte.ZERO.getChar());
		this.file.write(EnumMidiByte.FF.getChar());
		this.file.write(EnumMidiByte.A58.getChar());
		this.file.write(EnumMidiByte.A04.getChar());
		this.file.write(nn);
		this.file.write(dd);
		this.file.write(midiClocksPerMetronomeTick);
		this.file.write(thirySecondNotesPer24MidiClocks);

		// key signature
		byte mi = (byte) this.tonality.getMidiMajMin();
		byte sf = (byte) this.tonality.getMidiIndex();

		this.file.write(EnumMidiByte.ZERO.getChar());
		this.file.write(EnumMidiByte.FF.getChar());
		this.file.write(EnumMidiByte.A59.getChar());
		this.file.write(EnumMidiByte.A02.getChar());
		this.file.write(sf);
		this.file.write(mi);

		this.file.write(EnumMidiByte.ZERO.getChar());
		this.file.write(EnumMidiByte.FF.getChar());
		this.file.write(EnumMidiByte.A2F.getChar());
		this.file.write(EnumMidiByte.ZERO.getChar());
	}

	private synchronized void writeVariableLengthInt(int value) throws IOException {
		int buffer = value & 0x7F;

		while ((value >>= 7) != 0) {
			buffer <<= 8;
			buffer |= ((value & 0x7F) | 0x80);
		}

		while (true) {
			this.file.write(buffer & 0xff);

			if ((buffer & 0x80) != 0)
				buffer >>>= 8;
			else
				break;
		}
	}

	private int variableLengthIntLength(int value) {

		int length = 0;
		int buffer = value & 0x7F;

		while ((value >>= 7) != 0) {
			buffer <<= 8;
			buffer |= ((value & 0x7F) | 0x80);
		}

		while (true) {
			length++;

			if ((buffer & 0x80) != 0)
				buffer >>>= 8;
			else
				break;
		}

		return length;
	}

	private int estimateSizeTrack() {

		int size = 0;
		int midiTime = 0;

		for (Note n : this.m) {
			if (n.getSilenceOrPlaying().equals(EnumSilence.PLAY)) {

				// note on
				size = size + variableLengthIntLength(midiTime) + (n.getListNoteNum().size() - 1) * 4 + 3; // estimate
																											// size of
																											// time key

				// note off
				midiTime = n.getDuration() * 10; // TODO, see PPQN
				size = size + variableLengthIntLength(midiTime) + (n.getListNoteNum().size() - 1) * 4 + 3; // estimate
																											// size of
																											// time key

				midiTime = 0;
			} else {
				midiTime = midiTime + n.getDuration() * 10;
			}

		}

		size = size + 12;

		return size;
	}

	private void writeTrack(int channel) throws IOException {

		int midiTime = 0, i = 0;

		for (Note n : this.m) {
			if (n.getSilenceOrPlaying().equals(EnumSilence.PLAY)) {

				// note on
				i = 0;

				for (EnumNoteTonality note : n.getListNoteNum()) {
					if (i != 0) {
						midiTime = 0;
					}
					writeVariableLengthInt(midiTime);
					this.file.write((char) EnumNoteOnOff.getNoteOnByIndex(channel));
					this.file.write((char) note.getIndex()); // numéro de la note
					this.file.write((char) n.getVolume()); // vélocité de la note
					i++;
				}

				// note off
				i = 0;
				midiTime = n.getDuration() * 10; // TODO, see PPQN

				for (EnumNoteTonality note : n.getListNoteNum()) {
					if (i != 0) {
						midiTime = 0;
					}
					writeVariableLengthInt(midiTime);
					this.file.write((char) EnumNoteOnOff.getNoteOffByIndex(channel));
					this.file.write((char) note.getIndex()); // numéro de la note
					this.file.write((char) n.getVolume()); // vélocité de la note
					i++;
				}

				midiTime = 0;
			} else {
				midiTime = midiTime + n.getDuration() * 10;
			}

		}
	}

	private void writeTrackMain(EnumInstruments ins, char channel) throws IOException {

		int sizeTrack;

		sizeTrack = estimateSizeTrack();

		this.file.write("MTrk".getBytes());
		this.file.write(fourToBytes(sizeTrack));
		this.file.write(EnumMidiByte.ZERO.getChar());
		this.file.write(EnumMidiByte.FF.getChar());
		this.file.write(EnumMidiByte.A20.getChar());
		this.file.write(EnumMidiByte.A01.getChar());
		this.file.write(channel);

		this.file.write(EnumMidiByte.ZERO.getChar());
		this.file.write((char) EnumMidiChannel.getChannelByIndex(channel));
		this.file.write(ins.getNnumInstrument());

		writeTrack(channel);

		this.file.write(EnumMidiByte.ZERO.getChar());
		this.file.write(EnumMidiByte.FF.getChar());
		this.file.write(EnumMidiByte.A2F.getChar());
		this.file.write(EnumMidiByte.ZERO.getChar());
	}

	private void writeMidi() throws IOException {

		this.file = new FileOutputStream(this.name + ".mid");

		writeHeader();
		writeParameters();

		char channel = 1;

		writeTrackMain(this.instrument, channel);

		this.file.close();
	}

	private byte[] fourToBytes(int x) {
		ByteBuffer buffer = ByteBuffer.allocate(4);
		buffer.putInt(x);
		return buffer.array();
	}

	private byte[] threeToBytes(int x) {
		ByteBuffer buffer = ByteBuffer.allocate(4);
		ByteBuffer buffer2 = ByteBuffer.allocate(3);
		buffer.putInt(x);
		buffer2.array()[0] = buffer.array()[1];
		buffer2.array()[1] = buffer.array()[2];
		buffer2.array()[2] = buffer.array()[3];
		return buffer2.array();
	}

	private byte[] shortToBytes(short x) {
		ByteBuffer buffer = ByteBuffer.allocate(Short.BYTES);
		buffer.putShort(x);
		return buffer.array();
	}

}
